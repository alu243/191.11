<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<?php
/***************************************************
未完成之功能：
教室使用時間相衝判斷
群組及期別與其他的群組及期別相同時會混淆，要注意
 ***************************************************/
/***************************************************
已完成之功能：
偵測全選(或取消全選)課程時間之判斷
增刪此群組之學生
刪除此群組
增刪課程時間(不可無時間)
加入學生衝堂判斷(需測試)
新增與修改群組之功能由 class_deatil_teacher.php 移至 class_group.php
 ***************************************************/
?>
<?php
/////////////////////////////////////////////////////////////////////////////
if ($g_modify == 1 && !empty($mgroup_sn)) { // 修改群組內容
    $str1 = "select no from group2 where no = '$mgroup_sn'"; // 檢查有無此群組
    list($rt1) = mysql_fetch_row(mysql_query($str1));
    if ( !empty($rt1) ) {
        $temp_str="";
        for ($i=0 ; $i<20 ; $i++) ${"t".$i}?$temp_str .= "1":$temp_str .= "0"; // 記錄排課時間(編碼)
        //echo "tset~~~~~~~~~~~~~".$temp_str;
        if ($temp_str != "00000000000000000000") { // 無排課時間時不更新

            // 更新排課時間時計算所有學生有無衝堂
            $m_str = "select class.stu_no, group2.term from class, group2
					  where class.group2 = group2.`group`
					  and class.term = group2.term
					  and group2.no = '$mgroup_sn'
					  order by class.stu_no asc";
            $m_rt = mysql_query($m_str);
            while ( list($stu_no, $now_term) = mysql_fetch_row($m_rt) ) { // 抓取每位學生
                // 此學生在其他群組所選的課的時間(考慮到在修改時可能會改到時間)
                $m2_str = "select group2.times from class, group2
						   where class.group2 = group2.`group`
						   and class.term = group2.term
						   and group2.term = '$now_term'
						   and class.stu_no = '$stu_no'
						   and group2.teacher = '$teahcer'
						   and group2.no not like '$mgroup_sn'";
                $m2_rt = mysql_query($m2_str);

                $bt = 0; // 此位學生是否已衝堂之 flag
                while ( list($m2_times) = mysql_fetch_row($m2_rt) ) { // 抓取此位學生的所有已選課之時間
                    //echo "test".$stu_no."<br>";
                    for ($i=0 ; $i<20 ; $i++) {
                        if (${"t".$i} && ${"t".$i} == $m2_times[$i]) {
                            list($m_name) = mysql_fetch_row(mysql_query("select name_ch from student where stu_no = '$stu_no'"));
                            $no_time_msg2 .= "<font size='2' color=#000099>學生 </font><font size='2' color=red>".$m_name."</font> <font size='2' color=#000099>在欲增加之時段已選擇其他群組之課程</font><br>";
                            $bt = 1;
                            break;
                        }
                    }
                    if($bt == 1) break;
                }
            }
            if (empty($no_time_msg2)) mysql_query("update group2 set course = '$course', classroom = '$classroom', book_no = '$book_no', times = '$temp_str', level = '$level' , ps = '$ps' where no = '$mgroup_sn'");
        }
    }
}
else if ($g_increase == 1 && !empty($groups)) { // 新增群組
    $str1 = "select `group` from group2 where `group` = '$groups' and term = '$term'"; // 檢查群組代碼有無重覆
    list($rt1) = mysql_fetch_row(mysql_query($str1));
    if ( empty($rt1) ) {
        $temp_str="";
        //echo "tset~~~~~~~~~~~~~".$groups;
        for ($i=0 ; $i<20 ; $i++) ${"t".$i}?$temp_str .= "1":$temp_str .= "0";
        if ($temp_str != "00000000000000000000") { // 無排課時間時不更新

            // 先搜尋是否已有新增過的相同資料
            $t_str = "select no from group2 where `group` = '$groups'
							         and people = '0'
							         and term = '$term'
							         and course = '$course'
							         and teacher = '$teacher'
							         and classroom = '$classroom'
							         and book_no = '$book_no'
							         and times = '$temp_str'
							         and level = '$level'
							         and ps = '$ps'";
            list($group_sn) = mysql_fetch_row(mysql_query($t_str));
            if (empty($group_sn)) { // 無相同已新增之資料
                echo $term;
                mysql_query("insert into group2 (`group`, people, term, course, teacher, classroom, book_no, times, level, ps) values
									('$groups', '0', '$term', '$course', '$teacher', '$classroom', '$book_no', '$temp_str', '$level', '$ps')") or die("判定為課程重覆");
                list($group_sn) = mysql_fetch_row(mysql_query($t_str)); // 新增後仍需要 $group_sn 來顯示資料
            }
            else {
                $modify = 1 ;
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////

if ($modify == 1) { // 若為修改群組模式(也就是所選的群組是存在的)
    $str = "select * from group2 where no = '$group_sn'";
    $group_data = mysql_fetch_array(mysql_query($str));
    $term = $group_data["term"];
    $teacher = $group_data["teacher"];
}
if (($increase_class == 1) && !empty($new_stu_no)) { // 增加此群組之學生
    $str1 = "select no from student where stu_no = '$new_stu_no'";
    list($rt1) = mysql_fetch_row(mysql_query($str1));
    $str2 = "select * from class where `term` = '$group_data[term]'
			and `group2` = '$group_data[group]'
			and stu_no = '$new_stu_no'";
    if ( !empty($rt1) && !(mysql_fetch_row(mysql_query($str2))) ) {
        //echo "test~~~~~~~".$group_data[group];
        $no_time_msg = ""; // 學生衝堂訊息
        $collision = 0; // 衝堂判斷依據

        // 取出此位老師在此群組的時間
        $tea_time = $group_data[times];

        // 取出此學生在此學期(term)所有已選群組(課程)的時間
        /*			$str = "select group2.times from class, group2, student
        where student.stu_no = class.stu_no
        and class.group2 = group2.`group`
        and class.term = group2.term
        and group2.term = '$term'
        and group2.teacher = '$teacher'
        and student.stu_no = '$new_stu_no'";
         */
        $str = "select group2.times from class, group2
				where class.group2 = group2.`group`
				and class.term = group2.term
				and group2.term = '$term'
				and class.stu_no = '$new_stu_no'";
        $result = mysql_query($str);
        while (	list($stu_time) = mysql_fetch_row($result) ) { // 檢查有無衝堂(學生)
            //echo "tested~~".$stu_time;
            for ($i=0 ; $i<20 ; $i++) { // 比對此位老師其他的群組(課程)的時間
                if (($stu_time[$i] + $tea_time[$i]) > 1) { $collision = 1; break; }
            }
            if ($collision == 1) break;
        }
        if ($collision == 0) mysql_query("insert into class (term, group2, stu_no) values ('$group_data[term]', '$group_data[group]', '$new_stu_no')") or die("新增失敗");
        else if ($collision == 1) {
            list($stu_name) = mysql_fetch_row(mysql_query("select name_ch from student where stu_no = '$new_stu_no'"));
            $no_time_msg = "<font color=#000099>學生</font> (<font color=red>".$new_stu_no."/".$stu_name."</font>) <font color=#000099>與此群組課程衝堂</font><br>";
        }
        else { die("新增學生時發生錯誤"); }
    }
}
else if (($delete_class == 1) && !empty($del_item)) { // 刪除此群組之學生
    $str1 = "select * from class where no = '$del_item'";
    if ( mysql_query($str1) ) {
        mysql_query("delete from class where no = '$del_item'") or die("刪除失敗");
    }
}

?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p>
        您現在所在位置：<font color="#FF9900">排課處理﹝依教師﹞-</font>
        <font color="#FF9900">群組選單﹝<?php	if ($increase == 1) echo "新增";
                                            else if ($modify == 1) echo "修改"; ?>﹞
  </font>
    </p>
    目前作業期別：{<?php echo $term; ?>}</br>
    <p>目前選擇教師：{<?php idtoname($teacher); ?>}</p>
    <table width="100%" border="1" cellspacing="0" cellpadding="0">
        <?php /* 已建立之群組才會出現"刪除群組"的功能 */ ?>
        <?php if (!empty($group_sn)) { ?>
        <tr align="left" valign="middle">
            <td colspan="2">
                <form name="form3" method="post" action="./class_detail_teacher.php?term=<?php echo $term; ?>&teacher=<?php echo $teacher; ?>">
                    <input type="hidden" name="delete_group" value="1">
                    <input type="hidden" name="dgroup_sn" value="<?php echo $group_sn; ?>">
                    <input type="submit" name="del_g" value="刪除此群組" onclick="return popMsg(this, '刪除此群組')">
                </form>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td width="70%">
                <form name="form1" method="post" action="./class_group.php?modify=1&group_sn=<?php echo $group_sn; ?>">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="14%" align="right">群組名稱：</td>
                            <?php /* 若為修改群組則顯示其內容 */ ?>
                            <td>
                                <?php
                                if ($modify == 1) { // 修改群組
                                ?>
                                <input type="hidden" name="g_modify" value="1">
                                <input type="hidden" name="mgroup_sn" value="<?php echo $group_sn; ?>">
                                <input type="text" name="groups" size="6" value="<?php echo $group_data['group']; ?>" onKeyDown="return false">
                                <?php
                                }
                                else if ($increase == 1) { // 新增群組
                                ?>
                                <input type="hidden" name="g_increase" value="1">
                                <input type="hidden" name="teacher" value="<?php echo $teacher; ?>">
                                <input type="hidden" name="term" value="<?php echo $term; ?>">
                                <input type="text" name="groups" size="6">
                                <?php	} ?>
                                <br>
                            </td>
                            <td rowspan="5">備註：<br>
                                <textarea name="ps" cols="30" rows="7"><?php echo $group_data['ps']; ?></textarea>
                            </td>
                        </tr>

                        <tr>
                            <td width="14%" align="right">教室：</td>
                            <?php /* 若為修改群組則顯示其內容 */ ?>
                            <td>
                                <input type="text" name="classroom" size="6" value="<?php echo $group_data['classroom']; ?>">
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td width="14%" align="right">課程名稱：</td>
                            <?php /* 若為修改群組則顯示其內容 */ ?>
                            <td>
                                <select name="course">
                                    <?php
                                    $str = "select course_no, course from course where term = '$term'";
                                    $cou_rt = mysql_query($str);
                                    while ( list($group_data["course_no"], $group_data["crouse_name"]) = mysql_fetch_row($cou_rt) ) {
                                        if ($group_data['course'] == $group_data['course_no']) {
                                            echo "<option value='".$group_data["course_no"]."' selected>".$group_data["crouse_name"]."</option>\n";
                                        }
                                        else {
                                            echo "<option value='".$group_data["course_no"]."'>".$group_data["crouse_name"]."</option>\n";
                                        }
                                    }
                                    // <input type="text" name="course" size="10" value="<?php echo $group_data["course"]; ?>">
?>
                                </select>
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td width="14%" align="right">教材： 
              <?php /* 若為修改群組則顯示其內容，載入教材(book)&nbsp; value=book_no,選項顯示book_no和name_ch */ ?>
                            </td>
                            <td>
                                <select size="1" name="book_no">
                                    <option value="0">選擇教材</option>
                                    <?php
                                    $book_data = listbook();
                                    for ($i=0 ; $i < count($book_data) ; $i++) {
                                        if ($book_data[$i]["book_no"] == $group_data["book_no"]) {
                                    ?>
                                    <option value="<?php echo $book_data[$i]["book_no"]; ?>" selected>
                                        <?php echo $book_data[$i]["name_ch"]; ?> </option>
                                    <?php
                                        }
                                        else {
                                    ?>
                                    <option value="<?php echo $book_data[$i]["book_no"]; ?>"><?php echo $book_data[$i]["name_ch"]; ?>
                                    </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td width="14%" align="right">教材範圍：
              <?php /* 若為修改群組則顯示其內容，載入教材(book)&nbsp; value=book_no,選項顯示book_no和name_ch */ ?>
                            </td>
                            <td>
                                <input type="text" name="level" size="8" value="<?php echo $group_data['level']; ?>">
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" valign="top">
                                <div></div>
                                <?php
                                $is_checked[0]="";
                                $is_checked[1]=" checked";
                                $course_time[0] = "8:10 | 10:00";
                                $course_time[1] = "10:10 | 12:00";
                                $course_time[2] = "13:40 | 15:30";
                                $course_time[3] = "15:40 | 17:30";
                                $tr_bgcolor[0] = "FFFFFF";
                                $tr_bgcolor[1] = "E6FFEB";
                                ?>
                                <table width="100%" border="1" cellpadding="0" cellspacing="0" align="center">
                                    <tr align="center" bgcolor="#E6FFEB">
                                        <td>&nbsp;</td>
                                        <td>一</td>
                                        <td>二</td>
                                        <td>三</td>
                                        <td>四</td>
                                        <td>五</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php
                                    // 此位老師其他的群組(課程)的時間(同一學期內)
                                    $sql = "select times from group2 where teacher = '$teacher' and (times not like '$group_data[times]') and term = '$term'";
                                    $result = mysql_query($sql);
                                    while( list($office_times) = mysql_fetch_row($result) ) {
                                        $temp_str2 = $compare_str;
                                        $compare_str="";
                                        for ($i=0 ; $i<20 ; $i++) { // 比對此位老師其他的群組(課程)的時間
                                            ($temp_str2[$i] || $office_times[$i])?$compare_str.="1":$compare_str.="0";
                                        }
                                        //echo $compare_str;
                                        // 此 while 迴圈做完的結果會出現一個叫 $compare_str 的字串變數，此為目前此位老師此學期的所有課程
                                        // 若要另外加上其他不可選課的時間時，則在此 while 迴圈前或後加，
                                    }


                                    // 教師的非排課時間處理
                                    list($tea_no) = mysql_fetch_row(mysql_query("select no from member where center_no = '$teacher'"));
                                    list($rest_times) = mysql_fetch_row(mysql_query("select rest_times from rest_times where term = '$term' and tea_no = '$tea_no'"));
                                    //echo $rest_times;
                                    $temp_str2 = $compare_str;
                                    $compare_str="";
                                    for ($i=0 ; $i<20 ; $i++) { // 比對此位老師其他的群組(課程)的時間
                                        ($temp_str2[$i] || $rest_times[$i])?$compare_str.="1":$compare_str.="0";
                                    }

                                    // 課程時間
                                    for ($i=0 ; $i<20 ; $i+=5) {
                                    ?>
                                    <tr align="center" bgcolor="#<?php echo $tr_bgcolor[(int)($i/5) % 2]; ?>">
                                        <td><?php echo $course_time[(int)($i/5)]; ?></td>
                                        <td>
                                            <?php if ( !$compare_str[$i+0] ) { ?>
                                            <input type="checkbox" name="<?php echo "t".($i+0); ?>" value="1"<?php echo $is_checked[ $group_data["times"][$i+0] ]; ?>>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ( !$compare_str[$i+1] ) { ?>
                                            <input type="checkbox" name="<?php echo "t".($i+1); ?>" value="1"<?php echo $is_checked[ $group_data["times"][$i+1] ]; ?>>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ( !$compare_str[$i+2] ) { ?>
                                            <input type="checkbox" name="<?php echo "t".($i+2); ?>" value="1"<?php echo $is_checked[ $group_data["times"][$i+2] ]; ?>>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ( !$compare_str[$i+3] ) { ?>
                                            <input type="checkbox" name="<?php echo "t".($i+3); ?>" value="1"<?php echo $is_checked[ $group_data["times"][$i+3] ]; ?>>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ( !$compare_str[$i+4] ) { ?>
                                            <input type="checkbox" name="<?php echo "t".($i+4); ?>" value="1"<?php echo $is_checked[ $group_data["times"][$i+4] ]; ?>>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <input type="button" value="全選" onClick="<?php
                                        echo "if (document.form1.t".($i+0)."!=null) document.form1.t".($i+0).".checked=true;";
            							echo "if (document.form1.t".($i+1)."!=null) document.form1.t".($i+1).".checked=true;";
            							echo "if (document.form1.t".($i+2)."!=null) document.form1.t".($i+2).".checked=true;";
            							echo "if (document.form1.t".($i+3)."!=null) document.form1.t".($i+3).".checked=true;";
            							echo "if (document.form1.t".($i+4)."!=null) document.form1.t".($i+4).".checked=true;";
                                                                                     ?>">
                                            <input type="button" value="取消全選" onClick="<?php
            							echo "if (document.form1.t".($i+0)."!=null) document.form1.t".($i+0).".checked=false;";
            							echo "if (document.form1.t".($i+1)."!=null) document.form1.t".($i+1).".checked=false;";
            							echo "if (document.form1.t".($i+2)."!=null) document.form1.t".($i+2).".checked=false;";
            							echo "if (document.form1.t".($i+3)."!=null) document.form1.t".($i+3).".checked=false;";
            							echo "if (document.form1.t".($i+4)."!=null) document.form1.t".($i+4).".checked=false;";
                                                                                       ?>">
                                            <input type="reset" value="重設">
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <?php	} ?>
                                </table>
                                <?php
                                /****************************************************************************************************************************
                                存入group(times)為一char(20)的字串,存入形式為<br>
                                $times=&quot;&quot;<br>
                                for i=1 to 4 {<br>
                                &nbsp; if ($g[i]==1){<br>
                                &nbsp;&nbsp;&nbsp; $times=$times+'11111';<br>
                                &nbsp; }else{<br>
                                &nbsp;&nbsp;&nbsp; for j=(i-1)*5+1 to i*5 {<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if ($t[j]==1){<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $times=$times+'1';<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }else{<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $times=$times+'0';<br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br>
                                &nbsp;&nbsp;&nbsp; }<br>
                                &nbsp; }<br>
                                } 
                                <p>&nbsp;其中有選擇的為1沒有選擇的為0</p>
                                <p>進階：看<a href="class_teacher_detail.php.htm">class_teacher_detail.php.htm</a>選擇哪一節次，只顯示那一個節次就好</p>
                                 ****************************************************************************************************************************/
                                ?>
                                <p>
                                    <input type="submit" value="確定送出" name="B1">
                                    <input type="reset" value="清除重填" name="B2">
                                </p>
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
            <td width="30%" valign="top">
                <?php
                if ($modify == 1) {
                ?>
                <form name="form2" method="post" action="./class_group.php?modify=1&group_sn=<?php echo $group_sn; ?>">
                    新增學生 
        <p>
            輸入學號(stu_no)：
          <input type="hidden" name="increase_class" value="1">
            <input type="text" name="new_stu_no" size="6">
            <input name="button" type="submit" value="新增">
            <input type="button" name="Submit" value="查詢"
                onclick="window.open('../search.php?returnid=new_stu_no','查詢','width=225,height=300,top=200,left=300')">
                    <p>
                </form>
                <?php echo $no_time_msg; /* 有衝堂時此訊息(新增學生)才會有內容 */ ?>
                <?php echo $no_time_msg2; /* 衝堂訊息(修改時間) */ ?>
                <table border="1" cellspacing="0" width="300" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
                    <tr>
                        <td width="53%" bgcolor="#E6FFEB" align="center" height="20">學號/中文姓名</td>
                        <td width="47%" bgcolor="#E6FFEB" align="center" height="20">刪除</td>
                    </tr>
                    <?php
                    $str = "select class.*, student.name_ch from class, student where class.stu_no = student.stu_no and class.group2 = '$group_data[group]' and class.term='$term' order by stu_no asc";
                    $result = mysql_query($str);
                    while ($class_data = mysql_fetch_array($result)) { // 新增學生
                    ?>
                    <tr>
                        <td width="53%" align="center" height="19"><?php echo $class_data["stu_no"] . "/" . $class_data["name_ch"]; ?></td>
                        <td width="47%" align="center" height="19"><a href="./class_group.php?modify=1&group_sn=<?php echo $group_sn; ?>&delete_class=1&del_item=<?php echo $class_data[no]; ?>" onClick="return popMsg(this,'刪除 ' + '<?php echo $class_data["name_ch"]; ?>' + ' 學生所選取的此項課程')">刪除</a>
                        </td>
                    </tr>
                    <?php	} ?>
                </table>
                <?php	} ?>
            </td>
        </tr>
    </table>
    <br>

    <p align="center"><a href="./class_detail_teacher.php?term=<?php echo $term; ?>&teacher=<?php echo $teacher; ?>">回上一頁</a></p>

    <p align="center"></p>

</body>

</html>
