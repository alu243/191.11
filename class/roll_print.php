<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php
    $str_term = "select * from term where term='$term'";
    $rt_term = mysql_query($str_term);
    $data_term = mysql_fetch_array($rt_term);

    //echo $t_year."/".$t_month."/".$s_day."-".$e_day;

    if ($t_year=="") $t_year=substr($data_term["start_date"],0,4);

    if ($t_month=="") $t_month=intval(substr($data_term["start_date"],5,2));

    //if ($s_day==""){$s_day=intval(substr($data_term["start_date"],8,2));}

    if ($s_day=="") $s_day=1; //只讀取開始日期會造成部分月份有誤，改成從每月1號開始

    if ($e_day==""){
        if ( $t_month==intval(substr($data_term["end_date"],5,2))) {
            $e_day=intval(substr($data_term["end_date"],8,2));
        }
        else {
            $e_day=count_days($t_year,$t_month);
        }
    }

    for ($i=0;$i<=6;$i++) $table_day[$i][0]=1;

    for ($i=$s_day;$i<=min(count_days($t_year,$t_month),$e_day);$i++){

        $x_day = date("Y-m-d", mktime (0,0,0,$t_month,$i,$t_year));
        $rtt = mysql_query("select * from holiday where hdate = '$x_day'");
        if (mysql_num_rows($rtt) > 0) continue;

        $temp=count_day($t_year,$t_month,$i);
        $temp2=$table_day[$temp][0];
        $table_day[$temp][$temp2]=$i;
        $table_day[$temp][0]=$table_day[$temp][0]+1;
    }

    $col_count=0;

    for ($i=1;$i<=5;$i++) {$col_count+=$table_day[$i][0];}

    $table_width=intval(1000/$col_count);
    //echo $table_width;//平均欄寬

    if (empty($center_no)) {
        $str_member = "select no,center_no,name_ch from member order by center_no";
    }
    else {
        $str_member = "select no,center_no,name_ch from member where center_no='$center_no'";
    }

    $rt_member = mysql_query($str_member);

    $member_count=0;


    while ($data_member = mysql_fetch_array($rt_member)) { // 計算有開課的教師
        if ($member_count!=0) {page_break();}
        $member_count++;
        $str = "select * from group2 where teacher='".$data_member["center_no"]."' and term='$term'";
        $rt1 = mysql_query($str);
        $data = mysql_fetch_array($rt1);
        if (empty($data)) continue;

        // teacher name
        print_table_head($t_year, $t_month, $e_day, $data_member["name_ch"], $col_count, $table_width, $table_day);
        // teacher id
        print_table_body($term, $data_member["center_no"], $table_width, $table_day);
        print_table_foot();
    }



    // 寫lab課程的點名表(或是大於7人的課程的點名表)
    if (empty($center_no)) {
        $str_member_lab = "select m.no, m.center_no, m.name_ch, g.`group`, g.times "
                ." from member as m, group2 as g "
                ." where m.center_no = g.teacher "
                ." and g.term = '$term' "
                ." and g.course like 'L%'"
                ." order by m.center_no";
    }
    else {
        $str_member_lab = "select m.no, m.center_no, m.name_ch, g.`group`, g.times "
                ." from member as m, group2 as g "
                ." where m.center_no = g.teacher "
                ." and g.term = '$term' "
                ." and g.course like 'L%'"
                ." and m.center_no = '$center_no' "
                ." order by m.center_no";
    }
    $rt_member_lab = mysql_query($str_member_lab);
    while ($data_member_lab = mysql_fetch_array($rt_member_lab)) { // 計算有開課的教師
        if ($member_count!=0) {page_break();}
        $member_count++;
        //echo $str_member_lab; //debug mode
        // teacher name
        print_table_head($t_year, $t_month, $e_day, $data_member_lab["name_ch"], $col_count, $table_width, $table_day);
        //group2.group           times
        print_table_lab_body($term, $data_member_lab['group'], $data_member_lab['times'], $table_width, $table_day);
        print_table_foot();
    }

    ?>

</body>
</html>



<?php function print_table_head ($t_year, $t_month, $e_day, $teacher, $col_count, $table_width, $table_day) { ?>
<div>
    <table width="1000">
        <tr>
            <td align="center"><font size="4"><b>輔仁大學語言中心<?php echo $t_year-1911;?>年<?php echo $t_month?>月份學生出缺席登記表</b></font></td>
        </tr>
        <tr>
            <td align="center">
                <table border="0" width="<?php echo $col_count*$table_width;?>pt">
                    <tr>
                        <td align="left"><font size="3"><?php echo $teacher;?>　老師</font></td>
                        <td align="right"><font size="2">本表請於<?php echo $t_month;?>月<?php echo $e_day;?>日前交回辦公室</font></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#000000" width="<?php echo $col_count*$table_width; ?>pt">
                    <tr>
                        <?php //列印星期？
          for ($i=1 ; $i<=5 ; $i++) {
              echo "<td colspan=".$table_day[$i][0]." width=".$table_day[$i][0]*$table_width."pt  align=center><font size='-1'>星期".num_to_ch($i)."</font></td>";
          }
                        ?>
                    </tr>
                    <tr align="center">
                        <?php
          for ($i=1;$i<=5;$i++){ //列印日期
              $temp=$table_day[$i][0]*$table_width-(($table_day[$i][0]-1)*20);
              echo "<td width=".$temp."pt><font size='-1'>日期</font></td>";
              for ($j=1;$j<$table_day[$i][0];$j++){
                  echo "<td width=20pt><font style=font-face:SimSun;font-size:10pt><center>".$table_day[$i][$j]."</center></font></td>";
              }
              unset($temp);
          }
                        ?>
                    </tr>
                    <?php ;
      } /* end print head */ ?>

                    <?php //列印點名表body(一般課程)
                    function print_table_body($term, $tea_no, $table_width, $table_day) {
                        for ($i=0;$i<20;$i++) $stu_list[$i][0]=0; //stu_list[i][j] i為星期幾第幾節(對應group2中的times) j=0表該時間中學生人數 j>=1存學生姓名

                        $str_group = "select * from group2 where teacher='$tea_no' and term='$term' order by times desc";
                        //echo $str_group;
                        $rt_group = mysql_query($str_group);
                        $check_data=0;
                        while ($data_group = mysql_fetch_array($rt_group)) { //準備stu_list
                            $check_data++;
                            for ($i=0;$i<20;$i++){
                                $temp=substr($data_group["times"],$i,1); // $data_group[8] = $data_group['times']
                                if ($temp==1){
                                    $str_class = "select * from class where group2='".$data_group["group"]."' and term='$term' order by stu_no";
                                    $rt_class = mysql_query($str_class);
                                    $temp2=0;
                                    while ($data_class = mysql_fetch_array($rt_class)) {
                                        $temp2++;
                                        $stu_list[$i][0]++;
                                        $stu_list[$i][$temp2]=idtonamech($data_class["stu_no"]);
                                    } // end while
                                }  // end if
                            } // end for
                        } // end while $data_group

                        $time_show=array("8:10-10:00","10:10-12:00","13:40-15:30","15:40-17:30");


                        for ($k=0;$k<4;$k++) {//4個時段
                            echo "<tr>";
                            $max_people=1;
                            for ($i=1;$i<=5;$i++){ // 列印時間
                                echo "<td colspan=".$table_day[$i][0]." width=".$table_day[$i][0]*$table_width."pt  align=center><font size='-1'>".$time_show[$k]."</font></td>\n";
                                $temp2=$k*5+($i-1);
                                if ($stu_list[$temp2][0]<=8){
                                    $max_people=max($max_people,$stu_list[$temp2][0]);
                                }//該時段最大的學生人數
                            } // end for $i
                            for ($l=1;$l<=$max_people;$l++){//學生人數
                                echo "<tr>";
                                for ($i=1;$i<=5;$i++){//temp:欄寬 temp2:哪一節課(共20) temp3:學生姓名欄內容 temp4:登記欄內容
                                    $temp=$table_day[$i][0]*$table_width-(($table_day[$i][0]-1)*20);
                                    $temp2=$k*5+($i-1);
                                    $temp4=" ";
                                    if ($stu_list[$temp2][0]>=$l && $stu_list[$temp2][0]<=7){$temp3=$stu_list[$temp2][$l];}
                                    else if ($stu_list[$temp2][0]>=8){$temp3="　"; $temp4=" ";}
                                    else{$temp3="　"; $temp4=" ";}
                                    echo "<td width=".$temp."pt><font style='font-size:14px'>".$temp3."</font></td>\n";
                                    for ($j=1;$j<$table_day[$i][0];$j++){echo "<td width=20pt align=center>".$temp4."</td>\n";}//j
                                } // end for $i
                                echo "</tr>";
                            } //l end for $l
                            $temp_people=$max_people;
                            for ($l=1;$temp_people < 7;$l++) { //填滿每節到7個人
                                echo "<tr>";
                                for ($i=1;$i<=5;$i++){
                                    $temp=$table_day[$i][0]*$table_width-(($table_day[$i][0]-1)*20);
                                    echo "<td width=".$temp."pt><font style='font-size:12px'>　</font></td>\n";
                                    for ($j=1;$j<$table_day[$i][0];$j++){echo "<td width=20pt align=center> </td>\n";} //j
                                    unset($temp);
                                } // $i
                                $temp_people++;
                            } // $l
                            echo "</tr>\n";
                        } // end of for $k
                    } /* end print body */ ?>



                    <?php //列印點名表body(lab課程)
                    function print_table_lab_body ($term, $group2, $times, $table_width, $table_day) {
                        // 因為此lab點名表是一門課一張，學生不管在這張點名表都是一樣，所以此處不用像上面一樣管時間問題
                        $stu_list[0]=0; //stu_list[j] j=0表學生人數 j>=1存學生姓名

                        $str_class = "select stu_no from class where term = '$term' and `group2` = '$group2'"; // 該課程中所有的學生
                        $rt_class = mysql_query($str_class);
                        $temp2 = 0;
                        while ($data_class = mysql_fetch_array($rt_class)) {
                            $temp2++; // temp2即為j => j=0表學生人數 j>=1存學生姓名
                            $stu_list[0]++;
                            $stu_list[$temp2]=idtonamech($data_class["stu_no"]);
                        } // end while

                        $time_show=array("8:10-10:00","10:10-12:00","13:40-15:30","15:40-17:30");

                        $max_people = 31; // 最多可印31個人(
                        // 第一行
                        echo "<tr>"; // 先印時段
                        for ($i=0;$i<5;$i++) { // 星期一、二、三、四、五
                            echo "<td colspan=".$table_day[$i+1][0]." width=".$table_day[$i+1][0]*$table_width."pt  align=center><font size='-1'>";
                            if ($times[$i] == "1") {
                                echo $time_show[0];
                            }
                            else if ($times[$i+5] == "1") {
                                echo $time_show[1];
                            }
                            else if ($times[$i+10] == "1") {
                                echo $time_show[2];
                            }
                            else if ($times[$i+15] == "1") {
                                echo $time_show[3];
                            }
                            else {
                                echo "&nbsp;";
                            }
                            echo "</font></td>\n";
                        }
                        echo "</tr>";
                        // 第二行以後
                        for ($j=1;$j<=$max_people;$j++) {
                            echo "<tr>";
                            for ($i=0;$i<5;$i++) { // 星期一、二、三、四、五
                                $empty_cell = "<td colspan=".$table_day[$i+1][0]." width=".$table_day[$i+1][0]*$table_width."pt  align=center><font size='-1'>&nbsp;</font></td>\n";
                                if ($j > $stu_list[0]) { // 表單$j行數超過目前該課人數後就可以直接印空格了
                                    echo $empty_cell;
                                }
                                else if ($times[$i] == "1" || $times[$i+5] == "1" || $times[$i+10] == "1" || $times[$i+15] == "1") {
                                    $temp_width = $table_day[$i+1][0]*$table_width-(($table_day[$i+1][0]-1)*20);
                                    $data_cell = "<td width=". $temp_width ."pt><font style='font-size:14px'>".$stu_list[$j]."</font></td>\n";
                                    echo $data_cell;
                                    for ($k=1;$k<$table_day[$i+1][0];$k++) {
                                        echo "<td width=20pt align=center>&nbsp;</td>\n"; // 印人名後面日期的空格(點名用的空格)
                                    }
                                }
                                else { // 剩下的狀況就直接印空格出去了
                                    echo $empty_cell;
                                }
                            }
                            echo "</tr>";
                        }
                    } /* end print lab body */ ?>





                    <?php function print_table_foot() { ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <font size="-1">缺席：X　　遲到：△　　早退：Φ　　出國：◎　　曠課：○</font>
            </td>
        </tr>
    </table>
</div>
<?php ;
                          } /* end print foot */ ?>

<?php
function idtonamech($id) //學號轉中文姓名
{
	$str = "select no,stu_no,name_ch from student where stu_no='$id'";
	$rt = mysql_query($str);
	while ($data = mysql_fetch_array($rt)) {
		return $data["name_ch"];
	}
}

function num_to_ch($id)//數字1-5轉中文
{
	switch($id){
        case 1:
            return "一";
        case 2:
            return "二";
        case 3:
            return "三";
        case 4:
            return "四";
        case 5:
            return "五";
	}
}

function count_day($t_year,$t_month,$s_day){//計算x年x月x日是星期幾
	$all_days=$s_day;
	for ($i=1;$i<$t_year;$i++){
		if (is_leap_year($i)==1){$all_days=$all_days+2;}
		else{$all_days=$all_days+1;}
	}
	for ($i=1;$i<$t_month;$i++){
		$all_days=$all_days+count_days($t_year,$i);
	}
	return $all_days%7;
}

function count_days($t_year,$t_month){//計算x年x月共有多少天
	switch ($t_month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
            break;
        case 2:
            return 28 + is_leap_year($t_year);
	}
	//if ($t_month==1 || $t_month==3 || $t_month==5 || $t_month==7 || $t_month==8 || $t_month==10 || $t_month==12){return 31;}
	//else if ($t_month==4 || $t_month==6 || $t_month==9 || $t_month==11){return 30;}
	//else if ($t_month==2){
	//	if (is_leap_year($t_year)==1){return 29;}
	//	else {return 28;}
	//}
}

function is_leap_year($t_year){//閏年判斷
	if ($t_year%4==0 && ($t_year%100!=0 || $t_year%400==0)){return 1;}
	else {return 0;}
}
?>