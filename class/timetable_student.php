<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php 
	if ( !empty($spe_no) ) {
		if ( list($exist_stu) = mysql_fetch_row(mysql_query("select stu_no from student where stu_no = '$spe_no'")) ) {
			if ( list($exist_stu) = mysql_fetch_row(mysql_query("select stu_no from class where stu_no = '$spe_no' and term = '$term'")) )
				stu_table($term, $spe_no);
			else { echo "此學生並沒有在此學期(".$term.")選課！！"; }
		}
		else {
			echo "無此學生！！";
		}
	}
	else if (!empty($stu_no1) && !empty($stu_no2)) {
		$str = "select stu_no from class where term = '$term'
			and stu_no between '$stu_no1' and '$stu_no2'
			group by stu_no order by stu_no asc";
		$result = mysql_query($str);
		if ( list($each_stu) = mysql_fetch_row($result) ) stu_table($term, $each_stu);
		while ( list($each_stu) = mysql_fetch_row($result) ) {
			page_break();
			stu_table($term, $each_stu);
		}
	}
	else {
		$str = "select stu_no from class where term = '$term' group by stu_no order by stu_no asc";
		$result = mysql_query($str);
		if ( list($each_stu) = mysql_fetch_row($result) ) stu_table($term, $each_stu);
		while ( list($each_stu) = mysql_fetch_row($result) ) {
			page_break();
			stu_table($term, $each_stu);
		}
	}
    ?>
</body>

</html>


<?php
function stu_table($term, $stu_no) {
?>
<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse; width: 560pt height:395pt" bordercolor="#111111">
    <tr>
        <td valign="top">
            <p align="center">
                <br>
                <b>
                    <font face="標楷體" size="5">
            <?php
    $title = getTermTitle($term);
            ?>
          </font>
                    <font face="標楷體" size="4">輔仁大學語言中心 <?php echo $title; ?> 學生課表</font>
                </b>
            </p>

            <div align="center">
                <center>
          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width=490pt" bordercolor="#111111" id="AutoNumber2">
            <tr> 
              <td width="80">&nbsp;</td>
              <td width="85" align="right"> 
                <font face="標楷體">印製日期：</font>
              </td>
              <td width="80"><font face="標楷體"><?php echo date("Y/n/d"); ?></font></td>
              <td width="85" align="right"> 
                <font face="標楷體">印製時間：</font>
              </td>
              <td width="80"> <font face="標楷體"><?php echo date("H:i:s"); ?></font></td>
              <td width="80">&nbsp;</td>
            </tr>

            <tr> 
              <td style="width: 80pt">&nbsp;</td>
              <td style="width: 80pt">&nbsp;</td>
              <td style="width: 80pt">&nbsp;</td>
              <td style="width: 80pt">&nbsp;</td>
              <td style="width: 80pt">&nbsp;</td>
              <td style="width: 80pt">&nbsp;</td>
            </tr>
          </table>
        </center>
            </div>
            <?php
	$str = "select stu_no, name_ch, name_enf, name_enl from student where stu_no = '$stu_no'"; // 學生姓名資料
	$stu_name_data = mysql_fetch_array(mysql_query($str));
            ?>
            <div align="center">
                <center>
          <table style="border-collapse: collapse; width:480pt" bordercolor="#000000" cellpadding="5" cellspacing="0" border="0">
            <tr> 
              <td align="center" style="width: 40pt"> 
                <p align="right"><font face="標楷體">學號：</font>
              </td>
              <td style="width: 40pt"><font face="標楷體"><?php echo $stu_name_data["stu_no"]; ?></font></td>
              <td style="width: 70pt"> 
                <p align="right"><font face="標楷體">英文姓名：</font>
              </td>
              <td style="width: 180pt" align="left"><font face="標楷體"><?php echo $stu_name_data["name_enf"]; if ( $stu_name_data["name_enl"] != "*") echo " ".$stu_name_data["name_enl"]; ?></font></td>
              <td style="width: 70pt"> 
                <p align="right"><font face="標楷體">中文姓名：</font>
              </td>
              <td style="width: 80pt" align="left"><font face="標楷體"><?php echo $stu_name_data["name_ch"]; ?></font></td>
            </tr>
            <tr> 
              <td align="center" style="width: 40pt">　</td>
              <td style="width: 40pt">　</td>
              <td style="width: 70pt">　</td>
              <td style="width: 180pt">　</td>
              <td style="width: 70pt">　</td>
              <td style="width: 80pt">　</td>
            </tr>
          </table>
        </center>
            </div>
            <div align="center">
                <center>
          <table style="border-collapse: collapse; width:480pt" bordercolor="#000000" cellpadding="5" cellspacing="0" border="1">
            <tr> 
              <?php /*<td align="center" style="width: 70pt; "><font face="標楷體" size="2">群組名稱</font></td>*/ ?>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2">課程名稱</font></td>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2">班級人數</font></td>
              <td align="center" style="width: 60pt; "><font face="標楷體" size="2">星　期</font></td>
              <td align="center" style="width: 80pt; "><font face="標楷體" size="2">上課時間</font></td>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2">授課教師</font></td>
              <td align="center" style="width: 60pt; "><font face="標楷體" size="2">教　室</font></td>
            </tr>
<?php
	$course_time[0] = "8:10-9:00<br>9:10-10:00";
	$course_time[1] = "10:10-11:00<br>11:10-12:00";
	$course_time[2] = "13:40-14:30<br>14:40-15:30";
	$course_time[3] = "15:40-16:30<br>16:40-17:30";
	$week_name[0] = "Mon";
	$week_name[1] = "Tue";
	$week_name[2] = "Wed";
	$week_name[3] = "Thu";
	$week_name[4] = "Fri";
	$sep_time[0] =  "1___________________"; // 星期一第一堂
	$sep_time[1] =  "_____1______________"; // 星期一第二堂
	$sep_time[2] =  "__________1_________";
	$sep_time[3] =  "_______________1____";
	$sep_time[4] =  "_1__________________";
	$sep_time[5] =  "______1_____________";
	$sep_time[6] =  "___________1________";
	$sep_time[7] =  "________________1___";
	$sep_time[8] =  "__1_________________";
	$sep_time[9] =  "_______1____________";
	$sep_time[10] = "____________1_______";
	$sep_time[11] = "_________________1__";
	$sep_time[12] = "___1________________";
	$sep_time[13] = "________1___________";
	$sep_time[14] = "_____________1______";
	$sep_time[15] = "__________________1_";
	$sep_time[16] = "____1_______________";
	$sep_time[17] = "_________1__________";
	$sep_time[18] = "______________1_____";
	$sep_time[19] = "___________________1";

	$str = "select group2.*, course.course as course_name, member.name_ch as teacher_name from class, group2, course, member
		where class.term = group2.term
		and course.term = group2.term
		and course.course_no = group2.course
		and class.group2 = group2.`group`
		and class.stu_no = '$stu_no'
		and class.term = '$term'
		and member.center_no = group2.teacher
		order by class.`group2` asc"; // 學生群組資料
	$result = mysql_query($str);
	$serial = 0;
	if ($stu_data) unset($stu_data);
	while ( $stu_data[$serial] = mysql_fetch_array($result) ) {
		list($stu_data[$serial]['people']) = mysql_fetch_row(mysql_query("select count(*) from class where group2 = '".$stu_data[$serial]['group']."' and term = '$term'"));
		$serial++;
		//echo "times=".$stu_data[$serial-1]['times']."<br>";
	}
    //and group2.times like '$sep_time[$i]'
	for ($i=0 ; $i<20 ; $i++) {
		foreach ($stu_data as $ser => $stu_cou_data) {
			//echo $i."test".$stu_cou_data['times']."<br>";
			if ($stu_cou_data['times'][(int)($i/4) + (($i*5)%20)] == '1') {
?>
            <tr> 
              <?php /*<td align="center" style="width: 70pt; "><font face="標楷體" size="2"><?php echo $stu_cou_data["group"]; ?></font></td>*/ ?>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2"><?php echo $stu_cou_data["course_name"]; ?></font></td>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2"><?php echo $stu_cou_data['people']; ?></font></td>
              <td align="center" style="width: 60pt; "><font face="標楷體" size="2"><?php echo $week_name[$i/4]; ?></font></td>
              <td align="center" style="width: 80pt; "><font face="標楷體" size="2"><?php echo $course_time[$i%4]; ?></font></td>
              <td align="center" style="width: 70pt; "><font face="標楷體" size="2"><?php echo $stu_cou_data["teacher_name"] ?></font></td>
              <td align="center" style="width: 60pt; "><font face="標楷體" size="2"><?php echo $stu_cou_data["classroom"]; ?></font></td>
            </tr>
<?php
			}
		}
	}
?>
          </table>
        </center>
            </div>
        </td>
    </tr>
</table>
<?php
}
?>