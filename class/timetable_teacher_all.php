<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php
	$sep_time[0] =  "1___________________";
	$sep_time[1] =  "_____1______________";
	$sep_time[2] =  "__________1_________";
	$sep_time[3] =  "_______________1____";
	$sep_time[4] =  "_1__________________";
	$sep_time[5] =  "______1_____________";
	$sep_time[6] =  "___________1________";
	$sep_time[7] =  "________________1___";
	$sep_time[8] =  "__1_________________";
	$sep_time[9] =  "_______1____________";
	$sep_time[10] = "____________1_______";
	$sep_time[11] = "_________________1__";
	$sep_time[12] = "___1________________";
	$sep_time[13] = "________1___________";
	$sep_time[14] = "_____________1______";
	$sep_time[15] = "__________________1_";
	$sep_time[16] = "____1_______________";
	$sep_time[17] = "_________1__________";
	$sep_time[18] = "______________1_____";
	$sep_time[19] = "___________________1";
	$count = 0; // 分頁計數
	$page_break = 6; // 6位老師為一頁

	$str = "select * from group2 where term = '$term' group by teacher order by teacher asc"; // 大課表基本群組資料
	$result = mysql_query($str);
	while ( $tea_data = mysql_fetch_array($result)) {
		if ( $count && (($count % $page_break) == 0) ) page_break(); // 從第二筆開始($count不為0)，若上一筆為分頁結束 且 仍有資料要列印時，則列印分頁(因為在$count++前面，所以第二筆為$count==1，第一筆為$count==0)
		$count++; // 分頁計數器
		if ( ($count % $page_break) == 1 ) print_tea_all_head($term); // 若此資料為分頁的第一筆時則列印所需的頁首
		list($rest_time) = mysql_fetch_row(
					mysql_query("select rest_times.rest_times from rest_times, member
						    where rest_times.tea_no = member.no
						    and rest_times.term = '$term'
						    and member.center_no = '$tea_data[teacher]'"
						   )
						  );
    ?>
    <tr>
        <td style="width: 45pt; height: 80pt" align="center">
            <font face="標楷體" size="2">
              <?php echo $tea_data["teacher"]; ?><p>
              <?php echo idtoonlyname($tea_data["teacher"]); ?>
            </font>
        </td>
        <?php
		for ($i=0 ; $i<20 ; $i++) {
			if ($i % 4 == 0) { ?>
        <td style="width: 53pt; height: 80pt; border-left: solid windowtext 1.5pt" align="left" valign="top">
        <?php			}
			else if ($i % 4 == 3) { ?>
        <td style="width: 53pt; height: 80pt; border-right: solid windowtext 1.5pt" align="left" valign="top">
        <?php			}
			else { ?>
        <td style="width: 53pt; height: 80pt" align="left" valign="top">
            <?php			} ?>
            <font face="標楷體" size="2"> 
<?php
			list($group_no, $classroom) = mysql_fetch_row(mysql_query("select group2.`no`, group2.classroom from group2
										   where group2.term = '$term'
										   and group2.teacher = '$tea_data[teacher]'
										   and group2.times like '$sep_time[$i]'"));
			$stu_classroom[$tea_data["teacher"]][$i] = $classroom; // 以老師的代碼( $tea_data["teacher"] )來作為分隔不同的教室的依據

			$str = "select student.name_ch from group2, class, student
				where group2.`group` = class.group2
				and group2.term = class.term
				and class.stu_no = student.stu_no
				and group2.no = '$group_no'
				order by student.stu_no";
			$rt2 = mysql_query($str);
			if ( mysql_num_rows($rt2) < 8) {
				$stu_count = 0; // 判斷是否要斷行的 flag
				while ( list($stu_name) = mysql_fetch_row($rt2) ) {
					if ($stu_count) echo "<br>\n";
					$stu_count++;
					list($name_short, $level) = mysql_fetch_row(mysql_query("select book.name_short, group2.level from book, group2
												 where group2.book_no = book.book_no
												 and group2.no = '$group_no'"));
					$stu_group[$tea_data["teacher"]][$i] = $name_short.$level; // 以老師的代碼( $tea_data["teacher"] )來作為分隔不同的課程範圍的依據
					echo $stu_name;
				}
			}
			else if ( mysql_num_rows($rt2) >= 8) { // 學生人數為8人以上時則印出課程名稱
				list($cou_name) = mysql_fetch_row(mysql_query("select course.course from course, group2 where course.course_no = group2.course and group2.no = '$group_no'"));
				echo "(".$cou_name.")";
			}
			if ($rest_time[(($i*5)+($i*5)/20)%20] == '1') echo "&nbsp;<br>\n&nbsp;<br>\n&nbsp;<br>\n<center>***</center>";
?>
            </font>
        </td>
        <?php
		}
        ?>
    </tr>


    <tr>
        <td style="width: 45pt" align="center"><font face="標楷體" size="2">課程範圍</font></td>
        <?php		for ($i=0 ; $i<20 ; $i++) {
                         if ($i % 4 == 0) { ?>
        <td style="width: 53pt; border-left: solid windowtext 1.5pt" align="center">
        <?php			}
                         else if ($i % 4 == 3) { ?>
        <td style="width: 53pt; border-right: solid windowtext 1.5pt" align="center">
        <?php			}
                         else { ?>
        <td style="width: 53pt" align="center">
            <?php			} ?>
            <font face="標楷體" size="1">
              <?php if ( !empty($stu_group[$tea_data['teacher']][$i]) ) echo $stu_group[$tea_data['teacher']][$i]; ?>
            </font>
        </td>
        <?php		} ?>
    </tr>


    <tr>
        <td style="width: 45pt" align="center"><font face="標楷體" size="2">教室</font></td>
        <?php		for ($i=0 ; $i<20 ; $i++) {
                         if ($i % 4 == 0) { ?>
        <td style="width: 53pt; border-left: solid windowtext 1.5pt" align="center">
        <?php			}
                         else if ($i % 4 == 3) { ?>
        <td style="width: 53pt; border-right: solid windowtext 1.5pt" align="center">
        <?php			}
                         else { ?>
        <td style="width: 53pt" align="center">
            <?php			} ?>
            <font face="標楷體" size="2">
              <?php if ( !empty($stu_classroom[$tea_data["teacher"]][$i]) ) echo $stu_classroom[$tea_data["teacher"]][$i]; ?>
            </font>
        </td>
        <?php		} ?>
    </tr>


    <?php
		if ( ($count % $page_break) == 0 ) {
			print_tea_all_tail(); // 若為分頁的最後一筆資料，則列印頁尾
		}
	} // end while
	if ( ($count % $page_break) != 0 ) {
		print_tea_all_tail(); // 若已印完所有的資料，且最後一筆資料不為分頁的最後的話，則仍需列印頁尾

	}




	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 列印 LAB 課程學生名單
	// 找出所選定學期的每班學生人數
	$str = "SELECT group2, count(NO) FROM class
		WHERE term = '$term'
		GROUP BY group2";
	$rt = mysql_query($str);
	while (list($lab_group, $lab_quentity) = mysql_fetch_row($rt)) {
        //echo $term;
		//echo $lab_quentity . " ";
		if ($lab_quentity > 7) { // 在八人以上的課
			$list_stu = "<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; width: 1105pt'><tr><td width='10'>&nbsp;<td><td>";

			list($lab_course_name) = mysql_fetch_row(mysql_query(
					"select course.course from course, group2
					where course.term = '$term'
					and group2.`group` = '$lab_group'
					and course.term = group2.term
					and group2.course = course.course_no"));

			$list_stu .= "<font face='標楷體' size='2'>" . $lab_course_name . "：</font></td><td><font face='標楷體' size='2'>";

			$str2 = "select student.name_ch from class, student
				where class.term = '$term'
				and class.group2 = '$lab_group'
				and class.stu_no = student.stu_no";
			$rt2 = mysql_query($str2);

			while (list($stu_name) = mysql_fetch_row($rt2)) {
				$list_stu .= $stu_name . " ";
			}

			$list_stu .= "</font></td></tr></table>";
			echo $list_stu;
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ?>

</body>

</html>


<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function print_tea_all_head($term) {
?>
<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse; width: 1120pt" bordercolor="#111111" id="AutoNumber1">
    <tr>
        <td>
            <?php
	$title = getTermTitle($term);
            ?>
            <p align="center"><b><font face="標楷體" size="5">輔仁大學附設語言中心 <?php echo $title; ?> 課程總表</font></b></p>
            <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 1105pt; border-right: solid windowtext 2pt;" bordercolor="#111111" id="AutoNumber2">
                <tr>
                    <td style="width: 45pt; height: 40pt" align="center" rowspan="2">
                        <font face="標楷體" size="2">
              教師代碼<br><br>
              教師姓名
            </font>
                    </td>
                    <td style="width: 212pt; height: 20pt; border-right: solid windowtext 1.5pt; border-left: solid windowtext 1.5pt" colspan="4" align="center"><font face="標楷體">星期一 (Monday)</font></td>
                    <td style="width: 212pt; height: 20pt; border-right: solid windowtext 1.5pt; border-left: solid windowtext 1.5pt" colspan="4" align="center"><font face="標楷體">星期二 (Tuesday)</font></td>
                    <td style="width: 212pt; height: 20pt; border-right: solid windowtext 1.5pt; border-left: solid windowtext 1.5pt" colspan="4" align="center"><font face="標楷體">星期三 (Wednesday)</font></td>
                    <td style="width: 212pt; height: 20pt; border-right: solid windowtext 1.5pt; border-left: solid windowtext 1.5pt" colspan="4" align="center"><font face="標楷體">星期四 (Thursday)</font></td>
                    <td style="width: 212pt; height: 20pt; border-right: solid windowtext 1.5pt; border-left: solid windowtext 1.5pt" colspan="4" align="center"><font face="標楷體">星期五 (Friday)</font></td>
                </tr>
                <tr>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-left: solid windowtext 1.5pt" align="center"><font face="標楷體">8:10-10:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">10:10-12:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">13:40-15:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-right: solid windowtext 1.5pt" align="center"><font face="標楷體">15:40-17:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-left: solid windowtext 1.5pt" align="center"><font face="標楷體">8:10-10:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">10:10-12:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">13:40-15:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-right: solid windowtext 1.5pt" align="center"><font face="標楷體">15:40-17:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-left: solid windowtext 1.5pt" align="center"><font face="標楷體">8:10-10:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">10:10-12:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">13:40-15:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-right: solid windowtext 1.5pt" align="center"><font face="標楷體">15:40-17:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-left: solid windowtext 1.5pt" align="center"><font face="標楷體">8:10-10:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">10:10-12:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">13:40-15:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">15:40-17:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-left: solid windowtext 1.5pt" align="center"><font face="標楷體">8:10-10:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">10:10-12:00</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt" align="center"><font face="標楷體">13:40-15:30</font></td>
                    <td style="font-size: 8pt; width: 53pt; height: 20pt; border-right: solid windowtext 1.5pt" align="center"><font face="標楷體">15:40-17:30</font></td>
                </tr>
                <?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ?>


                <?php
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function print_tea_all_tail() {
                ?>
            </table>
        </td>
    </tr>
</table>
<?php
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>