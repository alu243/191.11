<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php
    $term = $_POST['term'];
	if ( !empty($spe_no) ) {
		if ( list($exist_tea) = mysql_fetch_row(mysql_query("select center_no from member where center_no = '$spe_no'")) ) {
			if ( list($exist_tea) = mysql_fetch_row(mysql_query("select teacher from group2 where teacher = '$spe_no' and term = '$term'")) )
				tea_table($term, $spe_no);
			else { echo "此位教師並沒有在此學期(".$term.")開課！！"; }
		}
		else {
			echo "無此教師！！";
		}
	}
	else {
		$str = "select distinct teacher from group2 where term = '$term' order by teacher asc";
		$result = mysql_query($str);
		if ( list($each_tea) = mysql_fetch_row($result) ) tea_table($term, $each_tea);
		while ( list($each_tea) = mysql_fetch_row($result) ) {
			page_break();
			tea_table($term, $each_tea);
		}
	}
    ?>
</body>
</html>

<?php
function tea_table($term, $tea_no) {
	getTermTitle($term);
	list($tea_name) = mysql_fetch_row(mysql_query("select name_ch from member where center_no = '$tea_no'"));
	list($classroom, $ps) = mysql_fetch_row(mysql_query("select classroom, ps from group2 where teacher = '$tea_no' and term = '$term'"));
	list($rest_time) = mysql_fetch_row(mysql_query("select rest_times.rest_times from rest_times, member "
						." where rest_times.tea_no = member.no "
						." and rest_times.term = '$term' "
						." and member.center_no = '$tea_no' "));
	if ( $rest_time == "" ) $rest_time = "00000000000000000000";
?>
<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse; width: 490pt" bordercolor="#111111" id="AutoNumber1">
    <tr>
        <td>
            <p align="center"><b><font face="標楷體" size="4">語言中心<?php echo $title; ?>教師授課時間表</font></b></p>

            <div align="center">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 490pt" bordercolor="#111111" id="AutoNumber6">
                    <tr>
                        <td style="width: 160pt"></td>
                        <td style="width: 320pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 160pt"></td>
                        <td style="width: 320pt"></td>
                    </tr>

                    <tr>
                        <td style="width: 160pt"><font face="標楷體">授課教師：<?php echo $tea_name; ?></font></td>
                        <td style="width: 320pt"><font face="標楷體">教室：<?php echo $classroom; ?></font></td>
                    </tr>
                    <tr>
                        <td style="width: 160pt"></td>
                        <td style="width: 320pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 160pt"></td>
                        <td style="width: 320pt"></td>
                    </tr>
                </table>
            </div>
            <div align="center">
                <table border="1" cellpadding="2" cellspacing="0" style="border-collapse: collapse; width: 490pt" bordercolor="#111111" id="AutoNumber5" height="103">
                    <tr>
                        <td style="width: 40pt; height: 20pt" align="center"></td>
                        <td style="width: 90pt; height: 20pt" align="center"><font face="標楷體">一</font></td>
                        <td style="width: 90pt; height: 20pt" align="center"><font face="標楷體">二</font></td>
                        <td style="width: 90pt; height: 20pt" align="center"><font face="標楷體">三</font></td>
                        <td style="width: 90pt; height: 20pt" align="center"><font face="標楷體">四</font></td>
                        <td style="width: 90pt; height: 20pt" align="center"><font face="標楷體">五</font></td>
                    </tr>
                    <?php
	$course_time[0] = "8:10<br>│<br>10:00";
	$course_time[1] = "10:10<br>│<br>12:00";
	$course_time[2] = "13:40<br>│<br>15:30";
	$course_time[3] = "15:40<br>│<br>17:30";
	$sep_time[0] =  "1___________________";
	$sep_time[1] =  "_____1______________";
	$sep_time[2] =  "__________1_________";
	$sep_time[3] =  "_______________1____";
	$sep_time[4] =  "_1__________________";
	$sep_time[5] =  "______1_____________";
	$sep_time[6] =  "___________1________";
	$sep_time[7] =  "________________1___";
	$sep_time[8] =  "__1_________________";
	$sep_time[9] =  "_______1____________";
	$sep_time[10] = "____________1_______";
	$sep_time[11] = "_________________1__";
	$sep_time[12] = "___1________________";
	$sep_time[13] = "________1___________";
	$sep_time[14] = "_____________1______";
	$sep_time[15] = "__________________1_";
	$sep_time[16] = "____1_______________";
	$sep_time[17] = "_________1__________";
	$sep_time[18] = "______________1_____";
	$sep_time[19] = "___________________1";

    if (isset($stu_data)) unset($stu_data);
	if (isset($stu_nums)) unset($stu_nums);
	for ($i=0;$i<20;$i++) {
        $stu_nums[$i]=0;
        $stu_data[$i]="";
	}

	$str = "select student.stu_no, student.name_ch, group2.times, group2.course from student, class, group2 "
		. " where student.stu_no = class.stu_no "
		. " and class.term = group2.term "
		. " and class.group2 = group2.`group` "
		. " and group2.term = '$term' "
		. " and group2.teacher = '$tea_no' "
		. " order by student.stu_no";
    $rt = mysql_query($str);
    while (list($stu_no, $stu_name, $rest_times, $course_no) = mysql_fetch_row($rt)) {
        for ($i=0;$i<20;$i++) {
            if ($rest_times[$i]=='1') {
                $stu_nums[$i]++;
                if ($stu_nums[$i] > 7) {
                    $sql = "select course from course where term='$term' and course_no = '$course_no'";
                    list($course_name) = mysql_fetch_row(mysql_query($sql));
                    $stu_data[$i] = "(".$course_name.")";
                    if (empty($stu_data[$i])) $str_data[$i] = "(&nbsp;&nbsp;&nbsp;&nbsp;課程)";
                }
                else if ($stu_nums[$i] > 1) {
                    $stu_data[$i] .= "<br>\n".$stu_name;
                }
                else {
                    $stu_data[$i] = $stu_name;
                }
            }
        }
    }
    for ($i=0;$i<20;$i++) {
        if ($stu_nums[$i] <= 0) $stu_data[$i] = "&nbsp;<br>\n&nbsp;<br>\n<center>***</center>";
    }

	for ($i=0 ; $i<4 ; $i++) {
                    ?>
                    <tr>
                        <td align="center" style="width: 40pt; height: 120pt"><font face="標楷體"><?php echo $course_time[$i]; ?></font></td>
                        <?php
		for ($j=0 ; $j<5 ; $j++) {
            //			$temp = $sep_time[$i+4*$j];
            //			$str = "select student.name_ch from student, class, group2
            //				where student.stu_no = class.stu_no
            //				and class.term = group2.term
            //				and class.group2 = group2.`group`
            //				and group2.term = '$term'
            //				and group2.teacher = '$tea_no'
            //				and group2.times like '$temp'
            //				order by student.stu_no";
            //			$rt = mysql_query($str);
                        ?>
                        <td style="width: 90pt; height: 120pt">
                            <font face="標楷體">
<?php
            echo $stu_data[($i*5)+$j];
            //			if ( mysql_num_rows($rt) <= 7) {
            //				if ( list($stu_name) = mysql_fetch_row($rt) ) echo $stu_name;
            //				while ( list($stu_name) = mysql_fetch_row($rt) ) {
            //					echo "<br>\n";
            //					echo $stu_name;
            //				}
            //			}
            //			else if ( mysql_num_rows($rt) > 7 ) {
            //				echo "(Lab 課程)";
            //			}

            //			if ($rest_time[($i*5)+$j] == '1') echo "&nbsp;<br>\n&nbsp;<br>\n<center>***</center>";

?>
            </font>
                        </td>
                        <?php		} ?>
                    </tr>
                    <?php 	} ?>
                </table>
            </div>
            <div align="center">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 490pt; height: 80" bordercolor="#111111" id="AutoNumber4">
                    <tr>
                        <td style="width: 60pt; height: 12pt"></td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 12pt">
                            <div align="right"><font color="#FF0000" face="標楷體">備註：</font></div>
                        </td>
                        <?php // 該學期該位教師的所有授課的所有備註
	$new_ps = "";
	$psrs = mysql_query("select ps from group2 where teacher = '$tea_no' and term='$term'");
	while ( list($tempstr) = mysql_fetch_row($psrs) ) {
		$tempstr = trim($tempstr);
		if (!empty($tempstr)) {
			if (!empty($new_ps)) {
				$new_ps = $tempstr;
			}
			else {
				$new_ps .= "\n" . $tempstr;
			}
		}
	}
                        ?>
                        <td rowspan="6" valign="top" style="width: 190pt; height: 12pt"><font face="標楷體" color="#FF0000"><?php echo nl2br($new_ps); ?></font></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 12pt"></td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 12pt"></td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt"></td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 16">&nbsp;</td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 16">
                            <p align="right">
                                <font size="3" face="標楷體">主任蓋章：</font>
                        </td>
                        <td style="width: 150pt; height: 16" align="center"><font size="3" face="標楷體">&nbsp;<?php ;/*echo Date("Y.m.d");*/ ?></font></td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 12pt"></td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt">
                            <hr noshade color="#000000" size="1">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 60pt; height: 12pt"></td>
                        <td style="width: 190pt; height: 12pt"></td>
                        <td style="width: 80pt; height: 12pt"></td>
                        <td style="width: 150pt; height: 12pt"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
<?php
}
?>
