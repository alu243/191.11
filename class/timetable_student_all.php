<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<?php
$title = getTermTitle($term);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse; width: 560pt" bordercolor="#111111" id="AutoNumber1">
        <tr>
            <td>
                <p align="center"><b><font face="標楷體" size="4">輔仁大學語言中心<?php echo $title; ?>學生課程列表</font></b></p>
                <div align="center">
                    <center>
      <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 720" bordercolor="#111111" id="AutoNumber5">
        <tr>
          <td style="width: 40pt" align="center"><font face="標楷體">學號</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">中文姓名</font></td>
          <td style="width: 120pt" align="center"><font face="標楷體">外文姓名</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">一</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">二</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">三</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">四</font></td>
          <td style="width: 60pt" align="center"><font face="標楷體">五</font></td>
        </tr>
<?php
$course_time[0] = "8:10-10:00";
$course_time[1] = "10:10-12:00";
$course_time[2] = "13:40-15:30";
$course_time[3] = "15:40-17:30";


$str = "select no, stu_no, student.name_ch, student.name_enf, student.name_enl from student order by BINARY name_ch asc";
$stu_rt = mysql_query($str) or die("學生資料讀取失敗");

while ( $stu_data = mysql_fetch_array($stu_rt) ) { // 依序抓取每位學生之課程列表

    $compare_time = "";

    //for ($i=0 ; $i<20 ; $i++) $stu_time_info[$i] = ""; // 初始化課程列表時間所附加的教師名稱及教室
    $str = "select group2.times, member.name_ch, group2.classroom from class, group2, member
			where class.group2 = group2.`group`
			and class.term = group2.term
			and group2.teacher = member.center_no
			and class.term = '$term'
			and class.stu_no = '$stu_data[stu_no]'";
    $result = mysql_query($str) or die("學生時間資料讀取失敗");
    while ( list($time_data, $temp_tea_name, $temp_classroom) = mysql_fetch_row($result) ) { // 比對單一學生所選的所有課程時間(全部加起來)
        $temp_time = $compare_time;
        $compare_time = "";

        for ($i=0 ; $i<20 ; $i++) {
            ($temp_time[$i] || $time_data[$i])?$compare_time.="1":$compare_time.="0";
            if ($time_data[$i]) $stu_time_info[$i] = "<br>\n".$temp_tea_name."<br>\n(".$temp_classroom.")"; // 在該學生有課的時間後增加教師名稱及教室
        }
    }
    $stu_data["times"] = $compare_time;
    if ( !empty($compare_time) &&($compare_time != "00000000000000000000") )  {
		//echo $compare_time."<br>";
?>
        <tr>
          <td style="width: 40pt" align="center"><font face="標楷體"><?php echo $stu_data["stu_no"]; ?></font></td>
          <td style="width: 80pt" align="left"><font face="標楷體"><?php echo $stu_data["name_ch"]; ?></font></td>
          <td style="width: 120pt" align="left"><font face="標楷體"><?php echo $stu_data["name_enf"]; if ( $stu_data["name_enl"] != "*") echo " ".$stu_data["name_enl"]; ?></font></td>
          <td style="width: 60pt" align="center">
            <font face="標楷體" size="2">
<?php
		$count = 0 ;
        for ($i=0 ; $i<4 ; $i++) {
            if ($stu_data[times][5*$i] == "1") {
                if ($count > 0) echo "<br>";
                $count++;
                echo $course_time[$i].$stu_time_info[5*$i];
            }
        }
?>
            </font>
          </td>
          <td style="width: 60pt" align="center">
            <font face="標楷體" size="2">
<?php
		$count = 0 ;
        for ($i=0 ; $i<4 ; $i++) {
            if ($stu_data[times][5*$i+1] == "1") {
                if ($count > 0) echo "<br>";
                $count++;
                echo $course_time[$i].$stu_time_info[5*$i+1];
            }
        }
?>
            </font>
          </td>
          <td style="width: 60pt" align="center">
            <font face="標楷體" size="2">
<?php
		$count = 0 ;
        for ($i=0 ; $i<4 ; $i++) {
            if ($stu_data[times][5*$i+2] == "1") {
                if ($count > 0) echo "<br>";
                $count++;
                echo $course_time[$i].$stu_time_info[5*$i+2];
            }
        }
?>
            </font>
          </td>
          <td style="width: 60pt" align="center">
            <font face="標楷體" size="2">
<?php
		$count = 0 ;
        for ($i=0 ; $i<4 ; $i++) {
            if ($stu_data[times][5*$i+3] == "1") {
                if ($count > 0) echo "<br>";
                $count++;
                echo $course_time[$i].$stu_time_info[5*$i+3];
            }
        }
?>
            </font>
          </td>
          <td style="width: 60pt" align="center">
            <font face="標楷體" size="2">
<?php
		$count = 0 ;
        for ($i=0 ; $i<4 ; $i++) {
            if ($stu_data[times][5*$i+4] == "1") {
                if ($count > 0) echo "<br>";
                $count++;
                echo $course_time[$i].$stu_time_info[5*$i+4];
            }
        }
?>
            </font>
          </td>
        </tr>
<?php	}
} ?>
      </table>
      </center>
                </div>
                <p align="center">
                    <b><font face="標楷體" size="5"><br>
    　</font></b>
                </p>
            </td>
        </tr>
    </table>

</body>

</html>
