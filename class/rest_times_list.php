<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass","lang");
CheckAuthority($acptAccounts);
?>
<?php
$term = GetTermName($T_uid);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script>
      function mySubmit(teacher)
      {
	document.form1.teacher.value=teacher;
	document.form1.method = "GET";
	document.form1.submit();
	return;
      }
    </script>
</head>
<body>
    <form name="form1" method="GET" action="./rest_times_reg.php">
        <br />
        您現在所在位置：<a href="../list.php">主選單</a> → <span class="orange">非排課時間登記</span>
        &nbsp;&nbsp;&nbsp;目前作業期別：<span class="blue"><?php echo $term;?></span>
        <hr />
        <div class="center-block" style="width:80%">
            選擇欲登記的期別：
            <?php BuildSelectElementAllTerm($T_uid, "term", "term"); ?>
            <br />
<?php
    echo "<input type=\"hidden\" name=\"teacher\" value=\"\" />";
    echo "<table bordercolor=\"#008000\" bordercolorlight=\"#008000\" bordercolordark=\"#008000\" class=\"table table-bordered table-nonfluid\">";
    $i=1;
    $sql = "select * from member order by 'center_no' asc";
    $result = mysql_query($sql);
    while($data = mysql_fetch_array($result)) {
        if ($i % 5 == 1) echo "    <tr align=\"center\" bgcolor=\"#E6FFEB\">";
        echo "        <td width=\"20%\">";
        //echo "          <font color=\"#0000FF\" style=\"font-size: 14pt\">".$data["name_ch"]."</font><br />";
        //echo "          <input type=\"submit\" name=\"teacher\" value=\""."/".$data["center_no"]."\" class=\"btn btn-default\" >";
        echo "          <button type=\"button\" class=\"btn btn-default\" onclick='mySubmit(\"".$data["center_no"]."\");'>".$data["name_ch"]."/".$data["center_no"]."</button>\n";
        echo "      </td>";
        if ($i % 5 == 0) echo "    </tr>";
        $i++;
    }
    echo "</table>";
?>
        </div>
    </form>
</body>
</html>
