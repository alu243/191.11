<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
if ($delete == 1 && !empty($del_item)) {
    $str = "delete from student where no = '$del_item'";
    mysql_query($str) or die("刪除失敗");
}
else if ($update == 1 && !empty($upd_item)) {
    $birth=$birth_year."/".$birth_month."/".$birth_day;
    $graduation=$graduation_year."/".$graduation_month."/".$graduation_day;
    $taiwan=$taiwan_year."/".$taiwan_month."/".$taiwan_day;
    $school=$school_year."/".$school_month."/".$school_day;
    $leave=$leave_year."/".$leave_month."/".$leave_day;
    $str = "update student set
			stu_no = '$stu_no', 
			name_ch = '$name_ch', 
			name_enf = '$name_enf', 
			name_enl = '$name_enl', 
			sex = '$sex', 
			nationality_no = '$nationality_no', 
			birth ='$birth', birth_year = '$birth_year', birth_month = '$birth_month', birth_day = '$birth_day', 
			passport_no = '$passport_no',  
			highest_edu = '$highest_edu', 
			graduation = '$graduation', graduation_year = '$graduation_year', graduation_month = '$graduation_month', graduation_day = '$graduation_day',
			address_per = '$address_per', tel_per = '$tel_per', fax_per = '$fax_per',
			address_mail = '$address_mail', tel_mail = '$tel_mail', fax_mail = '$fax_mail', cell_mail = '$cell_mail', email_mail = '$email_mail',
			address_taiwan = '$address_taiwan', tel_taiwan = '$tel_taiwan', cell_taiwan = '$cell_taiwan',
			emergency_name='$emergency_name', emergency_tel='$emergency_tel', emergency_cell='$emergency_cell',
			visa='$visa',
			taiwan ='$taiwan', taiwan_year = '$taiwan_year', taiwan_month = '$taiwan_month', taiwan_day = '$taiwan_day',
			school ='$school', school_year = '$school_year', school_month = '$school_month', school_day = '$school_day',
			leave ='$birth', leave_year = '$leave_year', leave_month = '$leave_month', leave_day = '$leave_day',
			heir='$heir' 
			where no = '$upd_item'";
    mysql_query($str);
}
else if ($increase == 1) {
    $birth=$birth_year."/".$birth_month."/".$birth_day;
    $graduation=$graduation_year."/".$graduation_month."/".$graduation_day;
    $taiwan=$taiwan_year."/".$taiwan_month."/".$taiwan_day;
    $school=$school_year."/".$school_month."/".$school_day;
    $leave=$leave_year."/".$leave_month."/".$leave_day;
    if (!empty($stu_no)) {
        $str = "insert into student set 
				stu_no = '$stu_no', 
				name_ch = '$name_ch', 
				name_enf = '$name_enf', 
				name_enl = '$name_enl', 
				sex = '$sex', 
				nationality_no = '$nationality_no', 
				birth ='$birth', birth_year = '$birth_year', birth_month = '$birth_month', birth_day = '$birth_day', 
				passport_no = '$passport_no',  
				highest_edu = '$highest_edu', 
				graduation = '$graduation', graduation_year = '$graduation_year', graduation_month = '$graduation_month', graduation_day = '$graduation_day',
				address_per = '$address_per', tel_per = '$tel_per', fax_per = '$fax_per',
				address_mail = '$address_mail', tel_mail = '$tel_mail', fax_mail = '$fax_mail', cell_mail = '$cell_mail', email_mail = '$email_mail',
				address_taiwan = '$address_taiwan', tel_taiwan = '$tel_taiwan', cell_taiwan = '$cell_taiwan',
				emergency_name='$emergency_name', emergency_tel='$emergency_tel', emergency_cell='$emergency_cell',
				visa='$visa',
				taiwan ='$taiwan', taiwan_year = '$taiwan_year', taiwan_month = '$taiwan_month', taiwan_day = '$taiwan_day',
				school ='$school', school_year = '$school_year', school_month = '$school_month', school_day = '$school_day',
				leave ='$birth', leave_year = '$leave_year', leave_month = '$leave_month', leave_day = '$leave_day', 
				heir='$heir'";
        mysql_query($str);
    }
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">學生基本資料-列表</font></p>
    <p align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
    <p align="center"><a href="student_new.php">新增資料</a>　<a href="../list.php">回主選單</a>　<a href="search.php">查詢特定資料</a></p>
    <table border="1" width="100%" cellpadding="0" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000" height="114">
        <tr>
            <td width="8%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">學號</font></td>
            <td width="15%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">中文姓名</font></td>
            <td width="35%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">外文姓名</font></td>
            <td width="18%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">國籍</font></td>
            <td width="13%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">修改</font></td>
            <td width="11%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">刪除</font></td>
        </tr>
        <?php
        if ($page=="") $page=1;
        $page_people=50;/*每頁顯示人數*/
        $people_limit = ($page-1) * $page_people;
        $people_limit_end = ($page) * $page_people;

        $sql = "select * from student";
        //$sql = "select * from student order by `stu_no` desc limit ".$people_limit." , ".$people_limit_end;
        $result = mysql_query($sql);
        $count=0;
        while($data = mysql_fetch_array($result)) {
        ?>
        <tr>
            <td width="8%" align="center" height="25">
                <a href="student_speci.php?no=<?php echo $data[0]; ?>"><font size="4"><?php echo $data[1]; ?></font></a>
            </td>
            <td width="15%" align="left" height="25">
                <font size="4"><?php echo "　".$data[2]; ?></font>
            </td>
            <td width="15%" align="left" height="25">
                <font size="4"><?php echo "　".id_to_enname($data[1]); ?></font>
            </td>
            <td width="23%" align="center" height="25">
                <font size="4"><?php echo trans_nationality($data[6],1); ?></font>
            </td>
            <td width="13%" align="center" height="25">
                <a href="./student_modify.php?modify=1&mod_item=<?php echo $data[0]; ?>"><font size="4">修改</font></a>
            </td>
            <td width="11%" align="center" height="25">
                <a href="./student.php?delete=1&del_item=<?php echo $data[0]; ?>" onClick="return popMsg(this,'刪除學號: ' + '<?php echo $data[1]; ?>' + ' 的資料')"><font size="4">刪除</font></a>
            </td>
        </tr>
        <?php } ?>
    </table>

    <?php /*以下為頁碼列印*/?>
    <p align="center">
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right">
                    <font size="4">
<?php
$count = mysql_num_rows(mysql_query("select * from student"));

if ($page==1){ echo "上一頁"; }
else if ($page > 1) { $temp_f=$page-1; echo "<a href=student.php?page=".$temp_f.">上一頁</a>"; }
echo "</font></td><td width=40% align=center><font size=4>";
$temp_c=$count%$page_people;
if ($temp_c>0){ $temp=($count-$count%$page_people)/$page_people+1; }
else { $temp=($count-$count%$page_people)/$page_people; }
for ($i=1;$i<=$temp;$i++){
    if ($i==$page){ echo "<b>".$i."</b>";}
    else{ echo "<a href=student.php?page=".$i.">".$i."</a>"; }
    if ($i%10==0){ echo "<br>"; }
    else{ if($i<>$temp){echo " | ";} }
}
echo "</font<</td><td align=left><font size=4>";
if($page==$temp){ echo "下一頁"; }
else { $temp_n=$page+1; echo "<a href=student.php?page=".$temp_n.">下一頁</a>"; }
?>
                    </font>
                </td>
            </tr>
        </table>
    </p>
</body>
</html>
