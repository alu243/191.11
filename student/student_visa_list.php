<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="center"><font face="標楷體" style="font-size: 18pt">簽證到期日在<?php echo $t_year."/".$t_month."/".$t_day;?>之前</font></p>
    <div align="center">
        <center>
  <table border="1" width="760pt" cellpadding="4" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
    <input type="hidden" name="term" value="<?php echo $term;?>">
    <tr>
      <td align="center"><font face="標楷體" style="font-size:14pt">學號</font></td>
      <td align="center"><font face="標楷體" style="font-size:14pt">中文姓名</font></td>
      <td align="center"><font face="標楷體" style="font-size:14pt">英文姓名</font></td>
      <td align="center"><font face="標楷體" style="font-size:14pt">護照種類</font></td>
      <td align="center"><font face="標楷體" style="font-size:14pt">來台日期</font></td>
      <td align="center"><font face="標楷體" style="font-size:14pt">簽證到期日</font></td>
    </tr>

<?php
$sql = "select * from pay_list where term = '$term' order by 'stu' asc";
$result = mysql_query($sql);

while($data = mysql_fetch_array($result)) {
    
	$sql2 = "select no,stu_no,name_ch,name_enf,name_enl,visa,taiwan_year,taiwan_month,taiwan_day from student where stu_no = '$data[2]'";
	$result2 = mysql_query($sql2);
	while($data2 = mysql_fetch_array($result2)) {
        if(visa_deadline($data2[6],$data2[7],$data2[8],$data2[5])<($t_year."/".$t_month."/".$t_day)){
?>
    <tr>
      <td align="center"><font face="Times New Roman" style="font-size:14pt"><?php echo $data2[1]; ?></font></td>
      <td align="left"><font face="標楷體" style="font-size:14pt"><?php echo $data2[2]; ?></font></td>
      <td align="left"><font face="Times New Roman" style="font-size:14pt"><?php echo $data2[3]." ".$data2[4]; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:14pt"><?php echo $data2[5]; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:14pt"><?php echo $data2[6]."/".$data2[7]."/".$data2[8]; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:14pt"><?php echo visa_deadline($data2[6],$data2[7],$data2[8],$data2[5]); ?></font></td>
    </tr>
<?php }
    }
} ?>
  </table>

    </center>
    </div>

</body>

</html>
<?php
function visa_deadline($_year,$_month,$_day,$_type){
	if ( $_type="停留"){
		$temp_day=$_day+59;
	}else if ( $_type="居留"){
		$temp_day=$_day+119;
	}
	$temp_month=$_month;
	$temp_year=$_year;
	$loop_check=1;
	while ($loop_check){
		if ($temp_day>end_day($temp_year,$temp_month)){
			$temp_day=$temp_day-end_day($temp_year,$temp_month);
			$temp_month++;
			if ($temp_month>12){
				$temp_month=$temp_month-12;
				$temp_year++;
			}
		}else{$loop_check=0;}
	}
	return $temp_year."/".$temp_month."/".$temp_day;
}

function end_day($_year,$_month){
	$temp=0;
	if ($_year%4==0 && ($_year%100!=0 || $_year%400==0)) { $temp=1; }
	if($_month==1 || $_month==3 || $_month==5 || $_month==7 || $_month==8 || $_month==10 || $_month==12){ return "31"; }
	else if ( $_month==4 || $_month==6 || $_month==9 || $_month==11){ return "30"; }
	else if ($temp==1&& $_month==2) {return "20"; }
	else { return "28"; }
}

?>