<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">簽證延長名單</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>
    <hr />
    <form name="form1" method="POST" action="student_visa_list.php" target="_blank">
        <p align="center">
            選擇期別：
            <?php BuildSelectElementAllTerm($T_uid, "term", "term"); ?>
        </p>
        <p align="center">
            輸入通知日期:
            西元<input type="text" name="t_year" size="4" value="<?php echo Date("Y");?>">年
            <input type="text" name="t_month"  size="2" value="<?php echo Date("m");?>">月
            <input type="text" name="t_day"  size="2" value="<?php echo Date("d");?>">日
        </p>
        <p align="center">
            <input type="submit" name="submit" value="看列表">
        </p>
    </form>
</body>
</html>