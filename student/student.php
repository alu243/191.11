<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
if ($delete == 1 && !empty($del_item)) { // 刪除資料
    $str = "delete from student where no = '$del_item'";
    mysql_query($str) or die("刪除失敗");
}
else if ($update == 1 && !empty($upd_item)) { // 更新學生基本資料
    $birth=$birth_year."/".$birth_month."/".$birth_day;
    $graduation=$graduation_year."/".$graduation_month."/".$graduation_day;
    $taiwan=$taiwan_year."/".$taiwan_month."/".$taiwan_day;
    $school=$school_year."/".$school_month."/".$school_day;
    $leave=$leave_year."/".$leave_month."/".$leave_day;
    $str = "update student set
			stu_no = '$stu_no', 
			name_ch = '$name_ch', 
			name_enf = '$name_enf', 
			name_enl = '$name_enl', 
			sex = '$sex', 
			nationality_no = '$nationality_no', 
			birth ='$birth', birth_year = '$birth_year', birth_month = '$birth_month', birth_day = '$birth_day', 
			passport_no = '$passport_no',  
			highest_edu = '$highest_edu', 
			graduation = '$graduation', graduation_year = '$graduation_year', graduation_month = '$graduation_month', graduation_day = '$graduation_day',
			address_per = '$address_per', tel_per = '$tel_per', fax_per = '$fax_per',
			address_mail = '$address_mail', tel_mail = '$tel_mail', fax_mail = '$fax_mail', cell_mail = '$cell_mail', email_mail = '$email_mail',
			address_taiwan = '$address_taiwan', tel_taiwan = '$tel_taiwan', cell_taiwan = '$cell_taiwan',
			emergency_name='$emergency_name', emergency_tel='$emergency_tel', emergency_cell='$emergency_cell',
			visa='$visa',
			taiwan ='$taiwan', taiwan_year = '$taiwan_year', taiwan_month = '$taiwan_month', taiwan_day = '$taiwan_day',
			school ='$school', school_year = '$school_year', school_month = '$school_month', school_day = '$school_day',
			leave ='$birth', leave_year = '$leave_year', leave_month = '$leave_month', leave_day = '$leave_day',
			heir='$heir' 
			where no = '$upd_item'";
    mysql_query($str);
}
else if ($update == 2 && !empty($upd_item)) { // 更新學生備註(通常是用在獎學金上)
    $str = "update student set
			remark = '$remark'
			where no = '$upd_item'";
    mysql_query($str);
}
else if ($increase == 1) { // 新增學生資料
    $birth=$birth_year."/".$birth_month."/".$birth_day;
    $graduation=$graduation_year."/".$graduation_month."/".$graduation_day;
    $taiwan=$taiwan_year."/".$taiwan_month."/".$taiwan_day;
    $school=$school_year."/".$school_month."/".$school_day;
    $leave=$leave_year."/".$leave_month."/".$leave_day;
    if (!empty($stu_no)) {
        $str = "insert into student set 
				stu_no = '$stu_no', 
				name_ch = '$name_ch', 
				name_enf = '$name_enf', 
				name_enl = '$name_enl', 
				sex = '$sex', 
				nationality_no = '$nationality_no', 
				birth ='$birth', birth_year = '$birth_year', birth_month = '$birth_month', birth_day = '$birth_day', 
				passport_no = '$passport_no',  
				highest_edu = '$highest_edu', 
				graduation = '$graduation', graduation_year = '$graduation_year', graduation_month = '$graduation_month', graduation_day = '$graduation_day',
				address_per = '$address_per', tel_per = '$tel_per', fax_per = '$fax_per',
				address_mail = '$address_mail', tel_mail = '$tel_mail', fax_mail = '$fax_mail', cell_mail = '$cell_mail', email_mail = '$email_mail',
				address_taiwan = '$address_taiwan', tel_taiwan = '$tel_taiwan', cell_taiwan = '$cell_taiwan',
				emergency_name='$emergency_name', emergency_tel='$emergency_tel', emergency_cell='$emergency_cell',
				visa='$visa',
				taiwan ='$taiwan', taiwan_year = '$taiwan_year', taiwan_month = '$taiwan_month', taiwan_day = '$taiwan_day',
				school ='$school', school_year = '$school_year', school_month = '$school_month', school_day = '$school_day',
				leave ='$birth', leave_year = '$leave_year', leave_month = '$leave_month', leave_day = '$leave_day', 
				heir='$heir'";
        mysql_query($str);
    }
}

$page_people=50;/*每頁顯示人數*/

if ($_POST['search'] == 1) { // 進階查詢 term, stu_no, name_ch, name_enl, name_enf, nationality_no, birth_year, birth_month, birth_day, address_taiwan
    if ($_POST['term']<>"") $search_condition="select s.* from student as s, pay_list as p "
                         ." where s.stu_no = p.stu_no "
                         ." and p.term ='$_POST[term]' ";
    else $search_condition="select s.* from student as s where 1 ";
    
    if ($_POST['stu_no']<>"") $search_condition.=" and (s.stu_no like '".$_POST['stu_no']."%' "
                           . " or s.stu_no like '?".$_POST['stu_no']."%') ";
    if ($_POST['name_ch']<>"") $search_condition.=" and s.name_ch like '%".addslashes($_POST['name_ch'])."%'";
    if ($_POST['name_enl']<>"") $search_condition.=" and s.name_enl like '%".$_POST['name_enl']."%'";
    if ($_POST['name_enf']<>"") $search_condition.=" and s.name_enf like '%".$_POST['name_enf']."%'";
	if ($_POST['nationality_no']<>"") $search_condition.=" and s.nationality_no= '".$_POST['nationality_no']."'";
    if ($_POST['birth_year']<>"") $search_condition.=" and s.birth_year = '".$_POST['birth_year']."'";
    if ($_POST['birth_month']<>"") $search_condition.=" and s.birth_month like '%".$_POST['birth_month']."'";
    if ($_POST['birth_day']<>"") $search_condition.=" and s.birth_day like '%".$_POST['birth_day']."'";
	if ($_POST['address_taiwan']<>"") $search_condition.=" and s.address_taiwan like '%".$_POST['address_taiwan']."%'";
	$search_condition.=" order by s.`stu_no` desc";
	//echo $search_condition;
	$result = mysql_query($search_condition);
	$total_num =  mysql_num_rows($result); // 總人數
	//$total_num = mysql_num_rows($result);
	//$total_page=ceil($total_num/$page_people);
}
else {
	$page = ($_GET['page']=="" ? 1 : $_GET['page']);
	if ($_GET['leave'] == 1) { // 交換學生
		$sql_condition = " and stu_no like 'E%' ";
	}
	else if ($_GET['leave'] == 2) { // 遊學團學生 == 尚未分年度
		$sql_condition = " and stu_no like 'S%' ";
	}
	else if ($_GET['leave'] == 3) { // 所有學生
		$sql_condition = "";
	}
	else if ($_GET['leave'] == 4) { // 大學部學生(應該是非以上學生)
		$sql_condition = " and stu_no like 'F%' ";
	}
	else { // 學華語學生(
		$sql_condition = " and stu_no between '0' and '999999' ";
	}
	$sql_page = " limit " . (($page-1)*$page_people) . "," . $page_people;

	//////////////////////////////////////////////////////////////////
	// 取出每頁學生資料
	$sql = "select * from student where 1 = 1 "
	     . $sql_condition
	     . " order by `stu_no` desc ";
	$result = mysql_query($sql . $sql_page);
	//////////////////////////////////////////////////////////////////
	// 計算頁數用
	$rt = mysql_query($sql);
	$total_num = mysql_num_rows($rt); // 總人數
	$total_page=ceil($total_num/$page_people);
	//////////////////////////////////////////////////////////////////
}
//echo $search_condition; //debug mode
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
    <script type="text/javascript" language="javascript" src="../js/json.js"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">學生基本資料列表</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>
    <hr>
    <a href="#" onclick="(document.getElementById('search_table').style.display == 'none')?(document.getElementById('search_table').style.display = ''):(document.getElementById('search_table').style.display = 'none');">查詢特定資料</a>
    &nbsp;&nbsp;&nbsp;<a href="student.php?leave=3">所有學生列表</a>
    &nbsp;&nbsp;&nbsp;<a href="student.php">學華語學生列表</a>
    &nbsp;&nbsp;&nbsp;<a href="student.php?leave=1">交換學生列表</a>
    &nbsp;&nbsp;&nbsp;<a href="student.php?leave=4">大學部學生列表</a>
    &nbsp;&nbsp;&nbsp;<a href="student.php?leave=2">遊學團學生列表</a>

    <form method="POST" action="student.php">
      <table width="0%"  border="1" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC" <?php echo $_POST['search'] <> 1 ? "style=\"display:none\"" : ""; ?> id="search_table">
        <tr>
          <td>
            <input type="hidden" name="search" value="1">
            學號：<input type="text" name="stu_no" size="7" value='<?php echo $_POST["stu_no"] ?>'>
          </td>
          <td>
            中文姓名：<input type="text" name="name_ch" size="15" value='<?php echo $_POST["name_ch"] ?>'>
          </td>
          <td>
            英文姓：<input type="text" name="name_enl" size="25" value='<?php echo $_POST["name_enl"] ?>'>
          </td>
          <td>
            英文名：<input type="text" name="name_enf" size="25" value='<?php echo $_POST["name_enf"] ?>'>
          </td>
        </tr>
        <tr>
          <td colspan="2">國籍：
            <select size="1" name="nationality_no">
              <option value="">請選擇國籍</option>
<?php
 $sql1="Select * From nationality Order by people DESC";
 $result1=mysql_query($sql1) or die("no data");
 while ($data1=mysql_fetch_array($result1)){
   echo "    <option value='".$data1["nationality_no"]."' ".($data1["nationality_no"] == $_POST["nationality_no"] ? "selected" : "")."><font size=4>".trans_nationality($data1["nationality_no"],1).",".trans_nationality($data1["nationality_no"],3)."</font></option>\n";
 }
 ?>
            </select>
          </td>
          <td colspan="2">出生：西元
            <input type="text" name="birth_year" size="4" value='<?php echo $_POST["birth_year"] ?>'>年
            <input type="text" name="birth_month" size="2" value='<?php echo $_POST["birth_month"] ?>'>月
            <input type="text" name="birth_day" size="2" value='<?php echo $_POST["birth_day"] ?>'>日
          </td>
          </tr>
          <tr>
            <td colspan="4">
              在臺地址：<input type="text" name="address_taiwan" size="100" value='<?php echo $_POST["address_taiwan"] ?>'>
            </td>
          </tr>
          <tr>
            <td colspan="4">
              限定期別(已繳費)：
              <select name="term">
                <option value="">請選擇期別</option>
                <?php echo listallterm(GetTermUid($_POST["term"]));?>
              </select>
              (上面沒設條件此項無用)
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <input type="submit" value="查詢" name="B1">
              <input type="reset" value="重新設定" name="B2">
            </td>
          </tr>
    </table>
    </form>
    <hr>
    <p align="center">
        <a href="student_new.php">新增資料</a>　　<a href="../class/timetable.php?timetable=4" target='_blank'>學生課程列表</a>
        &nbsp;&nbsp;&nbsp;(<?php echo $total_num; /* 印出總人數 */ ?>)
    </p>
    <center>
<?php
/*頁碼列印*/
page_link($page,$total_page,$_GET['leave']);
?>


<table border="1" cellpadding="0" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
  <tr> 
    <td width="8%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">學號</font></td>
    <td width="15%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">中文姓名</font></td>
    <td width="30%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">外文姓名</font></td>
    <td width="10%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">國籍</font></td>
    <td width="7%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">Lastest</font></td>
    <td width="7%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">修改</font></td>
    <td width="7%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">刪除</font></td>
    <td width="16%" align="center" bgcolor="#E6FFEB" height="19"><font size="4">備註</font></td>
  </tr>
  
<?php
while($data = mysql_fetch_array($result)) {
?>
  <tr> 
    <td width="8%" align="center" height="25"><a href="student_speci.php?no=<?php echo $data[0]; ?>" target="_NEW"><font size="4"> 
      <?php echo $data[1]; ?>
      </font></a></td>
    <td width="15%" align="left" height="25"><font size="4"> 
      <?php echo "　".$data[2]; ?>
      </font></td>
    <td width="30%" align="left" height="25"><font size="4"> 
      <?php echo "　".id_to_enname($data[1]); ?>
      </font></td>
    <td width="10%" align="center" height="25"><font size="4"> 
      <?php echo trans_nationality($data[6],1); ?>
      </font></td>
    <td width="7%" align="center" height="25"><a href="../class/timetable_student.php?spe_no=<?php echo $data[1]; ?>&term=<?php student_lastterm($data[1]); ?>" target="_blank" title='<?php student_lastterm($data[1]); ?>'>
    <font size="4">課程</font></a></td>
    <td width="7%" align="center" height="25"><a href="./student_modify.php?modify=1&mod_item=<?php echo $data[0]; ?>"><font size="4">修改</font></a></td>
    <td width="7%" align="center" height="25"><font size="4"><a href="./student.php?delete=1&del_item=<?php echo $data[0]; ?>" onClick="return popMsg(this,'刪除學號: ' + '<?php echo $data[1]; ?>' + ' 的資料')">刪除</font></td>
    <td width="16%" align="left" height="25">
    	<a href='student_remark.php?modify=1&mod_item=<?php echo $data[0]; ?>'><img src="collapsed_yes.gif" border="0" title="修改標題"></a>
    	<font size="4"><?php echo "　".$data[48]; ?>
      </font></td>
  </tr>
  <?php } ?>
</table>
</center>

    <?php
    /*頁碼列印*/
    page_link($page,$total_page,$_GET['leave']);
    ?>

    <p align="center"><a href="student_new.php">新增資料</a></p>

</body>

</html>

<?php
function page_link($this_page,$total_page,$change) { // 查詢不同學生有不同的下一頁...orz
	echo "<p align='center'><font size='4'>";

	if ($change == "") $leave = "";
	else $leave = "&leave=" . $change;

	if ($total_page <= 1) return 0;

	if ($this_page==1 ) echo "上一頁 ";
	else echo "<a href='student.php?page=".($this_page-1).$leave."'>上一頁</a> ";

	echo "| ";
	for ($i=1;$i<=$total_page;$i++) {
		if ($i == $this_page) echo $i;
		else echo "<a href='student.php?page=".$i.$leave."'>".$i."</a>";
		echo " |";
	}

	if ($this_page==$total_page) echo " 下一頁";
	else echo " <a href='student.php?page=".($this_page+1).$leave."'>下一頁</a>";

	echo "</font></p>";
}
?>