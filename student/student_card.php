<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">列印學生證</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>
    <hr />
    <form name="form1" method="POST" action="student_card_print.php">
        <p align="center">
            請輸入學號：<input type="text" name="stu_no" size="20">
        </p>
        <p align="center">
            <input type="submit" name="submit" value="看學生證">
        </p>
    </form>
</body>
</html>
