<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php
	$sql = "select * from pay_list where term = '$term' order by 'stu' asc";
	$result = mysql_query($sql);
	$counter=0;
	$page=0;	
	$width_array=Array(30, 30, 70,170, 30, 60,
			       70, 60, 30, 60, 60,
			       60, 30,190,100, 70);
	//序號30,中文80,英文180,性別30,生日60,
	//國籍70,護照60,簽證30,來臺60,入學60,
	//結業60,每週時數30,地址200,電話100,手機70
	while($data = mysql_fetch_array($result)) {
		//$sql2 = "select * from student order by 'name_ch' asc";		
		$sql2 = "select * from student where stu_no = '$data[2]'";
		$result2 = mysql_query($sql2);
		while($data2 = mysql_fetch_array($result2)) {
			if ($counter%40==0){
    ?>
    <table border="0" width="1120pt" cellpadding="0" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
        <tr>
            <td></td>
            <td>
                <p align="center"><font face="標楷體" style="font-size: 10pt">輔仁大學語言中心<?php echo $term;?>外籍生名冊</font></p>
                <p align="right"><font face="Times New Roman" style="font-size: 10pt"><?php echo Date("Y/m/d");?></font></p>
                <div align="center">
                    <center>
  <table border="1" width="1190pt" cellpadding="1" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
    <input type="hidden" name="term" value="<?php echo $term;?>">
    <tr>
      <td align="center" width="<?php echo $width_array[0];?>pt"><font face="標楷體" style="font-size:10pt">序號</font></td>
      <td align="center" width="<?php echo $width_array[1];?>pt"><font face="標楷體" style="font-size:10pt">學號</font></td>
      <td align="center" width="<?php echo $width_array[2];?>pt"><font face="標楷體" style="font-size:10pt">中文姓名</font></td>
      <td align="center" width="<?php echo $width_array[3];?>pt"><font face="標楷體" style="font-size:10pt">英文姓名</font></td>
      <td align="center" width="<?php echo $width_array[4];?>pt"><font face="標楷體" style="font-size:10pt">性別</font></td>
      <td align="center" width="<?php echo $width_array[5];?>pt"><font face="標楷體" style="font-size:10pt">出生日期</font></td>
      <td align="center" width="<?php echo $width_array[6];?>pt"><font face="標楷體" style="font-size:10pt">洲別</font></td>
      <td align="center" width="<?php echo $width_array[6];?>pt"><font face="標楷體" style="font-size:10pt">國籍</font></td>
      <td align="center" width="<?php echo $width_array[7];?>pt"><font face="標楷體" style="font-size:10pt">護照號碼</font></td>
      <td align="center" width="<?php echo $width_array[8];?>pt"><font face="標楷體" style="font-size:10pt">簽證<br>種類</font></td>
      <td align="center" width="<?php echo $width_array[9];?>pt"><font face="標楷體" style="font-size:10pt">來臺日期</font></td>
      <td align="center" width="<?php echo $width_array[10];?>pt"><font face="標楷體" style="font-size:10pt">入學日期</font></td>
      <td align="center" width="<?php echo $width_array[11];?>pt"><font face="標楷體" style="font-size:10pt">結業日期</font></td>
      <td align="center" width="<?php echo $width_array[12];?>pt"><font face="標楷體" style="font-size:10pt">每週<br>時數</font></td>
      <td align="center" width="<?php echo $width_array[13];?>pt"><font face="標楷體" style="font-size:10pt">在臺地址</font></td>
      <td align="center" width="<?php echo $width_array[14];?>pt"><font face="標楷體" style="font-size:10pt">電話號碼</font></td>
      <td align="center" width="<?php echo $width_array[15];?>pt"><font face="標楷體" style="font-size:10pt">手機號碼</font></td>
    </tr>
<?php
			}
			$counter++;
?>
    <tr>
      <td align="center" width="<?php echo $width_array[0];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $counter; /*序號*/?></font></td>
      <td align="center" width="<?php echo $width_array[1];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[1]; /*學號*/?></font></td>
      <td align="left" width="<?php echo $width_array[2];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo $data2[2]; /*中文姓名*/?></font></td>
      <td align="left" width="<?php echo $width_array[3];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[3]." ".$data2[4]; /*英文姓名*/?></font></td>
      <td align="center" width="<?php echo $width_array[4];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo $data2[5]; /*性別*/?></font></td>
      <td align="center" width="<?php echo $width_array[5];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[9]."/".$data2[10]."/".substr($data2[8],2,2); /*出生日期*/?></font></td>
      <td align="center" width="<?php echo $width_array[6];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo trans_nationality($data2[6],3); /*洲別*/?></font></td>
      <td align="center" width="<?php echo $width_array[6];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo trans_nationality($data2[6],1); /*國籍*/?></font></td>
      <td align="left" width="<?php echo $width_array[7];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[11]; /*護照號碼*/?></font></td>
      <td align="center" width="<?php echo $width_array[8];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo $data2[31]; /*簽證種類*/?></font></td>
      <td align="center" width="<?php echo $width_array[9];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[36]."/".$data2[37]."/".substr($data2[35],2,2); /*來臺日期*/?></font></td>
      <td align="center" width="<?php echo $width_array[10];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[40]."/".$data2[41]."/".substr($data2[39],2,2); /*入學日期*/?></font></td>
      <td align="center" width="<?php echo $width_array[11];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[44]."/".$data2[45]."/".substr($data2[43],2,2); /*結業日期*/?></font></td>
      <td align="center" width="<?php echo $width_array[12];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo hours_aweek($term,$data2[1]); /*每週時數*/?></font></td>
      <td align="left" width="<?php echo $width_array[13];?>pt"><font face="標楷體" style="font-size:10pt"><?php echo $data2[25]; /*在臺地址*/?></font></td>
      <td align="left" width="<?php echo $width_array[14];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[26]; /*電話號碼*/?></font></td>
      <td align="center" width="<?php echo $width_array[15];?>pt"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[27]; /*手機號碼*/?></font></td>
    </tr>
<?php			if ($counter%40==0){?>
  </table>

    </center>
                </div>
                <p align="center"><font face="Times New Roman" style="font-size: 10pt">page:<?php echo $counter/40; ?></font></p>
            </td>
        </tr>
    </table>
    <?php
                     page_break();
                 }
		}
	}
    ?>
    </table>

    </center>
</div>
    <p align="center"><font face="Times New Roman" style="font-size: 10pt">Page:<?php echo ceil($counter/40);?></font></p>
    </td></tr></table>
</body>

</html>
<?php
function hours_aweek($term,$stu_no){
	$sql = "select * from pay where term = '$term' and stu_no = '$stu_no'";
	$result = mysql_query($sql);
	$temp=0;
	while($data = mysql_fetch_array($result)) {
		$temp=$temp+$data[5];
	}
	return $temp;
}
?>