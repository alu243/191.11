<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php 
	$sql = "select * from student where no='$no'"; 
	$result = mysql_query($sql); 

	$data = mysql_fetch_array($result) 
    ?>

    <p align="left">您現在所在位置：<font color="#FF9900">學生基本資料-詳細資料</font></p>

    <div align="center">
        <center>
          
    <table border="1" width="90%" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
      <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">學號</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo $data[1];?></font></td> 
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">中文姓名</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo $data[2];?></font></td> 
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">外文姓名</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[3]." ".$data[4];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">性別</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[5];?></font></td> 
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Sex</font></td>
              <td width="29%" align="left"><font size="4"><?php echo $data[5]=="男" ?"Male":"Female"; ?></font></td> 
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">出生年月日</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[8].".".$data[9].".".$data[10];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Date of  
                Birth</font></td>
              <td width="29%" align="left"><font size="4"><?php echo trans_month($data[9])." ".$data[10].", ".$data[8];?></font></td>
            </tr>
<!--
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">婚姻狀況</font></td>
              <td width="36%" align="left"><font size="4"><?php echo 
                                                                $data[33];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Marriage</font></td>
              <td width="29%" align="left"><font size="4"><?php echo 
                                                                $data[33]=="未婚" ? "Single" : 
                                                                "Married"; ?></font></td> 
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">職業</font></td>
              <td width="36%" align="left"><font size="4"><?php echo 
                                                                $data[32];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Profession</font></td>
              <td width="29%" align="left">　</td>
            </tr>
-->
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">國籍</font></td>
              <td width="36%" align="left"><font size="4"><?php echo 
                                                                trans_nationality($data[6],1);?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Nationality</font></td>
              <td width="29%" align="left"><font size="4"><?php echo 
                                                                trans_nationality($data[6],2);?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">洲別</font></td>
              <td width="36%" align="left"><font size="4"><?php echo 
                                                                trans_nationality($data[6],3);?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Continent</font></td>
              <td width="29%" align="left"><font size="4"><?php echo 
                                                                trans_nationality($data[6],4);?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">護照號碼</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[11];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">簽證種類</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[31];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">最高學歷</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[12];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">畢業日期</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[14].".".$data[15].".".$data[16];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Date of  
                Graduation</font></td>
              <td width="29%" align="left"><font size="4"><?php echo trans_month($data[15])." ".$data[16].", ".$data[14];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">來臺日期</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[35].".".$data[36].".".$data[37];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Date of 
                Taiwan</font></td>
              <td width="29%" align="left"><font size="4"><?php echo trans_month($data[36])." ".$data[37].", ".$data[35];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">入學日期</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[39].".".$data[40].".".$data[41];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Date of 
                Enroll</font></td>
              <td width="29%" align="left"><font size="4"><?php echo trans_month($data[40])." ".$data[41].", ".$data[39];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">結業日期</font></td>
              <td width="36%" align="left"><font size="4"><?php echo $data[43].".".$data[44].".".$data[45];?></font></td>
              <td width="17%" align="left" bgcolor="#E6FFEB"><font size="4">Date of 
                Breakup</font></td>
              <td width="29%" align="left"><font size="4"><?php echo trans_month($data[44])." ".$data[45].", ".$data[43];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">永久地址</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[17];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">永久電話</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[18];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">永久傳真</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[19];?></font></td>
            </tr>
            
     		<tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">通訊地址</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[20];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">通訊電話</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[21];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">通訊傳真</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[22];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">通訊手機</font></td>
              <td width="82%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[23];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">在台地址</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[25];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">在台電話</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[26];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">在台手機</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[27];?></font></td>
            </tr>
            <tr><td width="100%" bgcolor="#336600" colspan="4">&nbsp;</td></tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">緊急聯絡人姓名</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[28];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">緊急聯絡人電話</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo 
                                                                            $data[29];?></font></td>
            </tr>
            <tr>
              <td width="18%" bgcolor="#E6FFEB"><font size="4">緊急聯絡人手機</font></td>
              <td width="87%" align="left" colspan="3"><font size="4"><?php echo $data[30];?></font></td>
            </tr>
          </table>
  </center>
    </div>
    <p align="center">
    <p align="center"><a href="./student_modify.php?modify=1&mod_item=<?php echo $no; ?>">修改資料</a>　<a href="student.php">回上一頁</a></p>

    <p align="center"></p>
    <p align="center">click修改資料→<a href="student_modify.php.htm">student_modify.php.htm</a></p>

</body>

</html>
