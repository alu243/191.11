<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">學生國籍統計</font>&nbsp;&nbsp;<a href="../list.php">回主選單</a>
    <hr>
    <form name="form1" method="POST" action="student_nation_list.php">
        輸入統計時段:
  西元<input type="text" name="year_start" size="4" value="<?php echo (empty($_POST['year_start'])?Date("Y"):$_POST['year_start']) ;?>">年
  <input type="text" name="month_start"  size="2" value="<?php echo (empty($_POST['month_start'])?Date("m"):$_POST['month_start']);?>">月
  ∼
  <input type="text" name="year_end" size="4" value="<?php echo (empty($_POST['year_end'])?Date("Y"):$_POST['year_end']);?>">年
  <input type="text" name="month_end"  size="2" value="<?php echo (empty($_POST['month_end'])?Date("m"):$_POST['month_end']);?>">月
  <input type="submit" name="submit" value="看列表">
    </form>
    <hr>
    <?php
    if (!empty($_POST['year_start']) && !empty($_POST['month_start']) && !empty($_POST['year_end']) && !empty($_POST['month_end'])) {

        // 列印非遊學團 (非E開頭之學號)
        /*    $sql = "select n.name_en, n.name_ch, s.sex, count(distinct s.name_ch, s.name_enf, s.name_enl) as cnt ";
    	." from nationality n, student s, term t, class c "
    	." where n.nationality_no = s.nationality_no "
    	." and s.stu_no = c.stu_no "
    	." and c.term=t.term "
    	." and (DATE_FORMAT(t.start_date, '%Y%m') <= '" .$_POST['year_end'].$_POST['month_end']."' "
    	." and DATE_FORMAT(t.end_date, '%Y%m') >= '" .$_POST['year_start'].$_POST['month_start']."') "
    	." group by n.name_en, n.name_ch, s.sex "
    	." order by n.continent_en, n.name_en";
         */
        $sql = "select n.name_en, n.name_ch, n.continent, n.continent_en, s.sex, count(distinct s.name_ch, s.name_enf, s.name_enl) as cnt "
            ." from nationality n, student s, term t, pay c " // 由 class 改成 pay(課程繳費清單) 表格
            ." where n.nationality_no = s.nationality_no "
            ." and s.stu_no = c.stu_no "
            ." and s.stu_no not like 'E%' "
            ." and c.term=t.term "
            ." and (DATE_FORMAT(t.start_date, '%Y%m') <= '" .$_POST['year_end'].$_POST['month_end']."' "
            ." and DATE_FORMAT(t.end_date, '%Y%m') >= '" .$_POST['year_start'].$_POST['month_start']."') "
            ." group by n.name_en, n.name_ch, s.sex "
            ." order by n.continent_en, n.name_en";
        $rt = mysql_query($sql);
        $total=0;
        $sextotl['男']=0;
        $sextotl['女']=0;
        while ($data=mysql_fetch_array($rt)) {
            if (empty($stat_data[$data['name_en']]['男'])) $stat_data[$data['name_en']]['男'] = 0;
            if (empty($stat_data[$data['name_en']]['女'])) $stat_data[$data['name_en']]['女'] = 0;
            if (empty($stat_data[$data['name_en']]['subtotal'])) $stat_data[$data['name_en']]['subtotal'] = 0;
            $stat_data[$data['name_en']]['continent'] = $data['continent']; // 洲別
            $stat_data[$data['name_en']]['continent_en'] = $data['continent_en']; // 洲別
            $stat_data[$data['name_en']]['name_en'] = $data['name_en']; // 國別
            $stat_data[$data['name_en']]['name_ch'] = $data['name_ch']; // 國別
            $stat_data[$data['name_en']][$data['sex']] = $data['cnt'];  // 性別統計
            $stat_data[$data['name_en']]['subtotal'] += $data['cnt'];   // 小計
            $total+=$data['cnt'];					    // 總計
            $sextotal[$data['sex']]+=$data['cnt'];			    // 小計(性別)
        }
        echo "<center><table border='1' width='500'>";
        echo "<tr><td align='center' colspan='7'>".$_POST['year_start'].$_POST['month_start']."∼".$_POST['year_end'].$_POST['month_end']."</td></tr>";
        echo "<tr><td align='center'>洲別(英文)</td><td align='center'>洲別(中文)</td><td align='center'>國別(英文)</td><td align='center'>國別(中文)</td><td align='center'>男</td><td align='center'>女</td><td align='center'>人數</td></tr>";
        foreach ($stat_data as $k => $v) {
            echo "<tr><td>".$v['continent_en']."</td><td>".$v['continent']."</td><td>".$v['name_en']."</td><td>".$v['name_ch']."</td><td>".$v['男']."</td><td>".$v['女']."</td><td>".$v['subtotal']."</td></tr>";
        }
        echo "<tr><td colspan='3'>總和</td><td>&nbsp;</td><td>".$sextotal['男']."</td><td>".$sextotal['女']."</td><td>".$total."</td></tr>";
        echo "</table></center>";

        
        // 列印遊學團 (E開頭之學號)
        unset($sextotal);
        unset($total);
        unset($stat_data);
        $sql = "select n.name_en, n.name_ch, n.continent, n.continent_en, s.sex, count(distinct s.name_ch, s.name_enf, s.name_enl) as cnt "
            ." from nationality n, student s, term t, pay c "  // 由 class 改成 pay(課程繳費清單) 表格
            ." where n.nationality_no = s.nationality_no "
            ." and s.stu_no = c.stu_no "
            ." and s.stu_no like 'E%' "
            ." and c.term=t.term "
            ." and (DATE_FORMAT(t.start_date, '%Y%m') <= '" .$_POST['year_end'].$_POST['month_end']."' "
            ." and DATE_FORMAT(t.end_date, '%Y%m') >= '" .$_POST['year_start'].$_POST['month_start']."') "
            ." group by n.name_en, n.name_ch, s.sex "
            ." order by n.continent_en, n.name_en";
        $rt = mysql_query($sql);
        $total=0;
        $sextotl['男']=0;
        $sextotl['女']=0;
        while ($data=mysql_fetch_array($rt)) {
            if (empty($stat_data[$data['name_en']]['男'])) $stat_data[$data['name_en']]['男'] = 0;
            if (empty($stat_data[$data['name_en']]['女'])) $stat_data[$data['name_en']]['女'] = 0;
            if (empty($stat_data[$data['name_en']]['subtotal'])) $stat_data[$data['name_en']]['subtotal'] = 0;
            $stat_data[$data['name_en']]['continent'] = $data['continent']; // 洲別
            $stat_data[$data['name_en']]['continent_en'] = $data['continent_en']; // 洲別
            $stat_data[$data['name_en']]['name_en'] = $data['name_en']; // 國別
            $stat_data[$data['name_en']]['name_ch'] = $data['name_ch']; // 國別
            $stat_data[$data['name_en']][$data['sex']] = $data['cnt'];  // 性別統計
            $stat_data[$data['name_en']]['subtotal'] += $data['cnt'];   // 小計
            $total+=$data['cnt'];					    // 總計
            $sextotal[$data['sex']]+=$data['cnt'];			    // 小計(性別)
        }
        echo "<center><table border='1' width='500'>";
        echo "<tr><td align='center' colspan='7'>".$_POST['year_start'].$_POST['month_start']."∼".$_POST['year_end'].$_POST['month_end']."</td></tr>";
        echo "<tr><td align='center'>洲別(英文)</td><td align='center'>洲別(中文)</td><td align='center'>國別(英文)</td><td align='center'>國別(中文)</td><td align='center'>男</td><td align='center'>女</td><td align='center'>人數</td></tr>";

        if (!empty($stat_data)) {
            foreach ($stat_data as $k => $v) {
                echo "<tr><td>".$v['continent_en']."</td><td>".$v['continent']."</td><td>".$v['name_en']."</td><td>".$v['name_ch']."</td><td>".$v['男']."</td><td>".$v['女']."</td><td>".$v['subtotal']."</td></tr>";
            }
            echo "<tr><td colspan='3'>總和</td><td>&nbsp;</td><td>".$sextotal['男']."</td><td>".$sextotal['女']."</td><td>".$total."</td></tr>";
            echo "</table></center>";
        }
        
        
    }
    ?>


    <p align="center"><a href="../list.php">回上一頁</a></p>
</body>
</html>