<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
$sql_term = "select * from term where term = '$term' ";
$result_term = mysql_query($sql_term);
$data_term = mysql_fetch_array($result_term);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <table border="0" width="720pt" cellpadding="10" cellspacing="0" style="border-collapse: collapse" id="AutoNumber1">
        <tr>
            <td width="100pt">
                <br>
                <br>
            </td>
            <td align="left">
                <br>
                <p align="center"><font face="標楷體" style="font-size: 16pt">輔仁大學語言中心<?php echo substr($data_term[2],0,4)."年".substr($data_term[2],5,2)."月至".substr($data_term[3],0,4)."年".substr($data_term[3],5,2)."月";?>學生保險名單</font></p>
                <div align="center">
                    <center>
  <table border="1" width="620pt" cellpadding="4" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
    <input type="hidden" name="term" value="<?php echo $term;?>">
    <tr>
      <td align="center"><font face="標楷體" style="font-size:10pt">序號</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">中文姓名</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">英文姓名</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">出生日期</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">護照號碼</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">加保日期</font></td>
      <td align="center"><font face="標楷體" style="font-size:10pt">保險日期</font></td>
    </tr>

<?php
$sql = "select * from pay_list where term = '$term' ";
$result = mysql_query($sql);

$counter=0;
while($data = mysql_fetch_array($result)) {
	$counter++;
    
	$sql2 = "select no,name_ch,name_enf,name_enl,birth_year,birth_month,birth_day,passport_no from student where stu_no = '$data[2]'";
	$result2 = mysql_query($sql2);
	while($data2 = mysql_fetch_array($result2)) {
        
?>
    <tr>
      <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo $counter; ?></font></td>
      <td align="left"><font face="標楷體" style="font-size:10pt"><?php echo $data2[1]; ?></font></td>
      <td align="left"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[2]." ".$data2[3]; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[4]."/".$data2[5]."/".$data2[6]; ?></font></td>
      <td align="left"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2[7]; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo substr($data_term[2],0,4)."/".substr($data_term[2],5,2)."/01"; ?></font></td>
      <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo substr($data_term[3],0,4)."/".substr($data_term[3],5,2)."/".end_day(substr($data_term[3],0,4),substr($data_term[3],5,2)); ?></font></td>
    </tr>
<?php }
} ?>
  </table>

    </center>
                </div>

            </td>
        </tr>
    </table>
</body>

</html>
<?php
function end_day($_year,$_month){
    $temp=0;
    if ($_year%4==0 && ($_year%100!=0 || $_year%400==0)) { $temp=1; }
    if($_month==1 || $_month==3 || $_month==5 || $_month==7 || $_month==8 || $_month==10 || $_month==12){ return "31"; }
    else if ( $_month==4 || $_month==6 || $_month==9 || $_month==11){ return "30"; }
    else if ($temp==1&& $_month==2) {return "20"; }
    else { return "28"; }
}

?>