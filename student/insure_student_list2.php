<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
$sql_term = "select * from term where term = '$term' ";
$result_term = mysql_query($sql_term);
$data_term = mysql_fetch_array($result_term);
?>
<html>
<head>
  <meta http-equiv="Content-Language" content="zh-tw">
  <meta http-equiv="Content-Type" content="text/html; charset=big5">
  <title>語言中心行政處理系統</title>
  <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
  <table border="0" width="1080pt" cellpadding="0" cellspacing="0" style="border-collapse: collapse" id="AutoNumber1">
    <tr>
      <td width="1pt">
        <br />
        <br />
      </td>
      <td align="left">
        <br />
        <p align="center"><font face="標楷體" style="font-size: 16pt">輔仁大學語言中心<?php echo substr($data_term["start_date"],0,4)."年".substr($data_term["start_date"],5,2)."月至".substr($data_term["end_date"],0,4)."年".substr($data_term["end_date"],5,2)."月";?>學生保險名單</font></p>
        <div align="center">
          <center>
          <table border="1" width="100%" cellpadding="2" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
            <input type="hidden" name="term" value="<?php echo $term;?>">
            <tr>
              <td align="center"><font face="標楷體" style="font-size:10pt">序<br>號</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">員工ID</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">被保人ID</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">中文姓名</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">外文姓名</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">性<br>別</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">職業<br>類別</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">等級</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">生日</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">月薪</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">勞保薪</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">受益人</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">生效日</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">員工編碼</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">關<br>係</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">記名<br>記戶</font></td>
            </tr>
<?php
$sql = "select * from pay_list where term = '$term' order by pay_date";
$result = mysql_query($sql);

$counter=0;
while($data = mysql_fetch_array($result)) {
    $temp_m = getTotalFee($data["term"],$data["stu_no"],$data["is_admin_fee"],$data["is_old_fee"],$data["is_reg_fee"],$data["is_langlab_fee"]);
    if ($temp_m <= 0) continue;

    //是否已經繳清
	//$temp_pay=0;
    //$t=getPayStatusTerm($data["term"],$data["stu_no"],$data["is_admin_fee"],$data["is_old_fee"],$data["is_reg_fee"],$data["is_langlab_fee"]);
	//$temp_pay=$t["status"];

    //是否屬於要列印的日期區間
    $isInPrintDate = isDateInRange($data["pay_date"], $_POST["s_date"], $_POST["e_date"]);

    // 是否為暑期
    $isInSummer=substr_count($term,"暑期");
    
	//if ($temp_pay==2 && $isInPrintDate==1 && ($data["is_admin_fee"]>0 || $isInSummer>=1)){ // 已繳清 && 在所選的學期內 && (有行政費用 || 暑期)
	if ($isInPrintDate==1 && ($data["is_admin_fee"]>1 || $isInSummer >= 1)){ // 在所選的學期內 && (有行政費用已繳清 || 暑期)
		$counter++;
        //echo "stu".$counter;
		$sql2 = "select no,passport_no,name_ch,sex,birth_year,birth_month,birth_day,heir,stu_no from student where stu_no = '".$data["stu_no"]."'";
		$result2 = mysql_query($sql2);
		while($data2 = mysql_fetch_array($result2)) {
            
?>
            <tr>
              <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo $counter; ?></font></td>
              <td align="left"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2["passport_no"]; ?></font></td>
              <td align="left"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2["passport_no"]; ?></font></td>
              <td align="left"><font face="標楷體" style="font-size:10pt"><?php echo $data2["name_ch"]; ?></font></td>
              <td align="left"><font face="Times New Roman" style="font-size:10pt"><?php echo id_to_enname($data2["stu_no"]) ?></font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php if ($data2["sex"]=="男"){ echo "1";}else if($data2["sex"]=="女"){ echo "2";} ?></font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">1</font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">01</font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo ($data2["birth_year"]-1911).$data2["birth_month"].$data2["birth_day"]; ?></font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">　</font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">　</font></td>
              <td align="center"><font face="標楷體" style="font-size:10pt">法定繼承人</font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo (substr($data_term["start_date"],0,4)-1911).substr($data_term["start_date"],5,2)."01"; ?></font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt"><?php echo $data2["stu_no"]; ?></font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">0</font></td>
              <td align="center"><font face="Times New Roman" style="font-size:10pt">4</font></td>
            </tr>
<?php
            /*
            }
            if($counter%20==0){
            $pages=$counter/20;
            echo "</table><table width=1080pt><tr><td align=center>page".$pages."</td></tr></table></center></div></td></tr></table>";
            page_break();

            <table border="0"  width="1080pt" cellpadding="0" cellspacing="0" style="border-collapse: collapse" id="AutoNumber1">
            <tr><td width="1pt">　<br>　<br></td>
            <td align="left">　<br>
            <p align="center"><font face="標楷體" style="font-size:16pt">輔仁大學語言中心<?php echo substr($data_term[2],0,4)."年".substr($data_term[2],5,2)."月至".substr($data_term[3],0,4)."年".substr($data_term[3],5,2)."月";?>學生保險名單</font></p>
            <div align="center">
            <center>
            <table border="1" width="100%" cellpadding="0" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000" style="border-collapse: collapse">
            <input type="hidden" name="term" value="<?php echo $term;?>">
            <tr>
            <td align="center"><font face="標楷體" style="font-size:10pt">序<br>號</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">員工ID</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">被保人ID</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">姓名</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">性<br>別</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">職業<br>類別</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">等級</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">生日</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">月薪</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">勞保薪</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">受益人</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">生效日</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">員工編碼</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">關<br>係</font></td>
            <td align="center"><font face="標楷體" style="font-size:10pt">記名<br>記戶</font></td>
            </tr>
             */
?>
<?php
        }
    }
} ?>
          </table>
          </center>
        </div>
      </td>
    </tr>
  </table>
</body>
</html>
<?php
function end_day($_year,$_month){
    $temp=0;
    if ($_year%4==0 && ($_year%100!=0 || $_year%400==0)) { $temp=1; }
    if($_month==1 || $_month==3 || $_month==5 || $_month==7 || $_month==8 || $_month==10 || $_month==12){ return "31"; }
    else if ( $_month==4 || $_month==6 || $_month==9 || $_month==11){ return "30"; }
    else if ($temp==1&& $_month==2) {return "20"; }
    else { return "28"; }
}
function isDateInRange($myDate, $startDate, $endDate)
{
    if($myDate >= $startDate && $myDate <= $endDate) return 1;
	
    if($startDate == "" && $endDate == "") return 1;
    
    if (empty($startDate) && empty($endDate)) return 1;
    
    return 0;
}
?>