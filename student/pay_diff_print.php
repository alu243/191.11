<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <center><font size="5" face="標楷體"><?php echo $title;?></font></center>
    <table border="1" cellpadding="10" cellspacing="0" style="border-collapse: collapse; width: 1120pt" bordercolor="#000000">
    <?php
    $tA=0;$tB=0;$tC=0;$tD=0;$tE=0;$tF=0;
    $tadmin=0;$treg=0;$told=0;
    $tsm=0;$tpm=0;
    echo "<tr><td><font size=2 face=標楷體>序號</font></td>
	  <td><font size=2 face=標楷體>帳單編號</font></td>
	  <td><font size=2 face=標楷體>學號</font></td>
	  <td><font size=2 face=標楷體>身分<br>駐記</font></td>
	  <td><font size=2 face=標楷體>中文姓名</font></td>
	  <td><font size=2 face=標楷體>外文姓文</font></td>
	  <td><font size=2 face=標楷體>單人班<br>總時數</font></td>
	  <td><font size=2>2-3<font face=標楷體>人班<br>總時數</font></font></td>
	  <td><font size=2>4-7<font face=標楷體>人班<br>總時數</font></font></td>
	  <td><font size=2 face=標楷體>文化課程<br>總時數</font></td>
	  <td><font size=2>Speech Lab.<br><font face=標楷體>總時數</font></font></td>
	  <td><font size=2 face=標楷體>特殊課程<br>總時數</font></td>
	  <td><font size=2 face=標楷體>行政<br>管理費</font></td>
	  <td><font size=2 face=標楷體>新生<br>註冊費</font></td>
	  <td><font size=2 face=標楷體>舊生<br>折扣</font></td>
	  <td><font size=2 face=標楷體>應付金額</font></td>
	  <td><font size=2 face=標楷體>預繳金額</font></td>
	  <td><font size=2 face=標楷體>差額</font></td>
	  <td><font size=2 face=標楷體>備註</font></td></tr></font>";
    for($i=1;$i<=$count;$i++){
        $sm=${"admin_fee".$i}+${"old_fee".$i}+${"reg_fee".$i}+${"langlab_fee".$i}+s_money(${"courseA".$i},${"courseB".$i},${"courseC".$i},${"courseD".$i},${"courseE".$i},${"courseF".$i});
        $pm=${"admin_fee".$i}+${"old_fee".$i}+${"reg_fee".$i}+${"langlab_fee".$i}+getTotalFee($term,${"stu".$i},0,0,0,0);
        $tm=$pm-$sm;
        $tA=$tA+${"courseA".$i};$tB=$tB+${"courseB".$i};$tC=$tC+${"courseC".$i};$tD=$tD+${"courseD".$i};$tE=$tE+${"courseE".$i};$tF=$tF+${"courseF".$i};
        $tadmin=$tadmin+${"admin_fee".$i};$treg=$treg+${"reg_fee".$i};$told=$told+${"old_fee".$i};
        $tsm=$tsm+$sm;$tpm=$tpm+$pm;
        echo "<tr><td><font size=2>".$i."</font></td><td><font size=2>".${"pay_no".$i}."</font></td><td><font size=2>".${"stu".$i}."</font></td><td><font size=2 face=標楷體>".${"iden".$i}."</font></td><td><font size=2 face=標楷體>".idtoname(${"stu".$i})."</font></td><td><font size=2>".id_to_enname(${"stu".$i})."</font></td>";
        echo "<td><font size=2>".${"courseA".$i}."</font></td>";
        echo "<td><font size=2>".${"courseB".$i}."</font></td>";
        echo "<td><font size=2>".${"courseC".$i}."</font></td>";
        echo "<td><font size=2>".${"courseD".$i}."</font></td>";
        echo "<td><font size=2>".${"courseE".$i}."</font></td>";
        echo "<td><font size=2>".${"courseF".$i}."</font></td>";
        echo "<td><font size=2>".${"admin_fee".$i}."</font></td>";
        echo "<td><font size=2>".${"reg_fee".$i}."</font></td>";
        echo "<td><font size=2>".${"old_fee".$i}."</font></td>";
        echo "<td><font size=2>".$sm."</font></td><td><font size=2>".$pm."</font></td><td><font size=2>".$tm."</font></td>";
        echo "<td><font size=2 face=標楷體>".${"note".$i}."</td></tr>";
    }
    $temp=$tpm-$tsm;
    echo "<tr><td colspan=2><font size=2 face=標楷體>總計</font></td><td colspan=4></td><td><font size=2>".$tA."</font></td><td><font size=2>".$tB."<font></td><td><font size=2>".$tC."</font></td><td><font size=2>".$tD."</font></td><td><font size=2>".$tE."<font></td><td><font size=2>".$tF."</font></td><td><font size=2>".$tadmin."</font></td><td><font size=2>".$treg."</font></td><td><font size=2>".$told."</font></td><td><font size=2>".$tsm."</font></td><td><font size=2>".$tpm."</font></td><td><font size=2>".$temp."</font></td><td></td></tr>";
    echo "</table>";

    function idtoname($id)
    {
        $sql1 = "select * from student order by 'stu_no' asc";
        $result1 = mysql_query($sql1);
        while($data1 = mysql_fetch_array($result1))
            if($id ==  $data1[1]) {
                return $data1[2];
            }
    }

    function id_to_enname($id){
        $sql = "select * from student order by 'stu_no' asc";
        $result = mysql_query($sql);
        while($data = mysql_fetch_array($result)){
            if($id ==  $data[1]) {
                if ($data[6]==5 || $data[6]==10 || $data[6]==11){//日本(5)越南(10)韓國(11)last name在前面
                    return $data[4]." ".$data[3];//last name + first name
                }else{
                    return $data[3]." ".$data[4];//first name + ladt name
                }
            }
        }
    }

    function s_money($a,$b,$c,$d,$e,$f){
        return $a*415+$b*220+$c*155+$d*125+$e*180+$f*415;
    }
    ?>
</body>
</html>
