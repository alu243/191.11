<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
//
// student_modify.php 及 student_new.php 的改變國藉還沒完成
//
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php  
	if ($modify == 1 && !empty($mod_item)) {  
		$str = "select * from student where no = '$mod_item'";  
		$rt = mysql_query($str) or die("無此資料");  
		$data = mysql_fetch_array($rt);  
	}  
    ?>
    您現在所在位置：<font color="#FF9900">學生基本資料-修改</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>&nbsp;&nbsp;&nbsp;<a href="student.php">回上一頁</a><hr>
    <form method="POST" action="student.php?update=1">
        <br>
        <div align="center">
            <center>
      <table border="1" width="62%" height="60" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
        <input type="hidden" name="upd_item" value="<?php echo $data[0]; ?>">
        <input type="hidden" name="stu_no" value="<?php echo $data[1]; ?>">
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">學號</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <?php echo $data[1];?>
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">中文姓名</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="name_ch" size="20" value="<?php echo $data[2];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="2" bgcolor="#E6FFEB"><font size="4">英文名字</font></td>
          <td width="39%" height="2" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="name_enf" size="20" value="<?php echo $data[3];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">英文姓氏</font></td>
          <td width="39%" height="1" align="left"><font size="4"> 
            <input type="text" name="name_enl" size="20" value="<?php echo $data[4];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">性別</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"> 
            <select size="1" name="sex">
              <?php if($data[5] == "男" ) echo "<option value=男 selected>男</option><option value=女>女</option>";  
                    else echo"<option value=男>男</option><option value=女 selected>女</option>";?>
            </select>
          </td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">出生年月日</font></td>
          <td width="39%" height="1" align="left"><font size="4">西元 
            <input type="text" name="birth_year" size="7" value="<?php echo $data[8];?>">
            年 
            <input type="text" name="birth_month" size="3" value="<?php echo $data[9];?>">
            月 
            <input type="text" name="birth_day" size="5" value="<?php echo $data[10];?>">
            日</font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">統一証號</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="heir" size="20" value="<?php echo $data[47];?>">
            </font></td>
        </tr>
<!--
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">職業</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="profession" size="20" value="<?php echo $data[32];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">婚姻狀況</font></td>
          <td width="39%" height="4" align="left"> 
            <select size="1" name="marriage">
              <?php
              if($data[33] == "未婚" ){
                  echo "<option value=未婚 selected>未婚</option> <option value=已婚>已婚</option>"; 
              }else{
                  echo"<option value=未婚>未婚</option> <option value=已婚 selected>已婚</option>";
              }
              ?>
            </select>
          </td>
        </tr>
        
-->
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">來臺日期</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4">西元 
            <input type="text" name="taiwan_year" size="7" value="<?php echo $data[35];?>">
            年 
            <input type="text" name="taiwan_month" size="3" value="<?php echo $data[36];?>">
            月 
            <input type="text" name="taiwan_day" size="5" value="<?php echo $data[37];?>">
            日</font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">入學日期</font></td>
          <td width="39%" height="4" align="left"><font size="4">西元 
            <input type="text" name="school_year" size="7" value="<?php echo $data[39];?>">
            年 
            <input type="text" name="school_month" size="3" value="<?php echo $data[40];?>">
            月 
            <input type="text" name="school_day" size="5" value="<?php echo $data[41];?>">
            日</font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">結業日期</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4">西元 
            <input type="text" name="leave_year" size="7" value="<?php echo $data[43];?>">
            年 
            <input type="text" name="leave_month" size="3" value="<?php echo $data[44];?>">
            月 
            <input type="text" name="leave_day" size="5" value="<?php echo $data[45];?>">
            日</font></td>
        </tr>
        <tr> 
          <td width="22%" height="3"><font size="4">簽證種類</font></td>
          <td width="39%" height="3" align="left"> 
            <select size="1" name="visa">
            <?php
            if($data[31] == "停留"){
                echo "<option value=停留 selected><font size=4>停留</font></option><option value=居留 ><font size=4>居留</font></option>";
            }else{
                echo "<option value=停留 ><font size=4>停留</font></option><option value=居留 selected><font size=4>居留</font></option>";
            }
            ?>
            </select>
          </td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">國籍</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"> 


          <select name="contient" language=JavaScript  onChange="SetContinent(this.value)" size=1>
		<option value="">請先選擇洲別</option>
<?php
$str = "select continent, continent_en from nationality group by continent_en";
$rt = mysql_query($str);
while ( list($con,$con_en) = mysql_fetch_row($rt) ) {
?>
		<option value="<?php echo $con_en; ?>"><?php echo $con; ?></option>
<?php
}
?>
	  </select>

<script>
    function SetContinent(Country) {
        var i;
        var what_country;

<?php
mysql_data_seek($rt, 0);
while ( list($con,$con_en) = mysql_fetch_row($rt) ) {
	$str1 = "var " . $con_en . "_code";
	$str1 .= "= new Array(\"".$data[6]."\" ";
	$str1 .= ", \"不改變國別\" ";

	$rt2 = mysql_query("select nationality_no, name_ch from nationality where continent_en = '$con_en'");
	while (list($na_no, $na_name) = mysql_fetch_row($rt2)) {
		$str1 .= ", \"" . $na_no . "\" ";
		$str1 .= ", \"" . $na_name . "\" ";
	}
	$str1 .= ");\n";
	echo $str1;
}


mysql_data_seek($rt, 0);
while ( list($con,$con_en) = mysql_fetch_row($rt) ) {
	echo "\t\tif (Country == \"" . $con_en ."\") what_country = " . $con_en . "_code;\n";
}
?>
	    document.getElementById("nationality_no").length = what_country.length / 2;
	    for (i = 0 ; i < what_country.length ; i += 2) {
	        document.getElementById("nationality_no").options[i / 2].value = what_country[i];
	        document.getElementById("nationality_no").options[i / 2].text = what_country[i + 1];
	    }
	}
</script>
          <select size="1" id="nationality_no" name="nationality_no">
		<option value="<?php echo $data[6]; ?>">不改變國別</option>
          </select>

     
<?php
/*          	$file_name="student_modify?modify=1&mod_item=".$data[0]."&";
$sql1="Select * From nationality where nationality_no='$data[6]'";
$result1=mysql_query($sql1);
$i=0;
while ($data1=mysql_fetch_array($result1)){
$i++;
$con_type=$data1[5];
}
?>
<select name="contient" language=JavaScript  onchange='document.location.href=this.options[this.selectedIndex].value'; size=1>
<option value="<?php echo $file_name;?>.php?con_type=0">請先選擇洲別</option>
<option value="<?php echo $file_name;?>.php?con_type=Asia" <?php if ($con_type=="Asia") echo "selected";?>>亞洲</option>
<option value="<?php echo $file_name;?>.php?con_type=Africa" <?php if ($con_type=="Africa") echo "selected";?>>非洲</option>
<option value="<?php echo $file_name;?>.php?con_type=Europ" <?php if ($con_type=="Europ") echo "selected";?>>歐洲</option>
<option value="<?php echo $file_name;?>.php?con_type=America" <?php if ($con_type=="America") echo "selected";?>>美洲</option>
<option value="<?php echo $file_name;?>.php?con_type=Oceania" <?php if ($con_type=="Oceania") echo "selected";?>>大洋洲</option>
</select>
<select size="1" name="nationality_no">
<?php
changContinent($con_type,$data[6]);
?> 
</select>
 */
?>
            <?php echo "　".trans_nationality($data[6],1);?>
          </td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">護照號碼</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="passport_no" size="20" value="<?php echo $data[11];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="2" bgcolor="#E6FFEB"><font size="4">最高學歷</font></td>
          <td width="39%" height="2" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="highest_edu" size="20" value="<?php echo $data[12];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">畢業日期</font></td>
          <td width="39%" height="1" align="left"><font size="4">西元 
            <input type="text" name="graduation_year" size="7" value="<?php echo $data[14];?>">
            年 
            <input type="text" name="graduation_month" size="3" value="<?php echo $data[15];?>">
            月 
            <input type="text" name="graduation_day" size="5" value="<?php echo $data[16];?>">
            日</font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">永久地址</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="address_per" size="50" value="<?php echo $data[17];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">永久電話</font></td>
          <td width="39%" height="1" align="left"><font size="4"> 
            <input type="text" name="tel_per" size="20" value="<?php echo $data[18];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">永久傳真</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="fax_per" size="20" value="<?php echo $data[19];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">通訊地址</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="address_mail" size="50" value="<?php echo $data[20];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="2" bgcolor="#E6FFEB"><font size="4">通訊電話</font></td>
          <td width="39%" height="2" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="tel_mail" size="20" value="<?php echo $data[21];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">通訊傳真</font></td>
          <td width="39%" height="1" align="left"><font size="4"> 
            <input type="text" name="fax_mail" size="20" value="<?php echo $data[22];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">通訊手機</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="cell_mail" size="20" value="<?php echo $data[23];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">通訊email</font></td>
          <td width="39%" height="1" align="left"><font size="4"> 
            <input type="text" name="email_mail" size="20" value="<?php echo $data[24];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1"><font size="4">在台地址</font></td>
          <td width="39%" height="1" align="left"><font size="4"> 
            <input type="text" name="address_taiwan" size="50" value="<?php echo $data[25];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">在台電話</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="tel_taiwan" size="20" value="<?php echo $data[26];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">在台手機</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="cell_taiwan" size="20" value="<?php echo $data[27];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">緊急聯絡人姓名</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="emergency_name" size="20" value="<?php echo $data[28];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="4"><font size="4">緊急聯絡人電話</font></td>
          <td width="39%" height="4" align="left"><font size="4"> 
            <input type="text" name="emergency_tel" size="20" value="<?php echo $data[29];?>">
            </font></td>
        </tr>
        <tr> 
          <td width="22%" height="1" bgcolor="#E6FFEB"><font size="4">緊急聯絡人手機</font></td>
          <td width="39%" height="1" align="left" bgcolor="#E6FFEB"><font size="4"> 
            <input type="text" name="emergency_cell" size="20" value="<?php echo $data[30];?>">
            </font></td>
        </tr>
      </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="確定修改" name="B1"><input type="reset" value="清除重填" name="B2"></p>
    </form>
</body>

</html>
<?php
function changContinent($con_type,$id){
	$sql = "select * from nationality where continent_en='$con_type'";
	$rt = mysql_query($sql); 
	echo "<option value=0>請選擇國家</option>";
	while($data = mysql_fetch_array($rt)) {
		$i++;
		echo "<option value=".$data[1];
		if ($id==$data[1]) {echo " selected";}
		echo ">".$data[2]."</option>";
	}
	
}
?>