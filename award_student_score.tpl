<html>
<head>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=big5">
	<title>{Page_Title}</title>
</head>
<body>
	您現在所在位置：<font color="#FF9900">{Page_SiteMap}</font>&nbsp;&nbsp;&nbsp;<a href="{Page_RootLink}">{Page_RootLinkName}</a>
	<hr>
	期別範圍：<font color="blue">{Page_Term}</font><hr>
	<table border="1" cellpadding="4" cellspacing="0" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000">
		<tr>
			<td rowspan="2" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4"><b>編號</b></font></td>
			<td rowspan="2" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4">學號</b></font></td>
			<td rowspan="2" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4">姓名</b></font></td>
			<td colspan="4" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4">期別</b></font></td>
			<td rowspan="2" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4">總平均</b></font></td>
			<td rowspan="2" align="center" bgcolor="#008000"><font color="#FFFFFF" size="4">備註</b></font></td>
		</tr>
		<tr>
			<td bgcolor="#008000"><font color="#FFFFFF" size="4">{year1}年9~11月</b></font></td>
			<td bgcolor="#008000"><font color="#FFFFFF" size="4">{year2}年12~2月</b></font></td>
			<td bgcolor="#008000"><font color="#FFFFFF" size="4">{year2}年3~5月</b></font></td>
			<td bgcolor="#008000"><font color="#FFFFFF" size="4">{year2}年6~8月</b></font></td>
		</tr>
		<!-- BEGIN DYNAMIC BLOCK: row1 -->
		<tr valign="top">
			<td align="right">{sn}</td>
			<td>{stu_no}</td>
			<td>{stu_name}</td>
			<td>{stu_score1}</td>
			<td>{stu_score2}</td>
			<td>{stu_score3}</td>
			<td>{stu_score4}</td>
			<td>{stu_score_avg}</td>
			<td>{stu_remark}</td>
		</tr>
		<!-- END DYNAMIC BLOCK: row1 -->
	</table>
</body>
</html>
