<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
if ($modify == 1 && !empty($mod_item)) {
    $str = "select * from member where no = '$mod_item'";
    $rt = mysql_query($str) or die("無此資料");
    $data = mysql_fetch_array($rt);
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">人事基本資料-修改</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>&nbsp;&nbsp;&nbsp;<a href="member.php">回上一頁</a><hr>
    <form method="POST" action="./member.php?update=1">
        <br>
        <div align="center">
            <center>
          <table border="1" width="650" cellspacing="0" cellpadding="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
           <input type="hidden" name="upd_item" value="<?php echo $data["no"]; ?>">
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">中心代碼</font></td>
              <td width="500" height="1" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="center_no" size="20" value="<?php echo $data["center_no"];?>"></td>
            </tr>
            <tr>
              <td width="200"><font style="font-size:14pt">薪資代碼</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="salary_no" size="20" value="<?php echo $data["salary_no"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">姓名</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="name_ch" size="20" value="<?php echo $data["name_ch"];?>"></td>
            </tr>
            <tr>
              <td width="200"><font style="font-size:14pt">鐘點費</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="salary_ahour" size="20" value="<?php echo $data["salary_ahour"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB">性別</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB">
               <select style="font-size:14pt" name="sex" >
                      <?php if($data["sex"] == "男" ) echo "<option value=男 selected>男</option><option value=女>女</option>";  
                            else echo"<option value=男>男</option><option value=女 selected>女</option>";?>
              </select>
            </tr>
            <tr>
              <td width="200"><font style="font-size:14pt">出生年月日</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="brith" size="20" value="<?php echo $data["brith"];?>"></td>
            </tr>
            <tr>
              <td width="200"><font style="font-size:14pt">身分證字號</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="id_no" size="20"  value="<?php echo $data["id_no"];?>">　　密碼：<input style="font-size:14pt" type="password" name="pw" size="20"  value="<?php echo $data["pw"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">電話(辦公)</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="tel_o" size="20" value="<?php echo $data["tel_o"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">電話(住家)</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="tel" size="20" value="<?php echo $data["tel"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">電話(行動)</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="tel_cell" size="20" value="<?php echo $data["tel_cell"];?>"></td>
            </tr>
            <tr>
              <td width="200" bgcolor="#E6FFEB"><font style="font-size:14pt">e-mail</font></td>
              <td width="500" align="left" bgcolor="#E6FFEB"><input style="font-size:14pt" type="text" name="email" size="20" value="<?php echo $data["email"];?>"></td>
            </tr>
            <tr>
              <td width="200"><font style="font-size:14pt">詳細地址</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="address" size="50" value="<?php echo $data["address"];?>"></td>
            </tr>        
            <tr>
              <td width="200"><font style="font-size:14pt">籍貫</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="fam_reg" size="50" value="<?php echo $data["fam_reg"];?>"></td>
            </tr>    
            <tr>
              <td width="200"><font style="font-size:14pt">宗教</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="religion" size="20" value="<?php echo $data["religion"];?>"></td>
            </tr>  
            <tr>
              <td width="200"><font style="font-size:14pt">學歷</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="education" size="50" value="<?php echo $data["education"];?>"></td>
            </tr>  
            <tr>
              <td width="200"><font style="font-size:14pt">學歷修業起訖</font></td>
              <td width="500" align="left"><input style="font-size:14pt" type="text" name="edu_s" size="10" value="<?php echo $data["edu_s"];?>">起至<input style="font-size:14pt" type="text" name="edu_e" size="10" value="<?php echo $data["edu_e"];?>">止<br>(ex.[1995-09]起至[1999-06]止)</td>
            </tr>  
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="確定修改" name="B1">
            <input type="reset" value="清除重填" name="B2">
        <p align="center">
    </form>
</body>

</html>
