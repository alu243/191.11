<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">人事基本資料-新增</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>&nbsp;&nbsp;&nbsp;<a href="member.php">回上一頁</a><hr>
    <form method="POST" action="member.php?increase=1">
        <br>
        <div align="center">
            <center>
          <table border="1" width="495" cellspacing="0" cellpadding="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000" style="border-collapse: collapse">
           <input type="hidden" name="upd_item" value="<?php echo $data["no"]; ?>">
            <tr>
              <td width="171" bgcolor="#E6FFEB">中心代碼</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="center_no" size="20"></td>
            </tr>
            <tr>
              <td width="171">薪資代碼</td>
              <td width="421" align="left"><input type="text" name="salary_no" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">姓名</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="name_ch" size="20"></td>
            </tr>
            <tr>
              <td width="171">鐘點費</td>
              <td width="421" align="left"><input type="text" name="salary_ahour" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">性別</td>
              <td width="421" align="left" bgcolor="#E6FFEB">
               <select name="sex" >
                      <option value="1">男</option>
                      <option value="0">女</option>
              </select>
            </tr>
            <tr>
              <td width="171">出生年月日</td>
              <td width="421" align="left"><input type="text" name="brith" size="20"></td>
            </tr>
            <tr>
              <td width="171">身分證字號</td>
              <td width="421" align="left"><input type="text" name="id_no" size="20">　　密碼：<input type="text" name="pw" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">電話(辦公)</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="tel_o" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">電話(住家)</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="tel" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">電話(行動)</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="tel_cell" size="20"></td>
            </tr>
            <tr>
              <td width="171" bgcolor="#E6FFEB">e-mail</td>
              <td width="421" align="left" bgcolor="#E6FFEB"><input type="text" name="email" size="40"></td>
            </tr>
            <tr>
              <td width="171">詳細地址</td>
              <td width="421" align="left"><input type="text" name="address" size="50"></td>
            </tr>    
            <tr>
              <td width="171">籍貫</td>
              <td width="421" align="left"><input type="text" name="fam_reg" size="50"></td>
            </tr>    
            <tr>
              <td width="171">宗教</td>
              <td width="421" align="left"><input type="text" name="religion" size="50"></td>
            </tr>  
            <tr>
              <td width="171">學歷</td>
              <td width="421" align="left"><input type="text" name="education" size="50"></td>
            </tr>  
            <tr>
              <td width="171">學歷修業起訖</td>
              <td width="421" align="left"><input type="text" name="edu_s" size="20">起至<input type="text" name="edu_e" size="20">止<br>(ex.[1995-09]起至[1999-06]止)</td>
            </tr>  
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="新增" name="B1">
            <input type="reset" value="清除重填" name="B2">
        </p>
    </form>
</body>

</html>
