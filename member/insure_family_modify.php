<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lcclass", "fjdp", "lang", "lcgrade");
CheckAuthority($acptAccounts);
?>
<?php
if ($modify == 1 && !empty($mod_item)) {
    $str = "select * from insure_family where no = '$mod_item'";
    $rt = mysql_query($str) or die("無此資料");
    $data = mysql_fetch_array($rt);
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">健保眷屬加保資料-修改</font></p>
    <form method="POST" action="./insure_family.php?update=1">
        <br>
        <div align="center">
            <center>
          <table border="1" width="52%" height="117" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <input type="hidden" name="upd_item" value="<?php echo $data["no"];?>">
            <input type="hidden" name="center_no" value="<?php echo $data["center_no"];?>">
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">中心代碼/姓名</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><?php idtoname($data["center_no"]);?></td>
            </tr>
            <tr>
              <td width="39%" height="23">眷屬一</td>
              <td width="61%" height="23"><input type="text" name="family1" size="20" value=<?php echo $data["family1"];?>></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">眷屬一情況</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><select size="1" name="condition1">
                  <?php selectd($data["condition1"]);?>
                </select></td>
            </tr>
            <tr>
              <td width="39%" height="23">眷屬二</td>
              <td width="61%" height="23"><input type="text" name="family2" size="20" value=<?php echo $data["family2"];?>></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">眷屬二情況</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><select size="1" name="condition2">
                <?php selectd($data["condition2"]);?>
                </select></td>
            </tr>
            <tr>
              <td width="39%" height="23">眷屬三</td>
              <td width="61%" height="23"><input type="text" name="family3" size="20" value=<?php echo $data["family3"];?>></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">眷屬三情況</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><select size="1" name="condition3">
                <?php selectd($data["condition3"]);?>
                </select></td>
            </tr>
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="確定修改" name="B1">
            <input type="reset" value="清除重填" name="B2">
        </p>
    </form>
    <p align="center"><a href="insure_family.php">回上一頁</a></p>


</body>

</html>
