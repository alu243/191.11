<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lcclass", "fjdp", "lang", "lcgrade");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所的位置：<font color="#FF9900">勞保投保分級表-新增</font></p>
    <form method="POST" action="./insure_labor.php?increase=1">
        <br>
        <div align="center">
            <center>
          <table border="1" width="43%" height="117" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">投保等級</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="grade" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23">投保薪資金額</td>
              <td width="61%" height="23"><input type="text" name="money" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">最高實際薪資</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="ceiling" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="24">最低實際薪資</td>
              <td width="61%" height="24"><input type="text" name="floor" size="20"></td>
            </tr>
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="確定新增" name="B1">
            <input type="reset" value="清除重填" name="B2">
        </p>
    </form>
    <p align="center">
        <br>
        <a href="insure_labor.php">回上一頁</a>
</body>

</html>
