<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">成績處理-選擇學生</font></p>
    <p align="center"></p>
    <form name="form1" method="POST" action="score_student.php">
        <p>
            選擇查詢方式 ： 
    <select name="search">
        <option value="stu_no">學號</option>
        <option value="name_ch">中文姓名</option>
        <option value="name_enf">First name</option>
        <option value="name_enl">Last name</option>
        <option value="brith">出生年月日</option>
        <option value="nationality">國籍</option>

    </select>
        </p>
        <p>
            輸入查詢的資料： 
    <input type="text" name="search_data">
            <input type="submit" name="B1" value="送出">
        </p>
    </form>
    <p></p>
    <p></p>
    <?php 
    if ($search_data<>""){
        echo "<hr size=1 color=#009933 noshade>";
        echo "請從下列資料中選出要處理的資料(點選學號欄)";
        echo "<table width=100% border=1 cellpadding=0 cellspacing=0 bordercolor=#008000 bordercolorlight=#008000 bordercolordark=#008000 dwcopytype=CopyTableColumn>";
        echo  "<tr>";
        echo    "<td width=8% align=center bgcolor=#E6FFEB>學號</td>";
        echo    "<td width=16% align=center bgcolor=#E6FFEB>中文姓名</td>";
        echo    "<td width=27% align=center bgcolor=#E6FFEB>外文名字</td>";
        echo    "<td width=27% align=center bgcolor=#E6FFEB>外文姓氏</td>";
        echo    "<td width=12% align=center bgcolor=#E6FFEB>國籍</td>";
        echo    "<td width=10% align=center bgcolor=#E6FFEB>出生年月日</td>";
        echo  "</tr>";
        
        $sql="select no,stu_no,name_ch,name_enf,name_enl,nationality_no,birth from student where ".$search."='$search_data'";
        $result = mysql_query($sql);
        $counter=0;
        while($data = mysql_fetch_array($result)) {
            echo "<tr>";
            echo "<td width=8% align=center><a href=score_student_detail.php?no=".$data[0].">".$data[1]."</td>";
            echo "<td width=16% align=center>".$data[2]."</td>";
            echo "<td width=27% align=center>".$data[3]."</td>";
            echo "<td width=27% align=center>".$data[4]."</td>";
            echo "<td width=12% align=center>".trans_nationality($data[5],1)."</td>";
            echo "<td width=10% align=center>".$data[6]."</td>";
            echo "</tr>";
            $counter++;
        }
        echo "</table>";
        if ($counter==0){echo "<p></p><font color=#FF0000><b>資料庫內查無此人資料<br>您所輸入的資料可能有誤，請重新輸入一次</b></font>";}
    }

    ?>

    <p align="center"></p>
    <p align="center"><a href="../list.php">回上一頁</a></p>

</body>

</html>
