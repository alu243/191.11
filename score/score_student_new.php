<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<?php
$str = "select * from student where no = '$no'";
$rt = mysql_query($str);
$data = mysql_fetch_array($rt);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">成績資料表﹝依學生﹞-新增</font></p>
    <form method="POST" action="./score_student_detail.php?<?php if($old==1){echo "old=".$old."&";}?>increase=1&no=<?php echo $data[0];?>">
        <p align="center">
            <br>
            <font style="font-size: 14pt;">學號：<font color="#0000FF"><?php echo $data[1];?></font>　 中文姓名：<font color="#0000FF"><?php echo $data[2];?></font>　外文姓名：<font color="#0000FF"><?php echo $data[3]." ".$data[4];?></font>　國籍：<font color="#0000FF"><?php echo trans_nationality($data[6],1);?></font></font>
            <p align="center">
                &nbsp;&nbsp;&nbsp;
        <div align="center">
            <center>
          <table border="1" width="43%" height="64" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">期別</td>
              <td width="61%" height="1" bgcolor="#E6FFEB">
                起始日期:西元<input type="text" name="start_year" size="4">年<input type="text" name="start_month" size="2">月<input type="text" name="start_day" size="2">日<br>
                終止日期:西元<input type="text" name="end_year" size="4">年<input type="text" name="end_month" size="2">月<input type="text" name="end_day" size="2">日
              </td>
            </tr>
            <tr>
              <td width="39%" height="1">教材</td>
              <td width="61%" height="1"><select size="1" name="book_no">
                  <option value="">選擇教材</option>
                  <?php
                  $book_data = listbook();
                  for ($i=0 ; $i < count($book_data) ; $i++) {
                  ?>
                  <option value="<?php echo $book_data[$i]["book_no"]; ?>"> <?php echo $book_data[$i]["name_ch"]; ?> 
                  </option>
                  <?php
                  }
                  ?>
                </select></td>
            </tr>
            <tr>
              <td width="39%" height="1">教材2</td>
              <td width="61%" height="1"><select size="1" name="book_no2">
                  <option value="">選擇教材</option>
                  <?php
                  $book_data = listbook();
                  for ($i=0 ; $i < count($book_data) ; $i++) {
                  ?>
                  <option value="<?php echo $book_data[$i]["book_no"]; ?>"> <?php echo $book_data[$i]["name_ch"]; ?> 
                  </option>
                  <?php
                  }
                  ?>
                </select></td>
            </tr>
            <tr>
              <td width="39%" height="2" bgcolor="#E6FFEB">教師</td>
              <td width="61%" height="2" bgcolor="#E6FFEB"><input type="text" name="teacher" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1">應上課時數</td>
              <td width="61%" height="1"><input type="text" name="hours_full" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">請假時數</td>
              <td width="61%" height="1" bgcolor="#E6FFEB"><input type="text" name="hours_absence" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">請假備註</td>
              <td width="61%" height="1" bgcolor="#E6FFEB"><input type="text" name="note" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1">聽力</td>
              <td width="61%" height="1">
              <input type="text" name="compre" size="20"></td>
            </tr>
<?php
if ($old==1){
?>
            <tr>
              <td width="39%" height="1">發音</td>
              <td width="61%" height="1"><input type="text" name="pron" size="20"></td>
            </tr>
<?php } ?>
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">說話</td>
              <td width="61%" height="1" bgcolor="#E6FFEB"><input type="text" name="conver" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">閱讀</td>
              <td width="61%" height="1" bgcolor="#E6FFEB"><input type="text" name="read2" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1">寫字</td>
              <td width="61%" height="1"><input type="text" name="write2" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="1" bgcolor="#E6FFEB">作文</td>
              <td width="61%" height="1" bgcolor="#E6FFEB"><input type="text" name="compo" size="20"></td>
            </tr>
<?php
if ($old<>1){
?>
            <tr>
              <td width="39%" height="1">期末</td>
              <td width="61%" height="1"><input type="text" name="final" size="20"></td>
            </tr>
<?php } ?>
          </table>
          </center>
        </div>
                <p align="center">
                    <input type="submit" value="確定新增" name="B1"><input type="reset" value="清除重填" name="B2"></p>
    </form>
    <p align="center"><a href="score_student_detail.php?no=<?php echo $data[0];?>">回上一頁</a></p>

</body>
