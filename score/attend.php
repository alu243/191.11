<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<?php
/*
新增：無查詢資料時，列出該學期有加入繳費列表之所有學生
 */
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">列印在學證明暨出缺席紀錄表</font>&nbsp;&nbsp;<a href="../list.php">回主選單</a></p>
    </p>
    <p align="center"></p>
    <form name="form1" method="POST" action="attend.php">
        <p>
            選擇查詢方式 ： 
    <select name="search">
        <option value="stu_no">學號</option>
        <option value="name_ch">中文姓名</option>
        <option value="name_enf">First name</option>
        <option value="name_enl">Last name</option>
    </select>
        </p>
        <p>
            輸入查詢的資料： 
    <input type="text" name="search_data">
            <input type="submit" name="B1" value="送出">
        </p>
    </form>
    <p></p>
    <p></p>
    <?php 
    echo "<hr size=1 color=#009933 noshade>";
    echo "請從下列資料中選出要列印的紀錄表(點選學號欄)";
    echo "<table width=100% border=1 cellpadding=0 cellspacing=0 bordercolor=#008000 bordercolorlight=#008000 bordercolordark=#008000 dwcopytype=CopyTableColumn>";
    echo  "<tr>";
    echo    "<td width=8% align=center bgcolor=#E6FFEB>學號</td>";
    echo    "<td width=16% align=center bgcolor=#E6FFEB>中文姓名</td>";
    echo    "<td width=27% align=center bgcolor=#E6FFEB>外文名字</td>";
    echo    "<td width=27% align=center bgcolor=#E6FFEB>外文姓氏</td>";
    echo    "<td width=12% align=center bgcolor=#E6FFEB>國籍</td>";
    echo    "<td width=10% align=center bgcolor=#E6FFEB>出生年月日</td>";
    echo  "</tr>";
    if (!empty($_POST['search_data'])) {
        $sql="select no,stu_no,name_ch,name_enf,name_enl,nationality_no,birth from student where ".$search." like '%".$search_data."%'";
    }
    else {
        $sSql = "select term from term where no ='$T_uid'";
        list($T_name) = mysql_fetch_row(mysql_query($sSql));
        echo "<br><br><font color='#FF9900'>學期:$T_name 之所有學生：</font><br>";
        $sql = "select distinct s.no, s.stu_no, s.name_ch, s.name_enf, s.name_enl, s.nationality_no, s.birth "
              ." from student as s, pay_list as p, pay as pa "
              ." where p.stu_no = s.stu_no and p.term = '$T_name' and pa.term = p.term and pa.stu_no = p.stu_no"
              ." order by s.stu_no ";
    }
    $result = mysql_query($sql);
    $counter=0;
    while($data = mysql_fetch_array($result)) {
        echo "<tr>";
        echo "<td width=8% align=center><a href=attend_print_temp.php?no=".$data[no]." target=_blank>".$data[stu_no]."</td>";
        echo "<td width=16% align=left>　".$data[name_ch]."</td>";
        echo "<td width=27% align=left>　".$data[name_enf]."</td>";
        echo "<td width=27% align=left>　".$data[name_enl]."</td>";
        echo "<td width=12% align=center>".trans_nationality($data[nationality_no],1)."</td>";
        echo "<td width=10% align=center>".$data[birth]."</td>";
        echo "</tr>";
        $counter++;
    }
    echo "</table>";
    if ($counter==0){echo "<p></p><font color=#FF0000><b>資料庫內查無此人資料<br>您所輸入的資料可能有誤，請重新輸入一次</b></font>";}

    ?>

    <p align="center"></p>
    <p align="center"><a href="../list.php">回上一頁</a></p>

</body>

</html>
