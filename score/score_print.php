<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<?php
$sql="select * from student where no='$_GET[no]'";
$result = mysql_query($sql);
$data = mysql_fetch_array($result);
if($old==1){
    $str_score = "select * from score_old where stu_no='$data[1]' order by 'start_date','end_date' asc";
}else{
    $str_score = "select * from score where stu_no='$data[1]' order by 'start_date','end_date' asc";
}
$rt_score = mysql_query($str_score);
$page_count=0;
while ($data_score = mysql_fetch_array($rt_score)) {
    $page_count++;
    if ($data_score[23]<>""){$page_count++;}
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
    <style>
        <!--
        h2 {
            margin-bottom: .0001pt;
            text-align: center;
            page-break-after: avoid;
            font-size: 12.0pt;
            font-family: "Times New Roman";
            margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
        }

        table.MsoNormalTable {
            mso-style-parent: "";
            font-size: 10.0pt;
            font-family: "Times New Roman";
        }
        -->
    </style>
    <object id="factory" style="display: none" viewastext
        classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
        codebase="http://140.136.191.9/admin/ScriptX.cab#Version=6,1,428,11">
    </object>
    <script>
        function window.onload() {
            factory.printing.header = "";
            factory.printing.footer = "";
            factory.printing.portrait = false;

            factory.printing.leftMargin = 5.0;
            factory.printing.topMargin = 4.0;
            factory.printing.rightMargin = 0.0;
            factory.printing.bottomMargin = 0.0;
            window.print();
        }
    </script>
</head>

<body>
    <?php
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    function page_head($data,$old){ ?>
    <div align="left">
        <table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="1080" id="AutoNumber1">
            <tr>
                <td>
                    <p class="MsoNormal" align="center" style="text-align: center">
                        <b>
                            <span style="font-size: 13.0pt; font-family: 文鼎標準楷體">&nbsp;<br>
                                輔仁大學語言中心學生成績單<br>
                            </span></b><span lang="EN-US"><b>Fu Jen Catholic University Language Center</b><br>
                            </span>
                        <span lang="EN-US" style="font-size: 10.0pt">Xinzhuang Dist., New Taipei City, Taiwan, Republic of China<br>
                        </span>
                        <span lang="EN-US" style="font-family: 新細明體; color: black; font-weight: 700">Transcript 
      and Accomplishment Records</span>
                    </p>
                    <div align="center">
                        <center>
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="1055" id="AutoNumber3">
          <tr>
            <td width="39" height="13"></td>
            <td width="361" height="13"></td>
            <td width="90" height="13"></td>
            <td width="152" height="13"></td>
            <td width="57" height="13"></td>
            <td width="118" height="13"></td>
            <td width="87" height="13"></td>
            <td width="165" height="13"></td>
          </tr>
          <tr>
            <td width="39" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">姓名:</font></td>
            <td width="361" height="13" valign="bottom">
			  <font font style="font-size:10pt" face="Times New Roman"><b><?php echo $data[2]; ?></b></font></td>
            <td width="90" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　學號:</font></td>
            <td width="152" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b><?php echo $data[1]; ?></b></font></td>
            <td width="57" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　性別:</font></td>
            <td width="118" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b><?php echo $data[5]; ?></b></font></td>
            <td width="87" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　出生年月日:</font></td>
            <td width="151" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b><?php echo $data[7]; ?></b></font></td>
          </tr>
          <tr>
            <td width="39" valign="top" height="10"></td>
            <td width="361" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="90" valign="top" height="10"></td>
            <td width="152" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="57" valign="top" height="10"></td>
            <td width="118" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="87" valign="top" height="10"></td>
            <td width="151" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
          </tr>
          <tr>
            <td width="39" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">Name:</font></td>
            <td width="361" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b><?php echo id_to_enname($data[1]); ?></b></font></td>
            <td width="90" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　Student NO.:</font></td>
            <td width="152" height="13" valign="bottom">
			  <font size="2" face="Times New Roman"><b><?php echo $data[1]; ?></b></font></td>
            <td width="57" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　Sex:</font></td>
            <td width="118" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b>
			    <?php if ($data[5]=="男"){echo "Male";}else if($data[5]=="女"){ echo "Female";} ?></b></font></td>
            <td width="87" height="13" valign="bottom">
			  <font size="1" face="Times New Roman">　　Date of Birth:</font></td>
            <td width="151" height="13" valign="bottom">
			  <font style="font-size:10pt" face="Times New Roman"><b><?php echo trans_month($data[9],5,2)." ".$data[10].", ".$data[8]; ?></b></font></td>
          </tr>
          <tr>
            <td width="39" valign="top" height="10"></td>
            <td width="361" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="90" valign="top" height="10"></td>
            <td width="152" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="57" valign="top" height="10"></td>
            <td width="118" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
            <td width="87" valign="top" height="10"></td>
            <td width="151" valign="top" height="10">
              <hr noshade color="#000000" size="1">
            </td>
          </tr>
        </table>
        </center>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="1060" id="AutoNumber2">
                        <tr>
                            <td width="11%" align="center">
                                <table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none" width="100%" height="176">
                                    <?php
        if($old==1){
            $title=array("研習期間","教　　材","聽力","發音","說話","閱讀","寫字","作文");
            $title_en=array("Period of Study","Course Material(s)","Comprehension","Pronunciation","Conversation","Reading","Writing","Composition");
        }
        else{
            $title=array("研習期間","教　　材","聽力","說話","閱讀","寫字","作文","學期測驗");
            $title_en=array("Period of Study","Course Material(s)","Comprehension","Conversation","Reading","Writing","Composition","Final Exam.");
        }
                                    ?>
                                    <tr>
                                        <td style="width: 10%; height: 19; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[0];echo "/".$old;?></span><br>
                                                <span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title_en[0];?></span>
                                            </p>
                                        </td>
                                        <td style="width: 48%; height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 10.0pt; font-family: 華康特粗楷體"><?php echo $title[1];?></span><br>
                                                <span style="font-size: 10.0pt; font-family: 華康特粗楷體; color: black"><?php echo $title_en[1];?></span>
                                            </p>
                                        </td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[2];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><br>
                                                    <?php echo $title_en[2];?></span>
                                            </p>
                                        </td>
                                        <?php //if($old==1){ ?>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[3];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><br>
                                                    <?php echo $title_en[3];?></span>
                                            </p>
                                        </td>
                                        <?php //} ?>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[4];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><br>
                                                    <?php echo $title_en[4];?></span>
                                            </p>
                                        </td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[5];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><br>
                                                    <?php echo $title_en[5];?></span>
                                            </p>
                                        </td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[6];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體"><br>
                                                    <?php echo $title_en[6];?></span>
                                            </p>
                                        </td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <p class="MsoNormal" align="center" style="text-align: center">
                                                <span style="font-size: 8.0pt; font-family: 華康特粗楷體"><?php echo $title[7];?></span><span lang="EN-US" style="font-size: 8.0pt; font-family: 華康特粗楷體; color: black"><br>
                                                    <?php echo $title_en[7];?></span>
                                            </p>
                                        </td>
                                    </tr>
                                    <?php
                                                          /////////////////////////////////////////////end print_page///////////////////////////////////////////////////////////////////////////////////////////////// 
    } ?>



                                    <?php
                                    echo page_head($data,$old);
                                    if($old==1){
                                        $str_score = "select * from score_old where stu_no='$data[1]' order by 'start_date' asc";
                                    }else{
                                        $str_score = "select * from score where stu_no='$data[1]' order by 'start_date' asc";
                                    }
                                    $rt_score = mysql_query($str_score);
                                    for ($i=0;$i<=10;$i++){
                                        for($j=1;$j<50;$j++){
                                            $input_data[$i][$j]="";
                                            $score_count[$i][$j]=0;
                                        }
                                    }
                                    $count=0;
                                    while ($data_score = mysql_fetch_array($rt_score)) {
                                        $count=$count+1;
                                        $input_id[$count]=$count;
                                        $input_data[0][$count]=$data_score[15];/*start_date*/
                                        $input_data[1][$count]=$data_score[19];/*end_date*/
                                        $input_data[2][$count]=$data_score[4];/*teacher*/
                                        $input_data[3][$count]=$data_score[5];/*book_no*/
                                        $input_data[4][$count]=$data_score[23];/*book_no2*/
                                        $input_data[5][$count]=$data_score[8];/*compre*/	
                                        if ($data_score[8]=="*NA"){$score_count[5][$count]=0;}else{$score_count[5][$count]=1;}
                                        $input_data[6][$count]=$data_score[9];/*conver*/
                                        if ($data_score[9]=="*NA"){$score_count[6][$count]=0;}else{$score_count[6][$count]=1;}
                                        $input_data[7][$count]=$data_score[10];/*read2*/
                                        if ($data_score[10]=="*NA"){$score_count[7][$count]=0;}else{$score_count[7][$count]=1;}
                                        $input_data[8][$count]=$data_score[11];/*write2*/
                                        if ($data_score[11]=="*NA"){$score_count[8][$count]=0;}else{$score_count[8][$count]=1;}
                                        $input_data[9][$count]=$data_score[12];/*compo*/
                                        if ($data_score[12]=="*NA"){$score_count[9][$count]=0;}else{$score_count[9][$count]=1;}
                                        $input_data[10][$count]=$data_score[13];/*final*/
                                        if ($data_score[13]=="*NA"){$score_count[10][$count]=0;}else{$score_count[10][$count]=1;}
                                        if ($input_data[5][$count]=="" && $input_data[6][$count]=="" && $input_data[7][$count]=="" && $input_data[8][$count]=="" && $input_data[9][$count]=="" && $input_data[10][$count]==""){
                                            /*$input_data[0][$count]=="";$input_data[1][$count]=="";$input_data[2][$count]=="";$input_data[3][$count]=="";$input_data[4][$count]=="";
                                            $score_count[5][$count]="";$score_count[6][$count]="";$score_count[7][$count]="";$score_count[8][$count]="";$score_count[9][$count]="";$score_count[10][$count]="";*/
                                            $count=$count-1;
                                        }
                                    }
                                    $temp=$count;
                                    for ($i=1;$i<$temp;$i++){
                                        for ($j=$i+1;$j<=$temp;$j++){
                                            if ($input_data[1][$input_id[$j]]>$input_data[1][$input_id[$i]] && $input_data[0][$input_id[$j]]==$input_data[0][$input_id[$i]]){
                                                $temp_id=$input_id[$i];
                                                $input_id[$i]=$input_id[$j];
                                                $input_id[$j]=$temp_id;
                                                //echo "term $i $j<br>";
                                            }
                                        }
                                    }
                                    for ($i=1;$i<$temp;$i++){
                                        for ($j=$i+1;$j<=$temp;$j++){
                                            if ($input_data[3][$input_id[$j]]<$input_data[3][$input_id[$i]] && $input_data[0][$input_id[$j]]==$input_data[0][$input_id[$i]] && $input_data[1][$input_id[$j]]==$input_data[1][$input_id[$i]]){
                                                $temp_id=$input_id[$i];
                                                $input_id[$i]=$input_id[$j];
                                                $input_id[$j]=$temp_id;
                                                //echo "book $i $j<br>";
                                            }
                                        }
                                    }
                                    for ($i=1;$i<=$temp;$i++){
                                        $t_input_data[0][$i]=$input_data[0][$input_id[$i]];
                                        $t_input_data[1][$i]=$input_data[1][$input_id[$i]];
                                        $t_input_data[2][$i]=$input_data[2][$input_id[$i]];
                                        $t_input_data[3][$i]=$input_data[3][$input_id[$i]];
                                        $t_input_data[4][$i]=$input_data[4][$input_id[$i]];
                                        $t_input_data[5][$i]=$input_data[5][$input_id[$i]]; $t_score_count[5][$i]=$score_count[5][$input_id[$i]];
                                        $t_input_data[6][$i]=$input_data[6][$input_id[$i]]; $t_score_count[6][$i]=$score_count[6][$input_id[$i]];
                                        $t_input_data[7][$i]=$input_data[7][$input_id[$i]]; $t_score_count[7][$i]=$score_count[7][$input_id[$i]];
                                        $t_input_data[8][$i]=$input_data[8][$input_id[$i]]; $t_score_count[8][$i]=$score_count[8][$input_id[$i]];
                                        $t_input_data[9][$i]=$input_data[9][$input_id[$i]]; $t_score_count[9][$i]=$score_count[9][$input_id[$i]];
                                        $t_input_data[10][$i]=$input_data[10][$input_id[$i]]; $t_score_count[10][$i]=$score_count[10][$input_id[$i]];
                                    }
                                    for ($i=1;$i<=$temp;$i++){
                                        $input_data[0][$i]=$t_input_data[0][$i];
                                        $input_data[1][$i]=$t_input_data[1][$i];
                                        $input_data[2][$i]=$t_input_data[2][$i];
                                        $input_data[3][$i]=$t_input_data[3][$i];
                                        $input_data[4][$i]=$t_input_data[4][$i];
                                        $input_data[5][$i]=$t_input_data[5][$i]; $score_count[5][$i]=$t_score_count[5][$i];
                                        $input_data[6][$i]=$t_input_data[6][$i]; $score_count[6][$i]=$t_score_count[6][$i];
                                        $input_data[7][$i]=$t_input_data[7][$i]; $score_count[7][$i]=$t_score_count[7][$i];
                                        $input_data[8][$i]=$t_input_data[8][$i]; $score_count[8][$i]=$t_score_count[8][$i];
                                        $input_data[9][$i]=$t_input_data[9][$i]; $score_count[9][$i]=$t_score_count[9][$i];
                                        $input_data[10][$i]=$t_input_data[10][$i]; $score_count[10][$i]=$t_score_count[10][$i];
                                    }
                                    $i=0;
                                    $temp=1;
                                    while ($i<$count){
                                        $i=$i+1;
                                        $temp_chang=0;
                                        //echo "asdf";
                                        if ($input_data[0][$i]==$input_data[0][$i+1]
                                         && $input_data[1][$i]==$input_data[1][$i+1]){
                                            //echo "zxcv";
                                            // echo "book1:".$input_data[3][$i]." book2:".$input_data[3][$i+1];
                                            if( $input_data[3][$i]==$input_data[3][$i+1]){
                                                //echo "qwer<br>";
                                                /*$input_data[5][$input_id[$i]]=$input_data[5][$input_id[$i]]+$input_data[5][$input_id[$i+1]];	$score_count[5][$input_id[$i]]=$score_count[5][$input_id[$i]]+$score_count[5][$input_id[$i+1]];
                                                $input_data[6][$i]=$input_data[6][$i]+$input_data[6][$i+1];	$score_count[6][$i]=$score_count[6][$input_id[$i]]+$score_count[6][$input_id[$i+1]];
                                                $input_data[7][$i]=$input_data[7][$i]+$input_data[7][$i+1];	$score_count[7][$input_id[$i]]=$score_count[7][$input_id[$i]]+$score_count[7][$input_id[$i+1]];
                                                $input_data[8][$i]=$input_data[8][$i]+$input_data[8][$i+1];	$score_count[8][$input_id[$i]]=$score_count[8][$input_id[$i]]+$score_count[8][$input_id[$i+1]];
                                                $input_data[9][$i]=$input_data[9][$i]+$input_data[9][$i+1];	$score_count[9][$input_id[$i]]=$score_count[9][$input_id[$i]]+$score_count[9][$input_id[$i+1]];
                                                $input_data[10][$i]=$input_data[10][$i]+$input_data[10][$i+1];	$score_count[10][$input_id[$i]]=$score_count[10][$input_id[$i]]+$score_count[10][$input_id[$i+1]];*/
                                                $input_data[5][$i]=score_sum($input_data[5][$i],$input_data[5][$i+1]);	$score_count[5][$i]=$score_count[5][$i]+$score_count[5][$i+1];
                                                $input_data[6][$i]=score_sum($input_data[6][$i],$input_data[6][$i+1]);	$score_count[6][$i]=$score_count[6][$i]+$score_count[6][$i+1];
                                                $input_data[7][$i]=score_sum($input_data[7][$i],$input_data[7][$i+1]);	$score_count[7][$i]=$score_count[7][$i]+$score_count[7][$i+1];
                                                $input_data[8][$i]=score_sum($input_data[8][$i],$input_data[8][$i+1]);	$score_count[8][$i]=$score_count[8][$i]+$score_count[8][$i+1];
                                                $input_data[9][$i]=score_sum($input_data[9][$i],$input_data[9][$i+1]);	$score_count[9][$i]=$score_count[9][$i]+$score_count[9][$i+1];
                                                $input_data[10][$i]=score_sum($input_data[10][$i],$input_data[10][$i+1]);	$score_count[10][$i]=$score_count[10][$i]+$score_count[10][$i+1];
                                                $temp_chang=1;
                                            }
                                        }//if ($input_data[0][$i]==$input_data[0][$i+1] && $input_data[1][$i]==$input_data[1][$i+1] && $input_data[2][$i]==$input_data[2][$i+1] && $input_data[3][$i]==$input_data[3][$i+1])
                                        if ($temp_chang==1){
                                            $j=$i;
                                            while ($j<$count){
                                                $j++;
                                                $input_data[0][$j]=$input_data[0][$j+1];/*start_date*/
                                                $input_data[1][$j]=$input_data[1][$j+1];/*end_date*/
                                                $input_data[2][$j]=$input_data[2][$j+1];/*teacher*/
                                                $input_data[3][$j]=$input_data[3][$j+1];/*book_no*/
                                                $input_data[4][$j]=$input_data[4][$j+1];/*book_no2*/
                                                $input_data[5][$j]=$input_data[5][$j+1];/*compre*/	$score_count[5][$j]=$score_count[5][$j+1];
                                                $input_data[6][$j]=$input_data[6][$j+1];/*conver*/	$score_count[6][$j]=$score_count[6][$j+1];
                                                $input_data[7][$j]=$input_data[7][$j+1];/*read2*/	$score_count[7][$j]=$score_count[7][$j+1];
                                                $input_data[8][$j]=$input_data[8][$j+1];/*write2*/	$score_count[8][$j]=$score_count[8][$j+1];
                                                $input_data[9][$j]=$input_data[9][$j+1];/*compo*/	$score_count[9][$j]=$score_count[9][$j+1];
                                                $input_data[10][$j]=$input_data[10][$j+1];/*final*/	$score_count[10][$j]=$score_count[10][$j+1];
                                            }//while ($j<$count)
                                            for ($k=0;$k<=10;$k++){	$input_data[$k][$count]="";$score_count[$k][$count]="";}
                                            $count=$count-1;
                                            $i=$i-1;
                                        }//if ($temp_chang==1)
                                    }//while ($i<=$count) 結合重複的
                                    $i=1;
                                    $total_count=0;
                                    $total_pages=1;
                                    while ($i<=$count){
                                        if($input_data[3][$i]<>""){$total_count++;}
                                        if($input_data[4][$i]<>""){$total_count++;}
                                        if($total_count >= 17 && $i+1 <= $count) { // 總行數超過17行，且仍有下一筆資料換頁
                                            $total_pages++;
                                            $total_count=0;
                                        }
                                        $i++;
                                    }//while ($i<=$count) 計算總列數
                                    $i=1;
                                    $list_count=0; // 行數計算(有的一欄有兩行)
                                    // $count 資料總欄數
                                    $pages = 1;
                                    while ($i<=$count){
                                        table_list($input_data,$score_count,$i);
                                        if($input_data[3][$i]<>""){$list_count++;}
                                        if($input_data[4][$i]<>""){$list_count++;}
                                        /*if($list_count%17==0 || $list_count%18==0) {*/
                                        if($list_count >= 17 && $i+1 <= $count) { // 總行數超過17行，且仍有下一筆資料換頁
                                            $list_count=0;
                                            echo page_tail($pages,$total_pages);
                                            $pages++;
                                            echo "</div>";
                                            page_break();
                                            echo page_head($data,$old);
                                        }
                                        $i++;
                                    }//while ($i<=$count) 列出成績
                                    if ($list_count%17 <> 0){
                                        for($i=$list_count%17+1 ; $i<=17 ; $i++){table_null();}	
                                    }//if ($list_count%18<>0)
                                    echo page_tail($pages,$total_pages);
                                    
                                    

                                    ?>

                                    <?php
                                    function table_list($input_data,$score_count,$i){?>

                                    <tr>
                                        <td style="width: 10%; height: 19; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <span style="font-size: 7pt">
                                                <?php echo substr($input_data[0][$i],0,4)."/".substr($input_data[0][$i],5,2)."/".substr($input_data[0][$i],8,2)."-".substr($input_data[1][$i],0,4)."/".substr($input_data[1][$i],5,2)."/".substr($input_data[1][$i],8,2);?>
                                            </span></td>
                                        <td style="width: 48%; height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <span style="font-size: 7pt">
                                                <?php echo book_no_to_name($input_data[3][$i]); if($input_data[4][$i]<>""){echo "<br>".book_no_to_name($input_data[4][$i]);}?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[5][$i],$score_count[5][$i]);?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[6][$i],$score_count[6][$i]);?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[7][$i],$score_count[7][$i]);?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[8][$i],$score_count[8][$i]);?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[9][$i],$score_count[9][$i]);?>
                                            </span></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center">
                                            <?php echo score_trans($input_data[10][$i],$score_count[10][$i]);?>
                                            </span></td>
                                    </tr>
                                    <?php	}/*function table_list*/ ?>

                                    <?php
                                    function table_null(){ ?>
                                    <tr>
                                        <td style="width: 10%; height: 19; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td style="width: 48%; height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                        <td width="7%" style="height: 19; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 1.4pt; padding-right: 1.4pt; padding-top: 0cm; padding-bottom: 0cm" align="center"></td>
                                    </tr>
                                    <?php }/*function table_null*/?>

                                    <?php
                                    function page_tail($pages,$pages2){ ?>
                                </table>

                                <p class="MsoNormal">
                                    <b><span lang="EN-US" style="font-size: 6.0pt">Remarks</span><span style="font-size: 6.0pt; font-family: 新細明體">：</span><span lang="EN-US" style="font-size: 6.0pt">100 
      is the perfect score&nbsp; 100-95=A+&nbsp; 94-90=A&nbsp; 89-85=A-&nbsp; 84-80=B+&nbsp; 79-75=B&nbsp; 
      74-70=B-&nbsp; 69-65=C+&nbsp; 64-60=C&nbsp; 59 and below =F&nbsp; 60 is the passing grade</span><span style="font-size: 6.0pt; font-family: 新細明體">。</span><span lang="EN-US" style="font-size: 6.0pt">&nbsp;
      </span><span style="font-size: 7.0pt; font-family: 新細明體">＊</span><span lang="EN-US" style="font-size: 7.0pt">NT</span><span style="font-size: 7.0pt; font-family: 新細明體">：</span><span lang="EN-US" style="font-size: 7.0pt">Not 
      Taken (</span><span style="font-size: 7.0pt; font-family: 新細明體">缺考</span><span lang="EN-US" style="font-size: 7.0pt">)&nbsp;&nbsp;
      </span><span style="font-size: 7.0pt; font-family: 新細明體">＊</span><span lang="EN-US" style="font-size: 7.0pt">NA</span><span style="font-size: 7.0pt; font-family: 新細明體">：</span><span lang="EN-US" style="font-size: 7.0pt">Not 
      Applicable (</span><span style="font-size: 7.0pt; font-family: 新細明體">不須要</span><span lang="EN-US" style="font-size: 7.0pt">)&nbsp;
      </span><span style="font-size: 7.0pt; font-family: 新細明體">＊</span><span lang="EN-US" style="font-size: 7.0pt">D</span><span style="font-size: 7.0pt; font-family: 新細明體">：</span><span lang="EN-US" style="font-size: 7.0pt">Dropped(</span><span style="font-size: 7.0pt; font-family: 新細明體">不予計分</span><span lang="EN-US" style="font-size: 7.0pt">)</span></b>
                                </p>

                                <div align="center">
                                    <center>
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="1060" id="AutoNumber4">
          <tr>
            <td width="265"><span lang="EN-US" style="font-size: 8.0pt">Page <?php echo $pages;?>&nbsp; 
            OF&nbsp;<?php echo $pages2;?></span></td>
            <td width="461">
            　</td>
            <td width="289">　</td>
            <td width="45">　</td>
          </tr>
          <tr>
            <td width="265">
              <font face="Times New Roman" style="font-size: 6pt">　</font>
            </td>
            <td width="461">　</td>
            <td width="289">　</td>
            <td width="45">　</td>
          </tr>
          <tr>
            <td width="265">
          　</td>
            <td width="461">
            <p align="right"><span lang="EN-US" style="font-size: 10.0pt; font-family: GungsuhChe"><b>Signature</b></span></td>
            <td width="289">　</td>
            <td width="45">　</td>
          </tr>
          <tr>
            <td width="265">
      <span lang="EN-US" style="font-size: 6.0pt; font-family: Times New Roman">
      Issued on :&nbsp;<?php echo Date("Y/m/d");?></span></td>
            <td width="461">　</td>
            <td width="289"><hr noshade color="#000000" size="1"></td>
            <td width="45">　</td>
          </tr>
          <tr>
            <td width="265">　</td>
            <td width="461">　</td>
            <td width="289">
            <p align="center"><b><span lang="EN-US" style="font-size: 10.0pt; font-family: GungsuhChe">Director</span></b></td>
            <td width="45">　</td>
          </tr>
        </table>
        </center>
                                </div>
                            </td>
                        </tr>
                    </table>
        </table>
    </div>
    <?php }?>
</body>

</html>
<?php
function score_sum($n,$m){
	$temp_n=substr($n,0,1);
	$temp_m=substr($m,0,1);
	if ($temp_n=="*" || $temp_m=="*"){
		//＊NA：Not Applicable (不須要)
		if ($n=="*NA" && $m=="*NA"){return "*NA";}
		else if ($n=="*NA"){return $m;}
		else if ($m=="*NA"){return $n;}
		//＊NT：Not Taken (缺考)
		else if ($n=="*NT" && $m=="*NT"){;return "*NT";}
		else if ($n=="*NT"){return $m;}
		else if ($m=="*NT"){return $n;}
		//＊NT：Not Taken (缺考)
		else if ($n=="*NT" && $m=="*NT"){return "*NT";}
		else if ($n=="*NT"){return $m;}
		else if ($m=="*NT"){return $n;}
		//＊D：Dropped(不予計分)
		else if ($n=="*D" && $m=="*D"){;return "*D";}
		else if ($n=="*D"){return $m;}
		else if ($m=="*D"){return $n;}
	}
	//空白 未輸入
	else if ($n=="" && $m>=0){return $m;}
	else if ($m=="" && $n>=0){return $n;}
	else if ($n=="" && $m==""){return "NULL";}
	else if ($n>=0 && $m>=0){return $n+$m;}
}
function book_no_to_name($id){
	$sql="select * from book where book_no='$id'";
	$result = mysql_query($sql);
	$i=0;
	while($data = mysql_fetch_array($result)){
		if ($data[1]==$id){return $data[2]." ".$data[3];}
		$i++;
	}
	if($i==0){return "<font color=#FF0000>資料庫中無教材代碼為".$id."的書</font>";}
}
function score_trans($score,$count){
	if (empty($score)){return "Null";}
	if (substr($score,0,1)=="*"){return $score;}
	else{
		$temp=0;
		$temp=($score*100)/$count;
		if (($temp%100) >= 45){ $temp=ceil($temp/100); }
		else{$temp=round(($temp/100));}

        //轉換成績
		$sql="select * from score_trans";
		$result = mysql_query($sql);
		$i=0;
		while($data = mysql_fetch_array($result)) {
			if ($temp <= $data[1] && $temp >= $data[2]){$i++; return $data[3];}
		}
		//if($i==0){return "<font color=#FF0000>".$temp."</font>";}
		if($i==0){return "<font color=#FF0000>成績格式不合</font>";}
	}
}
?>
