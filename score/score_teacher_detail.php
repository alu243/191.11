<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<?php
function stu_name($id)
{
	$sql1 = "select * from student where stu_no = '$id'";
	$result1 = mysql_query($sql1);
	$data1 = mysql_fetch_array($result1);
	echo $data1[1]."/".$data1[2];
}
function teacher_name($id)
{
	$sql1 = "select * from member where center_no = '$id'";
	$result1 = mysql_query($sql1);
	$data1 = mysql_fetch_array($result1);
	return $data1[3];
}
function book_name($id)
{
	if ($id=="") {return "***";}
	$sql = "select * from book where book_no = '$id'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	return $data[2];
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p>您現在所在位置：<font color="#FF9900">成績處理﹝依教師﹞-輸入資料</font></p>
    <p>目前作業期別：<?php echo $term;?></p>
    <p>
        目前選擇教師：<?php echo $teacher."/".teacher_name($teacher);?>
    </p>
    <form method="POST" action="score_teacher_list.php">
        <input type="hidden" name="term" value="<?php echo $term; ?>">
        <input type="hidden" name="teacher" value="<?php echo $teacher; ?>">
        <input type="hidden" name="update" value="1">

        <center>
<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#008000" width="929">

  <tr>
    <td width="152" bgcolor="#E6FFEB" align="center">學號/姓名</td>
    <td width="89" bgcolor="#E6FFEB" align="center">課程名稱</td>
    <td width="150" bgcolor="#E6FFEB" align="center">教材</td>
    <td width="150" bgcolor="#E6FFEB" align="center">教材2</td>
    <td width="49" bgcolor="#E6FFEB" align="center">應上課<br>時數</td>
    <td width="49" bgcolor="#E6FFEB" align="center">請假<br>時數</td>
    <td width="44" bgcolor="#E6FFEB" align="center">聽力</td>
    <td width="44" bgcolor="#E6FFEB" align="center">說話</td>
    <td width="45" bgcolor="#E6FFEB" align="center">閱讀</td>
    <td width="45" bgcolor="#E6FFEB" align="center">寫字</td>
    <td width="45" bgcolor="#E6FFEB" align="center">作文</td>
    <td width="67" bgcolor="#E6FFEB" align="center">學期測驗</td>
  </tr>
<?php
$i=0;
$teacher_no=$teacher;
$teacher=addslashes(teacher_name($teacher));
$sql1 = "select * from score where term='$term' && teacher= '$teacher'";
$result1 = mysql_query($sql1);
while($data = mysql_fetch_array($result1)) {
?>  
<input type="hidden" name="no<?php echo $i;?>" value="<?php echo $data[0]; ?>">
  <tr>
    <td width="152" align="center"><?php stu_name($data[2]);?></td>
    <td width="89" align="center"><?php echo $data[3];?></td>
    <td width="150" align="left"><font size="2"><?php echo book_name($data[5]);?></font>
      <select size="1" name="book_no<?php echo $i;?>" style="font-size:12pt">
         <option value="">選擇教材</option>
           <?php
    $book_data = listbook();
    for ($j=0 ; $j < count($book_data) ; $j++) {
        if ($book_data[$j]["book_no"] == $data[5]) {
           ?>
        <option value="<?php echo $book_data[$j]["book_no"]; ?>" selected><?php echo $book_data[$j]["name_ch"]; ?> </option>
        <?php
        }
        else {
        ?>
        <option value="<?php echo $book_data[$j]["book_no"]; ?>"> <?php echo $book_data[$j]["name_ch"]; ?> </option>
        <?php
        }
    }
        ?>
      </select>
    </td>
    <td width="150" align="center"><?php echo book_name($data[23]);?>
      <select size="1" name="book2_no<?php echo $i;?>" style="font-size:12pt">
         <option value="">選擇教材</option>
           <?php
    for ($j=0 ; $j < count($book_data) ; $j++) {
        if ($book_data[$j]["book_no"] == $data[23]) {
           ?>
        <option value="<?php echo $book_data[$j]["book_no"]; ?>" selected><?php echo $book_data[$j]["name_ch"]; ?> </option>
        <?php
        }
        else {
        ?>
        <option value="<?php echo $book_data[$j]["book_no"]; ?>"> <?php echo $book_data[$j]["name_ch"]; ?> </option>
        <?php
        }
    }
        ?>
      </select>
    </td>
    <td width="49" align="center"><input type="text" name="hours_full<?php echo $i;?>" size="3" value = "<?php echo $data[6]?>"></td>
    <td width="49" align="center"><input type="text" name="hours_absence<?php echo $i;?>" size="3" value = "<?php echo $data[7]?>"></td>
    <td width="44" align="center"><input type="text" name="compre<?php echo $i;?>" size="3" value = "<?php echo $data[8]?>"></td>
    <td width="44" align="center"><input type="text" name="conver<?php echo $i;?>" size="3" value = "<?php echo $data[9]?>"></td>
    <td width="45" align="center"><input type="text" name="read2<?php echo $i;?>" size="3" value = "<?php echo $data[10]?>"></td>
    <td width="45" align="center"><input type="text" name="write2<?php echo $i;?>" size="3" value = "<?php echo $data[11]?>"></td>
    <td width="45" align="center"><input type="text" name="compo<?php echo $i;?>" size="3" value = "<?php echo $data[12]?>"></td>
    <td width="67" align="center"><input type="text" name="final<?php echo $i;?>" size="3" value = "<?php echo $data[13]?>"></td>
  </tr>
<?php 
    $i++;
} 
?>

</table>
</center>


        <p align="center">
            <input type="submit" value="確定送出"></p>
    </form>



    <p align="center"></p>



    <p align="center"><a href="score_teacher_list.php?term=<?php echo $term;?>">回上一頁</a></p>

    <p align="center">
        click確定送出→將所有的資料存入score中<br>
        {}中為抓取的資料
    </p>

</body>

</html>
