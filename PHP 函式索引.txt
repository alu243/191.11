PHP 函式索引 967 個函式
共有 967 個函式

Abs: 取得絕對值。
Acos: 取得反餘弦值。
ada_afetch: 取得資料庫的傳回列。
ada_autocommit: 開關自動更動功能。
ada_close: 關閉 Adabas D 連結。
ada_commit: 更動 Adabas D 資料庫。
ada_connect: 連結至 Adabas D 資料庫。
ada_exec: 執行 SQL 指令。
ada_fetchrow: 取得傳回一列。
ada_fieldname: 取得欄位名稱。
ada_fieldtype: 取得欄位資料形態。
ada_freeresult: 釋出傳回資料的記憶體。
ada_numfields: 取得欄位數目。
ada_numrows: 取得傳回列數目。
ada_result: 取得傳回資料。
ada_resultall: 傳回 HTML 表格資料。
ada_rollback: 撤消當前交易。
AddSlashes: 字串加入斜線。
apache_lookup_uri: 獲得所有的 URI 相關資訊。
apache_note: 獲得及設定阿帕契伺服器的請求紀錄。
array: 建立一個新的陣列。
array_walk: 讓使用者自訂函數能處理陣列中的每一個元素。
arsort: 將陣列的值由大到小排序。
Asin: 取得反正弦值。
asort: 將陣列的值由小到大排序。
aspell_check: 檢查一個單字。
aspell_check-raw: 檢查一個單字，即使拼錯也不改變或修正。
aspell_new: 載入一個新的字典。
aspell_suggest: 檢查一個單字，並提供拼字建議。
Atan: 取得反正切值。
Atan2: 計算二數的反正切值。
base64_decode: 將 BASE64 編碼字串解碼。
base64_encode: 將字串以 BASE64 編碼。
basename: 傳回不含路徑的檔案字串。
base_convert: 轉換數字的進位方式。
bcadd: 將二個高精確度數字相加。
bccomp: 比較二個高精確度數字。
bcdiv: 將二個高精確度數字相除。
bcmod: 取得高精確度數字的餘數。
bcmul: 將二個高精確度數字相乘。
bcpow: 求一高精確度數字次方值。
bcscale: 設定程式中所有 BC 函式庫的內定小數點位數。
bcsqrt: 求一高精確度數字的平方根。
bcsub: 將二個高精確度數字相減。
bin2hex: 二進位轉成十六進位。
BinDec: 二進位轉成十進位。
Ceil: 計算大於指定數的最小整數。
chdir: 改變目錄。
checkdate: 驗證日期的正確性。
checkdnsrr: 檢查指定網址的 DNS 記錄。
chgrp: 改變檔案所屬的群組。
chmod: 改變檔案的屬性。
Chop: 去除連續空白。
chown: 改變檔案的擁有者。
Chr: 傳回序數值的字元。
chunk_split: 將字串分成小段。
clearstatcache: 清除檔案狀態快取。
closedir: 關閉目錄 handle。
closelog: 關閉系統紀錄。
connection_aborted: 若連結中斷則傳回 true。
connection_status: 取得連線狀態。
connection_timeout: 若超過 PHP 程式執行時間則傳回 true。
convert_cyr_string: 轉換古斯拉夫字串成其它字串。
copy: 複製檔案。
Cos: 餘弦計算。
count: 計算變數或陣列中的元素個數。
crypt: 將字串用 DES 編碼加密。
current: 傳回陣列中目前的元素。
date: 將伺服器的時間格式化。
dbase_add_record: 加入資料到 dBase 資料表。
dbase_close: 關閉 dBase 資料表。
dbase_create: 建立 dBase 資料表。
dbase_delete_record: 刪除 dBase 資料表的資料。
dbase_get_record: 取得 dBase 資料表的資料。
dbase_numfields: 取得 dBase 資料表的欄位數。
dbase_numrecords: 取得 dBase 資料表的列數。
dbase_open: 開啟 dBase 資料表。
dbase_pack: 清理 dBase 資料表。
dba_close: 關閉資料庫。
dba_delete: 刪除指定資料。
dba_exists: 檢查鍵是否存在。
dba_fetch: 取回指定資料。
dba_firstkey: 取回首筆鍵值。
dba_insert: 加入資料。
dba_nextkey: 取回下筆鍵值。
dba_open: 開啟資料庫連結。
dba_optimize: 最佳化資料庫。
dba_popen: 開啟並保持資料庫連結。
dba_replace: 更動或加入資料。
dba_sync: 資料庫同步化。
dblist: 取得 DBM 的資訊。
dbmclose: 關閉 DBM 資料庫。
dbmdelete: 刪除指定資料。
dbmexists: 檢查鍵是否存在。
dbmfetch: 取回指定資料。
dbmfirstkey: 取回首筆鍵名。
dbminsert: 加入資料。
dbmnextkey: 取回下筆鍵值。
dbmopen: 開啟 DBM 資料庫連結。
dbmreplace: 更動或加入資料。
debugger_off: 關閉內建的 PHP 除錯器。
debugger_on: 使用內建的 PHP 除錯器。
DecBin: 十進位轉二進位。
DecHex: 十進位轉十六進位。
DecOct: 十進位轉八進位。
delete: 無用的項目。
die: 輸出訊息並中斷 PHP 程式。
dir: 目錄類別物件。
dirname: 取得路徑中的目錄名。
diskfreespace: 取得目錄所在的賸餘可用空間。
dl: 載入 PHP 擴充模組。
doubleval: 變數轉成倍浮點數型態。
each: 傳回陣列中下一個元素的索引及值。
easter_date: 計算復活節日期。
easter_days: 計算復活節與三月廿一日之間日期數。
echo: 輸出字串。
empty: 判斷變數是否已設定。
end: 將陣列的內部指標指到最後的元素。
ereg: 字串比對剖析。
eregi: 字串比對剖析，與大小寫無關。
eregi_replace: 字串比對剖析並取代，與大小寫無關。
ereg_replace: 字串比對剖析並取代。
error_log: 送出一個錯誤訊息。
error_reporting: 設定錯誤訊息回報的等級。
escapeshellcmd: 除去字串中的特殊符號。
eval: 將值代入字串之中。
exec: 執行外部程式。
exit: 結束 PHP 程式。
Exp: 自然對數 e 的次方值。
explode: 切開字串。
extract: 匯入陣列到符號表。
fclose: 關閉已開啟的檔案。
FDF_close: 關閉 FDF 文件。
FDF_create: 建立新的 FDF 文件。
FDF_get_file: 取得 /F 鍵的值。
FDF_get_status: 取得 /STATUS 鍵的值。
FDF_get_value: 取得欄位的值。
FDF_next_field_name: 下一欄位的名稱。
FDF_open: 打開 FDF 文件。
FDF_save: 將 FDF 文件存檔。
FDF_set_ap: 設定顯示欄位。
FDF_set_file: 設定 /F 鍵。
FDF_set_status: 設定 /STATUS 鍵。
FDF_set_value: 設定欄位的值。
feof: 測試檔案指標是否指到檔尾。
fgetc: 取得檔案指標所指的字元。
fgetcsv: 取得檔案指標所指行，並剖析 CSV 欄位。
fgets: 取得檔案指標所指的行。
fgetss: 取得檔案指標所指的行，並去掉 HTML 語言標記。
file: 將檔案全部讀入陣列變數中。
fileatime: 取得檔案最後的存取時間。
filectime: 取得檔案最後的改變時間。
filegroup: 取得檔案所屬的群組。
fileinode: 取得檔案的 inode 值。
filemtime: 取得檔案最後的修改時間。
fileowner: 取得檔案的擁有者。
fileperms: 取得檔案的權限設定。
filepro: 讀取 FilePro Map 檔。
filepro_fieldcount: 取得欄位數目。
filepro_fieldname: 取得欄位名稱。
filepro_fieldtype: 取得欄位型態。
filepro_fieldwidth: 取得欄位寬度。
filepro_retrieve: 取得指定儲存格資料。
filepro_rowcount: 取得列數目。
filesize: 獲得檔案的大小。
filetype: 獲得檔案的型態。
file_exists: 檢查檔案是否存在。
flock: 鎖住檔案。
Floor: 計算小於指定數的最大整數。
flush: 清出輸出緩衝區。
fopen: 開啟檔案或者 URL。
fpassthru: 輸出所有賸餘資料。
fputs: 寫到檔案指標。
fread: 位元組的方式讀取檔案。
FrenchToJD: 將法國共和曆法轉換成為凱撒日計數。
fseek: 移動檔案指標。
fsockopen: 打開網路的 Socket 連結。
ftell: 取得檔案讀寫指標位置。
ftp_cdup: 回上層目錄。
ftp_chdir: 改變路徑。
ftp_connect: 開啟 FTP 連結。
ftp_delete: 將檔案刪除。
ftp_fget: 下載檔案，並存在已開的檔中。
ftp_fput: 上傳已開啟檔案。
ftp_get: 下載檔案。
ftp_login: 登入 FTP 伺服器。
ftp_mdtm: 獲得指定檔案的最後修改時間。
ftp_mkdir: 建新目錄。
ftp_nlist: 列出指定目錄中所有檔案。
ftp_pasv: 切換主被動傳輸模式。
ftp_put: 上傳檔案。
ftp_pwd: 取得目前所在路徑。
ftp_quit: 關閉 FTP 連線。
ftp_rawlist: 詳細列出指定目錄中所有檔案。
ftp_rename: 將檔案改名。
ftp_rmdir: 刪除目錄。
ftp_size: 獲得指定檔案的大小。
ftp_systype: 顯示伺服器系統。
function_exists: 查核函式是否已定義。
fwrite: 二進位位元方式寫入檔案。
getallheaders: 獲得所有 HTTP 變數值。
getdate: 獲得時間及日期資訊。
getenv: 取得系統的環境變數
gethostbyaddr: 傳回機器名稱。
gethostbyname: 傳回 IP 網址。
gethostbynamel: 傳回機器名稱的所有 IP。
GetImageSize: 取得圖片的長寬。
getlastmod: 傳回該網頁的最後修改時間。
getmxrr: 取得指定網址 DNS 記錄之 MX 欄位。
getmyinode: 傳回該網頁的 inode 值。
getmypid: 傳回 PHP 的行程代號。
getmyuid: 傳回 PHP 的使用者代碼。
getrandmax: 亂數的最大值。
getrusage: 傳回系統資源使用率。
gettimeofday: 取得目前時間。
gettype: 取得變數的型態。
get_cfg_var: 取得 PHP 的設定選項值。
get_current_user: 取得 PHP 行程的擁有者名稱。
get_magic_quotes_gpc: 取得 PHP 環境變數 magic_quotes_gpc 的值。
get_magic_quotes_runtime: 取得 PHP 環境變數 magic_quotes_runtime 的值。
get_meta_tags: 抽出檔案所有 meta 標記的資料。
gmdate: 取得目前與 GMT 差後的時間。
gmmktime: 取得 UNIX 時間戳記的格林威治時間。
GregorianToJD: 將格里高里曆法轉換成為凱撒日計數。
gzclose: 關閉壓縮檔。
gzeof: 判斷是否在壓縮檔尾。
gzfile: 讀壓縮檔到陣列中。
gzgetc: 讀壓縮檔中的字元。
gzgets: 讀壓縮檔中的字串。
gzgetss: 讀壓縮檔中的字串，並去掉 HTML 指令。
gzopen: 開啟壓縮檔。
gzpassthru: 解壓縮指標後全部資料。
gzputs: 資料寫入壓縮檔。
gzread: 壓縮檔讀出指定長度字串。
gzrewind: 重設壓縮檔指標。
gzseek: 設壓縮檔指標至指定處。
gztell: 取得壓縮檔指標處。
gzwrite: 資料寫入壓
header: 送出 HTTP 協定的標頭到瀏覽器
HexDec: 十六進位轉十進位。
htmlentities: 將所有的字元都轉成 HTML 字串。
htmlspecialchars: 將特殊字元轉成 HTML 格式。
hw_Children: 取得子物件代碼。
hw_ChildrenObj: 取得子物件的物件記錄。
hw_Close: 關閉 Hyperwave 連線。
hw_Connect: 連上 Hyperwave 伺服器。
hw_Cp: 複製物件。
hw_Deleteobject: 刪除物件。
hw_DocByAnchor: 取得指定錨的文件物件代碼。
hw_DocByAnchorObj: 取得指定錨的文件物件。
hw_DocumentAttributes: 取得指定文件物件屬性。
hw_DocumentBodyTag: 取得指定文件物件的文件主體標記。
hw_DocumentContent: 取得指定文件物件的內容。
hw_DocumentSetContent: 重設指定文件物件的內容。
hw_DocumentSize: 取得文件大小。
hw_EditText: 更動文字文件。
hw_Error: 取得錯誤代碼。
hw_ErrorMsg: 取得錯誤訊息。
hw_Free_Document: 釋放文件使用的記憶體。
hw_GetAnchors: 取得文件的連結錨。
hw_GetAnchorsObj: 取得文件的連結錨記錄。
hw_GetAndLock: 取得並鎖住物件。
hw_GetChildColl: 取得子物件們的 ID。
hw_GetChildCollObj: 取得子物件們的資料。
hw_GetChildDocColl: 取得全部子文件聚集。
hw_GetChildDocCollObj: 取得全部子文件聚集記錄。
hw_GetObject: 取得物件。
hw_GetObjectByQuery: 搜尋物件。
hw_GetObjectByQueryColl: 搜尋聚集物件。
hw_GetObjectByQueryCollObj: 搜尋聚集物件。
hw_GetObjectByQueryObj: 搜尋物件。
hw_GetParents: 取得父物件的 ID。
hw_GetParentsObj: 取得父物件的資料。
hw_GetRemote: 取得遠端文件。
hw_GetRemoteChildren: 取得遠端的子文件。
hw_GetSrcByDestObj: 取得指定目的的文件內容。
hw_GetText: 取得純文字文件。
hw_GetUsername: 目前使用者名字。
hw_Identify: 使用者身份確認。
hw_InCollections: 檢查物件聚集。
hw_Info: 連線資訊。
hw_InsColl: 插入聚集。
hw_InsDoc: 插入文件。
hw_InsertDocument: 上傳文件。
hw_InsertObject: 插入物件記錄。
hw_Modifyobject: 修改物件記錄。
hw_Mv: 移動物件。
hw_New_Document: 建立新文件。
hw_Objrec2Array: 物件記錄轉為陣列。
hw_OutputDocument: 輸出文件。
hw_pConnect: 連上 Hyperwave 伺服器。
hw_PipeDocument: 取得文件。
hw_Root: 取得根物件代碼。
hw_Unlock: 取消鎖定。
hw_Who: 列出目前使用者。
ibase_bind: 連結 PHP 變數到 InterBase 參數。
ibase_close: 關閉 InterBase 伺服器連線。
ibase_connect: 開啟 InterBase 伺服器連線。
ibase_execute: 執行 SQL 的指令區段。
ibase_fetch_row: 傳回單列的各欄位。
ibase_free_query: 釋放查詢指令佔用記憶體。
ibase_free_result: 釋放傳回佔用記憶體。
ibase_pconnect: 保持 InterBase 伺服器連線。
ibase_prepare: 分析 SQL 語法。
ibase_query: 送出一個 query 字串。
ibase_timefmt: 設定時間格式。
ifxus_close_slob: 刪除 slob 物件。
ifxus_create_slob: 建立 slob 物件。
ifxus_open_slob: 開啟 slob 物件。
ifxus_read_slob: 讀取指定數目的 slob 物件。
ifxus_seek_slob: 設定目前檔案或找尋位置。
ifxus_tell_slob: 傳回目前檔案或找尋位置。
ifxus_write_slob: 將字串寫入 slob 物件中。
ifx_affected_rows: 得到 Informix 最後操作影響的列數目。
ifx_blobinfile_mode: 設定長位元物件模式。
ifx_byteasvarchar: 設定位元組模式內定值。
ifx_close: 關閉 Informix 伺服器連線。
ifx_connect: 開啟 Informix 伺服器連線。
ifx_copy_blob: 複製長位元物件。
ifx_create_blob: 建立長位元物件。
ifx_create_char: 建立字元物件。
ifx_do: 執行已準備 query 字串。
ifx_error: 取得 Informix 最後的錯誤。
ifx_errormsg: 取得 Informix 最後錯誤訊息。
ifx_fetch_row: 傳回單列的各欄位。
ifx_fieldproperties: 列出 Informix 的 SQL 欄位屬性。
ifx_fieldtypes: 列出 Informix 的 SQL 欄位。
ifx_free_blob: 刪除長位元物件。
ifx_free_char: 刪除字元物件。
ifx_free_result: 釋放傳回佔用記憶體。
ifx_free_slob: 刪除 slob 物件。
ifx_getsqlca: 取得 query 後的 sqlca 資訊。
ifx_get_blob: 取得長位元物件。
ifx_get_char: 取得字元物件。
ifx_htmltbl_result: 將 query 傳回資料轉成 HTML 表格。
ifx_nullformat: 設定空字元模式內定值。
ifx_num_fields: 取得傳回欄位的數目。
ifx_num_rows: 取得傳回列的數目。
ifx_pconnect: 開啟 Informix 伺服器長期連線。
ifx_prepare: 準備 query 字串。
ifx_query: 送出一個 query 字串。
ifx_textasvarchar: 設定文字模式內定值。
ifx_update_blob: 更改長位元物件。
ifx_update_char: 更改字元物件。
ignore_user_abort: 連線中斷後程式是否執行。
ImageArc: 畫弧線。
ImageChar: 寫出橫向字元。
ImageCharUp: 寫出直式字元。
ImageColorAllocate: 匹配顏色。
ImageColorAt: 取得圖中指定點顏色的索引值。
ImageColorClosest: 計算色表中與指定顏色最接近者。
ImageColorExact: 計算色表上指定顏色索引值。
ImageColorResolve: 計算色表上指定或最接近顏色的索引值。
ImageColorSet: 設定色表上指定索引的顏色。
ImageColorsForIndex: 取得色表上指定索引的顏色。
ImageColorsTotal: 計算圖的顏色數。
ImageColorTransparent: 指定透明背景色。
ImageCopyResized: 複製新圖並調整大小。
ImageCreate: 建立新圖。
ImageCreateFromGIF: 取出 GIF 圖型。
ImageCreateFromPNG: 取出 PNG 圖型。
ImageDashedLine: 繪虛線。
ImageDestroy: 結束圖形。
ImageFill: 圖形著色。
ImageFilledPolygon: 多邊形區域著色。
ImageFilledRectangle: 矩形區域著色。
ImageFillToBorder: 指定顏色區域內著色。
ImageFontHeight: 取得字型的高度。
ImageFontWidth: 取得字型的寬度。
ImageGIF: 建立 GIF 圖型。
ImageInterlace: 使用交錯式顯示與否。
ImageLine: 繪實線。
ImageLoadFont: 載入點陣字型。
ImagePNG: 建立 PNG 圖型。
ImagePolygon: 繪多邊形。
ImagePSBBox: 計算 PostScript 文字所佔區域。
ImagePSEncodeFont: PostScript 字型轉成向量字。
ImagePSFreeFont: 卸下 PostScript 字型。
ImagePSLoadFont: 載入 PostScript 字型。
ImagePSText: 寫 PostScript 文字到圖中。
ImageRectangle: 繪矩形。
ImageSetPixel: 繪點。
ImageString: 繪橫式字串。
ImageStringUp: 繪直式字串。
ImageSX: 取得圖片的寬度。
ImageSY: 取得圖片的高度。
ImageTTFBBox: 計算 TTF 文字所佔區域。
ImageTTFText: 寫 TTF 文字到圖中。
imap_8bit: 將八位元轉成 qp 編碼。
imap_alerts: 所有的警告訊息。
imap_append: 附加字串到指定的郵箱中。
imap_base64: 解 BASE64 編碼。
imap_binary: 將八位元轉成 base64 編碼。
imap_body: 讀信的內文。
imap_check: 傳回郵箱資訊。
imap_clearflag_full: 清除信件旗標。
imap_close: 關閉 IMAP 連結。
imap_createmailbox: 建立新的信箱。
imap_delete: 標記欲刪除郵件。
imap_deletemailbox: 刪除既有信箱。
imap_errors: 所有的錯誤訊息。
imap_expunge: 刪除已標記的郵件。
imap_fetchbody: 從信件內文取出指定區段。
imap_fetchheader: 取得原始標頭。
imap_fetchstructure: 獲取某信件的結構資訊。
imap_getmailboxes: 取得全部信件詳細資訊。
imap_getsubscribed: 列出所有訂閱郵箱。
imap_header: 獲取某信件的標頭資訊。
imap_headers: 獲取全部信件的標頭資訊。
imap_last_error: 最後的錯誤訊息。
imap_listmailbox: 獲取郵箱列示。
imap_listsubscribed: 獲取訂閱郵箱列示。
imap_mailboxmsginfo: 取得目前郵箱的資訊。
imap_mail_copy: 複製指定信件到它處郵箱。
imap_mail_move: 移動指定信件到它處郵箱。
imap_msgno: 列出 UID 的連續信件。
imap_num_msg: 取得信件數。
imap_num_recent: 取得新進信件數。
imap_open: 開啟 IMAP 連結。
imap_ping: 檢查 IMAP 是否連線。
imap_qprint: 將 qp 編碼轉成八位元。
imap_renamemailbox: 更改郵箱名字。
imap_reopen: 重開 IMAP 連結。
imap_rfc822_parse_adrlist: 剖析電子郵件位址。
imap_rfc822_write_address: 電子郵件位址標準化。
imap_scanmailbox: 尋找信件有無特定字串。
imap_search: 搜尋指定標準的信件。
imap_setflag_full: 設定信件旗標。
imap_sort: 將信件標頭排序。
imap_status: 目前的狀態資訊。
imap_subscribe: 訂閱郵箱。
imap_uid: 取得信件 UID。
imap_undelete: 取消刪除郵件標記。
imap_unsubscribe: 取消訂閱郵箱。
implode: 將陣列變成字串。
intval: 變數轉成整數型態。
iptcparse: 使用 IPTC 模組解析位元資料。
isset: 判斷變數是否已設定。
is_array: 判斷變數型態是否為陣列型態。
is_dir: 測試檔案是否為目錄。
is_double: 判斷變數型態是否為倍浮點數型態。
is_executable: 測試檔案是否為可執行檔。
is_file: 測試檔案是否為正常檔案。
is_float: 判斷變數型態是否為浮點數型態。
is_int: 判斷變數型態是否為整數型態。
is_integer: 判斷變數型態是否為長整數型態。
is_link: 測試檔案是否為連結檔。
is_long: 判斷變數型態是否為長整數型態。
is_object: 判斷變數型態是否為物件型態。
is_readable: 測試檔案是否可讀取。
is_real: 判斷變數型態是否為實數型態。
is_string: 判斷變數型態是否為字串型態。
is_writeable: 測試檔案是否可寫入。
JDDayOfWeek: 傳回日期在周幾。
JDMonthName: 傳回月份名。
JDToFrench: 將凱撒日計數轉換成為法國共和曆法。
JDToGregorian: 將凱撒日計數 (Julian Day Count) 轉換成為格里高里曆法 (Gregorian date)。
JDToJewish: 將凱撒日計數轉換成為猶太曆法。
JDToJulian: 將凱撒日計數轉換成為凱撒曆法。
JewishToJD: 將猶太曆法轉換成為凱撒日計數。
join: 將陣列變成字串。
JulianToJD: 將凱撒曆法轉換成為凱撒日計數。
key: 取得陣列中的索引資料。
ksort: 將陣列的元素依索引排序。
ldap_add: 增加 LDAP 名錄的條目。
ldap_bind: 繫住 LDAP 目錄。
ldap_close: 結束 LDAP 連結。
ldap_connect: 連上 LDAP 伺服器。
ldap_count_entries: 搜尋結果的數目。
ldap_delete: 刪除指定資源。
ldap_dn2ufn: 將 dn 轉成易讀的名字。
ldap_explode_dn: 切開 dn 的欄位。
ldap_first_attribute: 取得第一筆資源的屬性。
ldap_first_entry: 取得第一筆結果代號。
ldap_free_result: 釋放傳回資料記憶體。
ldap_get_attributes: 取得傳回資料的屬性。
ldap_get_dn: 取得 DN 值。
ldap_get_entries: 取得全部傳回資料。
ldap_get_values: 取得全部傳回值。
ldap_list: 列出簡表。
ldap_modify: 改變 LDAP 名錄的屬性。
ldap_mod_add: 增加 LDAP 名錄的屬性。
ldap_mod_del: 刪除 LDAP 名錄的屬性。
ldap_mod_replace: 新的 LDAP 名錄取代舊屬性。
ldap_next_attribute: 取得傳回資料的下筆屬性。
ldap_next_entry: 取得下一筆結果代號。
ldap_read: 取得目前的資料屬性。
ldap_search: 列出樹狀簡表。
ldap_unbind: 結束 LDAP 連結。
leak: 洩出記憶體。
link: 建立硬式連結。
linkinfo: 取得連結資訊。
list: 列出陣列中元素的值。
Log: 自然對數值。
Log10: 10 基底的對數值。
lstat: 取得連結檔相關資訊。
ltrim: 去除連續空白。
mail: 寄出電子郵件。
max: 取得最大值。
mcrypt_cbc: 使用 CBC 將資料加/解密。
mcrypt_cfb: 使用 CFB 將資料加/解密。
mcrypt_create_iv: 從隨機源將向量初始化。
mcrypt_ecb: 使用 ECB 將資料加/解密。
mcrypt_get_block_size: 取得編碼方式的區塊大小。
mcrypt_get_cipher_name: 取得編碼方式的名稱。
mcrypt_get_key_size: 取得編碼鑰匙大小。
mcrypt_ofb: 使用 OFB 將資料加/解密。
md5: 計算字串的 MD5 雜湊。
mhash: 計算雜湊值。
mhash_count: 取得雜湊 ID 的最大值。
mhash_get_block_size: 取得雜湊方式的區塊大小。
mhash_get_hash_name: 取得雜湊演算法名稱。
microtime: 取得目前時間的 UNIX 時間戳記的百萬分之一秒值。
min: 取得最小值。
mkdir: 建立目錄。
mktime: 取得 UNIX 時間戳記。
msql: 送出 query 字串。
msql_affected_rows: 得到 mSQL 最後操作影響的列數目。
msql_close: 關閉 mSQL 資料庫連線。
msql_connect: 開啟 mSQL 資料庫連線。
msql_createdb: 建立一個新的 mSQL 資料庫。
msql_create_db: 建立一個新的 mSQL 資料庫。
msql_data_seek: 移動內部傳回指標。
msql_dbname: 取得目前所在資料庫名稱。
msql_dropdb: 刪除指定的 mSQL 資料庫。
msql_drop_db: 刪除指定的 mSQL 資料庫。
msql_error: 取得最後錯誤訊息。
msql_fetch_array: 傳回陣列資料。
msql_fetch_field: 取得欄位資訊。
msql_fetch_object: 傳回物件資料。
msql_fetch_row: 傳回單列的各欄位。
msql_fieldflags: 獲得欄位的旗標。
msql_fieldlen: 獲得欄位的長度。
msql_fieldname: 傳回指定欄位的名稱。
msql_fieldtable: 獲得欄位的資料表 (table) 名稱。
msql_fieldtype: 獲得欄位的型態。
msql_field_seek: 設定指標到傳回值的某欄位。
msql_freeresult: 釋放傳回佔用記憶體。
msql_free_result: 釋放傳回佔用記憶體。
msql_listdbs: 列出可用資料庫 (database)。
msql_listfields: 列出指定資料表的欄位 (field)。
msql_listtables: 列出指定資料庫的資料表 (table)。
msql_list_dbs: 列出可用資料庫 (database)。
msql_list_fields: 列出指定資料表的欄位 (field)。
msql_list_tables: 列出指定資料庫的資料表 (table)。
msql_numfields: 取得傳回欄位的數目。
msql_numrows: 取得傳回列的數目。
msql_num_fields: 取得傳回欄位的數目。
msql_num_rows: 取得傳回列的數目。
msql_pconnect: 開啟 mSQL 伺服器長期連線。
msql_query: 送出一個 query 字串。
msql_regcase: 將字串逐字傳回大小寫字元。
msql_result: 取得查詢 (query) 的結果。
msql_selectdb: 選擇一個資料庫。
msql_select_db: 選擇一個資料庫。
msql_tablename: 傳回指定資料表的名稱。
mssql_affected_rows: 取得最後 query 影響的列數。
mssql_close: 關閉與資料庫的連線。
mssql_connect: 連上資料庫。
mssql_data_seek: 移動列指標。
mssql_fetch_array: 傳回陣列資料。
mssql_fetch_field: 取得欄位資訊。
mssql_fetch_object: 傳回物件資料。
mssql_fetch_row: 傳回單列的各欄位。
mssql_field_seek: 設定指標到傳回值的某欄位。
mssql_free_result: 釋放傳回佔用記憶體。
mssql_num_fields: 取得傳回欄位的數目。
mssql_num_rows: 取得傳回列的數目。
mssql_pconnect: 開啟 MS SQL 伺服器長期連線。
mssql_query: 送出一個 query 字串。
mssql_result: 取得查詢 (query) 的結果。
mssql_select_db: 選擇一個資料庫。
mt_getrandmax: 亂數的最大值。
mt_rand: 取得亂數值。
mt_srand: 設定亂數種子。
mysql_affected_rows: 得到 MySQL 最後操作影響的列數目。
mysql_close: 關閉 MySQL 伺服器連線。
mysql_connect: 開啟 MySQL 伺服器連線。
mysql_create_db: 建立一個 MySQL 新資料庫。
mysql_data_seek: 移動內部傳回指標。
mysql_db_query: 送查詢字串 (query) 到 MySQL 資料庫。
mysql_drop_db: 移除資料庫。
mysql_errno: 傳回錯誤訊息代碼。
mysql_error: 傳回錯誤訊息。
mysql_fetch_array: 傳回陣列資料。
mysql_fetch_field: 取得欄位資訊。
mysql_fetch_lengths: 傳回單列各欄資料最大長度。
mysql_fetch_object: 傳回物件資料。
mysql_fetch_row: 傳回單列的各欄位。
mysql_field_flags: 獲得目前欄位的旗標。
mysql_field_len: 獲得目前欄位的長度。
mysql_field_name: 傳回指定欄位的名稱。
mysql_field_seek: 設定指標到傳回值的某欄位。
mysql_field_table: 獲得目前欄位的資料表 (table) 名稱。
mysql_field_type: 獲得目前欄位的型態。
mysql_free_result: 釋放傳回佔用記憶體。
mysql_insert_id: 傳回最後一次使用 INSERT 指令的 ID。
mysql_list_dbs: 列出 MySQL 伺服器可用的資料庫 (database)。
mysql_list_fields: 列出指定資料表的欄位 (field)。
mysql_list_tables: 列出指定資料庫的資料表 (table)。
mysql_num_fields: 取得傳回欄位的數目。
mysql_num_rows: 取得傳回列的數目。
mysql_pconnect: 開啟 MySQL 伺服器長期連線。
mysql_query: 送出一個 query 字串。
mysql_result: 取得查詢 (query) 的結果。
mysql_select_db: 選擇一個資料庫。
mysql_tablename: 取得資料表名稱。
next: 將陣列的內部指標向後移動。
nl2br: 將換行字元轉成 <br>。
number_format: 格式化數字字串。
OCIBindByName: 讓動態 SQL 可使用 PHP 變數。
OCIColumnIsNULL: 測試傳回行是否為空的。
OCIColumnSize: 取得欄位型態的大小。
OCICommit: 將 Oracle 的交易處理付諸實行。
OCIDefineByName: 讓 SELECT 指令可使用 PHP 變數。
OCIExecute: 執行 Oracle 的指令區段。
OCIFetch: 取得傳回資料的一列 (row)。
OCIFetchInto: 取回 Oracle 資料放入陣列。
OCILogOff: 關閉與 Oracle 的連結。
OCILogon: 開啟與 Oracle 的連結。
OCINewDescriptor: 初始新的 LOB/FILE 描述。
OCINumRows: 取得受影響欄位的數目。
OCIParse: 分析 SQL 語法。
OCIResult: 從目前列 (row) 的資料取得一欄 (column)。
OCIRollback: 撤消當前交易。
OctDec: 八進位轉十進位。
odbc_autocommit: 開關自動更動功能。
odbc_binmode: 設定二進位資料處理方式。
odbc_close: 關閉 ODBC 連結。
odbc_close_all: 關閉所有 ODBC 連結。
odbc_commit: 更動 ODBC 資料庫。
odbc_connect: 連結至 ODBC 資料庫。
odbc_cursor: 取得游標名。
odbc_do: 執行 SQL 指令。
odbc_exec: 執行 SQL 指令。
odbc_execute: 執行預置 SQL 指令。
odbc_fetch_into: 取得傳回的指定列。
odbc_fetch_row: 取得傳回一列。
odbc_field_len: 取得欄位資料長度。
odbc_field_name: 取得欄位名稱。
odbc_field_type: 取得欄位資料形態。
odbc_free_result: 釋出傳回資料的記憶體。
odbc_longreadlen: 設定傳回欄的最大值。
odbc_num_fields: 取得欄位數目。
odbc_num_rows: 取得傳回列數目。
odbc_pconnect: 長期連結至 ODBC 資料庫。
odbc_prepare: 預置 SQL 指令。
odbc_result: 取得傳回資料。
odbc_result_all: 傳回 HTML 表格資料。
odbc_rollback: 撤消當前交易。
odbc_setoption: 調整 ODBC 設定。
opendir: 開啟目錄 handle。
openlog: 打開系統紀錄。
Ora_Bind: 連結 PHP 變數到 Oracle 參數。
Ora_Close: 關閉一個 Oracle 的 cursor。
Ora_ColumnName: 得到 Oracle 傳回列 (Column) 的名稱。
Ora_ColumnSize: 取得欄位型態的大小。
Ora_ColumnType: 得到 Oracle 傳回列 (Column) 的型態。
Ora_Commit: 將 Oracle 的交易處理付諸實行。
Ora_CommitOff: 關閉自動執行 Oracle 交易更動的功能。
Ora_CommitOn: 打開自動執行 Oracle 交易更動的功能。
Ora_Do: 快速的 SQL 查詢。
Ora_Error: 獲得 Oracle 錯誤訊息。
Ora_ErrorCode: 獲得 Oracle 錯誤代碼。
Ora_Exec: 執行 Oracle 的指令區段。
Ora_Fetch: 取得傳回資料的一列 (row)。
Ora_FetchInto: 取回 Oracle 資料放入陣列。
Ora_GetColumn: 從傳回列 (row) 的資料取得一欄 (column)。
Ora_Logoff: 結束與 Oracle 的連結。
Ora_Logon: 開啟與 Oracle 的連結。
Ora_Numcols: 取得欄位的數目。
Ora_Open: 開啟 Oracle 的 cursor。
Ora_Parse: 分析 SQL 語法。
Ora_PLogon: 開啟與 Oracle 的長期連結。
Ora_Rollback: 撤消當前交易。
Ord: 傳回字元的序數值。
pack: 壓縮資料到位元字串之中。
parse_str: 剖析 query 字串成變數。
parse_url: 剖析 URL 字串。
passthru: 執行外部程式並不加處理輸出資料。
pclose: 關閉檔案。
PDF_add_annotation: 加入註解。
PDF_add_outline: 目前頁面加入書籤。
PDF_arc: 繪弧。
PDF_begin_page: 啟始 PDF 檔案頁面。
PDF_circle: 繪圓。
PDF_clip: 組合所有向量。
PDF_close: 關閉 PDF 檔。
PDF_closepath: 形成封閉的向量形狀。
PDF_closepath_fill_stroke: 形成封閉的向量形狀沿向量繪線並填滿。
PDF_closepath_stroke: 形成封閉的向量形狀並沿向量繪線。
PDF_close_image: 關閉圖檔。
PDF_continue_text: 輸出文字。
PDF_curveto: 繪貝氏曲線。
PDF_endpath: 關閉目前向量。
PDF_end_page: 關閉 PDF 檔案頁面。
PDF_execute_image: 放置 PDF 檔中圖片到指定位置。
PDF_fill: 填滿目前的向量。
PDF_fill_stroke: 填滿目前的向量並沿向量繪線。
PDF_get_info: 傳回檔案資訊。
PDF_lineto: 繪直線。
PDF_moveto: 設定處理的坐標點。
PDF_open: 建立新的 PDF 檔。
PDF_open_gif: 開啟 GIF 圖檔。
PDF_open_jpeg: 開啟 JPEG 圖檔。
PDF_open_memory_image: 開啟記憶體圖檔。
PDF_place_image: 放置圖片到 PDF 檔指定位置。
PDF_put_image: 放置圖片到 PDF 檔。
PDF_rect: 繪長方形。
PDF_restore: 還原環境變數。
PDF_rotate: 旋轉物件。
PDF_save: 儲存環境變數。
PDF_scale: 縮放物件。
PDF_setdash: 設定虛線樣式。
PDF_setflat: 設定平滑值。
PDF_setgray: 指定繪圖的顏色為灰階並填入。
PDF_setgray_fill: 指定填入的顏色為灰階。
PDF_setgray_stroke: 指定繪圖的顏色為灰階。
PDF_setlinecap: 設定 linecap 參數。
ldap_get_dn: 取得 DN 值。
ldap_get_entries: 取得全部傳回資料。
ldap_get_values: 取得全部傳回值。
ldap_list: 列出簡表。
ldap_modify: 改變 LDAP 名錄的屬性。
ldap_mod_add: 增加 LDAP 名錄的屬性。
ldap_mod_del: 刪除 LDAP 名錄的屬性。
ldap_mod_replace: 新的 LDAP 名錄取代舊屬性。
ldap_next_attribute: 取得傳回資料的下筆屬性。
ldap_next_entry: 取得下一筆結果代號。
ldap_read: 取得目前的資料屬性。
ldap_search: 列出樹狀簡表。
ldap_unbind: 結束 LDAP 連結。
leak: 洩出記憶體。
link: 建立硬式連結。
linkinfo: 取得連結資訊。
list: 列出陣列中元素的值。
Log: 自然對數值。
Log10: 10 基底的對數值。
lstat: 取得連結檔相關資訊。
ltrim: 去除連續空白。
mail: 寄出電子郵件。
max: 取得最大值。
mcrypt_cbc: 使用 CBC 將資料加/解密。
mcrypt_cfb: 使用 CFB 將資料加/解密。
mcrypt_create_iv: 從隨機源將向量初始化。
mcrypt_ecb: 使用 ECB 將資料加/解密。
mcrypt_get_block_size: 取得編碼方式的區塊大小。
mcrypt_get_cipher_name: 取得編碼方式的名稱。
mcrypt_get_key_size: 取得編碼鑰匙大小。
mcrypt_ofb: 使用 OFB 將資料加/解密。
md5: 計算字串的 MD5 雜湊。
mhash: 計算雜湊值。
mhash_count: 取得雜湊 ID 的最大值。
mhash_get_block_size: 取得雜湊方式的區塊大小。
mhash_get_hash_name: 取得雜湊演算法名稱。
microtime: 取得目前時間的 UNIX 時間戳記的百萬分之一秒值。
min: 取得最小值。
mkdir: 建立目錄。
mktime: 取得 UNIX 時間戳記。
msql: 送出 query 字串。
msql_affected_rows: 得到 mSQL 最後操作影響的列數目。
msql_close: 關閉 mSQL 資料庫連線。
msql_connect: 開啟 mSQL 資料庫連線。
msql_createdb: 建立一個新的 mSQL 資料庫。
msql_create_db: 建立一個新的 mSQL 資料庫。
msql_data_seek: 移動內部傳回指標。
msql_dbname: 取得目前所在資料庫名稱。
msql_dropdb: 刪除指定的 mSQL 資料庫。
msql_drop_db: 刪除指定的 mSQL 資料庫。
msql_error: 取得最後錯誤訊息。
msql_fetch_array: 傳回陣列資料。
msql_fetch_field: 取得欄位資訊。
msql_fetch_object: 傳回物件資料。
msql_fetch_row: 傳回單列的各欄位。
msql_fieldflags: 獲得欄位的旗標。
msql_fieldlen: 獲得欄位的長度。
msql_fieldname: 傳回指定欄位的名稱。
msql_fieldtable: 獲得欄位的資料表 (table) 名稱。
msql_fieldtype: 獲得欄位的型態。
msql_field_seek: 設定指標到傳回值的某欄位。
msql_freeresult: 釋放傳回佔用記憶體。
msql_free_result: 釋放傳回佔用記憶體。
msql_listdbs: 列出可用資料庫 (database)。
msql_listfields: 列出指定資料表的欄位 (field)。
msql_listtables: 列出指定資料庫的資料表 (table)。
msql_list_dbs: 列出可用資料庫 (database)。
msql_list_fields: 列出指定資料表的欄位 (field)。
msql_list_tables: 列出指定資料庫的資料表 (table)。
msql_numfields: 取得傳回欄位的數目。
msql_numrows: 取得傳回列的數目。
msql_num_fields: 取得傳回欄位的數目。
msql_num_rows: 取得傳回列的數目。
msql_pconnect: 開啟 mSQL 伺服器長期連線。
msql_query: 送出一個 query 字串。
msql_regcase: 將字串逐字傳回大小寫字元。
msql_result: 取得查詢 (query) 的結果。
msql_selectdb: 選擇一個資料庫。
msql_select_db: 選擇一個資料庫。
msql_tablename: 傳回指定資料表的名稱。
mssql_affected_rows: 取得最後 query 影響的列數。
mssql_close: 關閉與資料庫的連線。
mssql_connect: 連上資料庫。
mssql_data_seek: 移動列指標。
mssql_fetch_array: 傳回陣列資料。
mssql_fetch_field: 取得欄位資訊。
mssql_fetch_object: 傳回物件資料。
mssql_fetch_row: 傳回單列的各欄位。
mssql_field_seek: 設定指標到傳回值的某欄位。
mssql_free_result: 釋放傳回佔用記憶體。
mssql_num_fields: 取得傳回欄位的數目。
mssql_num_rows: 取得傳回列的數目。
mssql_pconnect: 開啟 MS SQL 伺服器長期連線。
mssql_query: 送出一個 query 字串。
mssql_result: 取得查詢 (query) 的結果。
mssql_select_db: 選擇一個資料庫。
mt_getrandmax: 亂數的最大值。
mt_rand: 取得亂數值。
mt_srand: 設定亂數種子。
mysql_affected_rows: 得到 MySQL 最後操作影響的列數目。
mysql_close: 關閉 MySQL 伺服器連線。
mysql_connect: 開啟 MySQL 伺服器連線。
mysql_create_db: 建立一個 MySQL 新資料庫。
mysql_data_seek: 移動內部傳回指標。
mysql_db_query: 送查詢字串 (query) 到 MySQL 資料庫。
mysql_drop_db: 移除資料庫。
mysql_errno: 傳回錯誤訊息代碼。
mysql_error: 傳回錯誤訊息。
mysql_fetch_array: 傳回陣列資料。
mysql_fetch_field: 取得欄位資訊。
mysql_fetch_lengths: 傳回單列各欄資料最大長度。
mysql_fetch_object: 傳回物件資料。
mysql_fetch_row: 傳回單列的各欄位。
mysql_field_flags: 獲得目前欄位的旗標。
mysql_field_len: 獲得目前欄位的長度。
mysql_field_name: 傳回指定欄位的名稱。
mysql_field_seek: 設定指標到傳回值的某欄位。
mysql_field_table: 獲得目前欄位的資料表 (table) 名稱。
mysql_field_type: 獲得目前欄位的型態。
mysql_free_result: 釋放傳回佔用記憶體。
mysql_insert_id: 傳回最後一次使用 INSERT 指令的 ID。
mysql_list_dbs: 列出 MySQL 伺服器可用的資料庫 (database)。
mysql_list_fields: 列出指定資料表的欄位 (field)。
mysql_list_tables: 列出指定資料庫的資料表 (table)。
mysql_num_fields: 取得傳回欄位的數目。
mysql_num_rows: 取得傳回列的數目。
mysql_pconnect: 開啟 MySQL 伺服器長期連線。
mysql_query: 送出一個 query 字串。
mysql_result: 取得查詢 (query) 的結果。
mysql_select_db: 選擇一個資料庫。
mysql_tablename: 取得資料表名稱。
next: 將陣列的內部指標向後移動。
nl2br: 將換行字元轉成 <br>。
number_format: 格式化數字字串。
OCIBindByName: 讓動態 SQL 可使用 PHP 變數。
OCIColumnIsNULL: 測試傳回行是否為空的。
OCIColumnSize: 取得欄位型態的大小。
OCICommit: 將 Oracle 的交易處理付諸實行。
OCIDefineByName: 讓 SELECT 指令可使用 PHP 變數。
OCIExecute: 執行 Oracle 的指令區段。
OCIFetch: 取得傳回資料的一列 (row)。
OCIFetchInto: 取回 Oracle 資料放入陣列。
OCILogOff: 關閉與 Oracle 的連結。
OCILogon: 開啟與 Oracle 的連結。
OCINewDescriptor: 初始新的 LOB/FILE 描述。
OCINumRows: 取得受影響欄位的數目。
OCIParse: 分析 SQL 語法。
OCIResult: 從目前列 (row) 的資料取得一欄 (column)。
OCIRollback: 撤消當前交易。
OctDec: 八進位轉十進位。
odbc_autocommit: 開關自動更動功能。
odbc_binmode: 設定二進位資料處理方式。
odbc_close: 關閉 ODBC 連結。
odbc_close_all: 關閉所有 ODBC 連結。
odbc_commit: 更動 ODBC 資料庫。
odbc_connect: 連結至 ODBC 資料庫。
odbc_cursor: 取得游標名。
odbc_do: 執行 SQL 指令。
odbc_exec: 執行 SQL 指令。
odbc_execute: 執行預置 SQL 指令。
odbc_fetch_into: 取得傳回的指定列。
odbc_fetch_row: 取得傳回一列。
odbc_field_len: 取得欄位資料長度。
odbc_field_name: 取得欄位名稱。
odbc_field_type: 取得欄位資料形態。
odbc_free_result: 釋出傳回資料的記憶體。
odbc_longreadlen: 設定傳回欄的最大值。
odbc_num_fields: 取得欄位數目。
odbc_num_rows: 取得傳回列數目。
odbc_pconnect: 長期連結至 ODBC 資料庫。
odbc_prepare: 預置 SQL 指令。
odbc_result: 取得傳回資料。
odbc_result_all: 傳回 HTML 表格資料。
odbc_rollback: 撤消當前交易。
odbc_setoption: 調整 ODBC 設定。
opendir: 開啟目錄 handle。
openlog: 打開系統紀錄。
Ora_Bind: 連結 PHP 變數到 Oracle 參數。
Ora_Close: 關閉一個 Oracle 的 cursor。
Ora_ColumnName: 得到 Oracle 傳回列 (Column) 的名稱。
Ora_ColumnSize: 取得欄位型態的大小。
Ora_ColumnType: 得到 Oracle 傳回列 (Column) 的型態。
Ora_Commit: 將 Oracle 的交易處理付諸實行。
Ora_CommitOff: 關閉自動執行 Oracle 交易更動的功能。
Ora_CommitOn: 打開自動執行 Oracle 交易更動的功能。
Ora_Do: 快速的 SQL 查詢。
Ora_Error: 獲得 Oracle 錯誤訊息。
Ora_ErrorCode: 獲得 Oracle 錯誤代碼。
Ora_Exec: 執行 Oracle 的指令區段。
Ora_Fetch: 取得傳回資料的一列 (row)。
Ora_FetchInto: 取回 Oracle 資料放入陣列。
Ora_GetColumn: 從傳回列 (row) 的資料取得一欄 (column)。
Ora_Logoff: 結束與 Oracle 的連結。
Ora_Logon: 開啟與 Oracle 的連結。
Ora_Numcols: 取得欄位的數目。
Ora_Open: 開啟 Oracle 的 cursor。
Ora_Parse: 分析 SQL 語法。
Ora_PLogon: 開啟與 Oracle 的長期連結。
Ora_Rollback: 撤消當前交易。
Ord: 傳回字元的序數值。
pack: 壓縮資料到位元字串之中。
parse_str: 剖析 query 字串成變數。
parse_url: 剖析 URL 字串。
passthru: 執行外部程式並不加處理輸出資料。
pclose: 關閉檔案。
PDF_add_annotation: 加入註解。
PDF_add_outline: 目前頁面加入書籤。
PDF_arc: 繪弧。
PDF_begin_page: 啟始 PDF 檔案頁面。
PDF_circle: 繪圓。
PDF_clip: 組合所有向量。
PDF_close: 關閉 PDF 檔。
PDF_closepath: 形成封閉的向量形狀。
PDF_closepath_fill_stroke: 形成封閉的向量形狀沿向量繪線並填滿。
PDF_closepath_stroke: 形成封閉的向量形狀並沿向量繪線。
PDF_close_image: 關閉圖檔。
PDF_continue_text: 輸出文字。
PDF_curveto: 繪貝氏曲線。
PDF_endpath: 關閉目前向量。
PDF_end_page: 關閉 PDF 檔案頁面。
PDF_execute_image: 放置 PDF 檔中圖片到指定位置。
PDF_fill: 填滿目前的向量。
PDF_fill_stroke: 填滿目前的向量並沿向量繪線。
PDF_get_info: 傳回檔案資訊。
PDF_lineto: 繪直線。
PDF_moveto: 設定處理的坐標點。
PDF_open: 建立新的 PDF 檔。
PDF_open_gif: 開啟 GIF 圖檔。
PDF_open_jpeg: 開啟 JPEG 圖檔。
PDF_open_memory_image: 開啟記憶體圖檔。
PDF_place_image: 放置圖片到 PDF 檔指定位置。
PDF_put_image: 放置圖片到 PDF 檔。
PDF_rect: 繪長方形。
PDF_restore: 還原環境變數。
PDF_rotate: 旋轉物件。
PDF_save: 儲存環境變數。
PDF_scale: 縮放物件。
PDF_setdash: 設定虛線樣式。
PDF_setflat: 設定平滑值。
PDF_setgray: 指定繪圖的顏色為灰階並填入。
PDF_setgray_fill: 指定填入的顏色為灰階。
PDF_setgray_stroke: 指定繪圖的顏色為灰階。
PDF_setlinecap: 設定 linecap 參數。
PDF_setlinejoin: 設定連線參數。
PDF_setlinewidth: 設定線寬。
PDF_setmiterlimit: 設定斜邊界限。
PDF_setrgbcolor: 指定繪圖的顏色為彩色並填入。
PDF_setrgbcolor_fill: 指定填入的顏色為彩色。
PDF_setrgbcolor_stroke: 指定繪圖的顏色為彩色。
PDF_set_char_spacing: 設定字元間距。
PDF_set_duration: 設定二頁的切換時間。
PDF_set_font: 設定使用的字型及大小。
PDF_set_horiz_scaling: 設定文字水平間距。
PDF_set_info_author: 設定檔案作者。
PDF_set_info_creator: 設定建檔者字串。
PDF_set_info_keywords: 設定檔案的關鍵字。
PDF_set_info_subject: 設定檔案主題。
PDF_set_info_title: 設定檔案標題。
PDF_set_leading: 設定行距。
PDF_set_text_matrix: 設定文字矩陣。
PDF_set_text_pos: 設定文字位置。
PDF_set_text_rendering: 設定文字表現方式。
PDF_set_text_rise: 設定文字高度。
PDF_set_transition: 設定頁的轉換。
PDF_set_word_spacing: 設定字間距。
PDF_show: 輸出字串到 PDF 檔案。
PDF_show_xy: 輸出字串到指定坐標。
PDF_stringwidth: 計算字串的寬度。
PDF_stroke: 沿向量繪線。
PDF_translate: 移動原點。
pfsockopen: 打開網路的 Socket 持續連結。
pg_Close: 關閉 PostgreSQL 伺服器連線。
pg_cmdTuples: 取得被 SQL 指令影響的資料筆數。
pg_Connect: 開啟 PostgreSQL 伺服器連線。
pg_DBname: 取得目前的資料庫名稱。
pg_ErrorMessage: 傳回錯誤訊息。
pg_Exec: 執行 query 指令。
pg_Fetch_Array: 傳回陣列資料。
pg_Fetch_Object: 傳回物件資料。
pg_Fetch_Row: 傳回單列的各欄位。
pg_FieldIsNull: 檢查欄位是否有資料。
pg_FieldName: 傳回指定欄位的名稱。
pg_FieldNum: 取得指定欄位的行數。
pg_FieldPrtLen: 計算可列示的長度。
pg_FieldSize: 計算指定欄位的長度。
pg_FieldType: 獲得目前欄位的型態。
pg_FreeResult: 釋放傳回佔用記憶體。
pg_GetLastOid: 取得最後的物件代碼。
pg_Host: 取得連線機器名稱。
pg_loclose: 關閉大型物件。
pg_locreate: 建立大型物件。
pg_loopen: 開啟大型物件。
pg_loread: 讀取大型物件。
pg_loreadall: 讀取大型物件並輸出。
pg_lounlink: 刪除大型物件。
pg_lowrite: 讀取大型物件。
pg_NumFields: 取得傳回欄位的數目。
pg_NumRows: 取得傳回列的數目。
pg_Options: 取得連線機器選項。
pg_pConnect: 開啟 PostgreSQL 伺服器長期連線。
pg_Port: 取得連線機器埠號。
pg_Result: 取得查詢 (query) 的結果。
pg_tty: 取得連線機器終端機。
phpinfo: 傳回 PHP 所有相關資訊。
phpversion: 傳回 PHP 版本訊息。
pi: 圓週率。
popen: 開啟檔案。
pos: 傳回陣列目前的元素。
pow: 次方。
preg_match: 字串比對剖析。
preg_match_all: 字串整體比對剖析。
preg_replace: 字串比對剖析並取代。
preg_split: 將字串依指定的規則切開。
prev: 將陣列的內部指標往前移動。
print: 輸出字串。
printf: 輸出格式化字串。
putenv: 設定系統環境變數。
quoted_printable_decode: 將 qp 編碼字串轉成 8 位元字串。
QuoteMeta: 加入引用符號。
rand: 取得亂數值。
range: 建立一個整數範圍的陣列。
rawurldecode: 從 URL 專用格式字串還原成普通字串。
rawurlencode: 將字串編碼成 URL 專用格式。
readdir: 讀取目錄 handle。
readfile: 輸出檔案。
readgzfile: 讀出壓縮檔。
readlink: 傳回符號連結 (symbolic link) 目標檔。
recode_file: 記錄檔案或檔案請求到記錄中。
recode_string: 記錄字串到記錄中。
register_shutdown_function: 定義 PHP 程式執行完成後執行的函式。
rename: 更改檔名。
reset: 將陣列的指標指到陣列第一個元素。
rewind: 重置開檔的讀寫位置指標。
rewinddir: 重設目錄 handle。
rmdir: 刪除目錄。
round: 四捨五入。
rsort: 將陣列的值由大到小排序。
sem_acquire: 捕獲信號。
sem_get: 取得信號代碼。
sem_release: 釋出信號。
serialize: 儲存資料到系統中。
session_decode: Session 資料解碼。
session_destroy: 結束 session。
session_encode: Session 資料編碼。
session_id: 存取目前 session 代號。
session_is_registered: 檢查變數是否註冊。
session_module_name: 存取目前 session 模組。
session_name: 存取目前 session 名稱。
session_register: 註冊新的變數。
session_save_path: 存取目前 session 路徑。
session_start: 初始 session。
session_unregister: 刪除已註冊變數。
setcookie: 送出 Cookie 資訊到瀏覽器。
setlocale: 設定地域化資訊。
settype: 設定變數型態。
set_file_buffer: 設定檔案緩衝區大小。
set_magic_quotes_runtime: 設定 magic_quotes_runtime 值。
set_socket_blocking: 切換擱置與無擱置模式。
set_time_limit: 設定該頁最久執行時間。
shm_attach: 開啟建立共享記憶體區段。
shm_detach: 中止共享記憶體區段連結。
shm_get_var: 取得記憶體區段中指定的變數。
shm_put_var: 加入或更新記憶體區段中的變數。
shm_remove: 清除記憶體區段。
shm_remove_var: 刪除記憶體區段中指定的變數。
shuffle: 將陣列的順序弄混。
similar_text: 計算字串相似度。
Sin: 正弦計算。
sizeof: 獲知陣列的大小。
sleep: 暫停執行。
snmpget: 取得指定物件識別碼。
snmpwalk: 取得所有物件。
snmpwalkoid: 取得網路本體樹狀資訊。
snmp_get_quick_print: 取得 UCD 函式庫中的 quick_print 值。
snmp_set_quick_print: 設定 UCD 函式庫中的 quick_print 值。
solid_close: 關閉 solid 連結。
solid_connect: 連結至 solid 資料庫。
solid_exec: 執行 SQL 指令。
solid_fetchrow: 取得傳回一列。
solid_fieldname: 取得欄位名稱。
solid_fieldnum: 取得欄位數目。
solid_freeresult: 釋出傳回資料的記憶體。
solid_numfields: 取得欄位數目。
solid_numrows: 取得傳回列數目。
solid_result: 取得傳回資料。
sort: 將陣列排序。
soundex: 計算字串的讀音值
split: 將字串依指定的規則切開。
sprintf: 將字串格式化。
sql_regcase: 將字串逐字傳回大小寫字元。
Sqrt: 開平方根。
srand: 設定亂數種子。
stat: 取得檔案相關資訊。
strchr: 尋找第一個出現的字元。
strcmp: 字串比較。
strcspn: 不同字串的長度。
strftime: 將伺服器的時間本地格式化。
StripSlashes: 去掉反斜線字元。
strip_tags: 去掉 HTML 及 PHP 的標記。
strlen: 取得字串長度。
strpos: 尋找字串中某字元最先出現處。
strrchr: 取得某字元最後出現處起的字串。
strrev: 顛倒字串。
strrpos: 尋找字串中某字元最後出現處。
strspn: 找出某字串落在另一字串遮罩的數目。
strstr: 傳回字串中某字串開始處至結束的字串。
strtok: 切開字串。
strtolower: 字串全轉為小寫。
strtoupper: 字串全轉為大寫。
strtr: 轉換某些字元。
strval: 將變數轉成字串型態。
str_replace: 字串取代。
substr: 取部份字串。
sybase_affected_rows: 取得最後 query 影響的列數。
sybase_close: 關閉與資料庫的連線。
sybase_connect: 連上資料庫。
sybase_data_seek: 移動列指標。
sybase_fetch_array: 傳回陣列資料。
sybase_fetch_field: 取得欄位資訊。
sybase_fetch_object: 傳回物件資料。
sybase_fetch_row: 傳回單列的各欄位。
sybase_field_seek: 設定指標到傳回值的某欄位。
sybase_free_result: 釋放傳回佔用記憶體。
sybase_num_fields: 取得傳回欄位的數目。
sybase_num_rows: 取得傳回列的數目。
sybase_pconnect: 開啟伺服器長期連線。
sybase_query: 送出一個 query 字串。
sybase_result: 取得查詢 (query) 的結果。
sybase_select_db: 選擇一個資料庫。
symlink: 建立符號連結 (symbolic link)。
syslog: 紀錄至系統紀錄。
system: 執行外部程式並顯示輸出資料。
Tan: 正切計算。
tempnam: 建立唯一的臨時檔。
time: 取得目前時間的 UNIX 時間戳記。
touch: 設定最後修改時間。
trim: 截去字串首尾的空格。
uasort: 將陣列依使用者自定的函式排序。
ucfirst: 將字串第一個字元改大寫。
ucwords: 將字串每個字第一個字母改大寫。
uksort: 將陣列的索引依使用者自定的函式排序。
umask: 改變目前的檔案屬性遮罩 umask。
uniqid: 產生唯一的值。
unlink: 刪除檔案。
unpack: 解壓縮位元字串資料。
unserialize: 取出系統資料。
unset: 刪除變數。
urldecode: 還原 URL 編碼字串。
urlencode: 將字串以 URL 編碼。
usleep: 暫停執行。
usort: 將陣列的值依使用者自定的函式排序。
utf8_decode: 將 UTF-8 碼轉成 ISO-8859-1 碼。
utf8_encode: 將 ISO-8859-1 碼轉成 UTF-8 碼。
virtual: 完成阿帕契伺服器的子請求 (sub-request)。
vm_addalias: 加入新別名。
vm_adduser: 加入新使用者。
vm_delalias: 刪除別名。
vm_deluser: 刪除使用者。
vm_passwd: 改變使用者密碼。
wddx_add_vars: 將 WDDX 封包連續化。
wddx_deserialize: 將 WDDX 封包解連續化。
wddx_packet_end: 結束的 WDDX 封包。
wddx_packet_start: 開始新的 WDDX 封包。
wddx_serialize_value: 將單一值連續化。
wddx_serialize_vars: 將多值連續化。
xml_error_string: 取得 XML 錯誤字串。
xml_get_current_byte_index: 取得目前剖析為第幾個位元組。
xml_get_current_column_number: 獲知目前剖析的第幾欄位。
xml_get_current_line_number: 取得目前剖析的行號。
xml_get_error_code: 取得 XML 錯誤碼。
xml_parse: 剖析 XML 文件。
xml_parser_create: 初始 XML 剖析器。
xml_parser_free: 釋放剖析佔用的記憶體。
xml_parser_get_option: 取得剖析使用的選項。
xml_parser_set_option: 設定剖析使用的選項。
xml_set_character_data_handler: 建立字元資料標頭。
xml_set_default_handler: 建立內定標頭。
xml_set_element_handler: 設定元素的標頭。
xml_set_external_entity_ref_handler: 設定外部實體參引的標頭。
xml_set_notation_decl_handler: 設定記法宣告的標頭。
xml_set_object: 使 XML 剖析器用物件。
xml_set_processing_instruction_handler: 建立處理指令標頭。
xml_set_unparsed_entity_decl_handler: 設定未剖析實體宣告的標頭。
yp_errno: 取得先前 YP 操作的錯誤碼。
yp_err_string: 取得先前 YP 操作的錯誤字串。
yp_first: 傳回 map 上第一筆符合的資料。
yp_get_default_domain: 取得機器的 Domain。
yp_master: 取得 NIS 的 Master。
yp_match: 取得指定資料。
yp_next: 指定 map 的下筆資料。
yp_order: 傳回 map 的序數。

PHP 函式庫及函式 :Session 函式庫
本函式庫共有 11 個函式
session_start: 初始 session。
session_destroy: 結束 session。
session_name: 存取目前 session 名稱。
session_module_name: 存取目前 session 模組。
session_save_path: 存取目前 session 路徑。
session_id: 存取目前 session 代號。
session_register: 註冊新的變數。
session_unregister: 刪除已註冊變數。
session_is_registered: 檢查變數是否註冊。
session_decode: Session 資料解碼。
session_encode: Session 資料編碼

ODBC 資料庫連結函式庫
本函式庫共有 25 個函式
開放資料連結 (Open Database Connectivity, ODBC) 是連結資料庫的共通介面。ODBC 是由微軟主導的資料庫連結標準，實作環境也以微軟的系統最成熟。在 UNIX 系統中，通常要使用其它廠商所提供的 ODBC 介面，有些 UNIX 廠商會自己提供 ODBC 介面 (如 SUN 有為 Solaris 提供 ODBC)。
ODBC 和資料庫的查詢採用 SQL 語言，這和大部份的資料庫查詢方式一樣，這使得系統可以很容易和各種資料庫溝通。當然，透過 ODBC 介面，後端的資料庫不一定要 DBMS 這種大型資料庫系統，亦可以是資料表 (如 Microsoft Access)、或者是試算表 (如 Microsoft Excel)。
odbc_autocommit: 開關自動更動功能。
odbc_binmode: 設定二進位資料處理方式。
odbc_close: 關閉 ODBC 連結。
odbc_close_all: 關閉所有 ODBC 連結。
odbc_commit: 更動 ODBC 資料庫。
odbc_connect: 連結至 ODBC 資料庫。
odbc_cursor: 取得游標名。
odbc_do: 執行 SQL 指令。
odbc_exec: 執行 SQL 指令。
odbc_execute: 執行預置 SQL 指令。
odbc_fetch_into: 取得傳回的指定列。
odbc_fetch_row: 取得傳回一列。
odbc_field_name: 取得欄位名稱。
odbc_field_type: 取得欄位資料形態。
odbc_field_len: 取得欄位資料長度。
odbc_free_result: 釋出傳回資料的記憶體。
odbc_longreadlen: 設定傳回欄的最大值。
odbc_num_fields: 取得欄位數目。
odbc_pconnect: 長期連結至 ODBC 資料庫。
odbc_prepare: 預置 SQL 指令。
odbc_num_rows: 取得傳回列數目。
odbc_result: 取得傳回資料。
odbc_result_all: 傳回 HTML 表格資料。
odbc_rollback: 撤消當前交易。
odbc_setoption: 調整 ODBC 設定