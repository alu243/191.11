<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcclass", "fjdp", "lang", "lcgrade");
CheckAuthority($acptAccounts);
?>
<?php
if ($delete == 1 && !empty($del_item)) {
    $str = "delete from book where no = '$del_item'";
    mysql_query($str) or die("刪除失敗");
}
else if ($update == 1 && !empty($upd_item)) {
    $str = "update book "
        ." set book_no = '$book_no', name_ch  = '$name_ch', name_en = '$name_en', money  = '$money', name_short  = '$name_short ', bookorder = '$bookorder'"
        ." where no = '$upd_item'";
    mysql_query($str);
}
else if ($increase == 1) {
    if (!empty($book_no) && !empty($name_ch)) {
        $str = "insert into book set book_no = '$book_no', name_ch  = '$name_ch ', name_en = '$name_en', money  = '$money ', name_short  = '$name_short ', bookorder='$bookorder'";
        mysql_query($str);
    }
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">教材資料-列表</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主列表</a><hr>
    <a href="book_new.php">新增資料</a><hr>
    <div align="center">
        <center>
          <table border="1" width="85%" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="11%" bgcolor="#E6FFEB">教材代碼</td>
              <td width="27%" bgcolor="#E6FFEB">中文書名</td>
              <td width="18%" bgcolor="#E6FFEB">英文書名</td>
              <td width="10%" bgcolor="#E6FFEB">常用順序</td>
              <td width="8%" bgcolor="#E6FFEB">價錢</td>
              <td width="10%" bgcolor="#E6FFEB">修改</td>
              <td width="10%" bgcolor="#E6FFEB">刪除</td>
            </tr>
<?php
$sql = "select * from book order by 'book_no' asc";
$result = mysql_query($sql);

while($data = mysql_fetch_array($result)) {
?>
            <tr>
              <td width="11%"><?php echo $data['book_no']; ?></td>
              <td width="27%"><?php echo $data['name_ch']; ?></td>
              <td width="18%"><?php echo $data['name_en']; ?></td>
              <td width="10%"><?php echo $data['bookorder']; ?></td>
              <td width="8%"><?php echo $data['money']; ?></td>
              <td width="10%"><a href="./book_modify.php?modify=1&mod_item=<?php echo $data['no']; ?>">修改</a></td>
              <td width="10%"><a href="./book.php?delete=1&del_item=<?php echo $data['no']; ?>" onClick="return popMsg(this,'刪除教材代碼: ' + '<?php echo $data["book_no"]; ?>'+' ')">刪除</a></td>
            </tr>
<?php } ?>
          </table>
  </center>
    </div>
</body>

</html>
