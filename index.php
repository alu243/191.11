<?php
    require_once("./include/config.inc.php");
    session_destroy();
    $user_id = $_POST["user_id"];
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
</head>
<body onload="document.f.user_id.focus();">
    <form name="f" action="check_pw.php" method="post">
    <div style="margin: 0px auto;">
    <p align="center"></p>
    <p align="center">
        <font size="5" color="#008000">語言中心<br>行政處理系統</font>
    </p>
    <p align="center">
        <table border="0">
        <tr>
            <td>帳號：</td>
            <td>
                <input type="text" name="user_id" size="20" value=<?php echo $user_id;?>>
            </td>
        </tr>
        <tr>
            <td>密碼：</td>
            <td>
                <input type="password" name="user_pw" size="20">
            </td>
        </tr>
        <tr>
            <td>期別選擇：</td>
            <td><?php BuildSelectElementTerm(); ?></td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input type="submit" value="登入">
                <input type="reset" value="重設">
            </td>
        </tr>
        </table>
        <?php PrintErrMsg($err, $act, $user_id); ?>
    </p>
    </div>
    </form> 
</body>
</html>
<?php
    function BuildSelectElementTerm()
    {
        echo "                    <select name=\"term_uid\">";
        $rs = mysql_query("select * from term order by start_date desc;");
        while ($t = mysql_fetch_array($rs)) {
            echo "              <option value='".$t['no']."'>"."(".substr($t['start_date'],0,7)."~".substr($t['end_date'],0,7).")".$t['term']."</option>\n";
        }
        echo "                    </select>";
    }
    function PrintErrMsg($err, $act, $user_id)
    {
        if($err==1){echo "<font color=#FF0000>帳號或密碼輸入錯誤</font>";}
        if($err==2){echo "<font color=#FF0000>您沒有權限,請登入帳號密碼</font>";}
        if ($act=="logout")
        {
            //		$U_temp=$U_id;
            //		session_destroy();
            echo "<font color=#FF0000>".$user_id." 您已登出</font>";
        }
    }
?>