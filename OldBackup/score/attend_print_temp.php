<?php
/*
    Modified 
    部分陣列由序號改為名稱(全改好累)
    新增自訂製表日期欄位
*/
 include("../dbpw.php");
 if ($U_id<>"fjdp" && $U_id<>"lcgrade" && $U_id<>"lang"){
 	echo $U_id."你沒有此頁權限,請回到<a href=../list.php>主選單</a>";
 	exit;
}
?>
<?php require("../include/link_db"); ?>
<?php require("../include/function.php"); ?>
<?php
	$sql="select * from student where no='$no'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	$str_score = "select * from score where stu_no='$data[stu_no]'  order by `start_date`,`end_date` asc";
	$rt_score = mysql_query($str_score);
	$i=0;
	while ($data_score = mysql_fetch_array($rt_score)) {
		$i++;
		if($i==1){ $duration1y=$data_score[end_year]; $duration1m=$data_score[end_month]; $duration1d=$data_score[end_day];}
		$duration0y=$data_score[start_year];$duration0m=$data_score[start_month];$duration0d=$data_score[start_day];
	}
		
?>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>語言中心行政處理系統</title>
<script src="../include/function.js" language="JavaScript" type="text/JavaScript"></script>
</head>

<body>
<form name="form1" method="POST" action="attend_print3.php" target="_blank">
<input type="hidden" name="no" value="<?php echo $no;?>">
<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="690pt" id="AutoNumber1">
  <tr>
    <td width="190pt">　<br>　<br>
    </td>
    <td align="left">
    	<p align="center">　<br>　<br>
    	<font style="font-size: 19pt" face="標楷體"><b>私立輔仁大學附設語言中心</b></font><br>
    	<font style="font-size: 18pt" face="標楷體"><b>學生在學證明暨出缺席紀錄表</b></font><br>
    	<font style="font-size: 13pt" face="Times New Roman"><b>FU JEN CATHOLIC UNIVERSITY LANGUAGE CENTER</b></font><br>
    	<font style="font-size: 14pt" face="Times New Roman"><b>Student's Certificate of Registration &amp; Class Attendance Record</b></font>
    	</p>
    <div align="center">
      <center>
      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width:500pt" bordercolor="#111111" id="AutoNumber2">
        <tr>
          <td style="width: 10pt">　</td>
          <td valign="top" style="width: 160pt">
          	<font style="font-size: 10pt" face="標楷體">台北縣新莊市中正路510號</font><br>
          	<font style="font-size: 10pt" face="Times New Roman">510 Chung Cheng Road<br>Hsin Chuang, Taipei County<br>Taiwan, R.O.C.<br><br></font><font style="font-size: 10pt" face="標楷體">上課地點：校本部</font></td>
          <td valign="top" style="width: 120pt">
          	<p align="center">
          	<img border="0" src="logo.gif" width="60" height="71"></td>
          <td style="width: 20pt">　</td>
          <td valign="top" style="width: 190pt">
          	<font style="font-size: 10pt" face="標楷體">電話</font>
          	<font style="font-size: 10pt" face="Times New Roman">(Tel)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font>
          	<font style="font-size: 10pt" face="Times New Roman">886-2-29052414</font><br>
          	<font style="font-size: 10pt" face="標楷體">　　　　　</font>
          	<font style="font-size: 10pt" face="Times New Roman">&nbsp;&nbsp;886-2-29053721</font><br>
          	<font style="font-size: 10pt" face="標楷體">傳真</font>
          	<font style="font-size: 10pt" face="Times New Roman">(Fax)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font>
          	<font style="font-size: 10pt" face="Times New Roman">886-2-29052166</font><br>
          	<font style="font-size: 10pt" face="標楷體">電子郵件信箱</font>
          	<font style="font-size: 10pt" face="Times New Roman">(e-mail address)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font><br>
          	<font style="font-size: 10pt" face="Times New Roman">flcg1013@mails.fju.edu.tw</font></td>
        </tr>
        <tr>
          <td colspan="5" style="width: 500pt" >　</td>
        </tr>
        </table>
      </center>
    </div>
    <div align="center">
    <center>
      <table style="border-collapse: collapse; width:500pt" bordercolor="#000000" cellpadding="0" cellspacing="0" border="1">
        <tr>
          <td colspan="6" style="width: 500pt; height: 25pt" align="center" >
          	<font style="font-size: 14pt" face="標楷體">學生基本資料</font>
		<font style="font-size: 13pt" face="Times New Roman">(Student's Data)</font>
          </td>
        </tr>
        <tr>
          <td rowspan="2" style="width: 80pt; height: 40pt" align="center">
		<font style="font-size: 12pt" face="標楷體">學生姓名</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Student's Name</font></td>
          <td colspan="2" style="width: 140pt" align="left" >
          	<font style="font-size: 12pt" face="標楷體">中文</font>
		<font style="font-size: 10pt" face="Times New Roman">(in Chinese)</font></td>
          <td colspan="3" style="width: 260pt" align="center">
	  	<font style="font-size: 12pt" face="標楷體"><?php echo $data[name_ch]; ?></font></td>
        </tr>
        <tr>
          <td colspan="2" style="width: 140pt" align="left">
          	<font style="font-size: 12pt" face="標楷體">外文</font>
		<font style="font-size: 10pt" face="Times New Roman">(in Native Language)</font></td>
          <td colspan="3" style="width: 260pt" align="center">
		<font style="font-size: 12pt" face="Times New Roman"><?php echo id_to_enname($data[stu_no]); ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
		<font style="font-size: 12pt" face="標楷體">出生年月日</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Date of Birth</font></td>
          <td style="width: 90pt" align="center">
		<font style="font-size: 12pt" face="Times New Roman"><?php echo $data[birth_year]."/".$data[birth_month]."/".$data[birth_day]; ?></font></td>
          <td style="width: 70pt" align="center">
		<font style="font-size: 12pt" face="標楷體">國籍</font><br>
		<font style="font-size: 120t" face="Times New Roman">Nationality</font></td>
          <td style="width: 80pt" align="center">
		 <font style="font-size: 12pt" face="標楷體"><?php echo trans_nationality($data[nationality_no],1)."<br>";?></font><font style="font-size: 12pt" face="Times New Roman"><?php echo trans_nationality($data[nationality_no],2); ?></font></td>
          <td style="width: 60pt" align="center">
		<font style="font-size: 12pt" face="標楷體">性別</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Sex</font></td>
          <td style="width: 120pt" align="center">
		<font style="font-size: 12pt" face="標楷體"><?php echo $data[sex]."<br>";?></font><font style="font-size: 12pt" face="Times New Roman"><?php if ($data[sex]=="女"){echo "Female";}else if($data[sex]=="男"){echo "Male";}?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
		<font style="font-size: 12pt" face="標楷體">通訊地址</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Mailing Address</font></td>
          <td colspan="5" style="width: 420pt">
		 <font style="font-size: 12pt" face="標楷體"><?php echo "　".$data[address_taiwan]; ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt"  align="center">
            	<font style="font-size: 12pt" face="標楷體">電話號碼</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Telephone No.</font></td>
          <td colspan="2" style="width: 140pt" align="center">
            	<font style="font-size: 12pt" face="標楷體"><?php echo $data[tel_taiwan]; ?></font></td>
          <td style="width: 80pt" align="center">
            	<font style="font-size: 12pt" face="標楷體">行動電話號碼</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Cell Phone No.</font></td>
          <td colspan="2" style="width: 180pt" align="center">
            	<font style="font-size: 12pt" face="標楷體"><?php echo $data[cell_taiwan]; ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
            	<font style="font-size: 12pt" face="標楷體">抵台日期</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Date of arrival</font></td>
          <td style="width: 90pt" align="center">
          	<font style="font-size: 11pt" face="Times New Roman"><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[taiwan_year]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[taiwan_month];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[taiwan_day];?></font><font style="font-size: 11pt" face="標楷體">日</font><br><?php echo trans_month($data[taiwan_month])." ".$data[taiwan_day].",".$data[taiwan_year];?></font></td>
          <td colspan="2" style="width: 150pt"  align="center">
            		<font style="font-size: 12pt" face="標楷體">研習起訖時間</font><br>
            		<font style="font-size: 10pt" face="Times New Roman">Duration of Studies</font></td>
          <td colspan="2" style="width: 180pt">
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" height="100%"id="AutoNumber3">
            	<tr>
            	<td width="50%">
                  	<font style="font-size: 11pt" face="標楷體">&nbsp;自</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[school_year]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[school_month];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[school_day];?></font><font style="font-size: 11pt" face="標楷體">日</font><br>
                  	<font style="font-size: 10pt" face="Times New Roman">&nbsp;&nbsp;From&nbsp;&nbsp;<?php echo trans_simple_month($data[school_month])." ".$data[school_day].", ".$data[school_year];?></font></td>
                <td width="50%">
                  	<font style="font-size: 11pt" face="標楷體">至</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[leave_year]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[leave_month];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[leave_day];?></font><font style="font-size: 11pt" face="標楷體">日</font><br>
                  	<font style="font-size: 10pt" face="Times New Roman">&nbsp;to&nbsp;&nbsp;<?php echo trans_simple_month($data[leave_month])." ".$data[leave_day].", ".$data[leave_year];?></font></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
    </div>
    <div align="center">
    <center>
    
      <table style="border-collapse: collapse; width:550pt" bordercolor="#000000" cellpadding="5" cellspacing="0" border="1">
      <tr>
        <td colspan="5" style="width: 480pt" align="center">
        	<font style="font-size: 14pt" face="標楷體">各期上課出缺席紀錄</font>
        	<font style="font-size: 13pt" face="Times New Roman">(Class Attendance Records)</font></td>
        </tr>
      <tr>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">研習期間</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Period of Study</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">應上課時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Class<br>Hours Enrolled</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">請假時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Hours of<br>Excused Absences</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">實際上課時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Class<br>Hours Attended</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">備　　註</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Remarks</font></td>
        <td style="width: 50pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">往前合併數</font></td>
      </tr>
<?
	for ($i=0;$i<=10;$i++){
		for($j=1;$j<50;$j++){
			$input_data[$i][$j]="";
			$score_count[$i][$j]=0;
		}
	}
	
	$count=0;
	
	$str_score = "select * from score_old where stu_no='$data[stu_no]' order by `start_date`,`end_date` asc";
	$rt_score = mysql_query($str_score);
	while ($data_score = mysql_fetch_array($rt_score)) {
		$count=$count+1;
		$input_data[0][$count]=$data_score[15];/*start_date*/
		$input_data[1][$count]=$data_score[19];/*end_date*/
		$input_data[2][$count]=$data_score[6];/*hours_full*/
		$input_data[3][$count]=$data_score[7];/*hours_absence*/
		$input_data[4][$count]=$data_score[14];/*note */
		if ($input_data[2][$count]=="" && $input_data[3][$count]==""){
			$input_data[0][$count]=="";$input_data[1][$count]=="";$input_data[4][$count]=="";
			$count=$count-1;
		}
	}
	
	$str_score = "select * from score where stu_no='$data[1]' order by 'start_date','end_date' asc";
	$rt_score = mysql_query($str_score);
	while ($data_score = mysql_fetch_array($rt_score)) {
		$count=$count+1;
		$input_data[0][$count]=$data_score[15];/*start_date*/ //echo $data_score[15].".";
		$input_data[1][$count]=$data_score[19];/*end_date*/ //echo $data_score[19].".";
		$input_data[2][$count]=$data_score[6];/*hours_full*/ //echo $data_score[6].".";
		$input_data[3][$count]=$data_score[7];/*hours_absence*/ //echo $data_score[7].".";
		$input_data[4][$count]=$data_score[14];/*note */ //echo $data_score[14]."<br>";
		if ($input_data[2][$count]=="" && $input_data[3][$count]==""){
			$input_data[0][$count]=="";$input_data[1][$count]=="";$input_data[4][$count]=="";
			$count=$count-1;
		}
	}
	
	$i=0;
	$temp=1;
	while ($i<$count){
		$i=$i+1;
		$temp_chang=0;
		if ($input_data[0][$i]==$input_data[0][$i+1] && $input_data[1][$i]==$input_data[1][$i+1]){
			$input_data[2][$i]=$input_data[2][$i]+$input_data[2][$i+1];
			$input_data[3][$i]=$input_data[3][$i]+$input_data[3][$i+1];
			$input_data[4][$i]=$input_data[4][$i].$input_data[4][$i+1];
			$temp_chang=1;
		}//if ($input_data[0][$i]==$input_data[0][$i+1] && $input_data[1][$i]==$input_data[1][$i+1])
		if ($temp_chang==1){
			$j=$i;
			while ($j<$count){
				$j++;
				$input_data[0][$j]=$input_data[0][$j+1];/*start_date*/
				$input_data[1][$j]=$input_data[1][$j+1];/*end_date*/
				$input_data[2][$j]=$input_data[2][$j+1];/*hours_full*/
				$input_data[3][$j]=$input_data[3][$j+1];/*hours_absence*/
				$input_data[4][$j]=$input_data[4][$j+1];/*note */
			}//while ($j<$count)
			for ($k=0;$k<=4;$k++){	$input_data[$k][$count]="";}
			$count=$count-1;
			$i=$i-1;
		}//if ($temp_chang==1)
	}//while ($i<=$count) 結合重複的
	//echo $count;
	$i=1;
	//echo "*".$count;
	if ($count<=7){$temp_count=$count;$list_count=$count-1;}else{$temp_count=7;$list_count=6;}
	while ($i<=$temp_count){
		$temp=$count-$list_count;
		table_list($input_data,$temp,$i);
		$list_count=$list_count-1;
		$i++;
	}//while ($i<=$count) 列出出缺席
	$temp_i=$i;
	//if ($count<8){
		for($i=$temp_i;$i<=8;$i++){table_null($i);}
	//}//if ($count<8)
	page_tail($no);
?>
<?php
function table_list($input_data,$i,$j){ ?>
      <tr>
        <td align="center" style="width: 140pt; height: 30pt">
        	<input type="text" name="input1<?php echo $j;?>" value="<?php echo substr($input_data[0][$i],0,4)."/".substr($input_data[0][$i],5,2)."/".substr($input_data[0][$i],8)."-".substr($input_data[1][$i],0,4)."/".substr($input_data[1][$i],5,2)."/".substr($input_data[1][$i],8);?>" style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<input type="text" size="3" name="input2<?php echo $j;?>" value="<?php echo $input_data[2][$i];?>" style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<input type="text" size="3" name="input3<?php echo $j;?>" value="<?php echo $input_data[3][$i];?>" style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<input type="text" size="3" name="input4<?php echo $j;?>" value="<?php echo $input_data[2][$i]-$input_data[3][$i];?>" style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<input type="text" name="input5<?php echo $j;?>" value="<?php echo $input_data[4][$i];?>" style="font-size:12pt"></td>
        <td align="center" style="width: 50pt; height: 30pt">
        	<input type="text" size="3" name="input6<?php echo $j;?>" value="" style="font-size:12pt"></td>
      </tr>
<?php } ?>
<?php
function table_null($i){ ?>
      <tr>
        <td align="center" style="width: 140pt; height: 30pt"><input type="text" name="input1<?php echo $i;?>" value=""  style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt"><input type="text" size="3" name="input2<?php echo $i;?>" value=""  style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt"><input type="text" size="3" name="input3<?php echo $i;?>" value=""  style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt"><input type="text" size="3" name="input4<?php echo $i;?>" value=""  style="font-size:12pt"></td>
        <td align="center" style="width: 90pt; height: 30pt"><input type="text" name="input5<?php echo $i;?>" value=""  style="font-size:12pt"></td>
        <td align="center" style="width: 50pt; height: 30pt"><input type="text" size="3" name="input6<?php echo $i;?>" value=""  style="font-size:12pt"></td>
      </tr>
<?php }?>
<?php
function page_tail($no){ ?>
        <tr>
          <td colspan="6">製表日期：
            <input type="text" size="3" name="print_year" value="<?php echo Date("Y"); ?>"  style="font-size:12pt">年
            <input type="text" size="3" name="print_month" value="<?php echo Date("m"); ?>"  style="font-size:12pt">月
            <input type="text" size="3" name="print_day" value="<?php echo Date("d"); ?>"  style="font-size:12pt">日
          </td>
        </tr>
      </table>
    </center>
    </div>
    <p>往前合併數說明:<br>　1表將"實際上課時數"與"備註"合併<br>　2表將"請假時數"、"實際上課時數"與"備註"合併<br>　依此類推</p>
    <input type="submit" name="B1" value="產生列印表單">
    　　<a href="../student/student_modify.php?modify=1&mod_item=<?php echo $no;?>" target="_blank">修改學生基本資料</a>
    
    <div align="center">

    </div>
    </td>
  </tr>
</table>
</form>
<?php }?>



</body>

</html>