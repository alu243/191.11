<?php
 include("../dbpw.php");
 if ($U_id<>"fjdp" && $U_id<>"lcgrade" && $U_id<>"lang"){
 	echo $U_id."你沒有此頁權限,請回到<a href=../list.php>主選單</a>";
 	exit;
}
?>
<?php require("../include/link_db"); ?>
<?php require("../include/function.php"); ?>
<?php
	$sql="select * from student where no='$no'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	$str_score = "select * from score where stu_no='$data[1]'  order by 'start_date','end_date' asc";
	$rt_score = mysql_query($str_score);
	$i=0;
	while ($data_score = mysql_fetch_array($rt_score)) {
		$i++;
		if($i==1){ $duration1y=$data_score[20]; $duration1m=$data_score[21]; $duration1d=$data_score[22];}
		$duration0y=$data_score[16];$duration0m=$data_score[17];$duration0d=$data_score[18];
	}
		
?>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>語言中心行政處理系統</title>
<script src="../include/function.js" language="JavaScript" type="text/JavaScript"></script>
</head>

<body>

<table border="0" cellpadding="10" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="720pt" id="AutoNumber1">
  <tr>
    <td width="220pt">　<br>　<br>
    </td>
    <td align="left">
    	<p align="center">　<br>　<br>
    	<font style="font-size: 19pt" face="標楷體"><b>私立輔仁大學附設語言中心</b></font><br>
    	<font style="font-size: 18pt" face="標楷體"><b>學生在學證明暨出缺席紀錄表</b></font><br>
    	<font style="font-size: 13pt" face="Times New Roman"><b>FU JEN CATHOLIC UNIVERSITY LANGUAGE CENTER</b></font><br>
    	<font style="font-size: 14pt" face="Times New Roman"><b>Student's Certificate of Registration &amp; Class Attendance Record</b></font>
    	</p>
    <div align="center">
      <center>
      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width:500pt" bordercolor="#111111" id="AutoNumber2">
        <tr>
          <td style="width: 10pt">　</td>
          <td valign="top" style="width: 160pt">
          	<font style="font-size: 10pt" face="標楷體">台北縣新莊市中正路510號</font><br>
          	<font style="font-size: 10pt" face="Times New Roman">510 Chung Cheng Road<br>Hsin Chuang, Taipei County<br>Taiwan, R.O.C.<br><br></font><font style="font-size: 10pt" face="標楷體">上課地點：校本部</font></td>
          <td valign="top" style="width: 120pt">
          	<p align="center">
          	<img border="0" src="logo.gif" width="60" height="71"></td>
          <td style="width: 20pt">　</td>
          <td valign="top" style="width: 190pt">
          	<font style="font-size: 10pt" face="標楷體">電話</font>
          	<font style="font-size: 10pt" face="Times New Roman">(Tel)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font>
          	<font style="font-size: 10pt" face="Times New Roman">886-2-29031111ext.2414,3721</font><br>
          	<font style="font-size: 10pt" face="標楷體">傳真</font>
          	<font style="font-size: 10pt" face="Times New Roman">(Fax)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font>
          	<font style="font-size: 10pt" face="Times New Roman">886-2-29020981</font><br>
          	<font style="font-size: 10pt" face="標楷體">電子郵件信箱</font>
          	<font style="font-size: 10pt" face="Times New Roman">(e-mail address)</font>
          	<font style="font-size: 10pt" face="標楷體">：</font><br>
          	<font style="font-size: 10pt" face="Times New Roman">flcg1013@mails.fju.edu.tw</font></td>
        </tr>
        <tr>
          <td colspan="5" style="width: 500pt" >　</td>
        </tr>
        </table>
      </center>
    </div>
    <div align="center">
    <center>
      <table style="border-collapse: collapse; width:500pt" bordercolor="#000000" cellpadding="0" cellspacing="0" border="1">
        <tr>
          <td colspan="6" style="width: 500pt; height: 25pt" align="center" >
          	<font style="font-size: 14pt" face="標楷體">學生基本資料</font>
		<font style="font-size: 13pt" face="Times New Roman">(Student's Data)</font>
          </td>
        </tr>
        <tr>
          <td rowspan="2" style="width: 80pt; height: 40pt" align="center">
		<font style="font-size: 12pt" face="標楷體">學生姓名</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Student's Name</font></td>
          <td colspan="2" style="width: 140pt" align="left" >
          	<font style="font-size: 12pt" face="標楷體">中文</font>
		<font style="font-size: 10pt" face="Times New Roman">(in Chinese)</font></td>
          <td colspan="3" style="width: 260pt" align="center">
	  	<font style="font-size: 12pt" face="標楷體"><?php echo $data[2]; ?></font></td>
        </tr>
        <tr>
          <td colspan="2" style="width: 140pt" align="left">
          	<font style="font-size: 12pt" face="標楷體">外文</font>
		<font style="font-size: 10pt" face="Times New Roman">(in Native Language)</font></td>
          <td colspan="3" style="width: 260pt" align="center">
		<font style="font-size: 12pt" face="Times New Roman"><?php echo id_to_enname($data[1]); ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
		<font style="font-size: 12pt" face="標楷體">出生年月日</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Date of Birth</font></td>
          <td style="width: 90pt" align="center">
		<font style="font-size: 12pt" face="Times New Roman"><?php echo $data[8]."/".$data[9]."/".$data[10]; ?></font></td>
          <td style="width: 70pt" align="center">
		<font style="font-size: 12pt" face="標楷體">國籍</font><br>
		<font style="font-size: 120t" face="Times New Roman">Nationality</font></td>
          <td style="width: 80pt" align="center">
		 <font style="font-size: 12pt" face="標楷體"><?php echo trans_nationality($data[6],1)."<br>";?></font><font style="font-size: 12pt" face="Times New Roman"><?php echo trans_nationality($data[6],2); ?></font></td>
          <td style="width: 60pt" align="center">
		<font style="font-size: 12pt" face="標楷體">性別</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Sex</font></td>
          <td style="width: 120pt" align="center">
		<font style="font-size: 12pt" face="標楷體"><?php echo $data[5]."<br>";?></font><font style="font-size: 12pt" face="Times New Roman"><?php if ($data[5]=="女"){echo "Female";}else if($data[5]=="男"){echo "Male";}?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
		<font style="font-size: 12pt" face="標楷體">通訊地址</font><br>
		<font style="font-size: 10pt" face="Times New Roman">Mailing Address</font></td>
          <td colspan="5" style="width: 420pt">
		 <font style="font-size: 12pt" face="標楷體"><?php echo "　".$data[25]; ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt"  align="center">
            	<font style="font-size: 12pt" face="標楷體">電話號碼</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Telephone No.</font></td>
          <td colspan="2" style="width: 140pt" align="center">
            	<font style="font-size: 12pt" face="標楷體"><?php echo $data[26]; ?></font></td>
          <td style="width: 80pt" align="center">
            	<font style="font-size: 12pt" face="標楷體">行動電話號碼</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Cell Phone No.</font></td>
          <td colspan="2" style="width: 180pt" align="center">
            	<font style="font-size: 12pt" face="標楷體"><?php echo $data[27]; ?></font></td>
        </tr>
        <tr>
          <td style="width: 80pt; height: 30pt" align="center">
            	<font style="font-size: 12pt" face="標楷體">抵台日期</font><br>
            	<font style="font-size: 10pt" face="Times New Roman">Date of arrival</font></td>
          <td style="width: 90pt" align="center">
          	<font style="font-size: 11pt" face="Times New Roman"><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[35]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[36];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[37];?></font><font style="font-size: 11pt" face="標楷體">日</font><?php echo trans_month($data[36])." ".$data[37].",".$data[35];?></font></td>
          <td colspan="2" style="width: 150pt"  align="center">
            		<font style="font-size: 12pt" face="標楷體">研習起訖時間</font><br>
            		<font style="font-size: 10pt" face="Times New Roman">Duration of Studies</font></td>
          <td colspan="2" style="width: 180pt">
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" height="100%"id="AutoNumber3">
            	<tr>
            	<td width="50%">
                  	<font style="font-size: 11pt" face="標楷體">&nbsp;自</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[39]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[40];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[41];?></font><font style="font-size: 11pt" face="標楷體">日</font><br>
                  	<font style="font-size: 10pt" face="Times New Roman">&nbsp;&nbsp;From&nbsp;&nbsp;<?php echo trans_simple_month($data[40])." ".$data[41].", ".$data[39];?></font></td>
                <td width="50%">
                  	<font style="font-size: 11pt" face="標楷體">至</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[43]-1911;?></font><font style="font-size: 11pt" face="標楷體">年</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[44];?></font><font style="font-size: 11pt" face="標楷體">月</font><font style="font-size: 11pt" face="Times New Roman"><?php echo $data[45];?></font><font style="font-size: 11pt" face="標楷體">日</font><br>
                  	<font style="font-size: 10pt" face="Times New Roman">&nbsp;to&nbsp;&nbsp;<?php echo trans_simple_month($data[44])." ".$data[45].", ".$data[43];?></font></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
    </div>
    <div align="center">
    <center>
      <table style="border-collapse: collapse; width:500pt" bordercolor="#000000" cellpadding="5" cellspacing="0" border="1">
      <tr>
        <td colspan="5" style="width: 480pt" align="center">
        	<font style="font-size: 14pt" face="標楷體">各期上課出缺席紀錄</font>
        	<font style="font-size: 13pt" face="Times New Roman">(Class Attendance Records)</font></td>
        </tr>
      <tr>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">研習期間</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Period of Study</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">應上課時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Class<br>Hours Enrolled</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">請假時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Hours of<br>Excused Absences</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">實際上課時數</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Number of Class<br>Hours Attended</font></td>
        <td style="width: 100pt; height: 40pt" align="center">
        	<font style="font-size: 12pt" face="標楷體">備　　註</font><br>
        	<font style="font-size: 10pt" face="Times New Roman">Remarks</font></td>
      </tr>
<?php
	$str_score = "select * from score where stu_no='$data[1]' order by 'start_date','end_date' asc";
	$rt_score = mysql_query($str_score);
	$initial=1;
	$count=0;
	while ($data_score = mysql_fetch_array($rt_score)) {
		if($initial==1){
			$input_data0=$data_score[15];
			$input_data1=$data_score[19];
			$input_data2=$data_score[6];
			$input_data3=$data_score[7];
			$input_data4=$data_score[14];
			$initial=0;
		}else if($data_score[15]==$input_data0 && $data_score[19]==$input_data1){
			$input_data2=$input_data2+$data_score[6];
			$input_data3=$input_data3+$data_score[7];
			if (!empty($data_score[14])){$input_data4=$input_data4.",".$data_score[14];}
		}else{
			$count++;
			//if ($input_data2<>"" && $input_data3<>""){
		
?>
      <tr>
        <td align="center" style="width: 140pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo substr($input_data0,0,4)."/".substr($input_data0,5,2)."/".substr($input_data0,8)."-".substr($input_data1,0,4)."/".substr($input_data1,5,2)."/".substr($input_data1,8);?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data2;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data3;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data2-$input_data3;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="標楷體"><?php echo $input_data4;?></font></td>
      </tr>
<?php
			//}
			$input_data0=$data_score[15];
			$input_data1=$data_score[19];
			$input_data2=$data_score[6];
			$input_data3=$data_score[7];
			$input_data4=$data_score[14];
		}
	}
	if ($count<>0 or initial<>1){
		$count++;
		//if ($input_data2<>"" && $input_data3<>""){
?>
      <tr>
        <td align="center" style="width: 140pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo substr($input_data0,0,4)."/".substr($input_data0,5,2)."/".substr($input_data0,8)."-".substr($input_data1,0,4)."/".substr($input_data1,5,2)."/".substr($input_data1,8);?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data2;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data3;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="Times New Roman"><?php echo $input_data2-$input_data3;?></font></td>
        <td align="center" style="width: 90pt; height: 30pt">
        	<font style="font-size: 12pt" face="標楷體"><?php echo $input_data4;?></font></td>
      </tr>
<?php
		//}
	}
	for ($i=$count;$i<=8;$i++){
?>
      <tr>
        <td align="center" style="width: 140pt; height: 30pt">　</td>
        <td align="center" style="width: 90pt; height: 30pt">　</td>
        <td align="center" style="width: 90pt; height: 30pt">　</td>
        <td align="center" style="width: 90pt; height: 30pt">　</td>
        <td align="center" style="width: 90pt; height: 30pt">　</td>
      </tr>
<?php
	}
	echo $count;
?>
      </table>
    </center>
    </div>
    <div align="center">
      <center>
      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 500pt; height:60pt" bordercolor="#111111" id="AutoNumber4">
        <tr>
          <td style="width: 70pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"></td>
          <td style="width: 130pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"></td>
        </tr>
        <tr>
          <td style="width: 70pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"></td>
          <td style="width: 130pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"></td>
        </tr>
        <tr>
          <td style="width: 70pt; height: 12pt">
          	<font style="font-size: 12pt" face="標楷體">主任簽名：</font></td>
          <td style="width: 150pt; height: 12pt"></td>
          <td style="width: 130pt; height: 12pt" align="right">
          	<font style="font-size: 12pt" face="標楷體">製發日期：</font></td>
          <td style="width: 150pt; height: 12pt" align="center">
          	<font style="font-size: 12pt"><font face="Times New Roman"><?php echo Date("Y");?></font><font face="標楷體">年</font><font face="Times New Roman"><?php echo Date("m");?></font><font face="標楷體">月</font><font face="Times New Roman"><?php echo Date("d");?></font><font face="標楷體">日</font></font></td>
        </tr>
        <tr>
          <td style="width: 70pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"><hr noshade color="#000000" size="1"></td>
          <td style="width: 130pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt"><hr noshade color="#000000" size="1"></td>
        </tr>
        <tr>
          <td style="width: 70pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt">
          <p align="center">
          	<font style="font-size: 10pt" face="Times New Roman">Signature of Director</font></td>
          <td style="width: 130pt; height: 12pt"></td>
          <td style="width: 150pt; height: 12pt">
          	<p align="center">
          	<font style="font-size: 10pt" face="Times New Roman">Date of issue</font></td>
        </tr>
      </table>
      </center>
    </div>
    </td>
  </tr>
</table>

</body>

</html>