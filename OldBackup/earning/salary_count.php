<?php
 include("../dbpw.php");
 if ($U_id<>"lcadmin" && $U_id<>"lang"){
 	echo $U_id."你沒有此頁權限,請回到<a href=../list.php>主選單</a>";
 	exit;
}
?>
<?php 
	require("../include/link_db");
	include("../include/function.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
</head>

<body bgcolor="#FFFFFF" text="#000000">
<p align="left">您現在所在位置：<font color="#FF9900">薪資計算</font></p>
<p><a href="../list.php">回上一頁</a></p>
<form name="form1" method="post" action="salary_count.php">
  <select name="s_year">
  <?php
   	for ($i=2003 ; $i<2010 ; $i++) {
   		echo "<option value='$i'".($i == date("Y")?" selected":"").">$i</option>";
   	}
  ?>
  </select>
  年
  <select name="s_month">
  <?php
   	for ($i=1 ; $i<=12 ; $i++) {
   		echo "<option value='$i'".($i == date("m")?" selected":"").">$i</option>";
   	}
  ?>
  </select>
  月
  <input type="submit" name="Submit" value="計算">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "現在檢視月份： ".$s_year." 年 ".$s_month." 月"; ?>
</form>
<table border="1">
  <tr> 
    <td><font face="標楷體" size="-1">代碼</font></td>
    <td><font face="標楷體" size="-1">教師<br>名稱</font></td>
    <td><font face="標楷體" size="-1">基本<br>時薪</font></td>
    <td><font face="標楷體" size="-1">單人班</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">C1</font></td>
    <td><font face="標楷體" size="-1">2-3<br>人班</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">C23</font></td>
    <td><font face="標楷體" size="-1">4-7<br>人班</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">C47</font></td>
    <td><font face="標楷體" size="-1">Lab<br>課程</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">Lab</font></td>
    <td><font face="標楷體" size="-1">書法<br>國畫</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">S12</font></td>
    <td><font face="標楷體" size="-1">中國結</font></td>
    <td><font face="標楷體" size="-1">請假</font></td>
    <td><font face="標楷體" size="-1">代課</font></td>
    <td><font face="標楷體" size="-1">S3</font></td>
    <td><font face="標楷體" size="-1">備註</font></td>
  </tr>
<?php
	$money_level = mysql_fetch_array(mysql_query("select * from salary_level"));

	$str1 = "select center_no from member order by center_no asc";
	$rt1 = mysql_query($str1);
	while (list($tea_no) = mysql_fetch_row($rt1)) {
		$str = "select name_ch, salary_ahour from member where center_no = '$tea_no'";
		list($salary_data['tea_name'],$salary_data['salary_clock']) = mysql_fetch_row(mysql_query($str));
		$total_hours = get_work_hours($s_year, $s_month, $tea_no, $link);


		$total_money['C1']['total'] = $total_hours['C1']['total'] * $salary_data['salary_clock'];
		$total_money['C1']['ab'] = $total_hours['C1']['ab'] * $salary_data['salary_clock'];
		$total_money['C1']['sp'] = $total_hours['C1']['sp'] * $salary_data['salary_clock'];

		$total_money['C23']['total'] = $total_hours['C23']['total'] * ($salary_data['salary_clock']+$money_level['C23']);
		$total_money['C23']['ab'] = $total_hours['C23']['ab'] * ($salary_data['salary_clock']+$money_level['C23']);
		$total_money['C23']['sp'] = $total_hours['C23']['sp'] * ($salary_data['salary_clock']+$money_level['C23']);

		$total_money['C47']['total'] = $total_hours['C47']['total'] * ($salary_data['salary_clock']+$money_level['C47']);
		$total_money['C47']['ab'] = $total_hours['C47']['ab'] * ($salary_data['salary_clock']+$money_level['C47']);
		$total_money['C47']['sp'] = $total_hours['C47']['sp'] * ($salary_data['salary_clock']+$money_level['C47']);

		$total_money['Lab']['total'] = $total_hours['Lab']['total'] * $money_level['Lab'];
		$total_money['Lab']['ab'] = $total_hours['Lab']['ab'] * $money_level['Lab'];
		$total_money['Lab']['sp'] = $total_hours['Lab']['sp'] * $money_level['Lab'];

		$total_money['S12']['total'] = $total_hours['S12']['total'] * $money_level['S12'];
		$total_money['S12']['ab'] = $total_hours['S12']['ab'] * $money_level['S12'];
		$total_money['S12']['sp'] = $total_hours['S12']['sp'] * $money_level['S12'];

		$total_money['S3']['total'] = $total_hours['S3']['total'] * $money_level['S3'];
		$total_money['S3']['ab'] = $total_hours['S3']['ab'] * $money_level['S3'];
		$total_money['S3']['sp'] = $total_hours['S3']['sp'] * $money_level['S3'];
?>
  <tr> 
    <td><font face="標楷體" size="-1"><?php echo $tea_no; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $salary_data['tea_name']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $salary_data['salary_clock']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C1']['total']."->".$total_money['C1']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C1']['ab']."->".$total_money['C1']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C1']['sp']."->".$total_money['C1']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C23']['total']."->".$total_money['C23']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C23']['ab']."->".$total_money['C23']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C23']['sp']."->".$total_money['C23']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C47']['total']."->".$total_money['C47']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C47']['ab']."->".$total_money['C47']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['C47']['sp']."->".$total_money['C47']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['Lab']['total']."->".$total_money['Lab']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['Lab']['ab']."->".$total_money['Lab']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['Lab']['sp']."->".$total_money['Lab']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S12']['total']."->".$total_money['S12']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S12']['ab']."->".$total_money['S12']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S12']['sp']."->".$total_money['S12']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S3']['total']."->".$total_money['S3']['total']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S3']['ab']."->".$total_money['S3']['ab']; ?></font></td>
    <td><font face="標楷體" size="-1"><?php echo $total_hours['S3']['sp']."->".$total_money['S3']['sp']; ?></font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
    <td><font face="標楷體" size="-1">&nbsp;</font></td>
  </tr>
<?php	} ?>
</table>
</body>
</html>


<?php
/*
   時數的計數
   	該月所處學期及日期
   		在 (X1 年 X2 月)，需換成 (Y1) 或 (Y1 & Y2) 期
   		(也許有 Y1 及 Y2 的前 YY 天不能算，因為包含加退選)
	單周授課時數
   		找出該 (Y1 or Y2) 學期，該 T 教師的授課列表(C1, C2, ...)
   		然後就可以參照如大課表般的計算方式找出該課(C1 or C2 or ....)的學生人數(CS1)
   		如此就可以判斷一周內該課為 NN 人班或特殊課程的時數
   	時數判斷
   		有了單週時數列表，就可以判斷出該月的總時數
   		扣掉請假，扣掉放假，加上代課時數(分類過的)
   		最後再加上或扣掉加退選前的時數(必須由對方自行先記錄或計算)
   		然後再扣掉就是總時數
   		
   	總薪資計算
   		資料不見了...就這樣......

一(副程式：輸入年月，回傳當時學期(可能有兩個以上)) OK
二(副程式：輸入學期及教師代碼，回傳單周授課時間字串)(ex:00111000000000011111)
三(副程式：輸入年月及教師代碼，回傳當月應授課時數
四(副程式：字串運算合併，字串內容內含有'1'的計算) OK
(常數：加退選前 YY 天)

代課時要再加上考慮XX人班或是特殊班的問題，因為代課不是一定代一整天
(解決方法..加上小時標記)--完成

// 尚未計算代課教師的時間
// 尚未計算放假

*/

function get_term_by_month ($year, $month, $alink) { // 傳入年月，回傳此月份內所有的期別
	$date1 = $year."-".$month;
	$str = "select term, start_date, end_date from term
		where SUBSTRING(start_date,1,7) <= '$date1' and SUBSTRING(end_date,1,7) >= '$date1'";
	$rt = mysql_query($str,$alink);
	$tm['count'] = mysql_num_rows($rt);
	for($i=0 ; $i<mysql_num_rows($rt) ; $i++) list($tm[$i]['term'], $tm[$i]['start_date'], $tm[$i]['end_date']) = mysql_fetch_row($rt);
	return $tm;
}
function get_work_hours ($year, $month, $tid, $alink) { // 輸入'年' '月' 及 '教師代碼'，回傳所有類別時數
	// 學期數在一個以上時
	$terms = get_term_by_month($year, $month, $alink);

	$total_hours['C1']['total']  = 0;
	$total_hours['C23']['total'] = 0;
	$total_hours['C47']['total'] = 0;
	$total_hours['Lab']['total'] = 0;
	$total_hours['S12']['total'] = 0;
	$total_hours['S3']['total']  = 0;

	for ($i=0;$i<$terms['count'];$i++) {

		// 判斷後取得正確此月份此學期開始計算薪資的日期
		strtotime($terms[$i]['start_date']) > mktime(0,0,0,$month,1,$year) ? $ts_date=$terms[$i]['start_date']:$ts_date=$year."-".$month."-01";
		// 判斷後取得正確此月份此學期結束計算薪資的日期
		strtotime($terms[$i]['end_date']) < mktime(0,0,0,$month+1,0,$year) ? $te_date=$terms[$i]['end_date']:$te_date=date("Y-m-d", mktime(0,0,0,$month+1,0,$year));
		$day_week = get_week_days($ts_date, $te_date); // 取得周一至周五各有幾天
		$time_week = get_week_hours($terms[$i]['term'], $tid, $alink); // 取得此學期各課程在周一至周五的時間字串
		//echo $terms[$i]['term']."<br>";
		for ($j=1;$j<=5;$j++) {
			//echo "days=".$day_week[$j]."<br>";
			$total_hours['C1']['total']  += $day_week[$j] * substr_count(  substr( $time_week['C1']['times'],($j-1)*4,4 ),"1"  );
			$total_hours['C23']['total'] += $day_week[$j] * substr_count(  substr( $time_week['C23']['times'],($j-1)*4,4 ),"1"  );
			$total_hours['C47']['total'] += $day_week[$j] * substr_count(  substr( $time_week['C47']['times'],($j-1)*4,4 ),"1"  );
			$total_hours['Lab']['total'] += $day_week[$j] * substr_count(  substr( $time_week['Lab']['times'],($j-1)*4,4 ),"1"  );
			$total_hours['S12']['total'] += $day_week[$j] * substr_count(  substr( $time_week['S12']['times'],($j-1)*4,4 ),"1"  );
			$total_hours['S3']['total']  += $day_week[$j] * substr_count(  substr( $time_week['S3']['times'],($j-1)*4,4 ),"1"  );
			//echo $total_hours['C1']."C1==".$total_hours['C23']."C23==".$total_hours['C47']."C47==".
			//	$total_hours['Lab']."Lab==".$total_hours['S13']."S13==".$total_hours['S4']."S4<br>";
		}
	}
	$absence_hours = get_absence_hours($year, $month, $tid, "apply", $alink);
	$total_hours['C1']['ab']  = $absence_hours['C1'];
	$total_hours['C23']['ab'] = $absence_hours['C23'];
	$total_hours['C47']['ab'] = $absence_hours['C47'];
	$total_hours['Lab']['ab'] = $absence_hours['Lab'];
	$total_hours['S12']['ab'] = $absence_hours['S12'];
	$total_hours['S3']['ab']  = $absence_hours['S3'];

	$supply_hours = get_absence_hours($year, $month, $tid, "supply", $alink);
	$total_hours['C1']['sp']  = $supply_hours['C1'];
	$total_hours['C23']['sp'] = $supply_hours['C23'];
	$total_hours['C47']['sp'] = $supply_hours['C47'];
	$total_hours['Lab']['sp'] = $supply_hours['Lab'];
	$total_hours['S12']['sp'] = $supply_hours['S12'];
	$total_hours['S3']['sp']  = $supply_hours['S3'];
	return $total_hours;
}
function get_week_days ($s_date, $e_date) { // 輸入開始及結束日期，回傳此期間內周一至周五各有幾天
	$d_week['1'] = 0;
	$d_week['2'] = 0;
	$d_week['3'] = 0;
	$d_week['4'] = 0;
	$d_week['5'] = 0;
	$range_day = (strtotime($e_date) - strtotime($s_date)) / 86400;
	for ($i=0 ; $i <= $range_day ; $i++) {
		$temp = date("l",strtotime($s_date)+$i*60*60*24);

		$temp_date = date("Y-m-d",strtotime($s_date)+$i*60*60*24);

		if (is_holiday($temp_date)) continue; // 跳過學校放假不計算當日課程時數

		if ($temp == "Monday") $d_week['1']++;
		else if ($temp == "Tuesday") $d_week['2']++;
		else if ($temp == "Wednesday") $d_week['3']++;
		else if ($temp == "Thursday") $d_week['4']++;
		else if ($temp == "Friday") $d_week['5']++;
	}
	return $d_week;
}
function get_week_hours ($tm, $tid, $alink) { // $tm = $term
	// 班級依資料庫中的 course 表的代碼來分
	// CXXX=語言課程，分單人班，2~3人班，4~7人班
	// LXXX=Lab課程
	// S001~S003=文化課程
	// S004~...=特殊課程

	// 語言課程1人班=C1 (特殊課程同)
	// 語言課程2~3人班=C23 (特殊課程同)
	// 語言課程4~7人班=C47 (特殊課程同)
	// Lab課程=Lab
	// 文化課程=S12
	// 文化課程=S4
	// 文化課程的上課時間 = $w_hours['1']['S12']=...

	$week_hours['C1']['times']  = "00000000000000000000";
	$week_hours['C23']['times'] = "00000000000000000000";
	$week_hours['C47']['times'] = "00000000000000000000";
	$week_hours['Lab']['times'] = "00000000000000000000";
	$week_hours['S12']['times'] = "00000000000000000000";
	$week_hours['S3']['times']  = "00000000000000000000";

	$str = "select group2.`group`, group2.course, group2.times, count(class.group2) from group2, class
		where group2.`group` = class.group2
		and group2.term = class.term
		and group2.term = '$tm'
		and group2.teacher = '$tid'
		group by group2.`group`
		order by group2.`course` asc";
	$rt = mysql_query($str);
	for($i = 0 ; $i < mysql_num_rows($rt) ; $i++) {
		list($group_id, $g_course, $g_times, $people_no) = mysql_fetch_row($rt); // 至此取得了一位教師在一堂之所有語言課程，接下來要分人數班別
		//echo "id=".$g_course." ".$group_id."<br>times=".$g_times."<br>人數=".$people_no."<br><br>";
		switch ($g_course['0']) {
			case 'C': // 語言課程
				if ($people_no == 1)
					$week_hours['C1']['times'] = str_merge($week_hours['C1']['times'],$g_times);
				else if ($people_no >= 2 && $people_no <= 3)
					$week_hours['C23']['times'] = str_merge($week_hours['C23']['times'],$g_times);
				else if ($people_no >= 4 && $people_no <= 7)
					$week_hours['C47']['times'] = str_merge($week_hours['C47']['times'],$g_times);
				break;
			case 'L': // Lab 課程
				$week_hours['Lab']['times'] = str_merge($week_hours['Lab']['times'],$g_times);
				break;
			case 'S':
				switch ($g_course) {
					case 'S001': // 文化課程：書法、國畫
					case 'S002':
						$week_hours['S12']['times'] = str_merge($week_hours['S12']['times'],$g_times);
						break;
					case 'S003': // 文化課程：中國結
						$week_hours['S3']['times'] = str_merge($week_hours['S3']['times'],$g_times);
						break;
					default: // 特殊課程
						if ($people_no == 1)
							$week_hours['C1']['times'] = str_merge($week_hours['C1']['times'],$g_times);
						else if ($people_no >= 2 && $people_no <= 3)
							$week_hours['C23']['times'] = str_merge($week_hours['C23']['times'],$g_times);
						else if ($people_no >= 4 && $people_no <= 7)
							$week_hours['C47']['times'] = str_merge($week_hours['C47']['times'],$g_times);
				}							
				break;
		}
	}
	return $week_hours;
}
function str_merge($str1, $str2) { // 字串合併
	$temp_str = "00000000000000000000";
	for ($i=0;$i<20;$i++)
		empty($str2[$i])?$temp_str[$i]=$str1[$i]:$temp_str[$i]=$str2[$i];
	return $temp_str;
}
function get_absence_hours ($year, $month, $tid, $mode, $alink) { // 取得請假或代課當月總時數
	$ab_hours['C1']  = 0;
	$ab_hours['C23'] = 0;
	$ab_hours['C47'] = 0;
	$ab_hours['Lab'] = 0;
	$ab_hours['S12'] = 0;
	$ab_hours['S3']  = 0;

	$date1 = date("Y-m", mktime(0,0,0,$month,1,$year));

	if ($mode == "apply") { // 為請假時
		$str = "select absence.supplicant, absence_course.* from absence_course, absence
			where substring(absence_course.date,1,7) = '$date1'
			and absence.no = absence_course.absence_no
			and absence.applicant not like absence.supplicant
			and absence.applicant = '$tid'
			order by absence_course.date asc";
	}
	else if ($mode == "supply") { // 為代課時
		$str = "select absence.supplicant, absence_course.* from absence_course, absence
			where substring(absence_course.date,1,7) = '$date1'
			and absence.no = absence_course.absence_no
			and absence.applicant not like absence.supplicant
			and absence.supplicant = '$tid'
			order by absence_course.date asc";
	}


	$rt = mysql_query($str); // 在每一天內，所指定的請假(代課)者(教師)影響到的課程代碼(已扣除自行補課的部分)
	while ($abc_groups = mysql_fetch_array($rt)) {

		if (is_holiday($abc_groups['date'])) continue; // 跳過學校放假不計算當日課程時數

		for ($i=1;$i<=4;$i++) {
			if ($abc_groups['gpno'.$i]) {
				$str = "select group2.course, count(class.group2) from group2, class
					where group2.term = class.term
					and group2.`group` = class.group2
					and group2.no = '".$abc_groups['gpno'.$i]."'
					group by class.group2";
				$gp = mysql_fetch_array(mysql_query($str));

				switch ($gp['0']['0']) {
					case 'C': // 語言課程
						if ($gp['1'] == 1) $ab_hours['C1']++;
						else if ($gp['1'] >= 2 && $gp['1'] <=3) $ab_hours['C23']++;
						else if ($gp['1'] >= 4 && $gp['1'] <=7) $ab_hours['C47']++;
						break;
					case 'L': // Lab 課程
						$ab_hours['Lab']++;
						break;
					case 'S':
						switch ($gp['0']) {
							case 'S001': // 文化課程：書法、國畫
							case 'S002':
								$ab_hours['S12']++;
								break;
							case 'S003': // 文化課程：中國結
								$ab_hours['S3']++;
								break;
							default: // 特殊課程與語言課程計算方式相同
								if ($gp['1'] == 1) $ab_hours['C1']++;
								else if ($gp['1'] >= 2 && $gp['1'] <=3) $ab_hours['C23']++;
								else if ($gp['1'] >= 4 && $gp['1'] <=7) $ab_hours['C47']++;
								break;
						}
						break;
				}		
			}
		}
	}
	return $ab_hours;
}

function is_holiday ($date) { // 假日
	list($match) = mysql_fetch_row(mysql_query("select hdate from holiday where hdate = '$date'"));
	//if ($match) echo $match;
	return $match;
}

/*
$week_time['1']  = "1___________________"; $week_time['2']  = "_____1______________";
$week_time['3']  = "__________1_________"; $week_time['4']  = "_______________1____"; // 周一
$week_time['5']  = "_1__________________"; $week_time['6']  = "______1_____________";
$week_time['7']  = "___________1________"; $week_time['8']  = "________________1___"; // 周二
$week_time['9']  = "__1_________________"; $week_time['10'] = "_______1____________";
$week_time['11'] = "____________1_______"; $week_time['12'] = "_________________1__"; // 周三
$week_time['13'] = "___1________________"; $week_time['14'] = "________1___________";
$week_time['15'] = "_____________1______"; $week_time['16'] = "__________________1_"; // 周四
$week_time['17'] = "____1_______________"; $week_time['18'] = "_________1__________";
$week_time['19'] = "______________1_____"; $week_time['20'] = "___________________1"; // 周五
*/
?>
