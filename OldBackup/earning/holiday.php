<?php
 include("../dbpw.php");
 if ($U_id<>"lcadmin" && $U_id<>"lang"){
 	echo $U_id."你沒有此頁權限,請回到<a href=../list.php>主選單</a>";
 	exit;
}
?>
<?php 
	require("../include/link_db");
	include("../include/function.php");
?>
<?php
// 是一個 array ，儲存的資料為直接從假單的資料庫取出來的
// 目的為顯示資料，作為修改用
	if (!empty($year) && !empty($month) && !empty($day))
		$holiday_date = $year."-".$month."-".$day;

	if ($act == "delete" && !empty($no)) { // 刪除
		$str = "delete from holiday where no = '$no'";
		mysql_query($str) or die("刪除預定假日時發生錯誤<br>".$str);
	}
	else if ($act == "addnew" && !empty($holiday_date)) { // 新增
		$str = "insert into holiday set
			hdate = '$holiday_date' ,
			ps = '$_POST[ps]'";
		mysql_query($str) or die("新增預定假日時發生錯誤<br>".$str);
	}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>語言中心行政處理系統</title>
<script src="../include/function.js" language="JavaScript" type="text/JavaScript">
</script>

<script>
function change_act(no,str) {
	document.behave.act.value = str;
	document.behave.no.value = no;
	document.behave.submit();
}
function validation(data, mindata, maxdata) {
	if (data >= mindata && data <= maxdata) {
		return data;
	}
	else {
		alert("輸入的資料不符合格式，將以預設值代替");
		return mindata;
	}
}
</script>

</head>

<body>
<p align="left">您現在所在位置：<font color="#FF9900">行事曆管理</font></p>
<a href="../list.php">回上一頁</a>
<form name="update_form" method="post" action="">
  <table border="1" align="center" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
    <tr> 
      <td align="center" bgcolor="#E6FFEB"> 西元 
        <input name="year" type="text" size="4" maxlength="4" value='<?php echo $mod_ab['begin_year']; ?>' onChange="this.value=validation(this.value, 1999, 2020)">
        年 
        <input name="month" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_month']; ?>' onChange="this.value=validation(this.value, 01, 12)">
        月 
        <input name="day" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_day']; ?>' onChange="this.value=validation(this.value, 01, 31)">
        日 </td>
      <td align="center" bgcolor="#E6FFEB">備註：
        <input type="text" name="ps" size="17">
      </td>
      <td align="center" bgcolor="#E6FFEB"> 
        <input type='hidden' name='act' value='addnew'>
        <input type='submit' name='Submit' value='確定新增'>
      </td>
    </tr>
  </table>
</form>


<form name="behave" method ="post" action="holiday.php">
<table height="27" border="1" align="center" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
  <tr align="center" bgcolor="#E6FFEB"> 
    <td>日期</td>
    <td>備註</td>
    <td>刪除</td>
  </tr>
  <?php print_all_holiday($link); ?>
</table>
</form>





</body>

</html>

<?php
// 印出所有假日
function print_all_holiday($alink)
{
	$rt = mysql_query("select * from holiday order by hdate asc",$alink);

	if (mysql_num_rows($rt) > 0) {
		// 把所有假日的名稱及代碼存入陣列
		for ($i=1 ; $i <= mysql_num_rows($rt) ; $i++)
			$all_holiday[$i]=mysql_fetch_array($rt);

		// 列出所有假日
		foreach($all_holiday as $v) {
			echo "<tr>";
			echo "<td align='center'>".date("Y年m月d日", strtotime($v['hdate']))."</td>";
			echo "<td align='center'>".$v['ps']."</td>";
			echo "<td align='center'><input name='del' type='button' value='刪除資料'
				onClick=\"if (confirm('你確定要刪除此筆資料嗎？')) change_act('".$v['no']."', 'delete');\">\n";
			echo "</tr>";
		}
		echo "<input type='hidden' name='no' value=''>\n";
		echo "<input type='hidden' name='act' value=''>\n";
	}	
	return;
}
?>