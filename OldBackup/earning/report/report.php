<?php
// 分頁尚未實作出來
?>
<?php
    // 下載試作...
if ($down == '1') {
    $filename = date("Y_m_d")."_請假統計";
    $ext = 'doc';
    $mime_type = 'application/octetstream';

    $now = gmdate('D, d M Y H:i:s') . ' GMT';

    //header('Content-Type: '.$mime_type);
    header('Content-Type: '.$mime_type.'; charset=big5');
    header('Expires: ' . $now);

    header('Content-Disposition: inline; filename="' . $filename . '.' . $ext . '"');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
}
?>

<?php require("../../include/link_db"); ?>
<?php require("../../include/function.php"); ?>


<?php
include("../../include/class.FastTemplate.php3");

$tpl = new FastTemplate("./");

$tpl->define(array(main => "main.tpl"));

$tpl->define_dynamic("row", "main");

$tpl->assign(AB_MONTH,$rmonth);

////////////////////////////////////////
$weekno['Monday']=1;
$weekno['Tuesday']=2;
$weekno['Wednesday']=3;
$weekno['Thursday']=4;
$weekno['Friday']=5;

$absence_type['1']="病假";
$absence_type['2']="事假";
$absence_type['3']="公假";
$absence_type['4']="產假";
$absence_type['5']="喪假";

$total_money1 = 0; // 總扣薪
$total_money2 = 0; // 總加薪

if ($rmonth < 10 && $rmonth > 0) $rmonth = "0".$rmonth;
$str = "select absence.type, absence.applicant, absence.supplicant, absence.hours, absence.ps, absence_course.* from absence_course, absence
	where absence.no = absence_course.absence_no
	and substring(absence_course.`date`, 1, 7) = concat('$ryear','-','$rmonth')
	order by absence.applicant ,absence_course.`date`";
$rt = mysql_query($str);
while ($ab = mysql_fetch_array($rt)) {
	if (is_holiday($ab['date'])) continue;
	list($name) = mysql_fetch_row(mysql_query("select name_ch from member where center_no = '$ab[applicant]'"));
	//list($type) = mysql_fetch_row(mysql_query("select type from absence where no = '$ab[absence_no]'"));

	$dd = substr($ab['date'],5);
	$tpl->assign(AB_DATE, $dd);
	$tpl->assign(AB_WEEK, $weekno[date( "l",strtotime($ab['date']) )]);
	$tpl->assign(AB_NAME, $name);
	$tpl->assign(AB_TYPE, $absence_type[$ab['type']]);

	// 課程分類
	// $C1 == 單人班
	// $C23 == 2~3人合班
	// $C47 == 4~7人合班
	// $Lab == Lab 課程
	// $S12 == 書法、國畫(文化課程)
	// $S3 == 中國結(文化課程)
	// 特殊課程與語言課程算法一樣
	$C1 = 0;
	$C23 = 0;
	$C47 = 0;
	$Lab = 0;
	$S12 = 0;
	$S3 = 0;
	$hours = 0; // 總時數
	for ($i=1;$i<=4;$i++) { // 計算該課人數順便分類
		$course[$i]['people'] = 0;
		if ($ab['gpno'.$i]) {
			$hours++;
			$ab_gpno = $ab['gpno'.$i];
			list($course[$i]['people'], $course[$i]['course'], $course[$i]['course_name']) =
				mysql_fetch_row(mysql_query("select count(class.group2), group2.course, course.`course` from class, group2, course
								where class.term = group2.term
								and class.group2 = group2.`group`
								and group2.course = course.course_no
								and course.term = group2.term
								and group2.no = '$ab_gpno'
								group by class.group2"));
			switch ($course[$i]['course']['0']) {
				case 'C': // 語言課程
					if ($course[$i]['people'] == 1) $C1++;
					else if ($course[$i]['people'] >= 2 && $course[$i]['people'] <=3) $C23++;
					else if ($course[$i]['people'] >= 4 && $course[$i]['people'] <=7) $C47++;
					break;
				case 'L': // Lab 課程
					$Lab++;
					break;
				case 'S':
					//echo $course[$i]['course_name'];
					if ($course[$i]['course_name'] == "特殊課程") {
						if ($course[$i]['people'] == 1) $C1++;
						else if ($course[$i]['people'] >= 2 && $course[$i]['people'] <=3) $C23++;
						else if ($course[$i]['people'] >= 4 && $course[$i]['people'] <=7) $C47++;
					}
					else {
						switch ($course[$i]['course']) {
							case 'S001': // 文化課程：書法、國畫
							case 'S002':
								$S12++;
								break;
							case 'S003': // 文化課程：中國結
								$S3++;
								break;
							default: // 特殊課程
								if ($course[$i]['people'] == 1) $C1++;
								else if ($course[$i]['people'] >= 2 && $course[$i]['people'] <=3) $C23++;
								else if ($course[$i]['people'] >= 4 && $course[$i]['people'] <=7) $C47++;
								break;

						}
					}
					break;

			}							

		}

	}

	$tpl->assign(AB_HOURS, $hours*2); // 一堂課是以兩小時計

	$tpl->assign(AB_Q1, $course['1']['people']);
	$tpl->assign(AB_Q2, $course['2']['people']);
	$tpl->assign(AB_Q3, $course['3']['people']);
	$tpl->assign(AB_Q4, $course['4']['people']);




	if ($ab['applicant'] == $ab['supplicant']) { // 自行代課
		$tpl->assign(AB_RE, "v"); // 自行代課
		$tpl->assign(AB_SM, ""); // 
		$tpl->assign(AB_SP, "");
		$tpl->assign(AB_AM, "");
	}
	else { // 他人代課
		$money_level = mysql_fetch_array(mysql_query("select * from salary_level"));
		list($bm1) = mysql_fetch_row(mysql_query("select salary_ahour from member where center_no = '$ab[applicant]'"));
		list($names, $bm2) = mysql_fetch_row(mysql_query("select name_ch, salary_ahour from member where center_no = '$ab[supplicant]'"));

		list($money_level['Lab']) = mysql_fetch_row(mysql_query("select money from course where course_no like 'L%'"));
		list($money_level['S12']) = mysql_fetch_row(mysql_query("select money from course where course_no = 'S001' or course_no = 'S002'"));
		list($money_level['S3']) = mysql_fetch_row(mysql_query("select money from course where course_no = 'S003'"));



		$ap_total_money = $C1*$bm1 + $C23*($bm1+$money_level['C23']) + $C47*($bm1+$money_level['C47']) + $Lab*$money_level['Lab'] +
				$S12*$money_level['S12'] + $S3*$money_level['S3'];

		$sup_total_money = $C1*$bm2 + $C23*($bm2+$money_level['C23']) + $C47*($bm2+$money_level['C47']) + $Lab*$money_level['Lab'] +
				$S12*$money_level['S12'] + $S3*$money_level['S3'];

		$total_money1 += $ap_total_money;
		$total_money2 += $sup_total_money;

		$tpl->assign(AB_RE, "");
		if ($ab['type'] == 3 || $ab['type'] == 5) { // 排除公假跟喪假(不扣錢)
			$tpl->assign(AB_SM, "0"); // 一堂課是以兩小時計，因此時也要 * 2
			$tpl->assign(AB_AM, $sup_total_money*2);
		}
		else {
			$tpl->assign(AB_SM, $ap_total_money*2); // 一堂課是以兩小時計，因此時也要 * 2
			$tpl->assign(AB_AM, $sup_total_money*2);
		}

		$tpl->assign(AB_SP, $names);

	}

	$tpl->assign(AB_PS, $ab['ps']);

	$tpl->parse(ROWS, ".row");
}
////////////////////////////////////////
$tpl->assign(AB_TOTAL1, $total_money1*2);
$tpl->assign(AB_TOTAL2, $total_money2*2);

$tpl->parse(BODY, "main");

$tpl->FastPrint("BODY");
?>

<?php
function is_holiday ($date) { // 假日
	list($match) = mysql_fetch_row(mysql_query("select hdate from holiday where hdate = '$date'"));
	//if ($match) echo $match;
	return $match;
}
?>