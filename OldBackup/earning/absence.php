<?php
 include("../dbpw.php");
 if ($U_id<>"lcadmin" && $U_id<>"lang"){
 	echo $U_id."你沒有此頁權限,請回到<a href=../list.php>主選單</a>";
 	exit;
}
?>
<?php 
	require("../include/link_db");
	include("../include/function.php");
    session_start();
?>
<?php
// 變數 $mod_ab
// 是一個 array ，儲存的資料為直接從假單的資料庫取出來的
// 目的為顯示資料，作為修改用

// 在更新、刪除、新增時都必須寫入對應到 absence 中，請假日期所對應到的學期，課程時間之 group 代碼
// (方便將來計算方便)
// absence_course 的內容的與請假的日期期間有對應的學期及 group 還有節數資料
// 步驟：
//	一、洗掉對應 absence (no 欄位為準)的 absence_course 裡面的資料 (在修改及刪除時才需要
//	二、找到對應的所有學期，並且找到請假日期與學期日期的交集...
//	三、計算其所有在學期內的請假及影響到的課程

	if (!empty($begin_year) && !empty($begin_month) && !empty($begin_day)) {
		$begin_date = $begin_year."-".$begin_month."-".$begin_day;
	}
	if (!empty($end_year) && !empty($end_month) && !empty($end_day)) {
		$end_date = $end_year."-".$end_month."-".$end_day;
	}

	// 判斷為修改，新增，更新，刪除
	if ($act == "modify" && !empty($no)) { // 修改
		$str = "select * from absence where no = $no";
		$mod_ab = mysql_fetch_array(mysql_query($str));
		$selected[ $mod_ab['type'] ] = " selected";
		$mod_ab['begin_year'] = substr($mod_ab['begin_date'], 0, 4);
		$mod_ab['begin_month'] = substr($mod_ab['begin_date'], 5, 2);
		$mod_ab['begin_day'] = substr($mod_ab['begin_date'], 8, 2);
		$mod_ab['end_year'] = substr($mod_ab['end_date'], 0, 4);
		$mod_ab['end_month'] = substr($mod_ab['end_date'], 5, 2);
		$mod_ab['end_day'] = substr($mod_ab['end_date'], 8, 2);
	}
	else if ($act == "update" && !empty($no)) { // 更新

		$total_hour = "";
		for ($i = 0 ; $i < 4 ; $i ++) ${'total_hour'.$i}?$total_hour .= "1":$total_hour .= "0";

		$str = "update absence set
			type = '$type',
			applicant = '$applicant',
			supplicant = '$supplicant',
			begin_date = '$begin_date',
			end_date = '$end_date',
			hours = '$total_hour',
			ps = '$ps'
			where no = '$no'";
		mysql_query($str) or die("更新假條資料時發生錯誤<br>".$str);

		delete_abc($no, $link);
		entry_absence_course ($no, $link);
	}
	else if ($act == "delete" && !empty($no)) { // 刪除
		$str = "delete from absence where no = '$no'";
		mysql_query($str) or die("刪除假條資料時發生錯誤<br>".$str);

		delete_abc($no, $link);
	}
	else if ($act == "addnew" && !empty($type)) { // 新增

		$total_hour = "";
		for ($i = 0 ; $i < 4 ; $i ++) ${'total_hour'.$i}?$total_hour .= "1":$total_hour .= "0";

		$str = "insert into absence set
			type = '$type',
			applicant = '$applicant',
			supplicant = '$supplicant',
			begin_date = '$begin_date',
			end_date = '$end_date',
			hours = '$total_hour',
			ps = '$ps'";
		mysql_query($str) or die("新增假條資料時發生錯誤<br>".$str);
		list($nno) = mysql_fetch_row(mysql_query("select no from absence
							where type = '$type'
							and applicant = '$applicant'
							and supplicant = '$supplicant'
							and begin_date = '$begin_date'
							and end_date = '$end_date'
							and hours = '$total_hour'
							and ps = '$ps'"));
		delete_abc($nno, $link);
		entry_absence_course ($nno, $link);
		mysql_query("OPTIMIZE TABLE `absence`");
	}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>語言中心行政處理系統</title>
<script src="../include/function.js" language="JavaScript" type="text/JavaScript">
</script>
<script language="javascript">
<!--
function change_act(no,str) {
	document.behave.no.value = no;
	document.behave.act.value = str;
	document.behave.submit();
}
function validation(data, mindata, maxdata) {
	if (data >= mindata && data <= maxdata) {
		return data;
	}
	else {
		alert("輸入的資料不符合格式，將以預設值代替");
		return mindata;
	}
}
function self_sup() {
          if (document.getElementById("supplicant_self").checked == true) {
          	document.getElementById("sup_tea").style.visibility="hidden";
          	document.getElementById("supplicant_ot").options[document.getElementById("applicant_ot").selectedIndex].selected = true;
          	//document.getElementById("supplicant_self").value = document.getElementById("applicant_ot").options[document.getElementById("applicant_ot").selectedIndex].value;
          }
          else {
          	document.getElementById("sup_tea").style.visibility="visible";
          }
}
//-->
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
</head>

<body onLoad="self_sup();">
<p align="left">您現在所在位置：<font color="#FF9900">請假統計管理</font>
&nbsp;&nbsp;&nbsp;<a href="../list.php">回上一頁</a></p>
<?php
if (!empty($_POST['ryear']) && !empty($_POST['rmonth'])) {
    echo "現在檢視月份： ".$ryear." 年 ".$rmonth." 月 <font color='red'>(需先選擇檢視月份才能產生報表)</font>";
}
else {
    list($term_name) = mysql_fetch_row(mysql_query("select term from term where no = $T_uid;"));
    echo "現在檢視期別： ".$term_name." <font color='red'>(需先選擇檢視月份才能產生報表)</font>";
}

?>

<hr>
  <form id="mrange" name="mrange" method="post" action="absence.php" width="95%">
    檢視月份
    <select name="ryear">
    <?php
    	for ($i=2003 ; $i<2015 ; $i++) {
    		echo "<option value='$i'".($i == date("Y")?" selected":"").">$i</option>";
    	}
    ?>
    </select>
    年
    <select name="rmonth">
    <?php
    	for ($i=1 ; $i<=12 ; $i++) {
    		echo "<option value='$i'".($i == date("m")?" selected":"").">$i</option>";
    	}
    ?>
    </select>
    月
    <input type="submit" name="Submit" value="檢視" onMouseOver="document.getElementById('mrange').action='./absence.php'; document.getElementById('mrange').target='_SELF'">
    <input type="submit" name="Submit2" value="輸出報表" onMouseOver="document.getElementById('mrange').action='./report/report.php'; document.getElementById('mrange').target='_NEW'">
  </form>
<p align="center"><font size="+2" color="blue"><a href="absence.php">新增資料</a></font>
&nbsp;&nbsp;&nbsp;<a href="./report/report.php?down=1&ryear=<?php echo $ryear; ?>&rmonth=<?php echo $rmonth; ?>" target="_NEW"><font color="blue">下載報表(試作)</font></a>
<form name="update_form" method="post" action="">
  <table border="1" align="center" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
    <tr> 
      <td align="center" bgcolor="#E6FFEB"> 假別： 
        <select name="type" id="type">
          <option value="1"<?php if ($act == "modify" && !empty($no)) echo $selected['1'];
          			 else echo " selected"; ?>>病假</option>
          <option value="2"<?php echo $selected['2']; ?>>事假</option>
          <option value="3"<?php echo $selected['3']; ?>>公假</option>
          <option value="4"<?php echo $selected['4']; ?>>產假</option>
          <option value="5"<?php echo $selected['5']; ?>>喪假</option>
        </select> </td>
      <td align="center" bgcolor="#E6FFEB">請假教師： 
        <select name="applicant" id="applicant_ot">
          <option value="">請選擇</option>
          <?php	print_all_teacher($mod_ab['applicant'], $link); ?>
        </select>
      </td>
      <td align="center" bgcolor="#E6FFEB">
        從西元 <input name="begin_year" type="text" size="4" maxlength="4" value='<?php echo $mod_ab['begin_year']; ?>' onChange="this.value=validation(this.value, 1999, 2020)">
        年 <input name="begin_month" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_month']; ?>' onChange="this.value=validation(this.value, 01, 12)">
        月 <input name="begin_day" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_day']; ?>' onChange="this.value=validation(this.value, 01, 31)">
        日
      </td>
      <td align="center" bgcolor="#E6FFEB">
        至西元 <input name="end_year" type="text" size="4" maxlength="4" value='<?php echo $mod_ab['end_year']; ?>' onChange="this.value=validation(this.value, 1999, 2020)">
        年 <input name="end_month" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['end_month']; ?>' onChange="this.value=validation(this.value, 01, 12)">
        月 <input name="end_day" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['end_day']; ?>' onChange="this.value=validation(this.value, 01, 31)">
        日
      </td>
      <td rowspan="2" align="center" bgcolor="#E6FFEB">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr><td><input name="total_hour0" type="checkbox" value="1"<?php if($mod_ab['hours']['0']=='1') echo " checked";?>>08:10~10:00</td></tr>
          <tr><td><input name="total_hour1" type="checkbox" value="1"<?php if($mod_ab['hours']['1']=='1') echo " checked";?>>10:10~12:00</td></tr>
          <tr><td><input name="total_hour2" type="checkbox" value="1"<?php if($mod_ab['hours']['2']=='1') echo " checked";?>>13:40~15:30</td></tr>
          <tr><td><input name="total_hour3" type="checkbox" value="1"<?php if($mod_ab['hours']['3']=='1') echo " checked";?>>15:40~17:30</td></tr>
        </table>
      </td>
      <td align="center" bgcolor="#E6FFEB">
<?php
	if (!empty($ryear) && !empty($rmonth)) { // 選定檢視月份會一直持續
		echo "<input type='hidden' name='ryear' value='".$ryear."'>\n";
		echo "<input type='hidden' name='rmonth' value='".$rmonth."'>\n";
	}

  	if ($act == "modify" && !empty($no)) {
  		echo "<input type='hidden' name='act' value='update'>\n";
  		echo "<input type='hidden' name='no' value='".$mod_ab['no']."'>\n";
	        echo "<input type='submit' name='Submit' value='確定修改'>\n";
  	}
  	else {
  		echo "<input type='hidden' name='act' value='addnew'>\n";
	        echo "<input type='submit' name='Submit' value='確定新增'>\n";
	}
?>
      </td>
    </tr>
    <tr> 
      <td align="center" bgcolor="#E6FFEB">&nbsp; </td>
      <td align="center" bgcolor="#E6FFEB">是否自行補課：<input name="select001" type="checkbox" id="supplicant_self" onClick="self_sup();"></td>
      <td align="left" bgcolor="#E6FFEB">
        <div id='sup_tea'>代課教師：
          <select name="supplicant" id="supplicant_ot">
            <option value="">請選擇</option>
            <?php	print_all_teacher($mod_ab['supplicant'], $link); ?>
          </select>
	</div>
      </td>
      <td align="left" bgcolor="#E6FFEB">備註：
        <input type="text" name="ps" value="<?php echo $mod_ab['ps']; ?>">
      </td>
      <td align="center" bgcolor="#E6FFEB">&nbsp;</td>
    </tr>
  </table>
</form>

<form name="behave" method ="post" action="absence.php">
<table width="80%" height="27" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
  <tr align="center" bgcolor="#E6FFEB"> 
    <td>請假教師</td>
    <td>假別</td>
    <td>代課教師</td>
    <td>日期</td>
    <td>時段</td>
    <td>備註</td>
    <td>修改</td>
    <td>刪除</td>
  </tr>
<?php
	if (!empty($ryear) && !empty($rmonth)) { // 選定檢視月份會一直持續
		echo "<input type='hidden' name='ryear' value='".$ryear."'>\n";
		echo "<input type='hidden' name='rmonth' value='".$rmonth."'>\n";
	}
?>
  <?php print_all_absences($ryear, $rmonth, $link, $T_uid) ?>
</table>
</form>





</body>

</html>

<?php
// 印出所有教師
function print_all_teacher($tid, $alink)
{
	$rt = mysql_query("select center_no, name_ch from member order by center_no asc",$alink);

	// 把所有教師的名稱及代碼存入陣列
	for ($i=1 ; $i <= mysql_num_rows($rt) ; $i++)
		$all_teacher[$i]=mysql_fetch_array($rt);

	// 列出所有教師
	foreach($all_teacher as $v) {
		if ($v['center_no'] == $tid) echo "<option value='".$v['center_no']."' selected>".$v['name_ch']."</option>\n";
		else echo "<option value='".$v['center_no']."'>".$v['name_ch']."</option>\n";
	}
		
	return;
}


// 印出所有假條
function print_all_absences($ryear, $rmonth, $alink, $T_uid) {
	$date1 = date("Y-m", mktime(0,0,0,$rmonth,1,$ryear));

	$str = "select center_no from member order by center_no asc";
	$trt = mysql_query($str);
	while (list($ttid) = mysql_fetch_row($trt)) { // 依教師別

	if ($ryear && $rmonth) {
		$str = "select * from absence
			where (SUBSTRING(begin_date,1,7) <= '$date1' and SUBSTRING(end_date,1,7) >= '$date1') and applicant = '$ttid' order by applicant, begin_date asc";
			//echo "testdate = ".$date1;

			//where SUBSTRING(begin_date,1,7) <= '$date1' and SUBSTRING(end_date,1,7) >= '$date1'";
	}
	else {
        $str = "select start_date, end_date from term where no = $T_uid";
        $rs = mysql_fetch_array(mysql_query($str, $alink));
        $bdate = substr($rs['start_date'], 0, 7);
        $edate = substr($rs['end_date'], 0, 7);
		$str = "select * from absence where applicant = '$ttid' ";
        $str .= " and (SUBSTRING(begin_date,1,7) <= '$edate' and SUBSTRING(end_date,1,7) >= '$bdate') order by applicant, begin_date asc";
	}
	$rt = mysql_query($str, $alink) or die("test error");

	// 把所有假條的資料存入陣列
	for ($i=1 ; $i <= mysql_num_rows($rt) ; $i++)
		$all_absences[$i] = mysql_fetch_array($rt);

	if (mysql_num_rows($rt)) {
		foreach($all_absences as $v) {
			$type['1'] = "病假";
			$type['2'] = "事假";
			$type['3'] = "公假";
			$type['4'] = "產假";
			$type['5'] = "喪假";

			$supply = ($v['applicant']==$v['supplicant']?"自行補課":tid2name($v['supplicant'], $alink)." 代課");

			$duration_date = "從<font color='#FF6633'>".date("Y-m-d", strtotime($v['begin_date']))."</font><br>至<font color='#FF6633'>".date("Y-m-d", strtotime($v['end_date']))."</font>";

			$duration_time = "<table width='100%' border='1' cellspacing='0'><tr>";
			$v['hours']['0'] == '1' ? $duration_time .= "<td width='25%'><font color='blue'>08~10</font></td>" : $duration_time .= "<td width='25%'>&nbsp;</td>";
			$v['hours']['1'] == '1' ? $duration_time .= "<td width='25%'><font color='blue'>10~12</font></td>" : $duration_time .= "<td width='25%'>&nbsp;</td>";
			$v['hours']['2'] == '1' ? $duration_time .= "<td width='25%'><font color='blue'>13~15</font></td>" : $duration_time .= "<td width='25%'>&nbsp;</td>";
			$v['hours']['3'] == '1' ? $duration_time .= "<td width='25%'><font color='blue'>15~17</font></td>" : $duration_time .= "<td width='25%'>&nbsp;</td>";
			$duration_time .= "</tr></table>";

			echo "<tr>\n";
			//echo "<input type='hidden' name='type' value='site'>\n";
			echo "<td align='center'><font color='blue'>".$v['applicant']."</font>_".tid2name($v['applicant'], $alink)."</td>\n";
			echo "<td align='center'>".$type[$v['type']]."</td>\n";
			echo "<td align='center'>".$supply."</td>\n";
			echo "<td align='center'>".$duration_date."</td>\n";
			echo "<td align='center'>".$duration_time."</td>\n";
			echo "<td align='center'>".$v['ps']."&nbsp;</td>\n";
			echo "<td align='center'><input name='mod' type='button' value='修改資料' onClick=\"change_act('".$v['no']."', 'modify')\">\n";
			echo "<td align='center'><input name='del' type='button' value='刪除資料'
				onClick=\"if (confirm('你確定要刪除此筆資料嗎？')) change_act('".$v['no']."', 'delete');\">\n";
			echo "</tr>\n";
		}
	}

	// 清空假條的資料所使用的變數
	for ($i=1 ; $i <= mysql_num_rows($rt) ; $i++)
		unset($all_absences[$i]);

	}

	// 只需一筆...
	echo "<input type='hidden' name='no' value=''>\n";
	echo "<input type='hidden' name='act' value=''>\n";

}

// 轉換教師的 id 為名字
function tid2name($tid,$alink) {
	list($tname) = mysql_fetch_row(mysql_query("select name_ch from member where center_no = '$tid'", $alink));
	return $tname;
}




// 在新增或修改過請假資料後，需要重新計算其對應的學期及課程時間的 group
// 條件：( ) 為請假期間， [ ] 為學期期間
// 	一、 (  <  [  <=  ) and [  <=  ] ， (  <=  )
//	二、 [ <=  (  <=  ] and [  <=  ] ， (  <=  )
function entry_absence_course ($ab_no, $alink) {

	$weeks['Monday']    = 1;
	$weeks['Tuesday']   = 2;
	$weeks['Wednesday'] = 3;
	$weeks['Thursday']  = 4;
	$weeks['Friday']    = 5;

	$str = "select * from absence where no = '$ab_no'";
	$ab_data = mysql_fetch_array(mysql_query($str, $alink));

	// 條件一 (  <  [  <=  ) and [  <=  ] ， (  <=  )
	$str = "select * from term
		where start_date > '$ab_data[begin_date]'
		and start_date <= '$ab_data[end_date]'";
	$result = mysql_query($str);
	while ($semi_term = mysql_fetch_array($result)) {
		$ts_date=$semi_term['start_date'];
		$te_date=( strtotime($ab_data['end_date']) <= strtotime($semi_term['end_date'])?$ab_data['end_date']:$semi_term['end_date']);
		$days = (strtotime($te_date) - strtotime($ts_date)) / 86400;

		for ($i=0 ; $i <= $days ; $i++) {
			if ($weeks[date("l",strtotime($ts_date)+86400*$i)]) {
				$abc['gpno1'] = 0;
				$abc['gpno2'] = 0;
				$abc['gpno3'] = 0;
				$abc['gpno4'] = 0;
				
				$timestrpos = $weeks[date("l",strtotime($ts_date)+86400*$i)] - 1;
				$rs1 = mysql_query("select no, times from group2 where teacher = '$ab_data[applicant]' and term = '$semi_term[term]'");
				while (list($gpno,$timestr) = mysql_fetch_row($rs1)) {
					$abc['gpno1'] = ($ab_data['hours'][0]   && $timestr[$timestrpos]    ? $gpno : $abc['gpno1']);
					$abc['gpno2'] = ($ab_data['hours'][0+1] && $timestr[$timestrpos+5]  ? $gpno : $abc['gpno2']);
					$abc['gpno3'] = ($ab_data['hours'][0+2] && $timestr[$timestrpos+10] ? $gpno : $abc['gpno3']);
					$abc['gpno4'] = ($ab_data['hours'][0+3] && $timestr[$timestrpos+15] ? $gpno : $abc['gpno4']);
				}
				$abc['ab_no'] = $ab_no;
				$abc['date']  = date("Y-m-d",strtotime($ts_date)+86400*$i);
				$abc['term']  = $semi_term['term'];

				if ($abc['gpno1'] || $abc['gpno2'] || $abc['gpno3'] || $abc['gpno4']) {
					$wstr = "insert into `absence_course` set
						`absence_no` = '$abc[ab_no]',
						`date` = '$abc[date]',
						`term` = '$abc[term]',
						`gpno1` = '$abc[gpno1]',
						`gpno2` = '$abc[gpno2]',
						`gpno3` = '$abc[gpno3]',
						`gpno4` = '$abc[gpno4]'";
					mysql_query($wstr);
				}
			}
		}
	}

	// 條件二 [ <=  (  <=  ] and [  <=  ] ， (  <=  )
	$str = "select * from term
		where start_date <= '$ab_data[begin_date]'
		and end_date >= '$ab_data[begin_date]'";
	$result = mysql_query($str);
	while ($semi_term = mysql_fetch_array($result)) {
		$ts_date=$ab_data['begin_date'];
		$te_date=( strtotime($ab_data['end_date']) <= strtotime($semi_term['end_date'])?$ab_data['end_date']:$semi_term['end_date']);
		$days = (strtotime($te_date) - strtotime($ts_date)) / 86400;

		for ($i=0 ; $i <= $days ; $i++) {
			if ($weeks[date("l",strtotime($ts_date)+86400*$i)]) {
				$abc['gpno1'] = 0;
				$abc['gpno2'] = 0;
				$abc['gpno3'] = 0;
				$abc['gpno4'] = 0;
				
				$timestrpos = $weeks[date("l",strtotime($ts_date)+86400*$i)] - 1;
				$rs1 = mysql_query("select no, times from group2 where teacher = '$ab_data[applicant]' and term = '$semi_term[term]'");
				while (list($gpno,$timestr) = mysql_fetch_row($rs1)) { // 取得教師所有的課程
					$abc['gpno1'] = ($ab_data['hours'][0]   && $timestr[$timestrpos]    ? $gpno : $abc['gpno1']);
					$abc['gpno2'] = ($ab_data['hours'][0+1] && $timestr[$timestrpos+5]  ? $gpno : $abc['gpno2']);
					$abc['gpno3'] = ($ab_data['hours'][0+2] && $timestr[$timestrpos+10] ? $gpno : $abc['gpno3']);
					$abc['gpno4'] = ($ab_data['hours'][0+3] && $timestr[$timestrpos+15] ? $gpno : $abc['gpno4']);
				}
				$abc['ab_no'] = $ab_no;
				$abc['date']  = date("Y-m-d",strtotime($ts_date)+86400*$i);
				$abc['term']  = $semi_term['term'];

				if ($abc['gpno1'] || $abc['gpno2'] || $abc['gpno3'] || $abc['gpno4']) {
					$wstr = "insert into `absence_course` set
						`absence_no` = '$abc[ab_no]',
						`date` = '$abc[date]',
						`term` = '$abc[term]',
						`gpno1` = '$abc[gpno1]',
						`gpno2` = '$abc[gpno2]',
						`gpno3` = '$abc[gpno3]',
						`gpno4` = '$abc[gpno4]'";
					mysql_query($wstr);
				}
			}
		}
	}



}

// 刪除 absence_course 中對應到該請假日期(以序號為準)的所有資料
// 因為有兩個地方會用到(修改、刪除)，所以抽出來寫成一個短短的 function
function delete_abc ($ab_no, $alink) {
	$str = "delete from absence_course where absence_no = '$ab_no'";
	return mysql_query($str);
}
?>
