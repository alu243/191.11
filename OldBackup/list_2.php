<html>

<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>語言中心行政處理系統</title>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="3" width="100%">
<tr><td width=50%>
<b>
語言中心行政處理系統</b>
</td>
<td width=50% align=right>
<a href=index.php>登出</a>
</td>
</tr>
</table>
  
<p>資料庫－<a href="student/student.php">學生基本資料</a> | <a href="member/member.php">人事基本資料</a> 
| <a href="term/term.php">期別資料</a> | <a href="course/course_list.php">課程資料</a> | <a href="book/book.php">教材資料</a> 
| <a href="score/score_trans.php">成績轉換表</a><br>
．報表：<a href="student/student_card.php">學生證</a></p>
  
<p>繳費程序－（<a href="student/student.php">學生基本資料</a>、<a href="term/term.php">期別資料</a>、<a href="course/course_list.php">課程資料</a>）<br>
　　　　　<a href="pay/pay.php">繳費處理</a><br>
　　　　　．顯示資料：<a href="student/student_birth.php">當月壽星名單</a>　<a href="student/student_visa.php">簽證延長名單</a><br>
　　　　　．報表：<a href="student/insure_student.php">當期保險名單</a>　<a href="student/student_register.php">當期外籍學生名冊</a>　當期學費總收入</p>
  
<p>課務程序－（<a href="student/student.php">學生基本資料</a>、<a href="member/member.php">人事基本資料</a>、<a href="term/term.php">期別資料</a>、<a href="course/course_list.php">課程資料</a>、<a href="book/book.php">教材資料</a>）<br>
　　　　　繳費程序→<a href="class/rest_times_list.php">非排課時間登記</a>→<a href="class/class.php">排課處理﹝依老師﹞</a><br>
　　　　　．報表：<a href="class/timetable.php?timetable=1">大課表(A3)</a> 　<a href="教師個人課表">教師個人課表</a>　<a href="class/timetable.php?timetable=3">學生個人課表</a>　<a href="class/timetable.php?timetable=4">課程列表</a>　點名表</p>
  
<p>成績程序－（<a href="student/student.php">學生基本資料</a>、<a href="book/book.php">教材資料</a>、<a href="score/score_trans.php">成績轉換表</a>）<br>
　　　　　課務程序→<a href="score/score_teacher.php">成績輸入(依老師)</a><br>
　　　　　<a href="score/score_student.php">成績輸入(依學生)</a><br>
　　　　　．報表：<a href="score/score.php">成績單</a>　<a href="score/attend.php">出缺席證明表</a></p>
  
<p>證書程序－（<a href="student/student.php">學生基本資料</a>）<br>
　　　　　成績程序(依老師)→<a href="certificate/certificate.php?type=0">證書管理(多人)</a><br>
　　　　　<a href="certificate/certificate.php?type=1">證書管理(單人)</a><br>
　　　　　．報表：證書&#65339;請參照證書管理(多人、單人)&#65341;</p>
  
<p>薪資程序－（<a href="member/member.php">人事基本資料</a>、<a href="term/term.php">期別資料</a>）<br>
　　　　　課務程序→請假統計管理→薪資管理<br>
　　　　　．報表：請假統計表　每月授課薪資表</p>
  
<p>行事曆程序－（<a href="term/term.php">期別資料</a>）<br>
　　　　　　<a href="calendar/calendar.php">行事曆管理</a><br>
　　　　　．報表：行事曆</p>
<p>其他(多出來的)<br>
<a href="member/insure_labor.php">勞保投保分級表</a>　<a href="member/insure_health.php">健保投保分級表</a>　<a href="member/insure_family.php">健保眷屬加保表</a><br>
勞健保薪資調整表　保險費繳納證書</p>
  
</body>  
  
</html>