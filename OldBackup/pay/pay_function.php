<?php
//顯示中文姓名且加入修改連結
function id_to_link($id){
	$sql="select * from student where stu_no='$id'";
	$result = mysql_query($sql);
	while($data = mysql_fetch_array($result))
	{
		$tmp_no=$data["no"];
		$tmp_name=$data["name_ch"];
	}
	$temp_result="<a href=../student/student_modify.php?modify=1&mod_item=".$tmp_no." target=_blank>".$tmp_name."</a>";
	return $temp_result;
}
//顯示中文姓名
function id_to_chname($id){
	$sql="select * from student where stu_no='$id'";
	$result = mysql_query($sql);
	while($data = mysql_fetch_array($result)) {$name_ch=$data["name_ch"];}
	return $name_ch;
}
//顯示pay中$term,$id(stu_no)共選多少課程
function choise_course($term,$id) {
	$str = "select pay.*, course.course, course.people from pay, course
		where pay.term = '$term'
		and pay.stu_no = '$id'
		and pay.course_no = course.course_no
		and pay.term = course.term
		order by 'course_no' ASC";
	$rt = mysql_query($str) or die("無此資料");
	$i=0;
	$course_name = "";// X-X人班之字串
	$hour_name = "";// x hr 之字串
	while ( $data = mysql_fetch_array($rt) ) {
		if ($i!=0) {
			$course_name .= "<br>\n";
			$hour_name .= "<br>\n";
		}
		$course_name .= $data['course'].$data['people']."人班";
		$hour_name .= $data['hours']."hr.";
		if ($data['is_week']==1) $hour_name .= "/a week";
		$i++;
	}
	if ($course_name == "") $course_name = "&nbsp;";
	if ($tempstr2 == "") $hour = "&nbsp;";
	return $course_name."</font></td>\n<td><font size=\"4\">".$hour_name;
}
//轉換course_no成course(中文名稱)
function course_no_to_course($term,$id)
{
	$sql = "select * from course where term='$term' && course_no='$id'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	return $data["course"]."\0".$data["people"]."人班";
}

// 整合 is_pay 及 pay_money_sum 之函數
//function pay_info($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee)
function pay_info($paylist_data)
{
	$term = $paylist_data['term'];
	$id = $paylist_data['stu_no'];
	$is_admin_fee = $paylist_data['is_admin_fee'];
	$is_old_fee = $paylist_data['is_old_fee'];
	$is_reg_fee = $paylist_data['is_reg_fee'];
	$admin_fee = $paylist_data['admin_fee'];
	$old_fee = $paylist_data['old_fee'];
	$reg_fee = $paylist_data['reg_fee'];


	$pay_ok=0; // 已經付清之款項計數
	$pay_count=0; // 全部款項計數
	$pay_msg=""; // 回傳之訊息
	$temp_money=0; // 尚未繳清之金額
	$class_name="";  //選修之課程名稱
	$class_hours="";  //選修之課程時數
	$rt = mysql_query("select p.*, c.course, c.people, c.money from pay p, course c
			   where p.term = '$term'
			   and p.stu_no = '$id'
			   and p.course_no = c.course_no
			   and p.term = c.term
			   order by 'course_no' ASC");

	// 尋找應付多少錢(折扣)
	while ($data = mysql_fetch_array($rt)) {

		//寫入程式時數及名稱
		if (!empty($class_name)) {
			$class_name .= "<br>\n";
			$class_hours .= "<br>\n";
		}
		$class_name .= $data['course'].$data['people']."人班";
		$class_hours .= $data['hours']."hr.";
		if ($data['is_week']==1) $class_hours .= "/a week";

		//計算繳費
		$pay_ok += $data['is_ok'];
		$pay_count++;
		if ($data['is_week'] == '1') $temp_money += hours_sum($term,$data['hours'])*$data['money'];
		else $temp_money += $data['hours']*$data['money'];
	}

	if ($is_admin_fee>=1) $temp_money+=$admin_fee;
	if ($is_old_fee>=1) $temp_money-=$old_fee;
	if ($is_reg_fee>=1) $temp_money+=$reg_fee;

	$pay_msg = payed_msg($is_admin_fee, $is_old_fee, $is_reg_fee, $pay_ok, $pay_count);
	if ($class_name == "" ) $class_name = "&nbsp;";
	if ($class_hours == "" ) $class_hours = "&nbsp;";
	return array("class_name"=>$class_name,"class_hours"=>$class_hours,"pay_msg"=>$pay_msg,"pay_total_money"=>$temp_money); // 回傳之 array 值
}


//顯示繳費情況
function is_pay($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee)
{
	$str = "select *, sum(pay.is_ok) as pay_ok, count(*) as pay_count from pay
		where term = '$term'
		and stu_no = '$id'
		group by `term`, `stu_no`";
	$rt = mysql_query($str) or die("無此資料");
	$data = mysql_fetch_array($rt);

	return payed_msg($is_admin_fee, $is_old_fee, $is_reg_fee, $data['pay_ok'], $data['pay_count']);
}

function payed_msg($is_admin_fee, $is_old_fee, $is_reg_fee, $pay_ok, $pay_count)
{
	$payed = array(0,2); // 0=不需繳 2已繳清
	$nopayed = array(0,1); // 0=不需繳 1未繳
	if ( in_array($is_admin_fee, $payed) and in_array($is_old_fee, $payed) and in_array($is_reg_fee, $payed) and $pay_ok==$pay_count){
		return "繳清";
	}else if (in_array($is_admin_fee, $nopayed) and in_array($is_old_fee, $nopayed) and in_array($is_reg_fee, $nopayed) and $pay_ok==0){
		return "尚未繳款"; 
	}else {
		return "未繳清"; 
	}
}

//顯示總共應繳的費用
function pay_money_sum($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee)
{
	$temp_money=0;
/*	// 尋找應付多少錢
	$str = "select * from pay where term = '$term' and stu_no = '$id'";
	$rt = mysql_query($str) or die("無此資料");
	while ( $data = mysql_fetch_array($rt) ) {
		$temp_money+=pay_money($term,$data[4],$data[5],$data[7]);
	}*/

	// 尋找應付多少錢
	$str = "select * from pay where term = '$term' and stu_no = '$id' and `is_week` = '1'";
	$rt = mysql_query($str) or die("無此資料");
	while ( $data = mysql_fetch_array($rt) ) {
		$temp_money+=pay_money($term,$data["course_no"],$data["hours"],$data["is_week"]);
	}
	$str = "select sum(pay.hours*course.money) from pay, course
		where pay.term = course.term
		and pay.course_no = course.course_no
		and pay.term = '$term'
		and pay.stu_no = '$id'
		and pay.`is_week` not like '1'
		group by pay.term, pay.stu_no";
	$rt = mysql_query($str) or die("無此資料");
	list($money) = mysql_fetch_row($rt);
	$temp_money+=$money;

	// 尋找已付多少錢(折扣)
	if ($is_admin_fee>=1 || $is_old_fee>=1 || $is_reg_fee>=1) {
		$str2 = "select * from pay_list where term = '$term' and stu_no = '$id'";
		$rt2 = mysql_query($str2) or die("無此資料");
		mysql_data_seek($rt2,mysql_num_rows($rt2)-1);
		$data2 = mysql_fetch_array($rt2);
		$temp_admin=$data2["admin_fee"];
		$temp_old=$data2["old_fee"];
		$temp_reg=$data2["reg_fee"];

		if ($is_admin_fee>=1) $temp_money+=$temp_admin;
		if ($is_old_fee>=1) $temp_money-=$temp_old;
		if ($is_reg_fee>=1) $temp_money+=$temp_reg;
	}
	return $temp_money;
}
//顯示行政費用admin或舊生扣款old或新生註冊reg
function fee($fee_name)
{
	$file_name=$fee_name."_fee.data";
	$fd = fopen($file_name,"r+");

	if ($fd) { $fee = fgets($fd,5); }
	fclose($fd);

	if (!empty($fee)) { return $fee; }
	else{ return "error input"; }
}
//該期總時數
function hours_sum($term,$hours){
	$sql_term = "select * from term where term='$term'";
	$result_term = mysql_query($sql_term);
	$data_term = mysql_fetch_array($result_term);
	$w[1] = $data_term["mon"];
	$w[2] = $data_term["thu"];
	$w[3] = $data_term["wed"];
	$w[4] = $data_term["thr"];
	$w[5] = $data_term["fri"];
	asort($w); // 以倒排方式排序
	//for ($i=1; $i<=4; $i++) {
	//	for ($j=$i+1; $j<=5; $j++) {
	//		if ( $w[$i]<$w[$j] ) { $temp=$w[$j]; $w[$j]=$w[$i]; $w[$i]=$temp;}
	//	}
	//}
	$hours_sum=0;
	$k=1;
	while ($k<=$hours) {
		switch (($k%10)) {
			case 1:$hours_sum+=$w[1]; break;
			case 2:$hours_sum+=$w[1]; break;
			case 3:$hours_sum+=$w[2]; break;
			case 4:$hours_sum+=$w[2]; break;
			case 5:$hours_sum+=$w[3]; break;
			case 6:$hours_sum+=$w[3]; break;
			case 7:$hours_sum+=$w[4]; break;
			case 8:$hours_sum+=$w[4]; break;
			case 9:$hours_sum+=$w[5]; break;
			case 0:$hours_sum+=$w[5]; break;
			default: $hours_sum=0;break;
		}
		$k++;
	}
	return $hours_sum;
}
//該課程應繳多少錢
function pay_money($term,$course_id,$hours,$is_week)
{
	$sql = "select * from course where term='$term' && course_no='$course_id'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	if ($is_week==1){ return hours_sum($term,$hours)*$data["money"]; }
	else{ return $hours*$data["money"]; }
}
//數字轉成有","的格式
function format_num($n) {
	if($n<0){$temp="";$len=strlen($n)-1;$i=1;}
	else{$temp="";$len=strlen($n);$i=0;}
	while ($i<=$len) {
		$temp.=substr($n,$i,1);
		if ( $len-$i<>1 and ($len-$i-1)%3==0 ) { $temp.=","; }
		$i++;
	}	
	return $temp;
}
//數字轉英文
Function num_to_en($numstr){

	If ($numstr==0) { return "ZERO"; }

	If ($numstr>1000000000000000000000000) { return "TOO BIG"; }

	If ($numstr>=1000000000000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000000000000))) / (1000000000000) );
		$numstr = $numstr%1000000000000;
		If ($numstr==0) { $tempstr .= $newstr."BILLION "; }
		else{ $tempstr .= $newstr."BILLION "; }
	}
	If ($numstr>=1000000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000000))) / (1000000) );
		$numstr =$numstr%1000000;
		If ($numstr==0) { $tempstr .= $newstr."MILLION "; }
		else{ $tempstr .= $newstr."MILLION "; }
	}

	If ($numstr>=1000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000))) / (1000) );
		$numstr = $numstr%1000;
		If ($numstr == 0) { $tempstr .= $newstr."THOUSAND "; }
		else{ $tempstr .= $newstr."THOUSAND "; }
	}

	If ($numstr >= 100) {
		$newstr = num_to_en( ($numstr-($numstr%(100))) / (100) );
		$numstr = $numstr%100;
		If ($numstr == 0) { $tempstr .= $newstr."HUNDRED "; }
		else{ $tempstr .= $newstr."HUNDRED AND "; }
	}

	If ($numstr >= 20) {
		Switch (($numstr - $numstr%10)/ 10) {
			Case 2: $tempstr .= "TWENTY "; break;
			Case 3: $tempstr .= "THIRTY "; break;
			Case 4: $tempstr .= "FORTY "; break;
			Case 5: $tempstr .= "FIFTY "; break;
			Case 6: $tempstr .= "SIXTY "; break;
			Case 7: $tempstr .= "SEVENTY "; break;
			Case 8: $tempstr .= "EIGHTY "; break;
			Case 9: $tempstr .= "NINETY "; break;
			Default: 
		}
		$numstr = $numstr%10;
	}

	If ($numstr > 0) {
		Switch ($numstr) {
			Case 1: $tempstr .= "ONE "; break;
			Case 2: $tempstr .= "TWO "; break;
			Case 3: $tempstr .= "THREE "; break;
			Case 4: $tempstr .= "FOUR "; break;
			Case 5: $tempstr .= "FIVE "; break;
			Case 6: $tempstr .= "SIX "; break;
			Case 7: $tempstr .= "SEVEN "; break;
			Case 8: $tempstr .= "EIGHT "; break;
			Case 9: $tempstr .= "NINE "; break;
			Case 10: $tempstr .= "TEN "; break;
			Case 11: $tempstr .= "ELEVEN "; break;
			Case 12: $tempstr .= "TWELVE "; break;
			Case 13: $tempstr .= "THIRTEEN "; break;
			Case 14: $tempstr .= "FOURTEEN "; break;
			Case 15: $tempstr .= "FIFTEEN "; break;
			Case 16: $tempstr .= "SIXTEEN "; break;
			Case 17: $tempstr .= "SEVENTEEN "; break;
			Case 18: $tempstr .= "EIGHTEEN "; break;
			Case 19: $tempstr .= "NINETEEN "; break;
			Default:
		}
		$numstr = (($numstr / 10) - (($numstr - $numstr%10) / 10)) * 10;
	}
	return $tempstr;
} 
