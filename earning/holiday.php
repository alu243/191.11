<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin","lang");
CheckAuthority($acptAccounts);
?>
<?php
// 是一個 array ，儲存的資料為直接從假單的資料庫取出來的
// 目的為顯示資料，作為修改用
if (!empty($year) && !empty($month) && !empty($day))
    $holiday_date = $year."-".$month."-".$day;

if ($act == "delete" && !empty($no)) { // 刪除
    $str = "delete from holiday where no = '$no'";
    mysql_query($str) or die("刪除預定假日時發生錯誤<br>".$str);
}
else if ($act == "addnew" && !empty($holiday_date)) { // 新增
    $str = "insert into holiday set
			hdate = '$holiday_date' ,
			ps = '$_POST[ps]'";
    mysql_query($str) or die("新增預定假日時發生錯誤<br>".$str);
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
    <script type="text/javascript" language="javascript">
        function change_act(no, str) {
            document.behavior.act.value = str;
            document.behavior.no.value = no;
            document.behavior.submit();
        }
        function validation(data, mindata, maxdata) {
            if (data >= mindata && data <= maxdata) {
                return data;
            }
            else {
                alert("輸入的資料不符合格式，將以預設值代替");
                return mindata;
            }
        }
    </script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">放假日</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>
    <hr>
    <form name="update_form" method="post" action="">
        <table border="1" align="center" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
                <td align="center" bgcolor="#E6FFEB">西元 
        <input name="year" type="text" size="4" maxlength="4" value='<?php echo $mod_ab['begin_year']; ?>' onChange="this.value=validation(this.value, 1999, 2100)">
                    年 
        <input name="month" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_month']; ?>' onChange="this.value=validation(this.value, 01, 12)">
                    月 
        <input name="day" type="text" size="2" maxlength="2" value='<?php echo $mod_ab['begin_day']; ?>' onChange="this.value=validation(this.value, 01, 31)">
                    日 </td>
                <td align="center" bgcolor="#E6FFEB">備註：
        <input type="text" name="ps" size="17">
                </td>
                <td align="center" bgcolor="#E6FFEB">
                    <input type='hidden' name='act' value='addnew'>
                    <input type='submit' name='Submit' value='確定新增'>
                </td>
            </tr>
        </table>
    </form>


    <form name="behavior" method="post" action="holiday.php">
            <?php print_all_holiday(); ?>
    </form>
</body>

</html>

<?php
// 印出所有假日
function print_all_holiday()
{
    $rtyear = mysql_query("select distinct year(hdate) as yyyy from holiday order by year(hdate) desc");
    for ($i=1 ; $i <= mysql_num_rows($rtyear) ; $i++)
    {
        $yearList[$i]=mysql_fetch_array($rtyear);
    }
	foreach($yearList as $v) {
        printTableHead($v["yyyy"]);
        printTableBody($v["yyyy"]);
        printTableFoot();
    }
    

	return;
}
function printTableHead($year)
{
    echo "        <table height='27' border='1' align='center' cellspacing='0' bordercolor='#008000' bordercolorlight='#008000' bordercolordark='#008000'>\n";
    echo "            <tr align='center' bgcolor='#E6FFEB'>\n";
    echo "                <td colspan='3'>".$year."</td>\n";
    echo "            </tr>\n";
    echo "            <tr align='center' bgcolor='#E6FFEB'>\n";
    echo "                <td>日期</td>\n";
    echo "                <td>備註</td>\n";
    echo "                <td>刪除</td>\n";
    echo "            </tr>\n";
}
function printTableBody($year)
{
	$rt = mysql_query("select * from holiday where year(hdate)=".$year." order by hdate asc");
	if (mysql_num_rows($rt) > 0) {
		// 把所有假日的名稱及代碼存入陣列
		for ($i=1 ; $i <= mysql_num_rows($rt) ; $i++)
			$all_holiday[$i]=mysql_fetch_array($rt);

        $year=0;
		// 列出所有假日
		foreach($all_holiday as $v) {
			echo "<tr>";
			echo "<td align='center'>".date("Y年m月d日", strtotime($v['hdate']))."</td>";
			echo "<td align='center'>".$v['ps']."</td>";
			echo "<td align='center'><input name='del' type='button' value='刪除資料'
				onClick=\"if (confirm('你確定要刪除此筆資料嗎？')) change_act('".$v['no']."', 'delete');\">\n";
			echo "</tr>";
		}
		echo "<input type='hidden' name='no' value=''>\n";
		echo "<input type='hidden' name='act' value=''>\n";
	}	
}
function printTableFoot()
{
    echo "        </table>\n";
}
?>