<?php
//顯示該學期總共應繳的費用
// orig = pay_money_sum
function getTotalFee($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee,$is_langlab_fee)
{
	$temp_money=0;
	// 計算課程應付費用
	$str = "select * from pay where term = '$term' and stu_no = '$id' and `is_week` = '1'";
	$rt = mysql_query($str) or die("無此資料");
	while ( $data = mysql_fetch_array($rt) ) {
		$temp_money+=pay_money($term,$data["course_no"],$data["hours"],$data["is_week"]);
	}
	$str = "select sum(pay.hours*course.money) from pay, course
		where pay.term = course.term
		and pay.course_no = course.course_no
		and pay.term = '$term'
		and pay.stu_no = '$id'
		and pay.`is_week` not like '1'
		group by pay.term, pay.stu_no";
	$rt = mysql_query($str) or die("無此資料");
	list($money) = mysql_fetch_row($rt);
	$temp_money+=$money;

	// 計算行政費用(折扣)
	if ($is_admin_fee>=1 || $is_old_fee>=1 || $is_reg_fee>=1 || $is_langlab_fee>=1) {
		$str2 = "select * from pay_list where term = '$term' and stu_no = '$id'";
		$rt2 = mysql_query($str2) or die("無此資料");
		mysql_data_seek($rt2,mysql_num_rows($rt2)-1);
		$data2 = mysql_fetch_array($rt2);

		if ($is_admin_fee>=1) $temp_money+=$data2["admin_fee"];
		if ($is_old_fee>=1) $temp_money-=$data2["old_fee"];
		if ($is_reg_fee>=1) $temp_money+=$data2["reg_fee"];
		if ($is_langlab_fee>=1) $temp_money+=$data2["langlab_fee"];
	}
	return $temp_money;
}

// 已知課程、時數及該課程是否為每周課程，計算該課程所需金額
// referenced by pay_diff.php
// same as pay_function orig=pay_money
function getCourseFee($term,$course_id,$hours,$is_week)
{
	$sql = "select * from course where term='$term' && course_no='$course_id'";
	$result = mysql_query($sql);
	$data = mysql_fetch_array($result);
	if ($is_week==1){
		return getHoursForWeekly($term,$hours)*$data['money'];
	}else{
		return $hours*$data['money'];
	}
}

//已知每周時數，計算該期總時數(需事先知道該課程是否為每周課程)
// orig = hours_sum
function getHoursForWeekly($term,$hours){
	$sql_term = "select * from term where term='$term'";
	$result_term = mysql_query($sql_term);
	$data_term = mysql_fetch_array($result_term);
	$w[1] = $data_term["mon"];
	$w[2] = $data_term["thu"];
	$w[3] = $data_term["wed"];
	$w[4] = $data_term["thr"];
	$w[5] = $data_term["fri"];
	asort($w); // 以倒排方式排序
	//for ($i=1; $i<=4; $i++) {
	//	for ($j=$i+1; $j<=5; $j++) {
	//		if ( $w[$i]<$w[$j] ) { $temp=$w[$j]; $w[$j]=$w[$i]; $w[$i]=$temp;}
	//	}
	//}
	$hours_sum=0;
	$k=1;
	while ($k<=$hours) {
		switch (($k%10)) {
			case 1:$hours_sum+=$w[1]; break;
			case 2:$hours_sum+=$w[1]; break;
			case 3:$hours_sum+=$w[2]; break;
			case 4:$hours_sum+=$w[2]; break;
			case 5:$hours_sum+=$w[3]; break;
			case 6:$hours_sum+=$w[3]; break;
			case 7:$hours_sum+=$w[4]; break;
			case 8:$hours_sum+=$w[4]; break;
			case 9:$hours_sum+=$w[5]; break;
			case 0:$hours_sum+=$w[5]; break;
			default: $hours_sum=0;break;
		}
		$k++;
	}
	return $hours_sum;
}

//計算已經繳交的費用
// referenced by pay_diff.php
// orig = calc_pay
// 此function目前只有pay_diff在用
function getPayedFee($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee,$is_langlab_fee)
{
	$str = "select * from pay where term = '$term' and stu_no = '$id'";
	$rt = mysql_query($str) or die("無此資料");
	$temp_money=0;
	// 計算已繳之課程式費用
	while ( $data = mysql_fetch_array($rt) ) {
		if($data["is_ok"]==1){
			$temp_money=$temp_money+getCourseFee($term,$data["course_no"],$data["hours"],$data["is_week"]);
		}
	}
	// 計算基本費用
    $t=calcBaseFee($is_admin_fee, $admin_fee, $is_old_fee, $old_fee, $is_reg_fee, $reg_fee, $is_langlab_fee, $langlab_fee);
    $temp_money += $t["payedFee"];
	return $temp_money;
}


// 整合 is_pay 及 pay_money_sum 之函數
// function pay_info($term,$id,$is_admin_fee,$is_old_fee,$is_reg_fee)
// orig pay_info
// 此function目前只有pay_list.php有使用
function getPayInfo($paylist_data)
{
	$term = $paylist_data['term'];
	$id = $paylist_data['stu_no'];
	$is_admin_fee = $paylist_data['is_admin_fee'];
	$is_old_fee = $paylist_data['is_old_fee'];
	$is_reg_fee = $paylist_data['is_reg_fee'];
	$admin_fee = $paylist_data['admin_fee'];
	$old_fee = $paylist_data['old_fee'];
	$reg_fee = $paylist_data['reg_fee'];
	$is_langlab_fee = $paylist_data['is_langlab_fee'];
	$langlab_fee = $paylist_data['langlab_fee'];


	$pay_ok=0; // 已經付清之款項計數
	$pay_count=0; // 全部款項計數
	$pay_msg=""; // 回傳之訊息
	$temp_money=0; // 尚未繳清之金額
	$class_name="";  //選修之課程名稱
	$class_hours="";  //選修之課程時數
	$rt = mysql_query("select p.*, c.course, c.people, c.money from pay p, course c
			   where p.term = '$term'
			   and p.stu_no = '$id'
			   and p.course_no = c.course_no
			   and p.term = c.term
			   order by 'course_no' ASC");

	// 計算課程應付費用
	while ($data = mysql_fetch_array($rt)) {

		//寫入程式時數及名稱
		if (!empty($class_name)) {
			$class_name .= "<br>\n";
			$class_hours .= "<br>\n";
		}
		$class_name .= $data['course'].$data['people']."人班";
		$class_hours .= $data['hours']."hr.";
		if ($data['is_week']==1) $class_hours .= "/a week";

		//計算繳費
		$pay_ok += $data['is_ok'];
		$pay_count++;
		if ($data['is_week'] == '1') $temp_money += getHoursForWeekly($term,$data['hours'])*$data['money'];
		else $temp_money += $data['hours']*$data['money'];
	}
    
    // 計算其他行政應付費用
    $t=calcBaseFee($is_admin_fee, $admin_fee, $is_old_fee, $old_fee, $is_reg_fee, $reg_fee, $is_langlab_fee, $langlab_fee);
    $temp_money += $t["allFee"];
    $t=getPayStatus($is_admin_fee, $is_old_fee, $is_reg_fee, $is_langlab_fee, $pay_ok, $pay_count);
	$pay_msg = $t["msg"];

    if ($class_name == "" ) $class_name = "&nbsp;";
	if ($class_hours == "" ) $class_hours = "&nbsp;";
	return array("class_name"=>$class_name,"class_hours"=>$class_hours,"pay_msg"=>$pay_msg,"pay_total_money"=>$temp_money); // 回傳之 array 值
}



//顯示繳費情況
// orig is_pay
function getPayStatusTerm($term,$stu_no,$is_admin_fee,$is_old_fee,$is_reg_fee, $is_langlab_fee)
{
	$str = "select *, sum(pay.is_ok) as pay_ok, count(*) as pay_count from pay
		where term = '$term'
		and stu_no = '$stu_no'
		group by `term`, `stu_no`";
	$rt = mysql_query($str) or die("無此資料");
	$data = mysql_fetch_array($rt);

    $t=getPayStatus($is_admin_fee, $is_old_fee, $is_reg_fee, $is_langlab_fee, $data['pay_ok'], $data['pay_count']);
	return $t;
}

// orig payed_msg
function getPayStatus($is_admin_fee, $is_old_fee, $is_reg_fee, $is_langlab_fee, $pay_ok_cnt, $pay_count)
{
    $msg = "";
    // 0:尚未繳款 1:未繳清 2:繳清
    $t=calcBaseFeeStatus($is_admin_fee, $is_old_fee, $is_reg_fee, $is_langlab_fee); // 計算基本費用繳費狀況
    $status = $t["status"];
	if ($status==2 && $pay_count==$pay_ok_cnt) { // 再判斷課程繳費狀況
        $status = 2;
		$msg = "繳清";
	} else if ($status==0 && $pay_ok_cnt==0) {
        $status = 0;
		$msg = "尚未繳款"; 
	} else {
        $status = 1;
		$msg = "未繳清"; 
	}
    return array("status"=>$status, "msg"=>$msg);
}

//顯示行政費用admin或舊生扣款old或新生註冊reg
// orig = fee
function getBaseFee($fee_name)
{
	$file_name=$fee_name."_fee.data";
    if (false == file_exists($file_name)) return 0;
	$fd = fopen($file_name,"r+");

	if ($fd) { $fee = fgets($fd,5); }
	fclose($fd);

	if (!empty($fee)) { return $fee; }
	else{ return "error input"; }
}

function setBaseFee($fee_name, $fee_cost)
{
    $fd = fopen($fee_name."_fee.data","w");
    fputs($fd,$fee_cost);fclose($fd);
}

// 0:尚未繳款 1:未繳清 2:繳清
function calcBaseFeeStatus($is_admin_fee, $is_old_fee, $is_reg_fee, $is_langlab_fee)
{
    $status = 0; // 0:尚未繳款 1:未繳清 2:繳清
    $msg = "";

    $payed = array(0,2); // 0=不需繳 2已繳清
	$nopayed = array(0,1); // 0=不需繳 1未繳
	if (in_array($is_admin_fee, $payed) && in_array($is_old_fee, $payed) && in_array($is_reg_fee, $payed) && in_array($is_langlab_fee, $payed)) {
        $status = 2;
		$msg = "繳清";
	} else if (in_array($is_admin_fee, $nopayed) && in_array($is_old_fee, $nopayed) && in_array($is_reg_fee, $nopayed) && in_array($is_langlab_fee, $nopayed)) {
        $status = 0;
		$msg = "尚未繳款"; 
	} else {
        $status = 1;
		$msg = "未繳清"; 
	}
    return array("status"=>$status, "msg"=>$msg);
}

function calcBaseFee($is_admin_fee, $admin_fee, $is_old_fee, $old_fee, $is_reg_fee, $reg_fee, $is_langlab_fee, $langlab_fee)
{
    $allFee = 0;
    if ($is_admin_fee>=1) $allFee+=$admin_fee;
	if ($is_old_fee>=1) $allFee-=$old_fee;
	if ($is_reg_fee>=1) $allFee+=$reg_fee;
	if ($is_langlab_fee>=1) $allFee+=$langlab_fee;
    
    $payedFee = 0;
    if ($is_admin_fee==2) $payedFee+=$admin_fee;
	if ($is_old_fee==2) $payedFee-=$old_fee;
	if ($is_reg_fee==2) $payedFee+=$reg_fee;
	if ($is_langlab_fee==2) $payedFee+=$langlab_fee;
    return array("allFee"=>$allFee, "payedFee"=>$payedFee);
}
?>