<?php
    $_server['sessionPath'] = "D:\WWW\admin\session_data";
    $_server['url'] = "http://140.136.191.11".substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"], "/", 4));
    $_server['dir'] = realpath(dirname(__FILE__));

    require_once($_server['dir']."/DBUtil.php");
    require_once($_server['dir']."/AuthUtil.php");
    require_once($_server['dir']."/function.php");
    require_once($_server['dir']."/TermUtil.php");
    require_once($_server['dir']."/PayUtil.php");
    require_once($_server['dir']."/CourseUtil.php");

    session_save_path($_server['sessionPath']);
    ini_set('session.gc_maxlifetime', 36000);
    session_start();
    session_register("_server");
    $_SESSION["_server"] = $_server;
    LinkDB();
?>