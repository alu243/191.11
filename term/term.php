<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
if ($_GET['delete'] == 1 && !empty($_GET['del_item'])) { // 刪除
    $str = "delete from term where no = '$_GET[del_item]'";
    mysql_query($str) or die("刪除失敗");
}
else if ($_GET['update'] == 1 && !empty($_POST['upd_item'])) { // 修改
    $str = "update term "
         . " set term = '$_POST[term]', start_date = '$_POST[start_date]', end_date = '$_POST[end_date]' "
         . " , mon = '$_POST[mon]', thu = '$_POST[thu]', wed = '$_POST[wed]', thr = '$_POST[thr]', fri = '$_POST[fri]' "
         . " , pay_limit = '$_POST[pay_limit]' "
         . " where no = '$_POST[upd_item]' ";
    mysql_query($str);
    //echo "upd =" .$str; //debug mode
}
else if ($_GET['increase'] == 1) { // 新增
    if (!empty($_POST['term']) && !empty($_POST['start_date']) && !empty($_POST['end_date'])
    && !empty($_POST['mon']) && !empty($_POST['thu']) && !empty($_POST['wed']) && !empty($_POST['thr']) && !empty($_POST['fri']) ) {
        $str = "insert into term set term = '$_POST[term]', start_date = '$_POST[start_date]', end_date = '$_POST[end_date]' "
             . " , mon = '$_POST[mon]', thu = '$_POST[thu]', wed = '$_POST[wed]', thr = '$_POST[thr]', fri = '$_POST[fri]' "
             . " , pay_limit = '$_POST[pay_limit]' ";
        mysql_query($str);
        //echo "inc =" .$str; //debug mode
    }
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    您現在所在位置：<font color="#FF9900">期別資料-列表</font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主列表</a><hr>
    <a href="term_detail_new.php?increase=1">新增資料</a><br>
    <hr>
    <table border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="3" bordercolorlight="#008000" bordercolordark="#008000">
        <tr>
            <td width="24%" bgcolor="#E6FFEB">期別名稱</td>
            <td width="12%" bgcolor="#E6FFEB">起始日期</td>
            <td width="12%" bgcolor="#E6FFEB">終止日期</td>
            <td width="12%" bgcolor="#E6FFEB">繳費期限</td>
            <td width="4%" bgcolor="#E6FFEB">一</td>
            <td width="4%" bgcolor="#E6FFEB">二</td>
            <td width="4%" bgcolor="#E6FFEB">三</td>
            <td width="4%" bgcolor="#E6FFEB">四</td>
            <td width="4%" bgcolor="#E6FFEB">五</td>
            <td width="10%" bgcolor="#E6FFEB" align="center">修改</td>
            <td width="10%" bgcolor="#E6FFEB" align="center">刪除</td>
        </tr>
        <?php
        $sql = "select * from term order by end_date desc";
        $result = mysql_query($sql);

        while($data = mysql_fetch_array($result)) {
            echo "        <tr>";
            echo "            <td width=\"24%\">".$data["term"]."</td>";
            echo "            <td width=\"12%\">".$data["start_date"]."</td>";
            echo "            <td width=\"12%\">".$data["end_date"]."</td>";
            echo "            <td width=\"12%\">".($data['pay_limit']=="" ? "&nbsp;" : $data['pay_limit'])."</td>";
            echo "            <td width=\"4%\">".$data["mon"]."</td>";
            echo "            <td width=\"4%\">".$data["thu"]."</td>";
            echo "            <td width=\"4%\">".$data["wed"]."</td>";
            echo "            <td width=\"4%\">".$data["thr"]."</td>";
            echo "            <td width=\"4%\">".$data["fri"]."</td>";
            echo "            <td width=\"10%\" align=\"center\"><a href=\"./term_detail.php?modify=1&mod_item=".$data["no"]."\">修改</a></td>";
            echo "            <td width=\"10%\" align=\"center\"><a href=\"./term.php?delete=1&del_item=".$data["no"]."\" onClick=\"return popMsg(this,'刪除' + '".$data["term"]."')\">刪除</a></td>";
            echo "        </tr>";
        }
        ?>
    </table>
</body>

</html>
