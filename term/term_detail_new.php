<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">期別資料-新增</font></p>
    <form method="POST" action="./term.php?increase=1">
        <br>
        <div align="center">
            <center>
          <table border="1" width="43%" height="117" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">期別名稱</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="term" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23">起始日期</td>
              <td width="61%" height="23"><input type="text" name="start_date" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">終止日期</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="end_date" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">繳費期限</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="pay_limit" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="24">星期一天數</td>
              <td width="61%" height="24"><input type="text" name="mon" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">星期二天數</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="thu" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="24">星期三天數</td>
              <td width="61%" height="24"><input type="text" name="wed" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">星期四天數</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="thr" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="24">星期五天數</td>
              <td width="61%" height="24"><input type="text" name="fri" size="20"></td>
            </tr>
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="確定新增" name="B1">
            <input type="reset" value="清除重填" name="B2">
        </p>
    </form>
    <br>
    <p align="center"><a href="term.php">回上一頁</a></p>

</body>

</html>
