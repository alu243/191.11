<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin","lcclass","lang");
CheckAuthority($acptAccounts);
?>

<?php
if ($modify == 1 && !empty($mod_item)) {
    $str = "select * from course where no = '$mod_item'";
    $rt = mysql_query($str) or die("無此資料");
    $data = mysql_fetch_array($rt);
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">課程資料-修改/拷貝新增</font></p>
    <form method="POST" action="./course_list.php?update=1">
        <br>
        <center>期別名稱：&lt;<?php echo $data["term"];?>&gt;</center>
        <div align="center">
            <center>
          <table border="1" width="43%" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000" height="98">
              <input type="hidden" name="upd_item" value="<?php echo $data["no"]; ?>">
              <input type="hidden" name="term" value="<?php echo $data["term"]; ?>">
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">課程代碼</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="course_no" size="20" value="<?php echo $data["course_no"];?>"></td>
            </tr>
            <tr>
              <td width="39%" height="23">課程名稱</td>
              <td width="61%" height="23"><input type="text" value="<?php echo $data["course"];?>" name="course" onkeydown="return false;">
              
              </td>
            </tr>
            <tr>
              <td width="39%" height="21" bgcolor="#E6FFEB">人數</td>
              <td width="61%" height="21" bgcolor="#E6FFEB"><input type="text" name="people" size="20" value="<?php echo $data["people"];?>"></td>
            </tr>
            <tr>
              <td width="39%" height="23">費用/時</td>
              <td width="61%" height="23"><input type="text" name="money" size="20" value="<?php echo $data["money"];?>"></td>
            </tr>
          </table>
          </center>
        </div>
        <p align="center">
            <input type="submit" value="修改" name="B1">
            <input type="reset" value="清除重填" name="B2">
        </p>
    </form>
    <p align="center"><a href="course_list.php">回上一頁</a></p>
    <?php/*
儲存資料時，需判斷核取方塊course_select值為何
1:預設課程,新增course1
0:其他課程,新增course2
新增→新增資料後回course_list.php
修改→course_detail.php
修改資料後回course_list.php
*/?>
</body>

</html>
