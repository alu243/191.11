<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
$term = GetTermName($T_uid);
require("./pay_function.php");
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    當前期別：<font color="#FF9900"><?php echo $term;?></font>&nbsp;&nbsp;&nbsp;<a href="../list.php">回主選單</a>&nbsp;&nbsp;&nbsp;<a href="pay_list.php">回繳費列表</a>
    <hr>
    <center>
<table width="80%" border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC">
  <tr>
    <td width="100%" height="30"><div id="msg"></div></td>
  </tr>
  <tr>
    <td width="100%" height="30">
    <form id="addpayform" method="POST" action="pay_modify.php?increase=1" name="formName">
      <input type="hidden" name="term" value="<?php echo $term;?>">
      <font size="4">輸入學號：</font>
      <input type="text" id="stu_no" name="stu_no" size="10">
      <input type="button" name="Submit" value="查詢" 
      	onClick="window.open('../search.php?returnid=stu_no', '查詢', 'width=225,height=300,top=200,left=300')">
      <br><br>
    
      <select id="myterm" size="1" name="selectedterm" onChange="location.assign('pay_new.php?check='+document.formName.selectedterm.value)">
        <option>選擇繳費學期</option>
        <?php listallterm($T_uid);?>
      </select>
      <br> 
      <font size="4">輸入繳費號碼：</font>
      <input type="text" name="print_no" size="10" value="<?php echo print_lastno($check);?>">
      <br><br>
      <font size="4">行政費用：
        <input type="checkbox" name="is_admin_fee" value="1" checked>　(需繳此費用者打勾)<br>
    　  <input type="text" name="admin_fee" size="5" value="<?php echo getBaseFee("admin");?>">元<br><br>
      </font>
      <font size="4">舊生優待：
        <input type="checkbox" name="is_old_fee" value="1">　(有優待者打勾)<br>
    　  <input type="text" name="old_fee" size="5" value="<?php echo getBaseFee("old");?>">元<br><br>
      </font>
      <font size="4">新生註冊：
        <input type="checkbox" name="is_reg_fee" value="1">　(需繳此費用者打勾)<br>
    　  <input type="text" name="reg_fee" size="5" value="<?php echo getBaseFee("reg");?>">元<br><br>
      </font>
      <font size="4">語言教室使用費：
        <input type="checkbox" name="is_langlab_fee" value="1">　(需繳此費用者打勾)<br>
    　  <input type="text" name="langlab_fee" size="5" value="<?php echo getBaseFee("langlab");?>">元<br><br>
      </font>
      <input id="addbtn" type="button" value="確定新增" name="B2" style="font-size: 14pt">
      <input type="submit" value="清除重填" name="B3" style="font-size: 14pt">
    </form>
    </td>
  </tr>
</table>
</center>
    <script>
        $("#addbtn").click(function () {
            //alert("before post");
            $.post("check_repay_stu.php", { stu_no: $("#stu_no").val(), term: $("#myterm").val() }, function (data) {
                $("#msg").text(data);
                if (data.match("OK")) {
                    $("#addpayform").submit();
                }
            });
        });
    </script>
</body>

</html>
