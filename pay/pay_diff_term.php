<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>

    <p align="left">您現在所在位置：<font color="#FF9900">計算差額-選擇期別</font></p>
    <p align="center"></p>
    <form method="POST" action="pay_diff.php">
        <p align="center">
            選擇期別：<select size="1" name="term">
                <?php listallterm($T_uid);?>
            </select><input type="submit" value="確定送出">
        </p>
    </form>
    <p align="center"></p>
    <p align="center"><a href="../list.php">回上一頁</a></p>
</body>

</html>
