<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
require("./pay_function.php");
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <?php
	$str = "select * from pay_list where no = '$check_item'";
	$rt = mysql_query($str) or die("無此資料");
	$data_paylist = mysql_fetch_array($rt);
    ?>

    <div align="center">
        <center>
  <table border="0" cellpadding="2" width="80%">
    <tr height="30"><td></td></tr>
    <tr>
      <td width="100%"><font size="4">學號：<font color="#0000FF"><?php echo $data_paylist["stu_no"]; ?></font>　中文姓名：<font color="#0000FF"><?php echo $data_paylist["stu"]; ?></font>　繳費單編碼：<font color="#0000FF"><?php echo $data_paylist["print_no"]; ?></font><br>
        </font></td>
    </tr>
    <tr>
      <td width="100%" >
      <form name="form1" method="POST" action="pay_list.php?check=1">
      <input type="hidden" name="check_item" value="<?php echo $check_item; ?>">
      <input type="hidden" name="term" value="<?php echo $data_paylist["term"]; ?>">
      <font size="4">請選擇要繳費的項目</font>
      <table border="1" cellpadding="2" cellspacing="0" width="50%" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000">
        <tr>
          <td width="26%" bgcolor="#008000" align="left"><font color="#FFFFFF" size="4"><b>項目名稱</b></font></td>
          <td width="12%" bgcolor="#008000" align="center"><font color="#FFFFFF" size="4"><b>費用</b></font></td>
          <td width="12%" bgcolor="#008000" align="center"><font color="#FFFFFF" size="4"><b>繳費與否</b></font></td>
        </tr>
        <?php
        if ($data_paylist["is_admin_fee"]>0) { // 用 hidden 值來讓沒勾選的資料成為1(trick)
        ?>
        <tr>
          <td width="26%" align="left"><font size="4">行政費用</font></td>
          <td width="12%" align="center"><?php echo $data_paylist["admin_fee"];?></td>
          <td width="12%" align="center"><input type="hidden" name="is_admin_fee" value="1"><input type="checkbox" name="is_admin_fee" value="2" <?php if($data_paylist["is_admin_fee"]==2) echo "checked";?>></td>
        </tr>
        <?php
        }
        if ($data_paylist["is_old_fee"]>0) { // 用 hidden 值來讓沒勾選的資料成為1(trick)
        ?>
        <tr>
          <td width="26%" align="left"><font size="4">舊生優待</font></td>
          <td width="12%" align="center"><?php echo $data_paylist["old_fee"];?></td>
          <td width="12%" align="center"><input type="hidden" name="is_old_fee" value="1"><input type="checkbox" name="is_old_fee" value="2" <?php if($data_paylist["is_old_fee"]==2) echo "checked";?>></td>
        </tr>
        <?php
        }
        if ($data_paylist["is_reg_fee"]>0) { // 用 hidden 值來讓沒勾選的資料成為1(trick)
        ?>
        <tr>
          <td width="26%" align="left"><font size="4">新生註冊</font></td>
          <td width="12%" align="center"><?php echo $data_paylist["reg_fee"];?></td>
          <td width="12%" align="center"><input type="hidden" name="is_reg_fee" value="1"><input type="checkbox" name="is_reg_fee" value="2" <?php if($data_paylist["is_reg_fee"]==2) echo "checked";?>></td>
        </tr>
        <?php
        }
        if ($data_paylist["is_langlab_fee"]>0) { // 用 hidden 值來讓沒勾選的資料成為1(trick)
        ?>
        <tr>
          <td width="26%" align="left"><font size="4">語言教室使用費</font></td>
          <td width="12%" align="center"><?php echo $data_paylist["langlab_fee"];?></td>
          <td width="12%" align="center"><input type="hidden" name="is_langlab_fee" value="1"><input type="checkbox" name="is_langlab_fee" value="2" <?php if($data_paylist["is_langlab_fee"]==2) echo "checked";?>></td>
        </tr>
        <?php
        }
        $sql2 = "select * from pay where term = '".$data_paylist["term"]."' and stu_no = '".$data_paylist["stu_no"]."' order by 'course_no' ASC";
        $result2 = mysql_query($sql2) or die("無此資料");
        $check_count=0;
		while ( $data_pay = mysql_fetch_array($result2) ) {
			$check_count++;
        ?>	
        <input type="hidden" name="course<?php echo $check_count;?>" value="<?php echo $data_pay["no"];?>">
        <tr>
          <td width="26%" height="16" align="left"><font size="4"><?php echo getCourseName($data_pay["term"],$data_pay["course_no"]); ?></font></td>
          <td width="12%" height="16" align="center"><?php echo getCourseFee($data_pay["term"],$data_pay["course_no"],$data_pay["hours"],$data_pay["is_week"]); ?></td>
          <td width="12%" height="16" align="center"><input type="checkbox" name="<?php echo "is_ok".$check_count;?>" value="1" <?php if($data_pay["is_ok"]==1) echo "checked";?>></td>
        </tr>
        <?php 
        }
        ?>
        <input type="hidden" name="check_count" value="<?php echo $check_count;?>">
      </table>
      <p>　
	<input type="button" value="全選" name="B1" style="font-size: 14pt"
		 onClick="if (document.form1.is_admin_fee != null) document.form1.is_admin_fee[1].checked = true;
                  if (document.form1.is_old_fee != null) document.form1.is_old_fee.checked[1] = true;
                  if (document.form1.is_reg_fee != null) document.form1.is_reg_fee.checked[1] = true;
                  if (document.form1.is_langlab_fee != null) document.form1.is_langlab_fee.checked[1] = true;
			  <?php
              for ($i=1;$i<=$check_count;$i++){
                  echo "if (document.form1.is_ok".$i." !=null) document.form1.is_ok".$i.".checked=true;";
              }  
              ?>
	">　<input type="reset" value="清除重選" name="B2" style="font-size: 14pt">　<input type="submit" value="確定繳費" name="B3" style="font-size: 14pt"></p>
      </form>
      </td>
    </tr>
    <tr>
      <td width="100%" height="30">
        <p><br>
        <font size="4"><a href="pay_modify.php?no=<?php echo $data_paylist["no"]; ?>">修改繳費資料</a>　<a href="pay.php">回選擇繳費期別</a>　<a href="pay_list.php?term=<?php echo $data_paylist["term"];?>">回繳費列表</a>　<a href=../list.php>回主選單</a></font></td>
    </tr>
  </table>
  </center>
    </div>

    <p align="center"></p>

</body>

</html>
