<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp", "lcgrade", "lang");
CheckAuthority($acptAccounts);
?>
<table border="1" cellpadding="2" cellspacing="0" width="95%" bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#000000">
    <tr>
        <td width="20%" align="center" valign="middle"><font color="#FFFFFF" size="4"><b>單據編號</b></font></td>
        <td width="20%" align="center" valign="middle"><font color="#FFFFFF" size="4"><b>學號/姓名</b></font></td>
        <td width="20%" align="center" valign="middle"><font color="#FFFFFF" size="4"><b>已付費用</b></font></td>
        <td width="20%" align="center" valign="middle"><font color="#FFFFFF" size="4"><b>實際應繳</b></font></td>
        <td width="20%" align="center" valign="middle"><font color="#FFFFFF" size="4"><b>差額</b></font></td>
    </tr>
    <?php
    if ($sort_by=="") { $sort_by="print_no"; }
    $sql = "select * from pay_list where term = '$term' order by '$sort_by' ASC";
	$result = mysql_query($sql);
	while($data = mysql_fetch_array($result)) {
    ?>
    <tr>
        <td width="20%" align="center" valign="middle"><font size="4"><?php //單據編號?>
    	<?php echo $data['print_no'];?>
    </font></td>
        <td width="20%" valign="middle"><font size="4"><?php //學號,姓名?>
    	<?php echo $data['stu_no']."/".$data['stu'];?>
    </font></td>
        <td width="20%" valign="middle" align="right"><font size="4"><?php //已付費用?>
    	<?php echo getFullHours($data["stu_no"],$term); ?>
    </font></td>
        <td width="20%" align="center" valign="middle"><font size="4"><?php //實際應繳?>
    	<?php echo getPayedFee($data['term'],$data['stu_no'],$data['is_admin_fee'],$data['is_old_fee'],$data['is_reg_fee'],$data['is_langlab_fee']); ?>
    </font></td>
        <td width="20%" align="center" valign="middle"><font size="4"><?php //差額?>
    	<?php echo getPayedFee($data['term'],$data['stu_no'],$data['is_admin_fee'],$data['is_old_fee'],$data['is_reg_fee'],$data['is_langlab_fee']); ?>
    </font></td>
    </tr>
    <?php
    }
    ?>
</table>