<?php
echo "num=".$num."<br>num_to_en=".num_to_en($num);
?>
<?php
function num_to_en($numstr){
	if ($numstr==0) {
		return "zero";
	}
	if ($numstr>1000000000000000000000000) {
		return "Too big";
	}
	if ($numstr>=1000000000000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000000000000))) / (1000000000000) );
		$numstr = $numstr%1000000000000;
		if ($numstr==0) {
			$tempstr = $tempstr.$newstr."billion ";
		}else{
			$tempstr = $tempstr.$newstr."billion, ";
		}
	}
	if ($numstr>=1000000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000000))) / (1000000) );
		$numstr =$numstr%1000000;
		if ($numstr==0) {
			$tempstr = $tempstr.$newstr."million ";
		}else{
			$tempstr = $tempstr.$newstr."million, ";
		}
	}
	if ($numstr>=1000) {
		$newstr = num_to_en( ($numstr-($numstr%(1000))) / (1000) );
		$numstr = $numstr%1000;
		if ($numstr == 0) {
			echo $tempstr.$newstr."thousand <br>";
			$tempstr = $tempstr.$newstr."thousand ";
		}else{
			echo $tempstr.$newstr."thousand, ";
			$tempstr = $tempstr.$newstr."thousand, ";
		}
	}
	if ($numstr >= 100) {
		$newstr = num_to_en( ($numstr-($numstr%(100))) / (100) );
		$numstr = $numstr%100;
		if ($numstr == 0) {
			$tempstr = $tempstr.$newstr."hundred ";
		}else{
			$tempstr = $tempstr.$newstr."hundred and ";
		}
	}
	if ($numstr >= 20) {
		switch (($numstr - $numstr%10)/ 10) {
            case 2:
                $tempstr = $tempstr."twenty ";
                break;
            case 3:
                $tempstr = $tempstr."thirty ";
                break;
            case 4:
                $tempstr = $tempstr."forty ";
                break;
            case 5:
                $tempstr = $tempstr."fifty ";
                break;
            case 6:
                $tempstr = $tempstr."sixty ";
                break;
            case 7:
                $tempstr = $tempstr."seventy ";
                break;
            case 8:
                $tempstr = $tempstr."eighty ";
                break;
            case 9:
                $tempstr = $tempstr."ninety ";
                break;
            Default:
		}
		$numstr = $numstr%10;
	}
	if ($numstr > 0) {
		switch ($numstr) {
            case 1:
                $tempstr = $tempstr."one ";
                break;
            case 2:
                $tempstr	= $tempstr."two ";
                break;
            case 3:
                $tempstr = $tempstr."three ";
                break;
            case 4:
                $tempstr = $tempstr."four ";
                break;
            case 5:
                $tempstr = $tempstr."five ";
                break;
            case 6:
                $tempstr = $tempstr."six ";
                break;
            case 7:
                $tempstr = $tempstr."seven ";
                break;
            case 8:
                $tempstr = $tempstr."eight ";
                break;
            case 9:
                $tempstr = $tempstr."nine ";
                break;
            case 10:
                $tempstr = $tempstr."ten ";
                break;
            case 11:
                $tempstr = $tempstr."eleven ";
                break;
            case 12:
                $tempstr = $tempstr."twelve ";
                break;
            case 13:
                $tempstr = $tempstr."thirteen ";
                break;
            case 14:
                $tempstr = $tempstr."fourteen ";
                break;
            case 15:
                $tempstr = $tempstr."fifteen ";
                break;
            case 16:
                $tempstr = $tempstr."sixteen ";
                break;
            case 17:
                $tempstr = $tempstr."seventeen ";
                break;
            case 18:
                $tempstr = $tempstr."eighteen ";
                break;
            case 19:
                $tempstr = $tempstr."nineteen ";
                break;
            Default:
		}
		$numstr = (($numstr / 10) - (($numstr - $numstr%10) / 10)) * 10;
	}
	return $tempstr;
} 
