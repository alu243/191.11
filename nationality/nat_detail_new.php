<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
<p align="left">您現在所在位置：<font color="#FF9900">國家資料-新增</font></p>

   <form method="POST" action="./nat.php?increase=1">
      <br>
        <div align="center">
          <center>
          <table border="1" width="43%" height="117" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="39%" height="23">國家中文名</td>
              <td width="61%" height="23"><input type="text" name="name_ch" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">國家英文名</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="name_en" size="20"></td>
            </tr>
            <tr>
              <td width="39%" height="24">洲別</td>
              <td width="61%" height="24">
              <select name="continent_en" size="1">
		<option value="0">請先選擇洲別</option>
		<option value="Asia">亞洲</option>
		<option value="Africa">非洲</option>
		<option value="Europ">歐洲</option>
		<option value="America">美洲</option>
		<option value="Oceania">大洋洲</option>
	  </select></td>
            </tr>
          </table>
          </center>
        </div>
        <p align="center">
        <input type="submit" value="確定新增" name="B1">
        <input type="reset" value="清除重填" name="B2"></p>
      </form>
      <br>
      <p align="center"><a href="nat.php">回上一頁</a></p>

</body>

</html>
