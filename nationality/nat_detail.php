<?php
require_once("../include/config.inc.php");
$acptAccounts=array("lcadmin", "lang");
CheckAuthority($acptAccounts);
?>
<?php
if (($increase==1 ||$modify == 1) && !empty($mod_item)) {
    $str = "select * from nationality where no = '$mod_item'";
    $rt = mysql_query($str) or die("無此資料");
    $data = mysql_fetch_array($rt);
}
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">國別資料-修改</font></p>
    <?php if($increase==1)echo "<form method=\"POST\" action=\"./nat.php?increase=1\">";
          else echo "<form method=\"POST\" action=\"./nat.php?update=1\">";
    ?>
    <input type="hidden" name="upd_item" value="<?php echo $data["no"];?>">
    <br>
    <div align="center">
        <center>
          <table border="1" width="43%" height="117" bordercolor="#008000" cellspacing="0" cellpadding="0" bordercolorlight="#008000" bordercolordark="#008000">
            <tr>
              <td width="39%" height="23">國家中文名</td>
              <td width="61%" height="23"><input type="text" name="name_ch" size="20" value="<?php echo $data["name_ch"];?>"></td>
            </tr>
            <tr>
              <td width="39%" height="23" bgcolor="#E6FFEB">國家英文名</td>
              <td width="61%" height="23" bgcolor="#E6FFEB"><input type="text" name="name_en" size="20" value="<?php echo $data["name_en"];?>"></td>
            </tr>
            <tr>
              <td width="39%" height="24">洲別</td>
              <td width="61%" height="24">
              <select name="continent_en" size="1">
		<option value="0">請先選擇洲別</option>
		<option value="Asia" <?php if ($data["continent_en"]=="Asia"){echo "selected";}?>>亞洲</option>
		<option value="Africa" <?php if ($data["continent_en"]=="Africa"){echo "selected";}?>>非洲</option>
		<option value="Europ" <?php if ($data["continent_en"]=="Europ"){echo "selected";}?>>歐洲</option>
		<option value="America" <?php if ($data["continent_en"]=="America"){echo "selected";}?>>美洲</option>
		<option value="Oceania" <?php if ($data["continent_en"]=="Oceania"){echo "selected";}?>>大洋洲</option>
	  </select></td>
            </tr>
          </table>
          </center>
    </div>
    <p align="center">
        <input type="submit" value="<?php if($increase == 1) echo "確定新增"; else echo "修改";?>" name="B1">
        <input type="reset" value="清除重填" name="B2">
    </p>
    </form>
      <br>
    <p align="center"><a href="nat.php">回上一頁</a></p>
</body>

</html>
