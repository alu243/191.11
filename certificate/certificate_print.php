<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp","lcgrade","lang");
CheckAuthority($acptAccounts);
?>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <!-- MeadCo ScriptX Control -->
    <object id="factory" style="display: none" viewastext
        classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
        codebase="http://140.136.191.9/admin/ScriptX.cab#Version=6,1,428,11">
    </object>

    <script>
        function window.onload() {
            factory.printing.header = "";
            factory.printing.footer = "";
            factory.printing.portrait = false;

            factory.printing.leftMargin = 0.0;
            factory.printing.topMargin = 0.0;
            factory.printing.rightMargin = 0.0;
            factory.printing.bottomMargin = 0.0;
            window.print();
        }
    </script>
    <?php
    $nowdate = date("Y-m-d");
    //echo $print_year."/".$print_month."/".$print_day;
    if($type==1) {
        for($i=1;$i<=$i_count;$i++){
            if ((${"no".$i}!="") && $i > 1) page_break();
            //echo $count_page . "<br>noi" . ${"no".$i} . "<br>i" . $i . "<br>icount". $i_count;
            if (${"no".$i}<>""){

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // 在列印之前紀錄証號(或更新列印次數)
                $str = "select uid, c_year, c_no from cert_no where term = '$term'
				and level = '". ${"level".$i} ."'
				and stu_no = '". ${"no".$i} ."'";
                $rt = mysql_query($str);
                if (mysql_num_rows($rt) > 0) { // 更新列印次數
                    list($c_uid, $c_year, $c_no) = mysql_fetch_row($rt);
                    $cert_sn = ($c_year-1911).(sprintf("%05d",$c_no));
                    //echo "Test".$cert_sn;
                    //mysql_query("update cert_no set count = count+1 where uid = '$c_uid'");
                    mysql_query("insert into cert_no set c_year = '$c_year',
								     c_no = '$c_no',
								     stu_no = '". ${"no".$i} ."',
								     term = '$term',
								     level = '". ${"level".$i} ."',
								     print_date = '$nowdate'");
                }
                else { // 紀錄証號(注意年份)
                    // 取得該學期年份(以結束算)
                    list($p_year) = mysql_fetch_row(mysql_query("select end_date from term where term = '$term'"));
                    $c_year = substr($p_year,0,4);

                    // 取得該年份之証書流水號(若無則從1起跳)
                    list($c_no) = mysql_fetch_row(mysql_query("select max(c_no) from cert_no where c_year = '$c_year' group by c_year"));
                    if ($c_no > 0) $c_no++;
                    else {$c_no = 1;}
                    mysql_query("insert into cert_no set c_year = '$c_year',
								     c_no = '$c_no',
								     stu_no = '". ${"no".$i} ."',
								     term = '$term',
								     level = '". ${"level".$i} ."',
     								     print_date = '$nowdate'") or die("新增証書流水號失敗");
                    $cert_sn = ($c_year-1911).(sprintf("%05d",$c_no));
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                print_page(${"no".$i},$term,${"level".$i},$print_date,$cert_sn);
            }
        }
    }
    else {

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 在列印之前紀錄証號(或更新列印次數)
        $str = "select uid, c_year, c_no from cert_no where term = '$term'
		and level = '$level'
		and stu_no = '$no'";
        $rt = mysql_query($str);
        if (mysql_num_rows($rt) > 0) { // 更新列印次數
            list($c_uid, $c_year, $c_no) = mysql_fetch_row($rt);
            $cert_sn = ($c_year-1911).(sprintf("%05d",$c_no));
            //mysql_query("update cert_no set count = count+1 where uid = '$c_uid'");
            mysql_query("insert into cert_no set c_year = '$c_year',
						     c_no = '$c_no',
						     stu_no = '$no',
						     term = '$term',
						     level = '$level',
						     print_date = '$nowdate'");
        }
        else { // 紀錄証號(注意年份)
            // 取得該學期年份(以結束算)
            list($p_year) = mysql_fetch_row(mysql_query("select end_date from term where term = '$term'"));
            $c_year = substr($p_year,0,4);
            // 取得該年份之証書流水號(若無則從1起跳)
            list($c_no) = mysql_fetch_row(mysql_query("select max(c_no) from cert_no where c_year = '$c_year' group by c_year"));
            if ($c_no > 0) $c_no++;
            else {$c_no = 1;}
            mysql_query("insert into cert_no set c_year = '$c_year',
						     c_no = '$c_no',
						     stu_no = '$no',
						     term = '$term',
						     level = '$level',
						     print_date = '$nowdate'") or die("新增証書流水號失敗");
            $cert_sn = ($c_year-1911).(sprintf("%05d",$c_no));
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        print_page($no,$term,$level,$print_date,$cert_sn);
    }
    ?>
    <?php
    function print_page($no,$term,$level,$print_date, $cert_sn){
        if($print_date==""){
            $print_year=Date(Y);
            $print_month=Date(n);
            $print_day=Date(j);
        }else{
            $print_year=substr($print_date,0,4);
            $print_month=substr($print_date,4,2);
            $print_day=substr($print_date,6,2);
        }
        $en_height=array(50,50,80,40,40,40,40,40,50,70,40);
        $ch_width =array(25,25,48,25,48,48,48,48,48,48,25);
        $str = "update student set level= '$level' where stu_no = '$no'";
        mysql_query($str);
        $sql = "select * from student where stu_no='$no'";
        $result = mysql_query($sql);
        $data_stu = mysql_fetch_array($result);
        $sql2 = "select * from term Where term='$term'";
        $result2 = mysql_query($sql2);
        $data_term = mysql_fetch_array($result2);	
    ?>

    <div>
        <table border="0" width="1035pt" height="715pt" bordercolor="#000000" cellspacing="0" cellpadding="0" bordercolorlight="#000000" bordercolosrdark="#000000">
            <tr>
                <td colspan="3" height="125pt"></td>
            </tr><?php /*調整上邊界*/?>
            <tr>
                <td width="69pt"></td><?php /*調整左邊界*/?>
                <td width="470pt" valign="top"><?php /*英文證書*/?>
                    <div align="center">
                        <center>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr><td width="100%" height="5pt">　</td></tr><?php /*英文上邊界微調*/?>
        <tr>
          <td width="100%" height="<?php echo $en_height[0];?>pt" align="center" valign="top">
            <font face="OldPrint" style="font-size:20pt">
            <b>Fu&nbsp;Jen&nbsp;Catholic&nbsp;University</b>
            </font>     
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[1];?>pt" align="center" valign="top">
            <font face="OldPrint" style="font-size:20pt">
            <b>Language&nbsp;Center</b>
            </font>     
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[2];?>pt" align="center" valign="top">
            <font face="OldPrint" style="font-size:20pt;">
            <b>Certificate&nbsp;of&nbsp;Studies</b>
            </font>
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[3];?>pt" align="left" valign="top">
            <font face="JazzCondensed"  style="font-size:14pt;">
              This is to certify that <b><?php echo $data_stu["name_enf"]." ".$data_stu["name_enl"];?></b>
            </font> 
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[4];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              born on <b><?php echo full_date($data_stu["birth_year"],$data_stu["birth_month"],$data_stu["birth_day"]);?></b>
            </font>
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[5];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              has studied Chinese Language and Culture from
            </font> 
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[6];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              <b><?php echo full_date(substr($data_stu["school"],0,4),substr($data_stu["school"],5,2),substr($data_stu["school"],-2));?></b> to <b><?php echo full_date(substr($data_term["end_date"],0,4),substr($data_term["end_date"],5,2),substr($data_term["end_date"],-2));?></b>
            </font>
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[7];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              at the Center and has passed the
            </font>  
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[8];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              General Course <?php echo trans_level_en($level); ?> level examinations.
            </font>
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[9];?>pt" align="left" valign="top">
            <font face="JazzCondensed" style="font-size:14pt">
              Issued on: <?php echo full_date($print_year,$print_month,$print_day);?>
            </font> 
          </td>
        </tr>
        <tr>
          <td width="100%" height="<?php echo $en_height[10];?>pt" align="left" valign="middle">
            <font face="JazzCondensed" style="font-size:14pt">
              Director: </font><font face="Times New Roman">______________________________________
            </font>
          </td>
        </tr>
      </table>
      </center>
                    </div>
                </td>
                <td width="490pt"><?php /*中文證書*/?>
                    <div>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="11" height="2pt"></td>
                            </tr><?php /*中文上邊界微調*/?>
                            <tr>
                                <td width="<?php echo $ch_width[0];?>pt" align="center" valign="center">
                                    <font face="華康楷書體W5" style="font-size: 12pt">
              <p style="margin-top: 10; margin-bottom: 10">中</p>
              <p style="margin-top: 10; margin-bottom: 10">華</p>
              <p style="margin-top: 10; margin-bottom: 10">民</p>
              <p style="margin-top: 10; margin-bottom: 10">國</p>
              <?php echo cut_string(toChYear($print_year-1911));?><br><br><br>
              <p style="margin-top: 10; margin-bottom: 10">年</p>
              <?php echo cut_string(num_to_chinese($print_month));?><br><br><br>
              <p style="margin-top: 10; margin-bottom: 10">月</p>
              <?php echo cut_string(num_to_chinese($print_day));?><br><br><br>
              <p style="margin-top: 10; margin-bottom: 10">日</p></font>
                                </td>
                                <td width="<?php echo $ch_width[1];?>pt" align="center" valign="top"></td>
                                <td width="<?php echo $ch_width[2];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 22pt">　<br>　<br>　<br>　<br>　<br>　<br>　<br>
              主<br>
           任<br><br>
            </font>
                                    <font face="華康特粗楷體" style="font-size: 22pt">
            <?php
        $fd_c=fopen("director_cname.data","r");
		$D_cn=fgets($fd_c,80);
		fclose($fd_c);
		$i=strlen($D_cn);
		$j=0;
		echo "<br>";
		while($i>0){
			$t=substr($D_cn,$j,2);
			echo $t;
			$i=$i-2;

			if ($i>0) echo "<BR><BR>";

			$j+=2;
		}
            ?>
            </font>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </td>
                                <td width="<?php echo $ch_width[3];?>pt" align="center" valign="top"></td>
                                <td width="<?php echo $ch_width[4];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 18pt">
              修<br>
              業<br>
              期<br>
              滿<br>
              成<br>
              績<br>
              及<br>
              格<br>
              特<br>
              頒<br>
              證<br>
              書<br>
              以<br>
              玆<br>
              證<br>
              明<br><br>
              此<br>
              證
            </font>
                                </td>
                                <td width="<?php echo $ch_width[5];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 18pt">
              在<br>
              本<br>
              中<br>
              心<br><br>
              一<br>
              般<br>
              華<br>
              語<br>
              課<br>
              程<br>
              <?php echo trans_level($level); ?><br>
              
            </font>
                                </td>
                                <td width="<?php echo $ch_width[6];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 15pt">
              自<br>
            </font>
                                    <font face="華康楷書體W5" style="font-size: 15pt">
              中<br>
              華<br>
              民<br>
              國
              <?php echo cut_string(toChYear(substr($data_stu["school"],0,4)-1911));?><br>
              年
              <?php echo cut_string(num_to_chinese(substr($data_stu["school"],5,2)));?><br>
              月
              <?php echo cut_string(num_to_chinese(substr($data_stu["school"],-2)));?><br>
              日<br>
              <font face="華康楷書體W5" style="font-size:15pt">
              至
            </font>
            <font face="華康楷書體W5" style="font-size:15pt">
              <?php echo cut_string(toChYear(substr($data_term["end_date"],0,4)-1911));?><br>
              年
              <?php echo cut_string(num_to_chinese(substr($data_term["end_date"],5,2)));?><br>
              月
              <?php echo cut_string(num_to_chinese(substr($data_term["end_date"],-2)));?><br>
              日
            </font></td>
                                <td width="<?php echo $ch_width[7];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 18pt">
              中<br>
              華<br>
              民<br>
              國<br>
              <?php echo cut_string(toChYear($data_stu["birth_year"]-1911));?><br><br>
              年<br>
              <?php echo cut_string(num_to_chinese($data_stu["birth_month"]));?><br><br>
              月<br>
              <?php echo cut_string(num_to_chinese($data_stu["birth_day"]));?><br><br>
              日<br>
              生<br>
            </font>
                                </td>
                                <td width="<?php echo $ch_width[8];?>pt" align="center" valign="top">
                                    <font face="華康楷書體W5" style="font-size: 18pt">
              學<br><br>
              生<br>
              <b><?php echo cut_string($data_stu['name_ch']);?></b>
            </font>
                                </td>
                                <td width="<?php echo $ch_width[9];?>pt" align="center" valign="top">

                                    <font face="華康楷書體W5" style="font-size: 26pt"><b>
              <p style="line-height: 24pt">私</p><p style="line-height: 20pt">立</p><p style="line-height: 24pt">輔</p><p style="line-height: 24pt">
              仁</p><p style="line-height: 24pt">
              大</p><p style="line-height: 24pt">
              學</p><p style="line-height: 24pt">
              語</p><p style="line-height: 24pt">
              言</p><p style="line-height: 24pt">
              中</p><p style="line-height: 24pt">
              心</p><p style="line-height: 24pt">
              證</p><p style="line-height: 20pt">
              書</p>
            </b></font>
                                </td>
                                <td width="<?php echo $ch_width[10];?>pt" align="center" valign="bottom">
                                    <font face="華康楷書體W5" style="font-size: 10pt">
              <img border="0" src="up.jpg" width="10" height="10">
              <?php echo cut_string(num_to_chinese(substr($cert_sn,0,2)));?>
              <img border="0" src="down.jpg" width="10" height="10"><br>
              輔<br>
              語<br>
              字<br>
              第<br>
              <?php 
		/*
        $fd = fopen("c_code","r+");
        if ($fd){
        $code=fgets($fd,8);
        echo code_img($code);
        $code=doubleval($code)+1;
        fseek($fd,0);
        fputs($fd,$code);
        }
        fclose($fd);	
         */
        echo code_img($cert_sn);
              ?>
              <br><br>
              號<br><br><br><br><br>            </font>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <?php }?>
</body>

</html>
<?php
//level:1表初級 2表中級 3表高級
function trans_level($id){
	switch($id){
        case '1':return "初<br>級<br>班";
        case '2':return "中<br>級<br>班";
        case '3':return "高<br>級<br>班<br>進<br>階<br>一";
        case '4':return "高<br>級<br>班<br>進<br>階<br>二";
        case '5':return "高<br>級<br>班<br>進<br>階<br>三";
        case '6':return "高<br>級<br>班<br>進<br>階<br>四";
        default:return "未設定";
	}
}
function trans_level_en($id){
	switch($id){
        case '1':return "Elementary";
        case '2':return "Intermediate";
        case '3':return "Advanced I";
        case '4':return "Advanced II";
        case '5':return "Advanced III";
        case '6':return "Advanced IV";
        default:return "<b>Level error</b>";
	}
}

function code_img($n){
	$temp2="";
	$i=0;
	while ($i<strlen($n)){
		switch ($n[$i]){
            case 0:
                $temp="<br><img border=0 src=0.jpg width=10 height=10>";
                break;
            case 1:
                $temp="<br><img border=0 src=1.jpg width=10 height=10>";
                break;
            case 2:
                $temp="<br><img border=0 src=2.jpg width=10 height=10>";
                break;
            case 3:
                $temp="<br><img border=0 src=3.jpg width=10 height=10>";
                break;
            case 4:
                $temp="<br><img border=0 src=4.jpg width=10 height=10>";
                break;
            case 5:
                $temp="<br><img border=0 src=5.jpg width=10 height=10>";
                break;
            case 6:
                $temp="<br><img border=0 src=6.jpg width=10 height=10>";
                break;
            case 7:
                $temp="<br><img border=0 src=7.jpg width=10 height=10>";
                break;
            case 8:
                $temp="<br><img border=0 src=8.jpg width=10 height=10>";
                break;
            case 9:
                $temp="<br><img border=0 src=9.jpg width=10 height=10>";
                break;
            default:
                return "error";
		}
		
		$temp2=$temp2.$temp;
		$i++;
	}
	return $temp2;
}

function cut_string($s){
	$i=0;
	$temp="";
	while ($i<strlen($s)) {
		// 加入排除 html entitiles 資料
		if ($s[$i]==='&') {
			while ($i<strlen($s)) {
                $temp=$temp.$s[$i];
				$i++;
				if ($s[$i-1]===';') break;
			}
		}
		else
		{
            $temp=$temp."<br />".$s[$i].$s[$i+1];
			$i=$i+2;
		}
	}
	return $temp;
}

function full_date($y,$m,$c){
	if ($m[0]==0){ $n=$m[1]; }
	else{ $n=$m; }

	if ($c[0]==0){ $d=$c[1]; }
	else{ $d=$c;}

	switch ($n) {
		case 1: return "January ".$d.", ".$y;
		case 2: return "February ".$d.", ".$y;		
		case 3: return "March ".$d.", ".$y;
		case 4: return "April ".$d.", ".$y;
		case 5: return "May ".$d.", ".$y;
		case 6: return "June ".$d.", ".$y;
		case 7: return "July ".$d.", ".$y;
		case 8: return "August ".$d.", ".$y;
		case 9: return "September ".$d.", ".$y;
		case 10: return "October ".$d.", ".$y;
		case 11: return "November ".$d.", ".$y;
		case 12: return "December ".$d.", ".$y;
		default: return "error date";
	}
}
function toChYear($year){
    $cy[0] = "○";$cy[1] = "一";$cy[2] = "二";$cy[3] = "三";$cy[4] = "四";
    $cy[5] = "五";$cy[6] = "六";$cy[7] = "七";$cy[8] = "八";$cy[9] = "九";

    $chYear="";

    $temp=$year%1000; // 濾掉千位數以上的值(防呆用)

    if ($temp>=100){ //大於100用一００、一０一、一一０等方式顯示

        $chYear.= $cy[floor($temp/100)]; //百位數

        $temp=$temp%100; // 取十位數
        $chYear.= $cy[floor($temp/10)]; //十位數

        $temp=$temp%10; // 取個位數
        $chYear.= $cy[$temp]; //個位數

    } else { //小於100還是用九十九、十一等方式顯示
        $temp=$year%100; //十位數
        if ($temp>=10){
            if ($temp<20){
                $chYear.= "十"; // 十一、十二...
            } else {
                $chYear.=trans_num(($temp-$temp%10)/10)."十"; // 二十一、三十一...
            }
        }
        $temp=$year%10; //個位數
        if ($temp>0){ $chYear.=trans_num($temp); }
    }
    return $chYear;
}

function num_to_chinese($n){
	$chinese_num="";
	$temp=$n%1000;
	if ($temp>=100){
		$chinese_num=$chinese_num . trans_num(($temp-$temp%100)/100) . "百";
	}
	$temp=$n%100;
	if ($temp>=10){
		if ($temp<20){
			$chinese_num=$chinese_num."十";
		}else{
			$chinese_num=$chinese_num.trans_num(($temp-$temp%10)/10)."十";
		}
	}else if ($n>=100 & $temp > 0){
		$chinese_num=$chinese_num.trans_num(0);
	}
	$temp=$n%10;
	if ($temp>0){
		$chinese_num=$chinese_num.trans_num($temp);
	}
	return $chinese_num;
}

function trans_num($n){
	switch ($n) {
		case 0: return "零";
		case 1: return "一";
		case 2: return "二";
		case 3: return "三";
		case 4: return "四";
		case 5: return "五";
		case 6: return "六";
		case 7: return "七";
		case 8: return "八";
		case 9: return "九";
        default: return "error";
	}

}

?>