<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp","lcgrade","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">證書管理</font>&nbsp;&nbsp;<a href="../list.php">回主選單</a></p>
    <?php
	if ($is_D_Change=="1"){
		$fd_c=fopen("director_cname.data","w");
		fputs($fd_c,$D_cn);
		fclose($fd_c);
		//$fd_e=fopen("director_ename.data","w");
		//fputs($fd_e,$D_en);
		//fclose($fd_e);
	}
	//$fd_e=fopen("director_ename.data","r");
	//$D_en=fgets($fd_e,80);
	$fd_c=fopen("director_cname.data","r");
	$D_cn=fgets($fd_c,80);
	fclose($fd_c);
    ?>
    <table width="95%" border="0">
        <tr>
            <td width="33%">
                <form name="cert_search" method="GET" action="cert_search.php" target="_new">
                    查詢學生証書核發資料
       
                    <br>
                    <input type="text" name="stu_no">
                    <input type="submit" name="sb" value="查詢">
                </form>
            </td>
            <td width="33%" align="left">
                <form name="D_Change" method="post" action="certificate.php">
                    <?php echo "目前使用主任名: ".$D_cn; //." , ".$D_en; ?>
                    <?php /*<BR>英文:<input type="text" name="D_en" value="echo $D_en;">*/ ?>
                    <br>
                    <input type="hidden" name="is_D_Change" value="1">
                    <br>
                    變更主任名&nbsp;&nbsp;<input type="submit" name="D_B" value="變更">
                    <br>
                    中文:<input type="text" name="D_cn" value="<?php echo $D_cn;?>">
                </form>
            </td>
            <td width="34%">&nbsp;</td>
        </tr>
    </table>
    <hr>

    <?php if($type==0){ ?>
    <form name="form1" method="POST" action="certificate_list.php">
        <table width="95%" border="0">
            <tr>
                <td width="33%">&nbsp;</td>
                <td width="33%" align="left">
                    <p align="left">
                        選擇期別：
         
                        <select size="1" name="term">
                            <?php listallterm($T_uid);?>
                        </select>
                    </p>

                    <p align="left">
                        選擇老師：
         
                        <select size="1" name="center_no">
                            <?php listallid();?>
                        </select>
                    </p>

                    <input type="submit" name="submit" value="看列表">
                </td>
                <td width="34%">&nbsp;</td>
            </tr>
        </table>
    </form>
    <?php }else{ ?>
    <form name="form2" method="POST" action="certificate.php?type=1">
        <table width="95%" border="0">
            <tr>
                <td width="33%">&nbsp;</td>
                <td width="33%" align="left">
                    <p>
                        選擇查詢方式 ： 
       
                        <select name="search">
                            <option value="stu_no">學號</option>
                            <option value="name_ch">中文姓名</option>
                            <option value="name_enf">First name</option>
                            <option value="name_enl">Last name</option>
                        </select>
                    </p>
                    <p>
                        輸入查詢的資料： 
         
                        <input type="text" name="search_data">
                        <input type="submit" name="B1" value="送出">
                    </p>
                </td>
                <td width="34%">&nbsp;</td>
            </tr>
        </table>
    </form>
    <p></p>
    <p></p>
    <?php 
              echo "<hr size=1 color=#009933 noshade>";
              echo "請從下列資料中選出要列印的紀錄表(點選學號欄)";
              echo "<table width=100% border=1 cellpadding=0 cellspacing=0 bordercolor=#008000 bordercolorlight=#008000 bordercolordark=#008000 dwcopytype=CopyTableColumn>";
              echo  "<tr>";
              echo    "<td width=8% align=center bgcolor=#E6FFEB>學號</td>";
              echo    "<td width=16% align=center bgcolor=#E6FFEB>中文姓名</td>";
              echo    "<td width=22% align=center bgcolor=#E6FFEB>外文名字</td>";
              echo    "<td width=22% align=center bgcolor=#E6FFEB>外文姓氏</td>";
              echo    "<td width=10% align=center bgcolor=#E6FFEB>國籍</td>";
              echo    "<td width=10% align=center bgcolor=#E6FFEB>出生年月日</td>";
              echo    "<td width=12% align=center bgcolor=#E6FFEB>證書等級</td>";
              echo  "</tr>";
              if ($search_data<>""){
                  $sql="select no,stu_no,name_ch,name_enf,name_enl,nationality_no,birth,level from student where ".$search."='$search_data'";
              }
              else {
                  $sSql = "select term from term where no ='$T_uid'";
                  list($T_name) = mysql_fetch_row(mysql_query($sSql));
                  echo "<br><br><font color='#FF9900'>學期:$T_name 之所有學生：</font><br>";
                  $sql = "select s.no, s.stu_no, s.name_ch, s.name_enf, s.name_enl, s.nationality_no, s.birth, s.level from student as s, pay_list as p where p.stu_no = s.stu_no and p.term = '$T_name' order by s.stu_no";
              } 
              $result = mysql_query($sql);
              $counter=0;
              while($data = mysql_fetch_array($result)) {
                  echo "<tr>";
                  echo "<td width=8% align=center><a href=certificate_single.php?no=".$data["no"].">".$data["stu_no"]."</td>";
                  echo "<td width=16% align=center>".$data["name_ch"]."</td>";
                  echo "<td width=22% align=center>".$data["name_enf"]."</td>";
                  echo "<td width=22% align=center>".$data["name_enl"]."</td>";
                  echo "<td width=10% align=center>".trans_nationality($data["nationality_no"],1)."</td>";
                  echo "<td width=10% align=center>".$data["birth"]."</td>";
                  echo "<td width=12% align=center>".trans_level($data["level"])."</td>";
                  echo "</tr>";
                  $counter++;
              }
              echo "</table>";
              if ($counter==0){echo "<p></p><font color=#FF0000><b>資料庫內查無此人資料<br>您所輸入的資料可能有誤，請重新輸入一次</b></font>";}
          }
    ?>

    <p align="center"></p>
    <p align="center"><a href="../list.php">回上一頁</a></p>

</body>

</html>
<?php
//level:1表初級 2表中級 3表高級
function trans_level($id){
	switch($id){
        case '1':return "初級";
        case '2':return "中級";
        case '3':return "高級1";
        case '4':return "高級2";
        case '5':return "高級3";
        case '6':return "高級4";
        default:return "未設定";
	}
}
?>