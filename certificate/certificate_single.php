<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp","lcgrade","lang");
CheckAuthority($acptAccounts);
?>
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    <p align="left">您現在所在位置：<font color="#FF9900">證書列表(單人)</font>&nbsp;&nbsp;<a href="./certificate.php?type=1">回上一頁</a>&nbsp;&nbsp;<a href="../list.php">回主選單</a></p>
    <hr>
    <form name="form2" method="POST" action="certificate_print.php?type=0" target="_blank">
        <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000" dwcopytype="CopyTableColumn">

            <tr>
                <td width="8%" align="center" bgcolor="#E6FFEB">學號</td>
                <td width="16%" align="center" bgcolor="#E6FFEB">中文姓名</td>
                <td width="25%" align="center" bgcolor="#E6FFEB">外文名字</td>
                <td width="25%" align="center" bgcolor="#E6FFEB">外文姓氏</td>
                <td width="15%" align="center" bgcolor="#E6FFEB">證書等級</td>
            </tr>
            <?php
            $sql="select no,stu_no,name_ch,name_enf,name_enl,level from student where no = '$no'";
            $result = mysql_query($sql);
            $data2=mysql_fetch_array($result);
            
            ?>
            <tr>
                <td width="8%" align="center"><?php echo $data2["stu_no"];?></td>
                <td width="16%" align="center"><?php echo $data2["name_ch"];?></td>
                <td width="25%" align="center"><?php echo $data2["name_enf"];?></td>
                <td width="25%" align="center"><?php echo $data2["name_enl"];?></td>
                <td width="15%" align="center">
                    <select name="level">
                        <?php
                        for($j=1;$j<=6;$j++){
                            echo "<option value=".$j;
                            if ($j==$data2["level"]){echo " selected";}
                            echo ">".trans_level($j)."</option>";
                        }?>
                    </select></td>

            </tr>
        </table>
        <p align="center">
            選擇期別：
 
            <select size="1" name="term">
                <?php listallterm($T_uid);?>
            </select>
        </p>
        <p align="center">
            輸入列印日期：<input type="text" name="print_date" size="8" value="<?php echo date("Ymd"); ?>" >(格式:20030101)
 
        </p>
        <input type="hidden" name="no" value="<?php echo $data2["stu_no"];?>">
        <p align="center">
            <input type="submit" name="B1" value="確定列印">
        </p>
    </form>

</body>
</html>
<?php
function teacher_name($id)
{
	$sql1 = "select * from member where center_no = '$id'";
	$result1 = mysql_query($sql1);
	$data1 = mysql_fetch_array($result1);
	return $data1["name_ch"];
}
//level:1表初級 2表中級 3表高級
function trans_level($id){
	switch($id){
        case '1':return "初級";
        case '2':return "中級";
        case '3':return "高級1";
        case '4':return "高級2";
        case '5':return "高級3";
        case '6':return "高級4";
        default:return "未設定";
	}
}
function trans_level_en($id){
	switch($id){
        case '1':return "Elementary";
        case '2':return "Intermediate";
        case '3':return "Advanced I";
        case '4':return "Advanced II";
        case '5':return "Advanced III";
        case '6':return "Advanced IV";
        default:return "Level error";
	}
}
?>