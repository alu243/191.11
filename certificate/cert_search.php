<?php
require_once("../include/config.inc.php");
$acptAccounts=array("fjdp","lcgrade","lang");
CheckAuthority($acptAccounts);
?>
<?php
$trans_level[1] = "初級";
$trans_level[2] = "中級";
$trans_level[3] = "高級1";
$trans_level[4] = "高級2";
$trans_level[5] = "高級3";
$trans_level[6] = "高級4";
$SQL = "select name_ch from student where stu_no = '$_GET[stu_no]'";
list($name_ch) = mysql_fetch_row(mysql_query($SQL));
$SQL = "select *, count(*) as cnt, max(print_date) as maxpd from cert_no "
      ." where stu_no = '" . $_GET[stu_no] . "' group by level order by level ";
$rt = mysql_query($SQL);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Language" content="zh-tw">
    <meta http-equiv="Content-Type" content="text/html; charset=big5">
    <title>語言中心行政處理系統</title>
    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/function.js" charset="big5"></script>
</head>
<body>
    學號：<?php echo $_GET['stu_no']; ?>
    <br>
    中文姓名：<?php echo $name_ch; ?>
    <table width="95%" border="1" cellpadding="0" cellspacing="0" bordercolor="#008000" bordercolorlight="#008000" bordercolordark="#008000" dwcopytype="CopyTableColumn">
        <tr>
            <td align="center" bgcolor="#E6FFEB">證書等級</td>
            <td align="center" bgcolor="#E6FFEB">列印資料</td>
            <td align="center" bgcolor="#E6FFEB">最後列印日期</td>
        </tr>
        <?php 
        while ($cert_data = mysql_fetch_array($rt)) {
            echo "<tr>";
            echo "<td>".$trans_level[$cert_data[level]]."</td>";
            echo "<td>";
            echo "核發學期：".$cert_data["term"];
            echo "<br>証號：".($cert_data["c_year"]-1911).(sprintf("%05d",$cert_data["c_no"]));
            echo "<br>列印次數：".$cert_data["cnt"];
            echo "</td>";
            echo "<td>".$cert_data["maxpd"]."</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>
</html>
